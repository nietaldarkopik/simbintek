<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


				<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
				<div id="top-panel">
					<div id="panel">
						<?php
							echo $this->data->show_panel_allowed("","",array("listing"));
						?>
					</div>
				</div>
				<br class="fclear"/><br/>
				<form method="post" id="form_penilaian" name="form_penilaian" action="<?php echo base_url();?>certification_results/penilaian_mandiri/<?php echo $sertifikasi['sertifikasi_id'];?>">
				<?php
				$total_penilaian = 0;
				if(isset($kompetensi) and is_array($kompetensi) and count($kompetensi) > 0)
				{
					$number = 1;
					foreach($kompetensi as $index => $kom)
					{
						
						echo '<br/><br/><br/><h4>Unit Kompetensi:</h4>';
						echo '<h4>Nomor : ' . $kom['kode_kompetensi'] . '</h4>';
						echo '<h4>Judul : ' . $kom['judul_kompetensi'] . '</h4><br/>';
						echo '	<table border="1" width="100%" class="table_report">
									<thead>
										<tr>
											<th width="30%">Komponen asesmen mandiri</th>
											<th width="50%">Daftar Pertanyaan <br/>(Asesmen Mandiri / Self Assessment)</th>
											<th>Penilaian</th>
											<th>Bukti-bukti Pendukung</th>
										</tr>
									</thead>
									<tbody>';
						$number2 = 1;
						$this->db->where(array("parent_kompetensi" => $kom['kode_kompetensi']));
						$qelement_kompetensi = $this->db->get("sertifikasi_kompetensi");
						$element_kompetensi = $qelement_kompetensi->result_array();
						
						if(is_array($element_kompetensi) and count($element_kompetensi) > 0)
						{
							$no_elkom = 1;
							foreach($element_kompetensi as $idx => $elkom)
							{
								$number2 = 1;
								$this->db->where(array("element_kompetensi" => $elkom['kode_kompetensi']));
								$qmaterial = $this->db->get("sertifikasi_material");
								$material = $qmaterial->result_array();
								if(is_array($material) and count($material) > 0)
								{
					?>
								<tr>
									<td colspan="4">
										<h4>Elemen Kompetensi : <?php echo $no_elkom . '. ' . $elkom['judul_kompetensi'];?></h4>
									</td>
								</tr>
							<?php
									foreach($material as $idx => $mat)
									{
										$this->db->where(array("sertifikasi_id" => $sertifikasi['sertifikasi_id'],"material_id" => $mat['sertifikasi_material']));
										$q_curr_nilai_material = $this->db->get("sertifikasi_nilai_mandiri");
										$curr_nilai_material = $q_curr_nilai_material->row_array();
										$curr_bukti = (isset($curr_nilai_material['bukti']))?$curr_nilai_material['bukti']:"";
										$curr_nilai = (isset($curr_nilai_material['nilai']))?$curr_nilai_material['nilai']:"";
										$nilai_mandiri_id = (isset($curr_nilai_material['nilai_mandiri_id']))?$curr_nilai_material['nilai_mandiri_id']:"";
										$checked = (!empty($nilai_mandiri_id))?' checked="checked" ':"";
										
										$total_penilaian += $curr_nilai;
							?>
										<tr>
											<?php
											if($idx == 0)
											{
											?>
											<td class="td_kompetensi" rowspan="<?php echo $qmaterial->num_rows();?>">Kriteria Unjuk Kerja</td>
											<?php
											}
											?>
											<td>
												<input type="checkbox" name="material[<?php echo $mat['sertifikasi_material'];?>]" value="1" <?php echo $checked?>> 
												<strong><?php echo $number . '.' . $number2 . '.'; ?></strong>
												<?php echo $mat['judul_material'];?>
											</td>
											<td>
												<select name="nilai[<?php echo $mat['sertifikasi_material'];?>]" style="width:100%;">
													<option value=""></option>
													<?php													$nilai = $this->master->get_nilai();
													if(is_array($nilai) and count($nilai) > 0)
													{
														foreach($nilai as $index => $n)
														{
															$selected = ($curr_nilai == $n['nilai'])?' selected="selected" ':'';
															echo '<option value="'.$n['nilai'].'" '.$selected.'>'.$n['nilai'].'</option>';
														}
													}
													?>
												</select>
											</td>
											<td>
												<textarea name="bukti[<?php echo $mat['sertifikasi_material'];?>]" style="width:100%;height:100%;"><?php echo $curr_bukti;?></textarea>
											</td>
										</tr>
					<?php
										$number2++;
									}
									$number++;
								}
								$no_elkom++;
							}
						}
						?>
						</tbody>
					</table><br/><br/>
				<?php
					}
				}
	?>				<table border="1" width="100%" class="table_report">
						<thead>
							<tr>
								<th colspan="2" width="80%"><h4>Total</h4></th>
								<td class="total_penilaian"><h4><?php echo $total_penilaian;?></h4></td>
							</tr>
						</thead>
					</table>
					<input type="hidden" name="sertifikasi_id" value="<?php echo (isset($sertifikasi['sertifikasi_id']))?$sertifikasi['sertifikasi_id']:"";?>">
					<input type="hidden" name="peserta_id" value="<?php echo (isset($sertifikasi['peserta_id']))?$sertifikasi['peserta_id']:"";?>">
					<input type="hidden" name="accessor_id" value="<?php echo (isset($sertifikasi['accessor_id']))?$sertifikasi['accessor_id']:"";?>">
					<input type="hidden" name="jenis_sertifikasi" value="<?php echo (isset($sertifikasi['jenis_sertifikasi']))?$sertifikasi['jenis_sertifikasi']:"";?>">
					
					<input type="hidden" name="is_ajax" value="1">
					<input type="hidden" name="ajax_target" value="#main_content .col1">
					<input type="submit" name="do_save_nilai" value="Simpan"/>
				</form>
				<?php
					echo $response;
				?>
