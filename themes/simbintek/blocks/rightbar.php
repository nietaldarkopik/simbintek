<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$user_id = $this->user_access->current_user_id;
	$current_url = $this->input->post("current_url");
	$controller = $this->uri->segment(1,"");
	$function = $this->uri->segment(2,"index");
	#$function = "index";
	
	if(!empty($current_url))
	{
		$tmp_current_url = str_replace(base_url(),'',$current_url);
		$tmp_current_url = str_replace('index.php/','',$tmp_current_url);
		$tmp_current_url = explode("/",$tmp_current_url);
		$controller = (isset($tmp_current_url[0]))?$tmp_current_url[0]:"";
		$function = (isset($tmp_current_url[1]))?$tmp_current_url[1]:"";
		$function = (isset($function) and !empty($function))?$function:"";
		$current_url = base_url() . $controller . "/" . $function;
	}
	
	$filter = array("controller" => $controller,"function" => $function);
	$parent_id = $this->user_access->get_menu($filter);
	
	$menu = array();
	
	$available_menu = false;
	if(isset($parent_id['parent_menu']))
	{
		$menu = $this->user_access->get_menu_by_parent($parent_id['parent_menu'],$user_id);
		#$menu2 = $this->user_access->get_menu_by_parent($parent_id['menu_id'],$user_id);
		#$menu =  array_merge_recursive($menu2,$menu);
		$available_menu = true;
	}else{
	
		$filter = array("controller" => $controller,"function" => 'index');
		$parent_id = $this->user_access->get_menu($filter);
		if(isset($parent_id['parent_menu']))
		{
			$menu = $this->user_access->get_menu_by_parent($parent_id['parent_menu'],$user_id);
		}
		$available_menu = false;
	}
	
	if(!empty($current_url))
	{
		if($available_menu == false)
		{
			$current_url = base_url() . $controller . "/index";
		}
	}
	
	if(is_array($menu) and count($menu) > 1)
	{
?>
	<div class="pad_left1">
		<h2>NAVIGATION</h2>
	</div>
	<ul class="list1 pad_bot1">
	<?php
			foreach($menu as $index => $m)
			{
				$href = "";
				if($available_menu == true)
				{
					$href = base_url().$m['controller'].'/'.$m['function'];
				}else{
					$href = base_url().$m['controller'].'/index';
				}
				if(strpos($href,'#') !== false)
					$href = '#';
				if($href == "#")
					continue;
				$active = (str_replace('index.php/','',$current_url) == str_replace('index.php/','',$href))?' active ':'';
				echo '<li><a href="'.$href.'" class="a_ajax '.$active.'">'.$m['menu_title'].'</a></li>';
			}
	?>      
	</ul>
<?php
}
?>
