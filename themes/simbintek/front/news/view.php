<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

			<?php
			if(isset($detail) and is_array($detail) and count($detail) > 0)
			{
			?>
				<div class="pad_left1">
					<h3><?php echo $detail['judul'];?></h3>
				</div>
				<div class="wrapper">
					<figure class="left marg_right1">
						<img src="<?php echo base_url();?>uploads/media/berita/<?php echo $detail['thumbnail'];?>" alt="">
					</figure>
					<!-- <p class="pad_bot1 pad_top2"><strong>&nbsp;</strong></p> -->
					<p class="pad_bot1 pad_top2 justify"><?php echo $detail['isiberita'];?></p>
				</div>
			<?php
			}
			else{
			?>
				<div class="pad_left1">
					<h2 class="pad_bot1">
						Data tidak ditemukan
					</h2>
				</div>
			<?php
			}
			?>

		<?php /*<article class="col2 pad_left2"></article>*/ ?>
	</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
	//load_rightbar();
});
</script>
