<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

			<div class="pad_left1">
				<h2 class="pad_bot1">
					<?php echo (isset($page_title))?$page_title:"";?>
				</h2>
			</div>
			<?php
			if(isset($news) and is_array($news) and count($news) > 0)
			{
				foreach($news as $index => $n)
				{
			?>
					<div class="pad_left1">
						<h3><?php echo $n['judul'];?></h3>
					</div>
					<div class="wrapper">
						<figure class="left marg_right1">
							<img src="<?php echo base_url();?>uploads/media/berita/<?php echo $n['thumbnail'];?>" alt="">
						</figure>
						<!-- <p class="pad_bot1 pad_top2"><strong>&nbsp;</strong></p> -->
						<p class="pad_bot1 pad_top2 justify"><?php echo $this->data->excerpt($n['isiberita'],550);?></p>
					</div>
					<div class="pad_top2"> <a href="<?php echo base_url();?>front/news/detail/<?php echo url_title($n['judul'],'-', TRUE) . '_' . $n['id'];?>" class="button"><span><span>Read More</span></span></a> </div>
			<?php
				}
			}else{
			?>
			<div class="pad_left1">
				<h3>Data Berita Masih Kosong</h3>
			</div>
			<?php
			}
			?>

		<?php /*<article class="col2 pad_left2"></article>*/?>
	</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
	//load_rightbar();
});
</script>
