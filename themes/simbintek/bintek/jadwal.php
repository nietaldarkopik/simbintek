<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$q = $this->db->query('	SELECT *
							FROM bintek_instruktur A,jadwal_bintek B, bintek_material C
							WHERE 
								A.instruktur_id = C.instructure_id AND 
								B.jadwal_bintek_id = C.jadwal_id 
						   ');
	$select_bintek = $q->result_array();
?>
<style>
.table_report td,.table_report th{
	border: 1px #000000 solid !important;
	min-width: 150px;
}
</style>

			<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
			<?php 
				echo $this->data->show_panel_allowed("","",array("listing"));
			?>
			<br class="fclear"/><br/>
			<?php
				echo $this->data->create_view();
			?>
			<?php
			if(isset($detail) and is_array($detail) and count($detail) > 0)
			{
				$start = strtotime($detail['tglawal']);
				$end = strtotime($detail['tglakhir']);
				$result = array();
				while ($start <= $end) {
					if (date('N', $start) <= 5) {
						$current = date('m/d/Y', $start);
						$result[$current] = $current;
					}
					$start += 86400;
				}
			?>
			<br/>
			<div style="overflow-x:auto; width:100%;" class="container_schedule">
				<table border="1" class="table_report">
					<thead>
						<tr>
							<th colspan="3">
								<h3><?php echo $detail['judul_bintek'];?></h3>
								<h4>
									<?php echo $detail['tempat'];?>,
									<?php echo $this->data->human_date($detail['tglawal']);?> -  
									<?php echo $this->data->human_date($detail['tglakhir']);?>
								</h4>
							</th>
						</tr>
						<tr>
							<th width="10%" rowspan="3">Hari</th>
						</tr>
						<tr>
							<th>Jam</th>
							<th width="5%"><a href="javascript:void(0);" class="icon add add_colomn_shcedule">add</a></th>
						</tr>
						<tr>
							<th><input type="text" value="00:00" size="5" style="width:35px;"/>-<input type="text" value="00:00" size="5" style="width:35px;"/></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(is_array($result) and count($result) > 0)
						{
							$no = 1;
							foreach($result as $date => $r)
							{
						?>
							<tr>
								<td align="center" rowspan="2">
									Hari ke<br/>
									<strong>
									<?php echo $no; ?>
									</strong>
									<br/>
									<?php echo $this->data->human_date($date,false,true); ?>
								</td>
								<td height="80" align="center" valign="middle" class="event_colomn">
									<select name="materi" class="materi" style="width:150px;">
										<option value="">--Pilih Materi--</option>
										<option value="custom">--Custom--</option>
										<?php
										foreach($select_bintek as $index => $value)
										{
										?>
										<option value="<?php echo $value['material_id'];?>" instructure_id="<?php echo $value['instruktur_id'];?>" instructure_name="<?php echo $value['nama'];?>"><?php echo $value['material_name'];?></option>
										<?php
										}
										?>
									</select>
								</td>
								<td height="80" align="center" valign="middle">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" class="instructure_colomn">
									<input type="text" value="" name="instructure_name[]" class="instructure_name"/>
									<input type="hidden" value="" name="instructure_id[]" class="instructure_id"/>
								</td>
								<td align="center">&nbsp;</td>
							</tr>
						<?php
							$no++;
							}
						}
						?>
					</tbody>
				</table>
			</div>
			<?php
			}
			?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
			});
			
			
				jQuery(document).on("click",".add_colomn_shcedule",function(){
					var table = jQuery(this).parents('table');
					var no_col = jQuery(table).find("tr").eq(2).find("th").index(jQuery(this).parents("th"));
					var curr_th = jQuery(this).parents("th");
					var no_col = curr_th.index(".table_report tr:eq(2) th");
					
					jQuery.each(jQuery(table).find("tr"),function(k,v){
						var td = jQuery(v).find("td");
						var td_curr = jQuery(v).find("td").eq(jQuery(v).find("td").length-2);
						var colspan = jQuery(td).attr("colspan");
						if(!colspan)
						{
							if(k >= 4)
							{
								jQuery(td_curr).before(jQuery(v).find("td").eq(jQuery(v).find("td").length-2).clone());
							}
						}else{
							colspan = parseInt(colspan);
							colspan = colspan + 1;
							jQuery(td).attr("colspan",colspan);
						}
					});
					jQuery.each(jQuery(table).find("tr"),function(k,v){
						var th = jQuery(v).find("th");
						var th_curr = jQuery(v).find("th").eq(no_col);
						var colspan = jQuery(th).attr("colspan");
						if(!colspan){}else{
							colspan = parseInt(colspan);
							colspan = colspan + 1;
							jQuery(th).attr("colspan",colspan);
						}
						var new_col = 	jQuery(v).find("th").eq(no_col-1).clone();
						if(jQuery(new_col).find("a.remove_colomn_shcedule").length == 0 && jQuery(new_col).find("input").length == 0)
						{
							new_col = jQuery(new_col).prepend('<a href="javascript:void(0)" class="icon delete remove_colomn_shcedule">remove</a><br/>');
						}
						jQuery(th_curr).before(new_col);
					});
					var curr_scroll = jQuery(".container_schedule").scrollLeft();
					jQuery(".container_schedule").scrollLeft(800+curr_scroll);
				});
				jQuery(document).on("click","a.remove_colomn_shcedule",function(){
					var table = jQuery(this).parents('table');
					var no_col = jQuery(table).find("tr").eq(2).find("th").index(jQuery(this).parents("th"));
					var curr_th = jQuery(this).parents("th");
					var no_col = curr_th.index(".table_report tr:eq(2) th");
					
					jQuery.each(jQuery(table).find("tr"),function(k,v){
						var td = jQuery(v).find("td");
						var td_curr = jQuery(v).find("td").eq(no_col);
						var colspan = jQuery(td).attr("colspan");
						if(!colspan){}else{
							colspan = parseInt(colspan);
							colspan = colspan - 1;
							jQuery(td).attr("colspan",colspan);
						}
						jQuery(td_curr).remove();
					});
					jQuery.each(jQuery(table).find("tr"),function(k,v){
						var th = jQuery(v).find("th");
						var th_curr = jQuery(v).find("th").eq(no_col);
						var colspan = jQuery(th).attr("colspan");
						if(!colspan){}else{
							colspan = parseInt(colspan);
							colspan = colspan - 1;
							jQuery(th).attr("colspan",colspan);
						}
						jQuery(th_curr).remove();
					});
				});
				
				var no_rows_after = 0;
				jQuery(document).on("change","select.materi",function(){
					var table = jQuery(this).parents('table');
					var no_row = jQuery(table).find("tr").index(jQuery(this).parents("tr"));
					var curr_th = jQuery(this).parents("td");
					var no_col = jQuery(this).parents("td").index(".table_report tr:eq("+no_row+") td");
					//alert(no_col);
					//alert(no_row);
					var text = jQuery(this).find("option:selected").text();
					var value = jQuery(this).find("option:selected").attr("value");
					var instructure_id = jQuery(this).find("option:selected").attr("instructure_id");
					var instructure_name = jQuery(this).find("option:selected").attr("instructure_name");
					
					//jQuery(table).find("tr").eq(no_row).find("td").eq(no_col-1).find("input.instructure_id").val(instructure_id);
					//jQuery(table).find("tr").eq(no_row).find("td").eq(no_col-1).find("input.instructure_name").val(instructure_name);
					
					jQuery(table).find("tr").eq(no_row+1).find("td").eq(no_col-1).find("input.instructure_id").val(instructure_id);
					jQuery(table).find("tr").eq(no_row+1).find("td").eq(no_col-1).find("input.instructure_name").val(instructure_name);
					if(value == 'custom')
					{
						if(jQuery(this).parent().find("div.text_materi").length > 0)
						{
							jQuery(this).parent().find("div.text_materi").remove();
						}
						if(jQuery(this).parent().find("textarea").length > 0)
						{
							jQuery(this).parent().find("textarea").replaceWith('<textarea style="width:90%;font-size:10px;font-style:italic;">Ketikan Acara disini</textarea>');
						}else{
							jQuery(this).parent().append('<textarea style="width:90%;font-size:10px;font-style:italic;">Ketikan Acara disini</textarea>');
						}
					}else if(value == '')
					{
						if(jQuery(this).parent().find("div.text_materi").length > 0)
						{
							jQuery(this).parent().find("div.text_materi").remove();
						}
						if(jQuery(this).parent().find("textarea").length > 0)
						{
							jQuery(this).parent().find("textarea").remove();
						}
					}else{
						if(jQuery(this).parent().find("textarea").length > 0)
						{
							jQuery(this).parent().find("textarea").remove();
						}
						if(jQuery(this).parent().find("div.text_materi").length > 0)
						{
							jQuery(this).parent().find("div.text_materi").replaceWith('<div class="text_materi">'+text+'</div>');
						}else{
							jQuery(this).parent().append('<div class="text_materi">'+text+'</div>');
						}
					}
				});
		</script>
