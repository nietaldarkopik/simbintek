<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
				<div class="pad_left1">
					<h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
					<ul class="action_menu">
						<li>
							<a class="listing a_ajax" href="<?php echo base_url(). 'bintek_relation_schedule_member/listing/'. $data_jadwal_bintek['jadwal_bintek_id'];?>">listing</a>
						</li>
					</ul>
					<br class="fclear"/><br/>
					
					<form id="form" action="<?php echo base_url(). 'bintek_relation_schedule_member/add/'. $data_jadwal_bintek['jadwal_bintek_id'];?>" method="post" class="form">
						<fieldset>
						<span class="value_view">
							<?php $curr_level = $this->user_access->get_level();?>
							<table border="1">
								<tbody>
									<tr>
										<td style="width:100px;">No</td>
										<td style="width:400px;">Nama Peserta</td>
										<td style="width:200px;">Tanggal Pendaftaran</td>
										<td style="width:200px;">Status Registrasi</td>
										<td style="width:150px;">Action</td>
									</tr>
									<tr style="display:none;" class="master">
										<td>1</td>
										<td>
											<select name="member_ids[]" style="width:350px;">
												<option value="">Nama (no. ktp)</option>
												<?php if(!empty($data_member)):?>
													<?php foreach($data_member as $member):?>
													<option value="<?php echo $member['peserta_bintek_id'];?>"><?php echo $member['nama'];?> (<?php echo $member['no_ktp'];?>)</option>												
													<?php endforeach;?>
												<?php endif;?>
											</select>
										</td>
										<td>
											<?php if($curr_level == 3){?>
												<input type="hidden" name="tanggal_pendaftaran[]" value="<?php echo date('Y-m-d');?>">
												<?php echo date('Y-m-d');?>
											<?php }else{ ?>
												<input type="text" name="tanggal_pendaftaran[]" class="input_date" value="<?php echo date('Y-m-d');?>">
											<?php } ?>
										</td>
										<td>
											<?php if($curr_level == 3){?>
												<input type="hidden" name="status_registrasi[]" value="belum disetujui"/>
												Belum Disetujui
											<?php }else{ ?>
												<select name="status_registrasi[]">
													<option value="belum disetujui">Belum Disetujui</option>
													<option value="sudah disetujui">Sudah Disetujui</option>
												</select>
											<?php
											}
											?>
										</td>
										<td>
											<a class="remove_row_input_multiple" href="javascript:void(0);">delete</a>
										</td>
									</tr>
									<?php 
									
									if(isset($data_peserta_bintek) and is_array($data_peserta_bintek) and count($data_peserta_bintek) > 0)
									{
										foreach($data_peserta_bintek as $index => $peserta)
										{
									?>
									<tr class="master2">
										<td><?php echo $index+1;?></td>
										<td>
											<select name="member_ids[]" style="width:350px;">
												<option value="">Nama (no. ktp)</option>
												<?php if(!empty($data_member)):?>
													<?php foreach($data_member as $member):?>
													<option value="<?php echo $member['peserta_bintek_id'];?>" <?php if($peserta['member_id'] == $member['peserta_bintek_id']){?>selected="selected"<?php }?>><?php echo $member['nama'];?> (<?php echo $member['no_ktp'];?>)</option>												
													<?php endforeach;?>
												<?php endif;?>
											</select>
										</td>
										<td>
											<?php if($curr_level == 3){?>
												<input type="hidden" name="tanggal_pendaftaran[]" value="<?php echo $peserta['tanggal_registrasi'];?>">
												<?php echo $peserta['tanggal_registrasi'];?>
											<?php }else{ ?>
												<input type="text" name="tanggal_pendaftaran[]" class="input_date" value="<?php echo $peserta['tanggal_registrasi'];?>">
											<?php } ?>
										</td>
										<td>
											<?php if($curr_level == 3){?>
												<input type="hidden" name="status_registrasi[]" value="<?php echo $peserta['status_registrasi'];?>"/>
												<?php echo $peserta['status_registrasi'];?>
											<?php }else{ ?>
												<select name="status_registrasi[]">
													<option value="belum disetujui" <?php if($peserta['status_registrasi'] == 'belum disetujui'){?>selected="selected"<?php }?>>Belum Disetujui</option>
													<option value="sudah disetujui" <?php if($peserta['status_registrasi'] == 'sudah disetujui'){?>selected="selected"<?php }?>>Sudah Disetujui</option>
												</select>	
											<?php
												}
											?>
										</td>
										<td>
											<a class="remove_row_input_multiple" href="javascript:void(0);">delete</a>
										</td>
									</tr>
											<?php
										}
									}
									?>
									<tr>
										<td colspan="6">
											<a class="add_row_input_multiple" href="javascript:void(0);">add new</a>
										</td>
									</tr>
								</tbody>
							</table>
						</span>
						<br class="fclear">
						<br class="fclear">
						
						<input type="hidden" value="<?php echo $nonce;?>" name="nonce"> 
						<input type="hidden" name="ajax_target" value="#main_content .col1"/> 
						<input type="hidden" name="is_ajax" value="1"/> 
						<input type="hidden" value="<?php echo $data_jadwal_bintek['jadwal_bintek_id'];?>" name="jadwal_bintek_id"> 
						<span class="value_view">
							&nbsp;&nbsp;<input type="submit" value="Simpan">
						</span>

						</fieldset>
					</form>
				<?php echo $response; ?>
