<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$q = $this->db->query('	SELECT *
							FROM bintek_instruktur A,jadwal_bintek B, bintek_material C
							WHERE 
								C.bintek_id = "' . ((isset($detail['kode_bintek'])?$detail['kode_bintek']:"")) . '" AND 
								A.instruktur_id = C.instructure_id AND 
								B.jadwal_bintek_id = C.jadwal_id 
						   ');
	$select_bintek = $q->result_array();
?>
			<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
			<?php 
				echo $this->data->show_panel_allowed("","",array("listing"));
			?>
			<br class="fclear"/><br/>
			<?php
				echo $this->data->create_view($this->init);
			?>
			<?php
			if(isset($detail) and is_array($detail) and count($detail) > 0)
			{
				$start = strtotime($detail['tglawal']);
				$end = strtotime($detail['tglakhir']);
				$result = array();
				while ($start <= $end) {
					if (date('N', $start) <= 5) {
						$current = date('Y-m-d', $start);
						$result[$current] = $current;
					}
					$start += 86400;
				}
			?>
			<br/>
			<form method="post" name="form" id="form" class="form" action="<?php echo current_url();?>">
				<div style="overflow-x:auto; width:100%;" class="container_schedule">
					
					<table border="1" class="table_report">
						<thead>
							<tr>
								<th colspan="<?php echo (isset($curr_hours) and is_array($curr_hours) and count($curr_hours) > 0)?1+count($curr_hours):3;?>">
									<h3><?php echo $detail['judul_bintek'];?></h3>
									<h4>
										<?php echo $detail['tempat'];?>,
										<?php echo $this->data->human_date($detail['tglawal']);?> -  
										<?php echo $this->data->human_date($detail['tglakhir']);?>
									</h4>
								</th>
							</tr>
							<tr>
								<th width="10%" rowspan="3">Hari</th>
								<?php
								if(isset($curr_hours) and is_array($curr_hours) and count($curr_hours) > 0)
								{
									$no = 0;
									foreach($curr_hours as $index => $h)
									{
										
								?>
									<th>
									Jam
									</th>
								<?php
										$no++;
									}
								}else{
								?>
								<th>Jam</th>
								<?php
								}
								?>
							</tr>
							<tr>
								<?php
								if(isset($curr_hours) and is_array($curr_hours) and count($curr_hours) > 0)
								{
									$hour_no = 0;
									foreach($curr_hours as $index => $h)
									{
								?>
								<th class="hours">
									<?php echo substr($h['jam_awal'],0,5);?> - 
									<?php echo substr($h['jam_akhir'],0,5);?>
								</th>
								<?php
										$hour_no++;
									}
								}else{
								?>
								<th class="hours">
									00:00 - 
									00:00
								</th>
								<?php
								}
								?>
							</tr>
						</thead>
						<tbody>
							<?php
							if(is_array($result) and count($result) > 0)
							{
								$no = 1;
								foreach($result as $date => $r)
								{
							?>
								<tr class="days">
									<td align="center" rowspan="2">
										Hari ke<br/>
										<strong>
										<?php echo $no; ?>
										</strong>
										<br/>
										<?php echo $this->data->human_date($date,false,true); ?>
									</td>
									<?php
									if(isset($material_structured[$no]) and is_array($material_structured[$no]) and count($material_structured[$no]) > 0)
									{
										$no_col = 0;
										foreach($material_structured[$no] as $index => $h)
										{
									?>
									<td height="80" align="center" valign="middle" class="event_colomn">
											<?php
											foreach($select_bintek as $index2 => $value)
											{
											?>
												<?php if($h['material_id'] == $value['material_id']){ echo $value['material_name']; } ?>
											<?php
											}
											?>
										</select>
										<?php if($h['material_id'] == '0'){?>
											<?php echo $h['acara'];?>
										<?php } ?>
									</td>
								<?php
										$no_col++;
									}
								}
								?>
								</tr>
								<tr class="instructure_rows">
								<?php
								if(isset($material_structured[$no]) and is_array($material_structured[$no]) and count($material_structured[$no]) > 0)
								{
									$no_col = 0;
									foreach($material_structured[$no] as $index => $h)
									{
								?>
									<td align="center" class="instructure_colomn">
										<strong><?php echo $h['instructure_name']; ?></strong>
									</td>
								<?php
									$no_col++;
									}
								}
								?>
								</tr>
							<?php
								$no++;
								}
							}
							?>
						</tbody>
					</table>
				</div>
			</form>
			<?php
			}
			?>
		<style>
		.button_save_schedule input{
			width: 100%;
		}
		.button_save_schedule input{
			margin-top: 20px;
			left: 35%;
			position: relative;
			width: 30%;
		}
		.table_report td,.table_report th{
			min-width: 150px;
		}
		</style>
