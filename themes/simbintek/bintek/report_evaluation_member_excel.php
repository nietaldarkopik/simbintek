<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	//get all unsur penilaian member
	$this->db->order_by('material_id', 'ASC');
	$q_unsur_penilaian = $this->db->get('bintek_material');
	$n_unsur_penilaian = $q_unsur_penilaian->num_rows();
	$unsur_penilaian = $q_unsur_penilaian->result_array();
	/*
	//get all evaluation data
	$query = "
		SELECT bintek_eval_member.*, bintek_peserta.nama, jadwal_bintek.judul_bintek
		FROM  bintek_eval_member, bintek_peserta, jadwal_bintek
		WHERE 
			bintek_eval_member.eval_member_id = bintek_peserta.peserta_bintek_id AND 
			bintek_eval_member.jadwal_id = jadwal_bintek.jadwal_bintek_id
		ORDER BY bintek_eval_member.jadwal_id ASC
	";
	*/
	
	$q_evaluation = $this->db->query($query);
	$n_evaluation = $q_evaluation->num_rows();
	$data_evaluation = $q_evaluation->result_array();
	$total_rata_rata = 0;
?>


			<style>
			
		<!--
			@media print
			{
				table {
					color: #003300;
					font-family: helvetica;
					font-size: 8pt;
					background-color: #ffffff;
					border: 2px #000000 solid;
				}
				td {
					border: 1px #000000 solid;
					background-color: #ffffff;
				}
				th {
					border: 1px #000000 solid;
					background-color: #ffffff;
					font-weight: bold;
					text-align: center;
				}
			}
			-->
			</style>
			<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
			
		<table class="data_member">
		<tr class="header">
			<td rowspan="2" width="35px">NO.</td>
			<td rowspan="2" width="300px" align="center">Bintek</td>
			<td rowspan="2" width="300px" align="center">Nama<br />Peserta</td>
			<td colspan="<?php echo $n_unsur_penilaian;?>" align="center">UNSUR YANG DINILAI</td>
			<td rowspan="2">RATA - RATA</td>
		</tr>
		<tr class="header points">
			<td>&nbsp;</td>
			<?php
				$data_penilaian = array();
				if(!empty($unsur_penilaian))
				{
					foreach($unsur_penilaian as $idx => $up)
					{
						$data_penilaian[$idx] = array(
							'unsur_penilaian' => $up['material_id'],
							'total_nilai' => 0
						);
						echo "<td>{$up['material_name']}</td>";
					}
				}
			?>
		</tr>
		
		<?php 
			if(!empty($data_evaluation)):
				$no = 1;
				foreach($data_evaluation as $idx => $evaluation):
					$q_get_bintek = 
					$nilai = json_decode($evaluation['nilai'], true);
					$total_rata_rata += $this->data->get_average_data($evaluation['nilai'], count($data_penilaian));
		?>		
			<tr>
				<td class="bold"><?php echo $no;?></td>
				<td class="bold alignleft"><?php echo $this->master->get_value('jadwal_bintek','judul_bintek',array('jadwal_bintek_id' => $evaluation['jadwal_id']));?></td>
				<td class="bold alignleft"><?php echo $this->master->get_value('bintek_peserta','nama',array('peserta_bintek_id' => $evaluation['member_id']));?></td>
				<?php
					foreach($data_penilaian as $idx_penilaian => $penilaian){
						foreach($nilai as $nl){
							if($nl['label'] == $penilaian['unsur_penilaian']){
								$data_penilaian[$idx_penilaian]['total_nilai'] += $nl['value'];
								echo "<td>{$nl['value']}</td>";
							}
						}
					}
				?>
				<td><?php echo $this->data->get_average_data($evaluation['nilai'], count($data_penilaian));?></td>
			</tr>	
			<?php 
				$no++;
				endforeach;
			?>
		<?php endif;?>
		
		<tr class="total">
			<td colspan="3" class="bold">JUMLAH</td>			
			<?php
				foreach($data_penilaian as $idx_penilaian => $penilaian){
					echo "<td>{$penilaian['total_nilai']}</td>";
				}
			?>
			<td><?php echo $total_rata_rata;?></td>
		</tr>	

		
	</table>	

