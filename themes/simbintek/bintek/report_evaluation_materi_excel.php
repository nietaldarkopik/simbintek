<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	//get all unsur penilaian instruktur
	$this->db->order_by('id_unsur_penilaian', 'ASC');
	$q_unsur_penilaian = $this->db->get('unsur_penilaian_materi');
	$n_unsur_penilaian = $q_unsur_penilaian->num_rows();
	$unsur_penilaian = $q_unsur_penilaian->result_array();
	
	//get all evaluation data
	/*
	$query = "
		SELECT bintek_eval_material.*, bintek_material.material_name, bintek_material.jam_pelajaran, jadwal_bintek.judul_bintek
		FROM  bintek_eval_material, bintek_material, jadwal_bintek
		WHERE 
			bintek_eval_material.material_id = bintek_material.material_id AND 
			bintek_eval_material.jadwal_id = jadwal_bintek.jadwal_bintek_id
		ORDER BY bintek_eval_material.jadwal_id ASC
	";
	*/
	$q_evaluation = $this->db->query($query);
	$n_evaluation = $q_evaluation->num_rows();
	$data_evaluation = $q_evaluation->result_array();
	
?>


			<style>
			
		<!--
			@media print
			{
				table {
					color: #003300;
					font-family: helvetica;
					font-size: 8pt;
					background-color: #ffffff;
					border: 2px #000000 solid;
				}
				td {
					border: 1px #000000 solid;
					background-color: #ffffff;
				}
				th {
					border: 1px #000000 solid;
					background-color: #ffffff;
					font-weight: bold;
					text-align: center;
				}
			}
			-->
			</style>
			<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
			
		<table class="data_instruktur">
		<tr class="header">
			<td rowspan="2" width="35px">NO.</td>
			<td rowspan="2" width="300px" align="center">NAMA MODUL</td>
			<td rowspan="2" width="300px" align="center">JUMLAH <br />JAM PELAJARAN</td>
			<td colspan="<?php echo $n_unsur_penilaian;?>" align="center">PENILAIAN</td>
			<td rowspan="2">KETERANGAN</td>
		</tr>
		<tr class="header points">
			<td>&nbsp;</td>
			<?php
				$data_penilaian = array();
				if(!empty($unsur_penilaian))
				{
					foreach($unsur_penilaian as $idx => $up)
					{
						$data_penilaian[$idx] = array(
							'unsur_penilaian' => $up['id_unsur_penilaian'],
							'total_nilai' => 0
						);
						echo "<td>{$up['unsur_penilaian']}</td>";
					}
				}
			?>
		</tr>
		
		<?php 
			if(!empty($data_evaluation)):
				$no = 1;
				$total_jampel = 0; 
				foreach($data_evaluation as $idx => $evaluation):
					$nilai = json_decode($evaluation['nilai'], true);
					$total_jampel += $evaluation['jam_pelajaran'];
		?>		
			<tr>
				<td class="bold"><?php echo $no;?></td>
				<td class="bold alignleft"><?php echo $this->master->get_value('bintek_material','material_name',array('material_id' => $evaluation['material_id']));?></td>
				<td class="bold alignleft"><?php echo $this->master->get_value('bintek_material','jam_pelajaran',array('material_id' => $evaluation['material_id']));?></td>
				<?php
					foreach($data_penilaian as $idx_penilaian => $penilaian){
						foreach($nilai as $nl){
							if($nl['label'] == $penilaian['unsur_penilaian']){
								$data_penilaian[$idx_penilaian]['total_nilai'] += $nl['value'];
								echo "<td>{$nl['value']}</td>";
							}
						}
					}
				?>
				<td>&nbsp;</td>
			</tr>	
			<?php 
				$no++;
				endforeach;
			?>
		<?php endif;?>
		
		<tr class="total">
			<td colspan="2" class="bold">JUMLAH</td>			
			<td><?php echo $total_jampel;?></td>			
			<?php
				foreach($data_penilaian as $idx_penilaian => $penilaian){
					echo "<td>{$penilaian['total_nilai']}</td>";
				}
			?>
			<td>&nbsp;</td>
		</tr>	

		
	</table>	

