<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
			<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
			<div id="top-panel">
				<div id="panel">			
					<ul class="action_menu">
						<li>
							<a class="add a_ajax" href="<?php echo base_url() .'bintek_relation_schedule_instructure/add/'. $data_jadwal_bintek['jadwal_bintek_id'];?>">add</a>
							<a class="" href="<?php echo base_url() .'bintek_schedule/index';?>">kembali ke jadwal bintek</a>
						</li>
					</ul>
					<form target="_self" class="form_bulk no_ajax" action="<?php echo base_url();?>index.php/bintek_relation_schedule_instructure/listing/<?php echo $object_id;?>" method="post">
						<select name="bulk_action">
							<option align="center" value=""> Choose Action </option>
							<option value="bintek_relation_schedule_instructure/download_excel/<?php echo $object_id;?>">download excel</option>
							<!--<option value="bintek_relation_schedule_instructure/download_pdf/<?php echo $object_id;?>">download pdf</option>-->
							<!--<option value="bintek_relation_schedule_instructure/printing/<?php echo $object_id;?>">printing</option>-->
						</select>
						<input type="hidden" value="#main_content .col1" name="ajax_target"> 
						<input type="hidden" value="0" name="is_ajax"> 
						<input type="hidden" id="bulk_data" value="" name="bulk_data"> 
						<button value="GO" name="do_bulk_action" type="submit">
		
						</button>
					</form>
				</div>
			</div>
			<table width="100%">
						<thead>
							<tr>
								<th width="100">No</th>
								<th>Nama</th>
								<th width="250">Action</th>
							</tr>
						</thead>				
						<tbody>
							<?php if(!empty($data_instruktur)):?>
								<?php foreach($data_instruktur as $idx => $instruktur):?>
								<tr>
									<td><?php echo ($idx+1);?></td>
									<td><?php echo $instruktur['nama'];?> (<?php echo $instruktur['nip'];?>)</td>
									<td>
									<ul class="action_menu">
										<li>
											<a class="delete a_ajax" href="<?php echo base_url() .'bintek_relation_schedule_instructure/delete/'. $instruktur['relation_id'] .'/'. $data_jadwal_bintek['jadwal_bintek_id'];?>">delete</a>
										</li>
									</ul>
									</td>
								</tr>
								<?php endforeach;?>
							<?php endif;?>
						</tbody>
			</table>
			
			<?php echo $response; ?>
