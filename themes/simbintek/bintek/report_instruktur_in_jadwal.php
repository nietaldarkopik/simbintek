<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	<table class="data_member">
		<tr>
			<td colspan="18" align="center" class="noborder">DATA DATA INSTRUKTUR</td>
		</tr>	
		<tr>
			<td colspan="18" class="noborder">Nama Bintek : <?php echo $data_jadwal['judul_bintek'];?></td>
		</tr>	
		<tr>
			<td colspan="18" class="noborder">Periode : <?php echo $this->data->human_date($data_jadwal['tglawal']);?> - <?php echo $this->data->human_date($data_jadwal['tglakhir']);?></td>
		</tr>
		<tr class="header">
			<td>No.</td>
			<td>NAMA INSTRUKTUR</td>
			<td>NIP</td>
			<td>GELAR</td>
			<td>GOLONGAN</td>
			<td>TEMPAT, TANGGAL LAHIR</td>
			<td>JABATAN</td>
			<td>STATUS</td>
			<td>INSTANSI</td>
			<td>ALAMAT INSTANSI</td>
			<td>TELEPON INSTANSI</td>
			<td>ALAMAT RUMAH</td>
			<td>TELEPON RUMAH</td>
			<td>EMAIL</td>
			<td>PENGALAMAN KERJA</td>
			<td>PENDIDIKAN TERTINGGI</td>
			<td>PELATIHAN TEKNIK</td>
			<td>SPESIALISASI PELATIHAN</td>
		</tr>
		
		<?php if(!empty($data_instruktur)):?>
			<?php foreach($data_instruktur as $idx => $instruktur):?>
				<tr>
					<td><?php echo ($idx + 1);?></td>
					<td><?php echo $instruktur['nama'];?></td>
					<td><?php echo $instruktur['nip'];?></td>
					<td><?php echo $instruktur['gelar'];?></td>
					<td><?php echo $instruktur['gol'];?></td>
					<td><?php echo @$instruktur['detail_kota_lahir']['nama'];?>, <?php echo @$instruktur['detail_propinsi_lahir']['nama'];?> / <?php echo $this->data->human_date($instruktur['tanggal_lahir']);?></td>
					<td><?php echo $instruktur['jabatan'];?></td>
					<td><?php echo $instruktur['status'];?></td>
					<td><?php echo $instruktur['instansi'];?></td>
					<td><?php echo $instruktur['alamat_instansi'];?><?php echo (isset($instruktur['detail_kota_instansi']['nama'])) ? ', '. $instruktur['detail_kota_instansi']['nama'] : '';?><?php echo (isset($instruktur['detail_propinsi_instansi']['nama'])) ? ', '. $instruktur['detail_propinsi_instansi']['nama'] : '';?></td>
					<td><?php echo $instruktur['telepon_instansi'];?> Fax: <?php echo (!empty($instruktur['fax_instansi'])) ? $instruktur['fax_instansi'] : '-';?></td>
					<td><?php echo $instruktur['alamat_rumah'];?><?php echo (isset($instruktur['detail_kota_rumah']['nama'])) ? ', '. $instruktur['detail_kota_rumah']['nama'] : '';?><?php echo (isset($instruktur['detail_propinsi_rumah']['nama'])) ? ', '. $instruktur['detail_propinsi_rumah']['nama'] : '';?></td>
					<td><?php echo $instruktur['telp_rumah'];?> Fax: <?php echo (!empty($instruktur['fax_rumah'])) ? $instruktur['fax_rumah'] : '-';?></td>
					<td><?php echo $instruktur['email'];?></td>
					<td><?php echo $instruktur['pengalaman_kerja'];?></td>
					<td><?php echo $instruktur['pendidikan_tertinggi'];?></td>
					<td><?php echo $instruktur['pelatihan_teknik'];?></td>
					<td><?php echo $instruktur['spesialisasi_pelatihan'];?></td>
				</tr>
			<?php endforeach;?>
		<?php endif;?>
		
	</table>	


			<style>
			
			@media print
			{
				table {
					color: #003300;
					font-family: helvetica;
					font-size: 8pt;
					background-color: #ffffff;
					border: 2px #000000 solid;
				}
				td {
					border: 1px #000000 solid;
					background-color: #ffffff;
				}
				td.noborder {
					border: none;
					background-color: #ffffff;
				}
				th {
					border: 1px #000000 solid;
					background-color: #ffffff;
					font-weight: bold;
					text-align: center;
				}
			}
			</style>
