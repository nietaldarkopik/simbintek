<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$q = $this->db->query('	SELECT *
							FROM bintek_instruktur A,jadwal_bintek B, bintek_material C
							WHERE 
								C.bintek_id = "' . ((isset($detail['kode_bintek'])?$detail['kode_bintek']:"")) . '" AND 
								A.instruktur_id = C.instructure_id AND 
								B.jadwal_bintek_id = C.jadwal_id 
						   ');
	$select_bintek = $q->result_array();
?>
			<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
			<?php 
				echo $this->data->show_panel_allowed("","",array("listing"));
			?>
			<br class="fclear"/><br/>
			<?php
				echo $this->data->create_form($this->init);
			?>
			<?php
			if(isset($detail) and is_array($detail) and count($detail) > 0)
			{
				$start = strtotime($detail['tglawal']);
				$end = strtotime($detail['tglakhir']);
				$result = array();
				while ($start <= $end) {
					if (date('N', $start) <= 5) {
						$current = date('Y-m-d', $start);
						$result[$current] = $current;
					}
					$start += 86400;
				}
			?>
			<br/>
			<form method="post" name="form" id="form" class="form" action="<?php echo current_url();?>">
				<div style="overflow-x:auto; width:100%;" class="container_schedule">
					
					<table border="1" class="table_report">
						<thead>
							<tr>
								<th colspan="<?php echo (isset($curr_hours) and is_array($curr_hours) and count($curr_hours) > 0)?2+count($curr_hours):3;?>">
									<h3><?php echo $detail['judul_bintek'];?></h3>
									<h4>
										<?php echo $detail['tempat'];?>,
										<?php echo $this->data->human_date($detail['tglawal']);?> -  
										<?php echo $this->data->human_date($detail['tglakhir']);?>
									</h4>
								</th>
							</tr>
							<tr>
								<th colspan="<?php echo (isset($curr_hours) and is_array($curr_hours) and count($curr_hours) > 0)?2+count($curr_hours):3;?>">
									&nbsp;
								</th>
							</tr>
							<tr>
								<th width="10%" rowspan="3">Hari</th>
								<?php
								if(isset($curr_hours) and is_array($curr_hours) and count($curr_hours) > 0)
								{
									$no = 0;
									foreach($curr_hours as $index => $h)
									{
										
								?>
									<th>
									<?php
									if($no > 0)
									{
										echo '<a href="javascript:void(0)" class="icon delete remove_colomn_schedule">remove</a><br/>';
										
									}
									?>
									Jam
									</th>
								<?php
										$no++;
									}
								}else{
								?>
								<th>Jam</th>
								<?php
								}
								?>
								<th width="5%"><a href="javascript:void(0);" class="icon add add_colomn_schedule">add</a></th>
							</tr>
							<tr>
								<?php
								if(isset($curr_hours) and is_array($curr_hours) and count($curr_hours) > 0)
								{
									$hour_no = 0;
									foreach($curr_hours as $index => $h)
									{
								?>
								<th class="hours">
									<input type="text" name="hour_from[<?php echo $hour_no;?>]" value="<?php echo substr($h['jam_awal'],0,5);?>" size="5" style="width:35px;" class="hour_from"/> - 
									<input type="text" name="hour_to[<?php echo $hour_no;?>]" value="<?php echo substr($h['jam_akhir'],0,5);?>" size="5" style="width:35px;" class="hour_to"/>
								</th>
								<?php
										$hour_no++;
									}
								}else{
								?>
								<th class="hours">
									<input type="text" name="hour_from[0]" value="00:00" size="5" style="width:35px;" class="hour_from"/> - 
									<input type="text" name="hour_to[0]" value="00:00" size="5" style="width:35px;" class="hour_to"/>
								</th>
								<?php
								}
								?>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(is_array($result) and count($result) > 0)
							{
								$no = 1;
								foreach($result as $date => $r)
								{
							?>
								<tr class="days">
									<td align="center" rowspan="2">
										Hari ke<br/>
										<strong>
										<?php echo $no; ?>
										</strong>
										<br/>
										<?php echo $this->data->human_date($date,false,true); ?>
										<input type="hidden" name="day_no[<?php echo $no-1; ?>]" value="<?php echo $no; ?>" class="day_no"/>
										<input type="hidden" name="day_date[<?php echo $no-1; ?>]" value="<?php echo $date; ?>" class="day_date"/>
									</td>
									<?php
									if(isset($material_structured[$no]) and is_array($material_structured[$no]) and count($material_structured[$no]) > 0)
									{
										$no_col = 0;
										foreach($material_structured[$no] as $index => $h)
										{
									?>
									<td height="80" align="center" valign="middle" class="event_colomn">
										<select name="material[<?php echo $no-1; ?>][<?php echo $no_col;?>]" class="material" style="width:150px;">
											<option value="">-- Pilih Materi --</option>
											<option value="custom" <?php if($h['material_id'] == '0'){?>selected="selected"<?php } ?>>-- Custom --</option>
											<?php
											foreach($select_bintek as $index2 => $value)
											{
											?>
											<option value="<?php echo $value['material_id'];?>" <?php if($h['material_id'] == $value['material_id']){?>selected="selected"<?php } ?> instructure_id="<?php echo $value['instruktur_id'];?>" instructure_name="<?php echo $value['nama'];?>"><?php echo $value['material_name'];?></option>
											<?php
											}
											?>
										</select>
										<?php if($h['material_id'] == '0'){?>
											<textarea style="width:90%;font-size:10px;font-style:italic;" name="textarea_acara[<?php echo $no-1; ?>][<?php echo $no_col;?>]"><?php echo $h['acara'];?></textarea>
										<?php } ?>
									</td>
								<?php
										$no_col++;
									}
								}else{
								?>
									<td height="80" align="center" valign="middle" class="event_colomn">
										<select name="material[<?php echo $no-1; ?>][0]" class="material" style="width:150px;">
											<option value="">-- Pilih Materi --</option>
											<option value="custom">-- Custom --</option>
											<?php
											foreach($select_bintek as $index => $value)
											{
											?>
											<option value="<?php echo $value['material_id'];?>" instructure_id="<?php echo $value['instruktur_id'];?>" instructure_name="<?php echo $value['nama'];?>"><?php echo $value['material_name'];?></option>
											<?php
											}
											?>
										</select>
									</td>
								<?php
								}
								?>
									<td height="80" align="center" valign="middle">&nbsp;</td>
								</tr>
								<tr class="instructure_rows">
								<?php
								if(isset($material_structured[$no]) and is_array($material_structured[$no]) and count($material_structured[$no]) > 0)
								{
									$no_col = 0;
									foreach($material_structured[$no] as $index => $h)
									{
								?>
									<td align="center" class="instructure_colomn">
										<input type="text" value="<?php echo $h['instructure_name']; ?>" name="instructure_name[<?php echo $h['hari_ke']-1; ?>][<?php echo $no_col;?>]" class="instructure_name" style="font-style:italic;text-align:center;" onblur="if(this.value == null || this.value == ''){this.value='instruktur';this.style.fontStyle='italic';}else{this.style.fontStyle='normal';}" onfocus="if(this.value == 'instruktur'){this.value='';}else{}" />
										<input type="hidden" value="<?php echo $h['instruktur']; ?>" name="instructure_id[<?php echo $h['hari_ke']-1; ?>][<?php echo $no_col;?>]" class="instructure_id"/>
									</td>
								<?php
									$no_col++;
									}
								}else{
								?>
									<td align="center" class="instructure_colomn">
										<input type="text" value="instruktur" name="instructure_name[<?php echo $no-1; ?>][0]" class="instructure_name" style="font-style:italic;text-align:center;" onblur="if(this.value == null || this.value == ''){this.value='instruktur';this.style.fontStyle='italic';}else{this.style.fontStyle='normal';}" onfocus="if(this.value == 'instruktur'){this.value='';}else{}" />
										<input type="hidden" value="" name="instructure_id[<?php echo $no-1; ?>][0]" class="instructure_id"/>
									</td>
								<?php
								}
								?>
									<td align="center">&nbsp;</td>
								</tr>
							<?php
								$no++;
								}
							}
							?>
						</tbody>
					</table>
				</div>
				<input type="hidden" name="nonce" value="138556094915295fb751d3981"> 
				<input type="hidden" name="ajax_target" value="#main_content .col1"> 
				<input type="hidden" name="is_ajax" value="1"> 
				<input type="hidden" name="jadwal_bintek_id" value="<?php echo $this->data->primary_key_value;?>"> 
				<input type="hidden" name="do_save_schedule" value="1"> 
				<span class="button_save_schedule">
					<input type="submit" value="Simpan Jadwal" >
				</span>
			</form>
			<?php
			}
			?>
		<style>
		.button_save_schedule input{
			width: 100%;
		}
		.button_save_schedule input{
			margin-top: 20px;
			left: 35%;
			position: relative;
			width: 30%;
		}
		.table_report td,.table_report th{
			min-width: 150px;
		}
		</style>
