<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

			<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
			<div id="top-panel">
				<div id="panel">
					<ul class="action_menu">
						<li>
							<a class="add a_ajax" href="<?php echo base_url() .'bintek_relation_schedule_member/add/'. $data_jadwal_bintek['jadwal_bintek_id'];?>">add</a>
							<a class="" href="<?php echo base_url() .'bintek_schedule/index';?>">kembali ke jadwal bintek</a>
						</li>
					</ul>
					<form target="_self" class="form_bulk no_ajax" action="<?php echo base_url();?>index.php/bintek_relation_schedule_member/listing/<?php echo $object_id;?>" method="post">
						<select name="bulk_action">
							<option align="center" value=""> Choose Action </option>
							<option value="bintek_relation_schedule_member/download_excel/<?php echo $object_id;?>">download excel</option>
							<!--<option value="bintek_relation_schedule_member/download_pdf/<?php echo $object_id;?>">download pdf</option>-->
							<!--<option value="bintek_relation_schedule_member/printing/<?php echo $object_id;?>">printing</option>-->
						</select>
						<input type="hidden" value="#main_content .col1" name="ajax_target"> 
						<input type="hidden" value="0" name="is_ajax"> 
						<input type="hidden" id="bulk_data" value="" name="bulk_data"> 
						<button value="GO" name="do_bulk_action" type="submit">
		
						</button>
					</form>
					<?php #echo $this->data->show_bulk_allowed("","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array("delete")));?>
				</div>
			</div>
			<table width="100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>No. KTP</th>
								<th>Tanggal Registrasi</th>
								<th>Status Registrasi</th>
								<th class="action_menu_col">Action</th>
							</tr>
						</thead>				
						<tbody>
							<?php if(!empty($data_member)):?>
								<?php foreach($data_member as $idx => $member):?>
								<tr>
									<td><?php echo ($idx+1);?></td>
									<td><?php echo $member['nama'];?></td>
									<td><?php echo $member['no_ktp'];?></td>
									<td><?php echo $member['tanggal_registrasi'];?></td>
									<td>
										<center>
										<?php $curr_level = $this->user_access->get_level();?>
										<?php if($member['status_registrasi'] == 'sudah disetujui'):?>
											<?php if($curr_level != 3){?>
											<a class="a_ajax" href="<?php echo base_url() .'bintek_relation_schedule_member/status_belum/'. $member['relation_id'] .'/'. $data_jadwal_bintek['jadwal_bintek_id'];?>" style="color:green;">
											<?php } ?>
												sudah disetujui
											<?php if($curr_level != 3){?>
											</a>
											<?php } ?>
										<?php else:?>
											<?php if($curr_level != 3){?>
											<a class="a_ajax" href="<?php echo base_url() .'bintek_relation_schedule_member/status_sudah/'. $member['relation_id'] .'/'. $data_jadwal_bintek['jadwal_bintek_id'];?>" style="color:red;">
											<?php } ?>
												belum disetujui
											<?php if($curr_level != 3){?>
											</a>
											<?php } ?>
										<?php endif;?>
										</center>
									</td>
									<td>
									<ul class="action_menu">
										<li>
											<a class="delete a_ajax" href="<?php echo base_url() .'bintek_relation_schedule_member/delete/'. $member['relation_id'] .'/'. $data_jadwal_bintek['jadwal_bintek_id'];?>">delete</a>
										</li>
									</ul>
									</td>
								</tr>
								<?php endforeach;?>
							<?php endif;?>
						</tbody>
			</table>
			
			<?php echo $response; ?>

