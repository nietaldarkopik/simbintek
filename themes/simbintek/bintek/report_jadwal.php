<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$q = $this->db->query('	SELECT *
							FROM bintek_instruktur A,jadwal_bintek B, bintek_material C
							WHERE 
								A.instruktur_id = C.instructure_id AND 
								B.jadwal_bintek_id = C.jadwal_id 
						   ');
	$q = $this->db->query($query);
	$select_bintek = $q->result_array();
?>
			<?php
			if(isset($detail) and is_array($detail) and count($detail) > 0)
			{
				$start = strtotime($detail['tglawal']);
				$end = strtotime($detail['tglakhir']);
				$result = array();
				while ($start <= $end) {
					if (date('N', $start) <= 5) {
						$current = date('Y-m-d', $start);
						$result[$current] = $current;
					}
					$start += 86400;
				}
			?>
					<?php
						echo $this->data->create_view($this->init);
					?>
					<table border="1" class="table_report">
							<tr>
								<td align="center" valign="middle" size="32" colspan="<?php echo (isset($curr_hours) and is_array($curr_hours) and count($curr_hours) > 0)?1+count($curr_hours):3;?>">
									<h3><b><?php echo $detail['judul_bintek'];?></b></h3><br/>
								</td>
							</tr>
							<tr>
								<td align="center" valign="middle" size="32" colspan="<?php echo (isset($curr_hours) and is_array($curr_hours) and count($curr_hours) > 0)?1+count($curr_hours):3;?>">
									<h4>
										<?php echo $detail['tempat'];?>, 
										<?php echo $this->data->human_date($detail['tglawal']);?> - <?php echo $this->data->human_date($detail['tglakhir']);?>
									</h4>
								</td>
							</tr>
							<tr>
								<td rowspan="2" align="center" valign="middle" ><b>Hari</b></td>
								<?php
								if(isset($curr_hours) and is_array($curr_hours) and count($curr_hours) > 0)
								{
									$no = 0;
									foreach($curr_hours as $index => $h)
									{
										
								?>
									<td align="center" valign="middle" >
										<b>Jam</b>
									</td>
								<?php
										$no++;
									}
								}else{
								?>
								<td align="center" valign="middle" ><b>Jam</b></td>
								<?php
								}
								?>
							</tr>
							<tr>
								<?php
								if(isset($curr_hours) and is_array($curr_hours) and count($curr_hours) > 0)
								{
									$hour_no = 0;
									foreach($curr_hours as $index => $h)
									{
								?>
								<td class="hours" align="center" valign="middle" >
									<b><?php echo substr($h['jam_awal'],0,5);?> - <?php echo substr($h['jam_akhir'],0,5);?></b>
								</td>
								<?php
										$hour_no++;
									}
								}else{
								?>
								<td class="hours" align="center" valign="middle" >
									<b>00:00 - 00:00</b>
								</td>
								<?php
								}
								?>
							</tr>
							<?php
							if(is_array($result) and count($result) > 0)
							{
								$no = 1;
								foreach($result as $date => $r)
								{
							?>
								<tr class="days">
									<td align="center" rowspan="2" valign="middle" >
										Hari ke<br/>
										<b>
										<?php echo $no; ?>
										</b>
										<br/>
										<?php echo $this->data->human_date($date,false,true); ?>
									</td>
									<?php
									if(isset($material_structured[$no]) and is_array($material_structured[$no]) and count($material_structured[$no]) > 0)
									{
										$no_col = 0;
										foreach($material_structured[$no] as $index => $h)
										{
									?>
									<td height="80" align="center" valign="top" class="event_colomn">
											<?php
											foreach($select_bintek as $index2 => $value)
											{
											?>
												<?php if($h['material_id'] == $value['material_id']){ echo $value['material_name']; } ?>
											<?php
											}
											?>
										<?php if($h['material_id'] == '0'){?>
											<?php echo $h['acara'];?>
										<?php } ?>
									</td>
								<?php
										$no_col++;
									}
								}
								?>
								</tr>
								<tr class="instructure_rows">
								<?php
								if(isset($material_structured[$no]) and is_array($material_structured[$no]) and count($material_structured[$no]) > 0)
								{
									$no_col = 0;
									foreach($material_structured[$no] as $index => $h)
									{
								?>
									<td class="instructure_colomn" align="center" valign="middle" >
										<b><?php echo $h['instructure_name']; ?></b>
									</td>
								<?php
									$no_col++;
									}
								}
								?>
								</tr>
							<?php
								$no++;
								}
							}
							?>
					</table>
			<?php
			}
			?>
