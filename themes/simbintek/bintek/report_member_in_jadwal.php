<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	<table class="data_member">
		<tr>
			<td colspan="11" align="center" class="noborder">DATA DATA PESERTA</td>
		</tr>	
		<tr>
			<td colspan="11" class="noborder">Nama Bintek : <?php echo $data_jadwal['judul_bintek'];?></td>
		</tr>	
		<tr>
			<td colspan="11" class="noborder">Periode : <?php echo $this->data->human_date($data_jadwal['tglawal']);?> - <?php echo $this->data->human_date($data_jadwal['tglakhir']);?></td>
		</tr>
		
		<tr class="header">
			<td width="35">No.</td>
			<td width="162">NAMA PESERTA</td>
			<td width="125">NIK</td>
			<td width="121">No. KTP</td>
			<td width="204">TEMPAT / TGL. LAHIR</td>
			<td width="68">PENDIDIKAN TERAKHIR</td>
			<td width="272">INSTANSI PENGUTUS / ALAMAT</td>
			<td width="281">JABATAN</td>
			<td width="283">Alamat Kantor</td>
			<td width="157">Telp. Kantor</td>
			<td width="80">Predikat</td>
		</tr>
		
		<?php if(!empty($data_member)):?>
			<?php foreach($data_member as $idx => $member):?>
				<tr>
					<td><?php echo ($idx + 1);?></td>
					<td class="alignleft"><?php echo $member['nama'];?></td>
					<td><?php echo $member['nip'];?></td>
					<td><?php echo $member['no_ktp'];?></td>
					<td><?php echo $member['tempat_lahir'];?>, <?php echo $this->data->human_date($member['tanggal_lahir']);?></td>
					<td><?php echo $member['pendidikan_terakhir'];?></td>
					<td><?php echo $member['detail_lembaga']['nmlembaga'];?></td>
					<td><?php echo $member['jabatan'];?></td>
					<td class="alignleft"><?php echo $member['detail_lembaga']['alamat'];?></td>
					<td class="alignleft"><?php echo $member['detail_lembaga']['telp'];?></td>
					<td></td>
				</tr>
			<?php endforeach;?>
		<?php endif;;?>
		
	</table>	

			<style>
			
			@media print
			{
				table {
					color: #003300;
					font-family: helvetica;
					font-size: 8pt;
					background-color: #ffffff;
					border: 2px #000000 solid;
				}
				td {
					border: 1px #000000 solid;
					background-color: #ffffff;
				}
				td.noborder {
					border: none;
					background-color: #ffffff;
				}
				th {
					border: 1px #000000 solid;
					background-color: #ffffff;
					font-weight: bold;
					text-align: center;
				}
			}
			</style>
