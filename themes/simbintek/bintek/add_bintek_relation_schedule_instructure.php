<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
				<div class="pad_left1">
					<h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
					<ul class="action_menu">
						<li>
							<a class="listing a_ajax" href="<?php echo base_url(). 'bintek_relation_schedule_instructure/listing/'. $data_jadwal_bintek['jadwal_bintek_id'];?>">listing</a>
						</li>
					</ul>
					<br class="fclear"/><br/>
					
					<form id="form" action="<?php echo base_url(). 'bintek_relation_schedule_instructure/add/'. $data_jadwal_bintek['jadwal_bintek_id'];?>" method="post" class="form">
						<fieldset>
						<span class="value_view">
							<table border="1">
								<tbody>
									<tr>
										<td style="width:100px;">No</td>
										<td style="width:400px;">Nama Instruktur</td>
										<td style="width:150px;">Action</td>
									</tr>
									<tr style="display:none;" class="master">
										<td>1</td>
										<td>
											<select name="instructure_ids[]" style="width:350px;">
												<option value="">Nama (NIP)</option>
												<?php if(!empty($data_instruktur)):?>
													<?php foreach($data_instruktur as $instruktur):?>
													<option value="<?php echo $instruktur['instruktur_id'];?>"><?php echo $instruktur['nama'];?> (<?php echo $instruktur['nip'];?>)</option>												
													<?php endforeach;?>
												<?php endif;?>
											</select>
										</td>
										<td>
											<a class="remove_row_input_multiple" href="javascript:void(0);">delete</a>
										</td>
									</tr>
									<tr>
										<td colspan="6">
											<a class="add_row_input_multiple" href="javascript:void(0);">add new</a>
										</td>
									</tr>
								</tbody>
							</table>
						</span>
						<br class="fclear">
						<br class="fclear">
						
						<input type="hidden" value="<?php echo $nonce;?>" name="nonce"> 
						<input type="hidden" name="ajax_target" value="#main_content .col1"/> 
						<input type="hidden" name="is_ajax" value="1"/> 
						<input type="hidden" value="<?php echo $data_jadwal_bintek['jadwal_bintek_id'];?>" name="jadwal_bintek_id"> 
						<span class="value_view">
							&nbsp;&nbsp;<input type="submit" value="Simpan">
						</span>

						</fieldset>
					</form>
				<?php echo $response; ?>
