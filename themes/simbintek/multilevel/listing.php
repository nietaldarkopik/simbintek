<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


			<div class="pad_left1"><h2 class="pad_bot1"><?php echo $page_title;?></h2></div>
			<?php
				echo $response;
				echo $this->data->create_form_filter();
			?>
			<div id="top-panel">
				<div id="panel">
					<?php
						echo $this->data->show_panel_allowed("","",array("add"));
					?>
				</div>
			</div>	
			<div id="sortable" class="fclear">
				<form method="post" action="" form="sort_menu" id="sort_menu">
					<?php
						echo $the_menus;
					?>
					<input type="hidden" name="do_save_menu" value="do_save_menu"/>
				</form>
			</div>
			<div id="output">
			
			</div>

<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/jquery.mjs.nestedSortable.js" ></script>
<script type="text/javascript">// Return a helper with preserved width of cells
	function sort_menu()
	{
		var inp_menu = jQuery("#sortable li");
		if(jQuery(inp_menu).length > 0)
		{
			for(var i = 0; i < jQuery(inp_menu).length; i++)
			{
				var parent_id = jQuery(inp_menu).eq(i).parents("li");
				if(parent_id.length > 0)
				{
					parent_id = jQuery(inp_menu).eq(i).parents("li").attr("id");
					parent_id = parent_id.replace("the_menu-","");
				}else{
					parent_id = 0;
				}
				jQuery(inp_menu).eq(i).find("input").val(parent_id);
			}
			
			jQuery.ajax({
				type: "post",
				url: base_url + 'menu/listing',
				data: jQuery("form#sort_menu").serialize(),
				success: function(msg){
					//jQuery("#output").html(msg);
				}
			});
		}
	}
	
	
	jQuery("span.showhide").on("click",function(){
		if(jQuery(this).hasClass("ui-icon-triangle-1-n"))
		{
			jQuery(this).removeClass("ui-icon-triangle-1-n");
			jQuery(this).addClass("ui-icon-triangle-1-s");
			jQuery(this).parent().children("ul").show();
		}else if(jQuery(this).hasClass("ui-icon-triangle-1-s")){
			jQuery(this).removeClass("ui-icon-triangle-1-s");
			jQuery(this).addClass("ui-icon-triangle-1-n");
			if(jQuery(this).parent().children("ul").find("li").length > 0)
			{
				//jQuery(this).parent().children("ul").hide();
			}
		}
	});

	jQuery(document).on("do_sortable","#sortable ul",function(){
		jQuery(this).sortable({
			connectWith: "#sortable ul",
			placeholder: "ui-state-highlight",
			tolerance: "intersect",
			items:  '> li',
			axis: 'y',
			revert: true,
			opacity: 0.4,
			update: function(event, ui) 
			{
				//var index = ui.placeholder.index();
				//alert(ui.item.html());
				sort_menu();
			}
		}).disableSelection();
	});
	
	jQuery("#sortable ul").trigger("do_sortable");

	
	jQuery(document).ready(function(){
		/*
		if(jQuery(".col2").length == 0 || !jQuery(".col2").html())
		{
			jQuery(".col1").width("100%");
		}
		*/
		jQuery("span.showhide").each(function(k,v){
			if(jQuery(v).parent().children("ul").find("li").length > 1)
			{
				//jQuery(v).removeClass("ui-icon-triangle-1-n");
				//jQuery(v).removeClass("ui-icon-triangle-1-s");
				jQuery(v).addClass("ui-icon-triangle-1-s");
				//jQuery(v).parent().children("ul").hide();
			}else{
				jQuery(v).removeClass("ui-icon-triangle-1-n");
				jQuery(v).removeClass("ui-icon-triangle-1-s");
				jQuery(v).addClass("ui-icon-blank");
			}
		});
	});
</script>
