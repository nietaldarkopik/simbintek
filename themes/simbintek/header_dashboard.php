<!DOCTYPE html>
<html lang="en">
<head>
<title>SIM Bintek - Batamsan II</title>
<meta charset="utf-8">
<link rel="stylesheet" href="<?php echo current_theme_url();?>static/js/jquery/css/ui-darkblue/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" href="<?php echo current_theme_url(); ?>static/css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo current_theme_url(); ?>static/css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo current_theme_url(); ?>static/css/style.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo current_theme_url(); ?>static/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo current_theme_url();?>static/css/ddsmoothmenu.css" type="text/css" />

<script type="text/javascript">
   var base_url = "<?php echo base_url();?>";
   var current_url = "<?php echo current_url();?>";
</script>

<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/jquery-1.9.1.js" ></script>
<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/cufon-yui.js"></script>
<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/cufon-replace.js"></script>
<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/Molengo_400.font.js"></script>
<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/Expletus_Sans_400.font.js"></script>
<script type="text/javascript" src="<?php echo current_theme_url() . 'static/js/jquery/validate/lib/jquery.form.js';?>"></script>
<script type="text/javascript" src="<?php echo current_theme_url() . 'static/js/jquery/validate/dist/jquery.validate.js';?>"></script>
<script type="text/javascript" src="<?php echo current_theme_url() . 'static/js/jquery/validate/dist/additional-methods.js';?>"></script>
<script type="text/javascript" src="<?php echo current_theme_url() . 'static/js/jquery.form.js';?>"></script>
<script type="text/javascript" src="<?php echo current_theme_url() . 'static/js/jquery/jquery_cookie/cookie.js';?>"></script>
<script type="text/javascript" src="<?php echo current_theme_url();?>static/js/jquery/js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="<?php echo current_theme_url();?>static/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo current_theme_url();?>static/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo current_theme_url() . 'static/js/blocks.js';?>"></script>
<script type="text/javascript" src="<?php echo current_theme_url() . 'static/js/data.js';?>"></script>	
<script type="text/javascript" src="<?php echo current_theme_url();?>static/js/ddsmoothmenu.js"></script>
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/html5.js"></script>
<style type="text/css">.bg, .box2{behavior:url("<?php echo current_theme_url(); ?>static/js/PIE.htc");}</style>
<![endif]-->
</head>
<body id="page1">