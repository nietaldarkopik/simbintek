<!DOCTYPE html>
<html lang="en">
<head>
<title>SIM Bintek - Batamsan II</title>
<meta charset="utf-8">
<link rel="stylesheet" href="<?php echo current_theme_url(); ?>static/css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo current_theme_url(); ?>static/css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo current_theme_url(); ?>static/css/style.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo current_theme_url(); ?>static/css/theme.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo current_theme_url();?>static/js/jquery/css/ui-darkblue/jquery-ui-1.10.3.custom.css" />

<script type="text/javascript">
   var base_url = "<?php echo base_url();?>";
   var current_url = "<?php echo current_url();?>";
</script>

<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/jquery-1.9.1.js" ></script>
<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/cufon-yui.js"></script>
<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/cufon-replace.js"></script>
<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/Molengo_400.font.js"></script>
<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/Expletus_Sans_400.font.js"></script>
<script type="text/javascript" src="<?php echo current_theme_url() . 'static/js/jquery/jquery_cookie/cookie.js';?>"></script>
<script src="<?php echo current_theme_url();?>static/js/jquery/js/jquery-ui-1.10.3.custom.min.js"></script>
<style>
	.right_menu > .ui_menu {width:25%; }
	.ui-menu-item {min-width:200px;}
</style>
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo current_theme_url(); ?>static/js/html5.js"></script>
<style type="text/css">.bg, .box2{behavior:url("<?php echo current_theme_url(); ?>static/js/PIE.htc");}</style>
<![endif]-->
</head>

<body id="page1">
<div class="body1">
  <div class="main">
    <!-- header -->
    <header>
      <div class="wrapper">
        <nav>
          <div id="top">&nbsp;</div>
        </nav>
      </div>
      <div class="wrapper">
        <h1><a href="index.html" id="logo">SIM BINTEK</a></h1>
      </div>
      <div id="slogan" style="font-size:30px;">Balai Teknik Air Minum Dan Sanitasi<span style="font-size:30px;">Wilayah II Surabaya</span> </div>
      <div class="banners">