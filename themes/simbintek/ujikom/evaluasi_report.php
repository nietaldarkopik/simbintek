<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


				<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
				<div id="top-panel">
					<div id="panel">
						<?php
							echo $this->data->show_panel_allowed("","",array("listing"));
						?>
					</div>
				</div>
				<br class="fclear"/><br/>
				<form method="post" id="form_penilaian" name="form_penilaian" action="">
					<table border="1" width="100%" class="table_report">
						<thead>
							<tr>
								<th width="30%">Unit Kompetensi</th>
								<th width="50%">Bukti (paling relevan) : <br/>Rincian Pendidikan/Pelatihan, Pengalaman Kerja, Pengalaman Hidup dan kode bukti</th>
								<th>Kesesuaian bukti</th>
								<th>Asesmen Lanjut</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$total_penilaian = 0;
						if(isset($kompetensi) and is_array($kompetensi) and count($kompetensi) > 0)
						{
							$number = 1;
							foreach($kompetensi as $index => $kom)
							{
								$number2 = 1;
								$this->db->where(array("kompetensi_id" => $kom['kode_kompetensi']));
								$qmaterial = $this->db->get("ujikom_material_support");
								$material = $qmaterial->result_array();
								if(is_array($material) and count($material) > 0)
								{
									foreach($material as $idx => $mat)
									{
										$this->db->where(array("ujikom_id" => $ujikom['ujikom_id'],"material_id" => $mat['ujikom_material']));
										$q_curr_nilai_material = $this->db->get("ujikom_bukti_pendukung");
										$curr_nilai_material = $q_curr_nilai_material->row_array();
										$curr_assesment = (isset($curr_nilai_material['assesment']))?$curr_nilai_material['assesment']:"";
										$curr_nilai = (isset($curr_nilai_material['nilai']))?$curr_nilai_material['nilai']:"";
										$bukti_pendukung_id = (isset($curr_nilai_material['bukti_pendukung_id']))?$curr_nilai_material['bukti_pendukung_id']:"";
										$checked = (!empty($bukti_pendukung_id))?' checked="checked" ':"";
										
										$total_penilaian += $curr_nilai;
						?>
										<tr>
											<?php
											if($idx == 0)
											{
											?>
											<td class="td_kompetensi" rowspan="<?php echo $qmaterial->num_rows();?>"><?php echo $kom['judul_kompetensi'];?></td>
											<?php
											}
											?>
											<td>
												<input type="checkbox" name="material[<?php echo $mat['ujikom_material'];?>]" <?php echo $checked?>> 
												<strong><?php echo $number . '.' . $number2 . '.'; ?></strong>
												<?php echo $mat['judul_material'];?>
											</td>
											<td>
												<input name="nilai[<?php echo $mat['ujikom_material'];?>]" style="width:100%;" value="<?php echo $curr_nilai;?>">
												<?php
												/*
												<select name="nilai[<?php echo $mat['ujikom_material'];?>]" style="width:100%;">
													<option value=""></option>
													<?php
													$nilai = $this->master->get_nilai();
													if(is_array($nilai) and count($nilai) > 0)
													{
														foreach($nilai as $index => $n)
														{
															$selected = ($curr_nilai == $n['nilai'])?' selected="selected" ':'';
															echo '<option value="'.$n['nilai'].'" '.$selected.'>'.$n['nilai'].'</option>';
														}
													}
													?>
												</select>
												*/
												?>
											</td>
											<td>
												<select name="assesment[<?php echo $mat['ujikom_material'];?>]" style="width:100%;">
													<option value=""></option>
													<?php
													$assesment = $this->master->get_assesment();
													if(is_array($assesment) and count($assesment) > 0)
													{
														foreach($assesment as $index => $n)
														{
															$selected = ($curr_assesment == $n['name'])?' selected="selected" ':'';
															echo '<option value="'.$n['name'].'" '.$selected.'>'.$n['label'].'</option>';
														}
													}
													?>
												</select>
											</td>
										</tr>
						<?php
										$number2++;
									}
								}
								$number++;
							}
						}
						?>
						</tbody>
					</table>
					<table border="1" width="100%" class="table_report">
						<thead>
							<tr>
								<th colspan="2" width="80%"><h4>Total</h4></th>
								<td class="total_penilaian"><h4><?php echo $total_penilaian;?></h4></td>
							</tr>
						</thead>
					</table>
					<input type="hidden" name="ujikom_id" value="<?php echo (isset($ujikom['ujikom_id']))?$ujikom['ujikom_id']:"";?>">
					<input type="hidden" name="peserta_id" value="<?php echo (isset($ujikom['peserta_id']))?$ujikom['peserta_id']:"";?>">
					<input type="hidden" name="accessor_id" value="<?php echo (isset($ujikom['accessor_id']))?$ujikom['accessor_id']:"";?>">
					<input type="hidden" name="jenis_ujikom" value="<?php echo (isset($ujikom['jenis_ujikom']))?$ujikom['jenis_ujikom']:"";?>">
					
					<input type="hidden" name="is_ajax" value="1">
					<input type="hidden" name="ajax_target" value="#main_content .col1">
					<input type="submit" name="do_save_nilai" value="Simpan"/>
				</form>
				<?php
					echo $response;
				?>
