<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

			<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
			<?php
				echo $response;
				echo $this->data->create_form_filter();
			?>
			<div id="top-panel">
				<div id="panel">
					<?php
						echo $this->data->show_panel_allowed("","",array("add"));
					?>
					<?php
						echo $this->data->show_bulk_allowed("","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array("delete")));
					?>
				</div>
			</div>
			<?php
				echo $this->data->create_listing();
				$paging_config = (isset($paging_config))?$paging_config:array();
				echo $this->data->create_pagination($paging_config);
			?>

