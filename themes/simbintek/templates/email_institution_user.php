<html lang="en" class="cufon-active cufon-ready">
<head>
<title>SIM Bintek - Batamsan II</title>
<meta charset="utf-8">
<link media="all" type="text/css" href="<?php echo base_url();?>themes/design2/static/css/reset.css" rel="stylesheet">
<link media="all" type="text/css" href="<?php echo base_url();?>themes/design2/static/css/layout.css" rel="stylesheet">
<link media="all" type="text/css" href="<?php echo base_url();?>themes/design2/static/css/style.css" rel="stylesheet">
<link media="all" type="text/css" href="<?php echo base_url();?>themes/design2/static/css/theme.css" rel="stylesheet">

</head>
<body id="page2">
	<div class="body2">
		<div class="main">
			<section id="content" style="display: block;">
				<div class="box1">
					<div class="wrapper">
						Data lembaga telah kami terima. Berikut ini user akses anda :<br/>
						Username : <?php echo $username;?><br/>
						Password : <?php echo $password;?><br/><br/>
						Silahkan login di <a href="<?php echo base_url();?>">sini</a>.
					</div>
				</div>
			</section>
			<footer>
			  <div class="wrapper">
				<div class="pad1">
				  <div class="pad_left1">
					
					<div class="wrapper">
					  <article class="call"> <span class="call1">Copyright &copy; 2013 SIM Bintek</span><br><span class="call2">Balai Teknik Air Minum Dan Sanitasi Wilayah II</span></article>
					  <article class="col_4 pad_left2">
						Direktorat Jenderal Cipta Karya<br>Kementrian Pekerjaan Umum          
					  </article>
					</div>
				  </div>
				</div>
			  </div>
			</footer>
		</div>
	</div>
</body>
</html>