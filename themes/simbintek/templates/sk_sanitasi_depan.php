<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$background = (isset($data['data']['background']) and !empty($data['data']['background']))?$data['data']['background']:'';
	$background = (empty($background))?'none' : 'url("' . base_url() . 'uploads/media/sertifikat/' . $background . '") no-repeat ';
?>

			<style>
				table {
					color: #003300;
					font-family: helvetica;
					font-size: 8pt;
					background-color: transparent;
					border: 2px #000000 solid;
				}
				td {
					border: 1px #000000 solid;
					background-color: transparent;
				}
				th {
					border: 1px #000000 solid;
					background-color: transparent;
					font-weight: bold;
					text-align: center;
				}

				#certificate_container{
					background: <?php echo $background;?>;
					background-size:100% 100%;
					background-position:center center;
					padding: 40px 0;
				}
				
				table.main_certificate{
					font-family: "Times New Roman";
					background-color: transparent !important;
					margin: 10px auto;
					font-size: 20px;
					line-height: 20px;
					border: 0px;
				}
					table.main_certificate td{
						padding: 5px;
					}
					table.main_certificate .institution_logo{
						text-align: center;
					}
					table.main_certificate .institution{
						font-family: "Times New Roman";
						text-align: center;
						font-size: 28px;
						font-weight: bold;
					}
					table.main_certificate .certificate_type{
						font-family: "Times New Roman";
						text-align: center;
						font-size: 32px;
						font-weight: bold;
					}
					table.main_certificate .certificate_no{
						font-family: arial;
						text-align: center;
					}
					table.main_certificate .give_to{
						font-family: arial;
						text-align: center;
						font-weight: bold;
					}
					table.main_certificate td.certificate_description{
						font-family: "Times New Roman";
						padding: 0 10%;
						line-height: 1.5;
						text-align: justify;
					}
				table.main_data_member{
					font-family: arial;
					background-color: transparent !important;
					font-size: 20px;
					line-height: 1;
					border: 0px;
				}
					table.main_data_member td.value_data{
						font-family: "Monotype Corsiva";
						font-size: 24px;
						white-space : nowrap;
					}
					table.main_data_member td.result_certificate{
						font-family: "Lucida Calligraphy";
						font-weight: bold;
						text-align: center;
						line-height: 1.5;
					}
					table.main_data_member td.label_data{
						white-space : nowrap;
					}
					table.main_data_member td.separator_data{
						width: 5%;
					}
				table.bottom_certificate,table.bottom_certificate td{
					border: 0;
				}
				table.bottom_certificate td.photo_member{
					vertical-align: middle;
				}
				table.bottom_certificate td.photo_member img{
					border: 1px #cccccc solid;
					width: 150px;
					height: 180px;
					padding: 5px;
					text-align: center;
				}
				table.ttd{
					text-align: center;
					border: 0px;
					padding: 0 10%;
					font-size: 18px;
					line-height: 20px;
				}
				table.ttd .ttd_name{
					text-align: center;
					text-decoration: underline;
					font-weight: bold;
					font-family: tahoma;
				}
				table.ttd .ttd_dateplace{
					text-align: center;
					font-family: tahoma;
				}
				table.ttd .ttd_nip{
					text-align: center;
					font-weight: bold;
					font-family: tahoma;
				}
				table.ttd .ttd_jabatan{
					text-align: center;
					font-weight: bold;
					font-family: tahoma;
				}
				.col1 table, .col1 table td{
					border: 0px;
				}
				.mce-edit-focus {
					outline: 1px solid #333;
					position: relative;
					display: block;
				}
			<!--
				@media print
				{
					body,.body1,.body2 {background: transparent !important; margin: 0px;}
					#certificate_container{
						background: <?php $background;?>;
						background-size:100% 100%;
						background-position:center center;
						padding: 30px 0 0;
						margin: 0;
					}		
					.col1,.box1,.wrapper{
						margin: 0px;
					}
					table.main_certificate{
						font-family: "Times New Roman";
						background-color: transparent !important;
						margin: 0;
						font-size: 14px;
						line-height: 14px;
						border: 0px;
						width: 100%;
					}
						table.main_certificate td{
							padding: 5px;
						}
						table.main_certificate .institution_logo{
							text-align: center;
						}
						table.main_certificate .institution{
							font-family: "Times New Roman";
							text-align: center;
							font-size: 16px;
							font-weight: bold;
						}
						table.main_certificate .certificate_type{
							font-family: "Times New Roman";
							text-align: center;
							font-size: 20px;
							font-weight: bold;
						}
						table.main_certificate .certificate_no{
							font-family: arial;
							text-align: center;
						}
						table.main_certificate .give_to{
							font-family: arial;
							text-align: center;
							font-weight: bold;
						}
						table.main_certificate td.certificate_description{
							font-family: "Times New Roman";
							padding: 0 10%;
							line-height: 1.5;
							text-align: justify;
						}
					table.main_data_member{
						font-family: arial;
						background-color: transparent !important;
						font-size: 14px;
						line-height: 1;
						border: 0px;
					}
						table.main_data_member td.value_data{
							font-family: "Monotype Corsiva";
							font-size: 16px;
							white-space : nowrap;
						}
						table.main_data_member td.result_certificate{
							font-family: "Lucida Calligraphy";
							font-weight: bold;
							text-align: center;
							line-height: 1.5;
						}
						table.main_data_member td.label_data{
							white-space : nowrap;
						}
						table.main_data_member td.separator_data{
							width: 5%;
						}
					table.bottom_certificate{
						border: 0;
						height: 170px;
					}
					table.bottom_certificate,table.bottom_certificate td{
						border: 0;
					}
					table.bottom_certificate td.photo_member{
						vertical-align: top;
					}
					table.bottom_certificate td.photo_member img{
						border: 1px #cccccc solid;
						width: 120px;
						height: 160px;
						padding: 5px;
						text-align: center;
					}
					table.ttd{
						text-align: center;
						border: 0px;
						padding: 0 10%;
						font-size: 14px;
						line-height: 14px;
					}
					table.ttd .ttd_name{
						text-align: center;
						text-decoration: underline;
						font-weight: bold;
						font-family: tahoma;
					}
					table.ttd .ttd_dateplace{
						text-align: center;
						font-family: tahoma;
					}
					table.ttd .ttd_nip{
						text-align: center;
						font-weight: bold;
						font-family: tahoma;
					}
					table.ttd .ttd_jabatan{
						text-align: center;
						font-weight: bold;
						font-family: tahoma;
					}
					.col1 table, .col1 table td{
						border: 0px;
					}
					.text_editor{
						padding-right: 0px;
						background: none;
					}
					@page { 
						size:landscape;
						margin: 1mm !important;
					}
			}
			-->
			</style>

			<div class="pad_left1 no-print"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
			<div id="top-panel">
				<div id="panel">
					<?php
						echo $this->data->show_panel_allowed("","",array("listing"));
						if(!isset($data['edit_mode']) or $data['edit_mode'] == true)
						{
					?>
					<ul class="fright no-print">
						<li>
							<form method="post" name="change_background" class="no_ajax" id="change_background_form" action="<?php echo base_url();?>setting_certificate/edit/<?php echo $data['id'];?>">
								<input type="file" name="background" class="change_background" style="display:none;"/>
							</form>
							<a class="edit do_change_background" href="javascript:void(0);" style="padding-left:20px;">Ubah Background</a>
						</li>
					</ul>
					<?php
						}
					?>
				</div>
			</div>
			<br class="clear no-print"/>			
			<br class="clear no-print"/>
			<div id="certificate_container">
				<table border="0" cellpadding="0" cellspacing="0" class="main_certificate">
					<tr>
						<td class="institution_logo"><img src="<?php echo base_url();?>uploads/media/sertifikat/bintek_sertifikat_logo.jpg"/></td>
					</tr>
					<tr>
						<td class="institution">{%lembaga%}</td>
					</tr>
					<tr>
						<td class="certificate_type">{%jenis_surat%}</td>
					</tr>
					<tr>
						<td class="certificate_no">Nomor : {$nomor_sertifikat$}</td>
					</tr>
					<tr>
						<td class="give_to">Diberikan kepada :</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center">
							<table border="0" cellpadding="0" cellspacing="0" class="main_data_member">
								<tr>
									<td class="label_data">Nama</td>
									<td class="separator_data">:</td>
									<td class="value_data">{$nama_peserta$}</td>
								</tr>
								<tr>
									<td class="label_data">NIK / NRP / NPP</td>
									<td class="separator_data">:</td>
									<td class="value_data">{$nik_peserta$}</td>
								</tr>
								<tr>
									<td class="label_data">Tempat dan Tanggal Lahir</td>
									<td class="separator_data">:</td>
									<td class="value_data">{$tempat_lahir_peserta$} / {$tanggal_lahir_peserta$}</td>
								</tr>
								<tr>
									<td class="label_data">Pangkat / Golongan</td>
									<td class="separator_data">:</td>
									<td class="value_data">{$pangkat_peserta$} / {$golongan_peserta$}</td>
								</tr>
								<tr>
									<td class="label_data">Jabatan</td>
									<td class="separator_data">:</td>
									<td class="value_data">{$jabatan_peserta$}</td>
								</tr>
								<tr>
									<td class="label_data">Instansi</td>
									<td class="separator_data">:</td>
									<td class="value_data">{$instansi_peserta$}</td>
								</tr>
								<tr>
									<td colspan="3" class="result_certificate">{$hasil_sertifikasi$}</td>
								</tr>
								<tr>
									<td class="label_data">Kualifikasi</td>
									<td class="separator_data">:</td>
									<td class="value_data">{$kualifikasi_sertifikasi$}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="certificate_description">
							{%deskripsi_sertifikat%}
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" class="bottom_certificate">
								<tr>
									<td>&nbsp;</td>
									<td width="30%"  align="right" class="photo_member">
										<img src="<?php echo base_url();?>uploads/media/<?php echo (isset($this->data_sertifikat['photo']) and !empty($this->data_sertifikat['photo']))?'bintek_member/'.$this->data_sertifikat['photo']:'sertifikat/blank.gif';?>" class="photo_member_img"/>
									</td>
									<td  width="50%" align="right" class="container_ttd">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" class="ttd">
											<tr>
												<td class="ttd_dateplace">{%ttd_tempat_sertifikat%}, {$ttd_tanggal_sertifikat$}</td>
											</tr>
											<tr>
												<td class="ttd_jabatan">
													{%ttd_jabatan%}
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td class="ttd_name">{%ttd_nama%}</td>
											</tr>
											<tr>
												<td class="ttd_nip">NIP. {%ttd_nip%}</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div class="no-print dumper" style="display:none;"></div>
				<script type="text/javascript">
					certificate_id = '<?php echo $data['id'];?>';
					<?php
					if(isset($data['data']) and is_array($data['data']) and count($data['data']) > 0)
					{
					?>
						data_json = <?php echo json_encode($data['data']);?>;
					<?php
					}
					?>
				</script>
			</div>

