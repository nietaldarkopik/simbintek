<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

    </section>
    <!-- footer -->
  	<div class="wrapper footer_container">
    	<article class="copyleft"><span class="call1">Copyright &copy; 2013 SIMBintek.</span>&nbsp;<span class="call2">Balai Teknik Air Minum Dan Sanitasi Wilayah II</span></article>
        <article class="copyright"><span class="call1"><a href="http://ciptakarya.pu.go.id/" target="_blank">Direktorat Jenderal Cipta Karya</a></span>&nbsp;-&nbsp;<span class="call2"><a href="http://www.pu.go.id/" target="_blank">Kementrian Pekerjaan Umum</a></span></article>
    </div>
    <div class="clear"></div>
    <!-- / footer -->
  </div>
</div>
<script type="text/javascript">Cufon.now();</script>

</body>
</html>
