<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="box">
	<h3>Forum</h3>
	<?php
		echo $response;
		echo $this->data->create_form_filter();
	?>
	<div id="top-panel">
		<div id="panel">
			<?php
				echo $this->data->show_panel_allowed("","",array("add"));
			?>
		</div>
	</div>
	<?php
		$paging_config = (isset($paging_config))?$paging_config:array();
		echo $this->data->create_listing($paging_config);
		echo $this->data->create_pagination($paging_config);
	?>
</div>
