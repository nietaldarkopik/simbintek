<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
		<h3><a href="#" class="user">Login</a></h3>          				
		<form id="form-login" action="<?php echo base_url().'dashboard/do_login';?>" method="post">
			<label for="user_name">Username : </label>
			<input name="user_name" type="text"/>
			<label for="password">password : </label>
			<input name="password" type="password"/>
			<br />
			<div align="center">
				<input type="hidden" value=".login_form" name="ajax_target"/> 
				<input type="submit" value="Login" />
			</div>
			<div align="left">
					<?php echo $error;?>
			</div>
		</form>
		<script type="text/javascript">
			load_all('load_login_form');
		</script>
