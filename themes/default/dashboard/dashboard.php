<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
                <div id="box">
									<h3>Selamat Datang</h3>
									<p align="justify">
										Selamat datang di situs SIM-Diklat Online PPPG Teknologi Bandung.
										Sistem ini dipersembahkan untuk anda komunitas pendidikan di lingkungan 
										Direktorat Menengah Kejuruan Republik Indonesia. <br> <br>
										Melalui sistem ini anda dapat menelusuri layanan pendidikan dan latihan 
										yang diselenggarakan oleh PPPG Teknologi Bandung. <br> <br>
										Bagi guru yang berminat mengikuti diklat dapat mendaftarkan 
										diri melalui situs ini atau melalui layanan SMS ke no server PPPG Teknologi Bandung.<br><br> 
										Kami berharap sistem ini dapat menjembatani komunikasi antar  anggota komunitas 
										pendidikan kejuruan pada khususnya dan pendidikan menengah pada umumnya. 
										Melalui fasilitas forum, jajak pendapat, berita dan buku tamu anda dapat 
										menyampaikan pendapat dan bertukar pikiran dengan sesama guru di seluruh Indonesia.<br><br> 
										Semoga situs ini dapat bermanfaat untuk mengembangkan kualitas pendidikan di Indonesia.
									</p>
									<hr>
								</div>
