<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="box1">
	<div class="wrapper">
		<article class="col1">
			<style>
				table {
					color: #003300;
					font-family: helvetica;
					font-size: 8pt;
					background-color: #ffffff;
					border: 2px #000000 solid;
				}
				td {
					border: 1px #000000 solid;
					background-color: #ffffff;
				}
				th {
					border: 1px #000000 solid;
					background-color: #ffffff;
					font-weight: bold;
					text-align: center;
				}
			</style>
			<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
			<?php
				echo $this->data->create_listing_report();
			?>
		</article>
