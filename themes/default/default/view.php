<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="box1">
	<div class="wrapper">
		<article class="col1">
			<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
			<?php 
				echo $this->data->show_panel_allowed("","",array("listing"));
			?>
			<br class="fclear"/><br/>
			<?php
				echo $this->data->create_view();
			?>
		</article>
