<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Users - Admin Template</title>
<link rel="stylesheet" type="text/css" href="<?php echo current_theme_url(); ?>static/css/theme.css" />
<link rel="stylesheet" type="text/css" href="<?php echo current_theme_url(); ?>static/css/style.css" />
<link rel="stylesheet" href="<?php echo current_theme_url();?>static/js/jquery/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
<script>
   document.cookie = '';
   var style = document.cookie.charAt(6);
   style = style.toLowerCase();
   var StyleFile = "theme" + style + ".css";
   document.writeln('<link rel="stylesheet" type="text/css" href="<?php echo current_theme_url(); ?>static/css/' + StyleFile + '">');
   var base_url = "<?php echo base_url();?>";
   var current_url = "<?php echo current_url();?>";
   
</script>
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="<?php echo current_theme_url(); ?>static/css/ie-sucks.css" />
<![endif]-->
<script type="text/javascript" src="<?php echo current_theme_url() . 'static/js/jquery/js/jquery-1.9.1.js';?>"></script>
<script type="text/javascript" src="<?php echo current_theme_url() . 'static/js/blocks.js';?>"></script>
<script type="text/javascript" src="<?php echo current_theme_url() . 'static/js/data.js';?>"></script>
<script src="<?php echo current_theme_url();?>static/js/jquery/js/jquery-ui-1.10.3.custom.min.js"></script>
<style>
	.right_menu > .ui_menu {width:25%; }
	.ui-menu {border-radius : 0 !important; margin-left: 0px !important;}
	.ui-menu-item {min-width:200px;}
	.ui-corner-all{ border-radius : 0 !important;}
</style>
</head>

<body>
