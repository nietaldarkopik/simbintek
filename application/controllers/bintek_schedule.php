<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bintek_schedule extends MY_Controller {

	var $init = array();
	function __construct()
	{
		parent::__construct();
		$this->_bulk_actions();
		$this->hook->add_action('hook_show_bulk_allowed_is_ajax',array($this,'_hook_show_bulk_allowed_is_ajax_set0'));
	}
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_show_panel_allowed_panel_bintek_schedule_report',array($this,'_hook_show_panel_allowed_panel_bintek_schedule_report'));	
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('bintek/listing_bintek_schedule',array('response' => '','page_title' => 'Jadwal Bintek'));
		else
			$this->load->view('dashboard/dashboard');
					
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function latest()
	{
		$this->index();
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'schedule/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title' => 'Jadwal Bintek'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$is_ajax = $this->input->post('is_ajax');
		$do_save_schedule = $this->input->post('do_save_schedule');
		$response = "";
		if($do_save_schedule)
		{
			$hour_from = $this->input->post('hour_from');
			$hour_to = $this->input->post('hour_to');
			$day_no = $this->input->post('day_no');
			$day_date = $this->input->post('day_date');
			$material = $this->input->post('material');
			$instructure_name = $this->input->post('instructure_name');
			$instructure_id = $this->input->post('instructure_id');
			$textarea_acara = $this->input->post('textarea_acara');
			$jadwal_bintek_id = $this->input->post('jadwal_bintek_id');
			
			$hours = array();
			$days = array();
			$the_materials = array();
			if(is_array($hour_from) and count($hour_from) > 0)
			{
				foreach($hour_from as $index => $h)
				{
					$the_hour_to = (isset($hour_to[$index]))?$hour_to[$index]:$h;
					$hours[$index] = array("hour_from" => $h,"hour_to" => $the_hour_to);
				}
			}
			if(is_array($day_no) and count($day_no) > 0)
			{
				foreach($day_no as $index => $d)
				{
					$the_day_date = (isset($day_date[$index]))?$day_date[$index]:$d;
					$days[$index] = array("day_no" => $d,"day_date" => $the_day_date);
				}
			}
			if(is_array($material) and count($material) > 0)
			{
				$this->db->where(array("jadwal_bintek_id" => $jadwal_bintek_id));
				$this->db->delete("jadwal_bintek_acara");
				foreach($material as $day_no => $m1)
				{
					foreach($m1 as $hour_no => $m)
					{
						$the_day = (isset($days[$day_no]))?$days[$day_no]:array();
						$the_hour = (isset($hours[$hour_no]))?$hours[$hour_no]:array();
						$the_instructure_name = (isset($instructure_name[$day_no][$hour_no]))?$instructure_name[$day_no][$hour_no]:"";
						$the_instructure_id = (isset($instructure_id[$day_no][$hour_no]))?$instructure_id[$day_no][$hour_no]:"";
						$the_textarea_acara = (isset($textarea_acara[$day_no][$hour_no]))?$textarea_acara[$day_no][$hour_no]:"";
						
						$the_instructure_name = array("instructure_name" => $the_instructure_name);
						$the_instructure_id = array("instructure_id" => $the_instructure_id);
						$the_textarea_acara = array("textarea_acara" => $the_textarea_acara);
						
						$the_material_tmp = array_merge(array('materi_id' => $m),(array) $the_textarea_acara,(array) $the_day,(array) $the_hour,(array) $the_instructure_name,(array) $the_instructure_id);

						$the_material = array(
																		'jadwal_bintek_id' => $jadwal_bintek_id,
																		'hari_ke' => ((isset($the_material_tmp['day_no']))?$the_material_tmp['day_no']:1),
																		'tanggal' => ((isset($the_material_tmp['day_date']))?$the_material_tmp['day_date']:""),
																		'jam_awal' => ((isset($the_material_tmp['hour_from']))?$the_material_tmp['hour_from']:""),
																		'jam_akhir' => ((isset($the_material_tmp['hour_to']))?$the_material_tmp['hour_to']:""),
																		'acara' => ((isset($the_material_tmp['textarea_acara']))?$the_material_tmp['textarea_acara']:""),
																		'material_id' => $m,
																		'instruktur' => ((isset($the_material_tmp['instructure_id']))?$the_material_tmp['instructure_id']:""),
																		'instructure_name' => ((isset($the_material_tmp['instructure_name']))?$the_material_tmp['instructure_name']:""),
																		'keterangan' => '',
																	);
						
						$this->db->insert("jadwal_bintek_acara",$the_material);
						$the_materials[] = $the_material;
					}
				}
				$response = "<p class='success'>Jadwal Berhasil dirubah</p>";
			}
		}else{
			$response = $this->data->edit("",$this->init['fields']);
		}
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		$is_login = $this->user_access->is_login();
		if($is_login)
		{
			$this->db->where(array('jadwal_bintek_id' => $object_id));
			$q = $this->db->get("jadwal_bintek");
			$detail = $q->row_array();
			$curr_hours = $this->_get_hours($object_id);
			$material_structured = $this->_get_material_structured($object_id);
			
			$this->load->view('bintek/edit_jadwal',array('material_structured' => $material_structured,'curr_hours' => $curr_hours,'response' => '','detail' => $detail,'page_title' => 'Jadwal Bintek'));
		}
		else
			$this->load->view('dashboard/dashboard');
		
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
		{
			$this->db->where(array('jadwal_bintek_id' => $object_id));
			$q = $this->db->get("jadwal_bintek");
			$detail = $q->row_array();
			$curr_hours = $this->_get_hours($object_id);
			$material_structured = $this->_get_material_structured($object_id);
			
			$this->load->view('bintek/view_jadwal',array('material_structured' => $material_structured,'curr_hours' => $curr_hours,'response' => '','detail' => $detail,'page_title' => 'Jadwal Bintek'));
		}
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_bintek_schedule',array('response' => '','page_title' => 'Jadwal Bintek'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function data_member($object_id = ""){
		redirect('/bintek_relation_schedule_member/listing/'. $object_id);
	}
	
	public function data_instruktur($object_id = ""){
		redirect('/bintek_relation_schedule_instructure/listing/'.$object_id);	
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'jadwal_bintek',
						'fields' => array(	
											array(
													'name' => 'kode_bintek',
													'label' => 'Bintek',
													'id' => 'kode_bintek',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'bintek',
													'select' => array('bintek_id AS value','concat(kode_bintek," - ",nama_bintek) AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'bintek_id',
													'rules' => 'required',
													'style' => 'width:200px;'
												),
											array(
													'name' => 'judul_bintek',
													'label' => 'Judul Bintek',
													'id' => 'judul_bintek',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'tempat',
													'label' => 'Tempat',
													'id' => 'tempat',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'tglawal',
													'label' => 'Tanggal Awal',
													'id' => 'tglawal',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'tglakhir',
													'label' => 'Tanggal Akhir',
													'id' => 'tglakhir',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'tglregawal',
													'label' => 'Tanggal Reg. Awal',
													'id' => 'tglregawal',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'tglregakhir',
													'label' => 'Tanggal Reg. Akhir',
													'id' => 'tglregakhir',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'thn',
													'label' => 'Tahun',
													'id' => 'thn',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'deskripsi',
													'label' => 'Deskripsi',
													'id' => 'thn',
													'value' => '',
													'type' => 'textarea',
													'class' => 'input_tinymce_simple2',
													'use_search' => false,
													'use_listing' => false,
													'rules' => ''
												)
										),
									'list_style' => 'style="width:250px;"',
									'bulk_options' => array('download_excel','download_pdf','printing'),
									'primary_key' => 'jadwal_bintek_id'
					);
		$this->init = $init;
	}
	

	public function report($object_id = "")
	{
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		#$this->hook->add_action('hook_show_bulk_allowed_form_target',array($this,'_hook_show_bulk_allowed_form_target'));
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		$query = $this->data->get_query();
		$is_login = $this->user_access->is_login();
		if($is_login)
		{
			if(!empty($object_id))
			{
				$this->db->where(array('jadwal_bintek_id' => $object_id));
				$q = $this->db->get("jadwal_bintek");
				$detail = $q->row_array();
				$curr_hours = $this->_get_hours($object_id);
				$material_structured = $this->_get_material_structured($object_id);
				
				$output = $this->load->view('bintek/report_jadwal',array('query' => $query,'material_structured' => $material_structured,'curr_hours' => $curr_hours,'response' => '','detail' => $detail,'page_title' => 'Jadwal Bintek'),true);

				#$output = str_replace("<br/>","",$output);
				#$output = str_replace("<br>","",$output);
				$output = str_replace("\n","",$output);
				$output = str_replace("\r","",$output);
				$output = str_replace("\t","",$output);
				
				$this->report->download_excel($output,date("ymdhis"));
			}
		}
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}

		if($is_login)			
			$this->load->view('default/report',array('response' => '', 'page_title' => 'Jadwal Bintek'));
		else
			$this->load->view('dashboard/dashboard');
		
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function download_excel()
	{
		$this->load->library("report");
		$this->_config();
		
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		$is_ajax = $this->input->post('is_ajax');
			$this->load->view('header');
			$this->load->view('topbar');
		if($is_login)			
			$output = $this->load->view('default/report',array('response' => 'Laporan Jadwal Bintek'),true);
		
		$this->report->download_excel($output,date("ymdhis"));
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		exit;
	}
		
	public function download_pdf()
	{
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		if($is_login)			
			$output = $this->load->view('default/report',array('response' => '','page_title' => 'Laporan Jadwal Bintek'),true);
		
		$this->report->download_pdf($output,date("ymdhis"));
		exit;
	}
		
	public function printing()
	{
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_show_bulk_allowed_form_target',array($this,'_hook_show_bulk_allowed_form_target'));
		$object_id = $this->data->primary_key_value;
		$is_ajax = $this->input->post('is_ajax');
		
		$query = $this->data->get_query();
		$is_login = $this->user_access->is_login();
		if($is_login)
		{
			if(!empty($object_id))
			{
				$this->db->where(array('jadwal_bintek_id' => $object_id));
				$q = $this->db->get("jadwal_bintek");
				$detail = $q->row_array();
				$curr_hours = $this->_get_hours($object_id);
				$material_structured = $this->_get_material_structured($object_id);
				
				$output = $this->load->view('bintek/report_jadwal',array('query' => $query,'material_structured' => $material_structured,'curr_hours' => $curr_hours,'response' => '','detail' => $detail,'page_title' => 'Jadwal Bintek'),true);

				#$output = str_replace("<br/>","",$output);
				#$output = str_replace("<br>","",$output);
				$output = str_replace("\n","",$output);
				$output = str_replace("\r","",$output);
				$output = str_replace("\t","",$output);
				
				$this->report->download_excel($output,date("ymdhis"));
			}
		}
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}

		if($is_login)			
			$this->load->view('default/report',array('response' => '', 'page_title' => 'Jadwal Bintek'));
		else
			$this->load->view('dashboard/dashboard');
		
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
		echo '<script type="text/javascript">window.print();</script>';
	}

	function _bulk_actions()
	{
		$action = $this->input->post("bulk_action");
		if(!empty($action))
		{
			$action;
		}
	}
		
	function _hook_show_bulk_allowed_is_ajax_set0($param = "")
	{
		return 0;
	}
		
	function _hook_show_bulk_allowed_form_target($param = "")
	{
		return "_blank";
	}
		
	function _hook_show_bulk_allowed_is_ajax_set1($param = "")
	{
		return 1;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}

	function _hook_report(){
		$init = $this->init;
		if(is_array($init['fields'])){
			foreach($init['fields'] as $idx => $field){
				if($field['name'] != 'foto') 
					$init['fields'][$idx]['use_listing'] = true;
			}
		}
		
		return $init;
	}

	function _hook_show_panel_allowed_panel_bintek_schedule_report($output=""){
		$output = str_replace("<a ","<a target='_blank' ",$output);
		$output = str_replace("a_ajax","",$output);
		return $output;
	}
	
	function _get_hour_per_day($field = "",$bintek_id = "",$day_no = "",$hour_from = "",$hour_to = "")
	{
		$this->db->select($field);
		$this->db->where(array('jadwal_bintek_id' => $bintek_id,'hari_ke' => $day_no,'jam_awal' => $hour_from,'jam_akhir' => $hour_to));
		$q = $this->db->get("jadwal_bintek_acara");
		$data = $q->row_array();
		return (isset($data[$field]))?$data[$field]:"";
	}
	
	function _get_material_structured($bintek_id = "")
	{
		$this->db->order_by('jadwal_acara_id','ASC');
		$this->db->where(array('jadwal_bintek_id' => $bintek_id));
		$q = $this->db->get("jadwal_bintek_acara");
		$data = $q->result_array();
		$output = array();
		if(is_array($data) and count($data) > 0)
		{
			foreach($data as $index => $dt)
			{
				$output[$dt['hari_ke']][substr($dt['jam_awal'],0,5).'-'.substr($dt['jam_akhir'],0,5)] = $dt;
			}
		}
		return $output;
	}
	
	function _get_hours($bintek_id = "")
	{
		$q = $this->db->query("SELECT * FROM jadwal_bintek_acara WHERE jadwal_bintek_id = '".$bintek_id."' GROUP BY jam_awal,jam_akhir ORDER BY jam_awal,jam_akhir ASC");
		$data = $q->result_array();
		return $data;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
