<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bintek_relation_schedule_member extends CI_Controller {

	var $init = array();
	function __construct()
	{
		parent::__construct();
		$this->_bulk_actions();
		$this->hook->add_action('hook_show_bulk_allowed_is_ajax',array($this,'_hook_show_bulk_allowed_is_ajax_set0'));
	}
	
	public function index($object_id = "")
	{
		$this->listing($object_id);
	}
	
	public function delete($relation_id = "", $jadwal_bintek_id = "")
	{
		
		if($relation_id == ''){
			$this->listing($jadwal_bintek_id);
			die();
		}
		
		$this->db->delete('relation_bintek_member', array('relation_id' => $relation_id));		
		$response = "<p class='success'>Data berhasil dihapus</p>";
		$this->listing($jadwal_bintek_id);
		
	}	
	
	/*
	 * ganti status pendataran dari sudah ke belum
	*/
	public function status_belum($relation_id = "", $jadwal_bintek_id = "")
	{
		
		if($relation_id == ''){
			$this->listing($jadwal_bintek_id);
			die();
		}
		
		$this->db->update(
			'relation_bintek_member', 
			array('status_registrasi' => 'belum disetujui'), 
			array('relation_id' => $relation_id)
		);		
		$response = "<p class='success'>Data berhasil diubah</p>";
		$this->listing($jadwal_bintek_id);
		
	}	
	
	/*
	 * ganti status pendataran dari belum ke sudah
	*/
	public function status_sudah($relation_id = "", $jadwal_bintek_id = "")
	{
		
		if($relation_id == ''){
			$this->listing($jadwal_bintek_id);
			die();
		}
		
		$this->db->update(
			'relation_bintek_member', 
			array('status_registrasi' => 'sudah disetujui'), 
			array('relation_id' => $relation_id)
		);		
		$response = "<p class='success'>Data berhasil diubah</p>";
		$this->listing($jadwal_bintek_id);
		
	}	
	
	public function add($object_id = "")
	{
		
		if($object_id == "")
			redirect('bintek_schedule/listing');
		
		$data_jadwal_bintek = $this->db->get_where('jadwal_bintek', array('jadwal_bintek_id' => $object_id))->row_array();
		if(empty($data_jadwal_bintek))
			redirect('bintek_schedule/listing');
		
		$response = '';
		$post_nonce = $this->input->post('nonce');
		$post_jadwal_bintek_id = $this->input->post('jadwal_bintek_id');
		$post_member_ids = $this->input->post('member_ids');
		$post_tanggal_pendaftaran = $this->input->post('tanggal_pendaftaran');
		$status_registrasi = $this->input->post('status_registrasi');
		
		$curr_user = $this->user_access->get_user_id();
		$curr_level = $this->user_access->get_level();
		$lembaga_id = "";
		$add_where_institution = "";
		if($curr_level == 3)
		{
			$this->db->where(array("user_id" => $curr_user));
			$q = $this->db->get("lembaga");
			$lembaga = $q->row_array();
			$lembaga_id = (isset($lembaga['lembaga_id']) and !empty($lembaga['lembaga_id']))?$lembaga['lembaga_id']:"-";
			$add_where_institution = " and bintek_peserta.lembaga = '".$lembaga_id."'";
		}
		
		if(!empty($post_nonce) AND !empty($post_jadwal_bintek_id)  AND !empty($post_member_ids) AND $post_nonce == $this->user_access->get_nonce())
		{
			
			#$this->db->where(array('jadwal_bintek_id' => $post_jadwal_bintek_id));
			#$this->db->delete('relation_bintek_member');
			
			foreach($post_member_ids as $idx => $member_id){
				if($member_id == '')
					continue;
					
				$post_data = array(
					'jadwal_bintek_id' => $post_jadwal_bintek_id,
					'member_id' => $member_id,
				);	
				
				$check = $this->db->get_where('relation_bintek_member', $post_data)->num_rows();
				if($check == 0){
					$post_data['tanggal_registrasi'] = $post_tanggal_pendaftaran[$idx];
					$post_data['status_registrasi'] = $status_registrasi[$idx];
					$this->db->insert('relation_bintek_member', $post_data);
				}else{
					$check = $this->db->get_where('relation_bintek_member', $post_data)->row_array();
					$post_data['tanggal_registrasi'] = $post_tanggal_pendaftaran[$idx];
					$post_data['status_registrasi'] = $status_registrasi[$idx];
					$this->db->where($check);
					$this->db->update('relation_bintek_member', $post_data);
				}
					
			}
			$response = "<p class='success'>Data berhasil disimpan</p>";
		
		}
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
		
		if(!empty($lembaga_id))
			$this->db->where('lembaga',$lembaga_id);
		$this->db->order_by('nama');
		$data_member = $this->db->get('bintek_peserta')->result_array();

		$data_peserta_bintek = $this->db->query('SELECT * FROM relation_bintek_member,bintek_peserta WHERE relation_bintek_member.member_id = bintek_peserta.peserta_bintek_id and relation_bintek_member.jadwal_bintek_id = "' . $object_id . '" '.$add_where_institution.' ORDER BY relation_id ASC')->result_array();

		$kode_bintek = $this->master->get_value("bintek","kode_bintek",array("bintek_id" => $data_jadwal_bintek['kode_bintek']));
		$is_login = $this->user_access->is_login();
		if($is_login){	
			$this->load->view(
				'bintek/add_bintek_relation_schedule_member',
				array(
					'response' => $response, 
					'data_jadwal_bintek' => $data_jadwal_bintek, 
					'data_peserta_bintek' => $data_peserta_bintek, 
					'data_member' => $data_member, 
					'nonce' => $this->user_access->get_nonce(), 
					'page_title' => 'Tambah Data Peserta Bintek "'. $kode_bintek .' - ' . $data_jadwal_bintek['judul_bintek'] .'"'
				)
			);
		}else{
			$this->load->view('dashboard/dashboard');
		}
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function listing($object_id = "")
	{
		
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		if($object_id == "")
			redirect('bintek_schedule/listing');
		
		$data_jadwal_bintek = $this->db->get_where('jadwal_bintek', array('jadwal_bintek_id' => $object_id))->row_array();
		if(empty($data_jadwal_bintek))
			redirect('bintek_schedule/listing');
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
		
		$curr_user = $this->user_access->get_user_id();
		$curr_level = $this->user_access->get_level();
		$lembaga_id = "";
		if($curr_level == 3)
		{
			$this->db->where(array("user_id" => $curr_user));
			$q = $this->db->get("lembaga");
			$lembaga = $q->row_array();
			$lembaga_id = (isset($lembaga['lembaga_id']) and !empty($lembaga['lembaga_id']))?$lembaga['lembaga_id']:"-";
		}
		
		//get data related member
		$this->db->select('*');
		$this->db->from('relation_bintek_member');
		$this->db->where('relation_bintek_member.jadwal_bintek_id', $object_id);
		if(!empty($lembaga_id))
			$this->db->where('bintek_peserta.lembaga',$lembaga_id);
		$this->db->order_by('relation_id', 'ASC');
		$this->db->join('bintek_peserta', "relation_bintek_member.member_id = bintek_peserta.peserta_bintek_id");
		$data_member = $data = $this->db->get()->result_array();		
		
		$kode_bintek = $this->master->get_value("bintek","kode_bintek",array("bintek_id" => $data_jadwal_bintek['kode_bintek']));
				
		$is_login = $this->user_access->is_login();
		if($is_login){
			$this->load->view(
				'bintek/listing_bintek_relation_schedule_member',
				array(
					'data_jadwal_bintek' => $data_jadwal_bintek, 
					'data_member' => $data_member, 
					'response' => @$response, 
					'object_id' => $object_id, 
					'page_title' => 'Data Member Bintek "' . $data_jadwal_bintek['judul_bintek'] . ' - ' . $kode_bintek . '"'
				)	
			);	
		}else{
			$this->load->view('dashboard/dashboard');
		}
		
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	
	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/listing',array('response' => '','page_title' => 'Data Peserta Bintek'));
		else
			$this->load->view('dashboard/dashboard');

			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		
	}


	public function download_excel($object_id)
	{
		if(empty($object_id))
			redirect('bintek_schedule/listing');
		
		//get all member that in current schedule
		$query = "
		SELECT * FROM bintek_peserta, relation_bintek_member
		WHERE bintek_peserta.peserta_bintek_id = relation_bintek_member.member_id
		AND relation_bintek_member.jadwal_bintek_id = '{$object_id}'
		ORDER BY bintek_peserta.nama ASC
		";
		$data_jadwal = $this->db->get_where('jadwal_bintek', array('jadwal_bintek_id' => $object_id))->row_array();
		$data_member = $this->db->query($query)->result_array();
		if(!empty($data_member)){
			foreach($data_member as $idx => $member){
				$data_member[$idx]['detail_lembaga'] = $this->db->get_where('lembaga', array('lembaga_id' => $member['lembaga']))->row_array();
			}
		}
		
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		$is_ajax = $this->input->post('is_ajax');
			$this->load->view('header');
			$this->load->view('topbar');
		if($is_login)			
			$output = $this->load->view(
				'bintek/report_member_in_jadwal',
				array(
					'response' => 'Laporan Data Peserta', 
					'data_member' => $data_member, 
					'data_jadwal' => $data_jadwal, 
				),
				true
			);
		
		$this->report->download_excel($output,date("ymdhis"));
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		exit;
	}
		
	public function download_pdf()
	{
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		if($is_login)			
			$output = $this->load->view('default/report',array('response' => '','page_title' => 'Laporan Data Peserta Bintek'),true);
		
		$this->report->download_pdf($output,date("ymdhis"));
		exit;
	}
		
	public function printing()
	{
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('response' => '', 'page_title' => 'Laporan Data Peserta Bintek'));
		
		echo '<script type="text/javascript">window.print();</script>';
	}

	function _bulk_actions()
	{
		$action = $this->input->post("bulk_action");
		if(!empty($action))
		{
			$action;
		}
	}
		
	function _hook_show_bulk_allowed_is_ajax_set0($param = "")
	{
		return 0;
	}
		
	function _hook_show_bulk_allowed_is_ajax_set1($param = "")
	{
		return 1;
	}
		
	public function _config($id_object = "")
	{
		$init = array(	'table' => 'relation_bintek_member',
						'fields' => array(	
											array(
													'name' => 'relation_id',
													'label' => 'relation_id',
													'id' => 'jadwal_id',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'jadwal_bintek_id',
													'label' => 'jadwal_bintek_id',
													'id' => 'jadwal_bintek_id',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'member_id',
													'label' => 'member_id',
													'id' => 'member_id',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
										),
									'bulk_options' => array('download_excel','download_pdf','printing'),
									'primary_key' => 'relation_is'
					);
					
		$this->init = $init;		
		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
