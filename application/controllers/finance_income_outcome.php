<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finance_income_outcome extends MY_Controller {

	var $init = array();
	function __construct()
	{
		parent::__construct();
		$this->_bulk_actions();
		$this->hook->add_action('hook_show_bulk_allowed_is_ajax',array($this,'_hook_show_bulk_allowed_is_ajax_set0'));
	}
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_listing_value_jumlah',array($this,'_hook_create_listing_value_jumlah'));	
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('default/listing',array('response' => '','page_title' => 'Laporan Biaya'));
		else
			$this->load->view('dashboard/dashboard');
			

			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		$response = $this->data->edit("",$this->init['fields']);
		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response,'page_title' => 'Laporan Biaya'));
		else
			$this->load->view('dashboard/dashboard');

			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title' => 'Laporan Biaya'));
		else
			$this->load->view('dashboard/dashboard');

			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '','page_title' => 'Laporan Biaya'));
		else
			$this->load->view('dashboard/dashboard');

			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_listing_value_jumlah',array($this,'_hook_create_listing_value_jumlah'));	

		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/listing',array('response' => '','page_title' => 'Laporan Keuangan'));
		else
			$this->load->view('dashboard/dashboard');

			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('response' => '','page_title' => 'Laporan Biaya'));
		else
			$this->load->view('dashboard/dashboard');

			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function download_excel()
	{
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
		if($is_login)			
			$output = $this->load->view('default/report',array('response' => '','page_title' => 'Laporan Keuangan'),true);
		
		$this->report->download_excel($output,date("ymdhis"));
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		exit;
	}
		
	public function download_pdf()
	{
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		if($is_login)			
			$output = $this->load->view('default/report',array('response' => '','page_title' => 'Laporan Biaya'),true);
		
		$this->report->download_pdf($output,date("ymdhis"));
		exit;
	}
		
	public function printing()
	{
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('response' => '', 'page_title' => 'Laporan Biaya'));
		
		echo '<script type="text/javascript">window.print();</script>';
	}
	
	public function grafic()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->load->library('highcharts_lib');

		# Set the graph using methods chaining
		$this->highcharts_lib
		->set_graph_type('spline')
		->toggle_legend(TRUE)
		->set_legend_option(array("layout" => "vertical",
													"align" => "right",
													"verticalAlign" => "middle",
													"borderWidth" => "1"))
		->display_shadow(TRUE)
		->set_xlabels_options('rotation', -35)
		->set_xlabels_options('align', "right")
		->set_graph_title('Grafik Pemasukan dan Pengeluaran')
		->set_yAxis(array(), 'Jumlah')
		->set_xAxis(array(), 'Tanggal')
		->add_serie('Pemasukan', array(),'',array('enabled' => true,
																							'rotation' => 0,
																							'color' => '#000000',
																							'align' => 'center'
																							)
							  )
		->add_serie('Pengeluaran', array(),'',array('enabled' => true,
																							'rotation' => 0,
																							'color' => '#000000',
																							'align' => 'center'
																							));
		
		#$this->highcharts_lib->js_file = current_theme_url() . 'static/js/highchart/highcharts.src.js';

		#$query = $this->data->query;
		$query_pemasukan = "SELECT status,tanggal,SUM(jumlah) total_jumlah FROM pemasukan where 1 = 1";
		$query_pengeluaran = "SELECT status,tanggal,SUM(jumlah) total_jumlah FROM pengeluaran WHERE 1 = 1";
		$query_both = "SELECT * FROM (".$query_pemasukan." GROUP BY tanggal UNION ".$query_pengeluaran." GROUP BY tanggal) as lm ";
		$filter = $this->data->get_filter("",true);
		
		#$q_pemasukan = $this->db->query($query_pemasukan . $filter . ' GROUP BY tanggal ORDER BY tanggal ASC');
		#$q_pengeluaran = $this->db->query($query_pengeluaran . $filter . ' GROUP BY tanggal ORDER BY tanggal ASC');
		$q_both = $this->db->query($query_both . ' WHERE 1 = 1 '.$filter . ' ORDER BY tanggal ASC');
		#$data_pemasukan = $q_pemasukan->result_array();
		#$data_pengeluaran = $q_pengeluaran->result_array();
		$data_both = $q_both->result_array();
				
		# Set random data for the series
		foreach($data_both as $i => $v)
		{
			if($v['status'] == 'CREDIT')
			{
				$this->highcharts_lib->push_serie_data('Pemasukan', $v['total_jumlah']);
				$this->highcharts_lib->push_xAxis_value($v['tanggal']);
			}else{
				$this->highcharts_lib->push_serie_data('Pengeluaran', $v['total_jumlah']);
				$this->highcharts_lib->push_xAxis_value($v['tanggal']);
			}
		}
		
		# We can use debug to check the content of the options array
		#$this->highcharts_lib->debug();
		$s_graph = $this->highcharts_lib->render("100%","400px");	
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/grafic',array('action' => base_url().'finance_income_outcome/grafic/','data_grafic' => $s_graph,'response' => '', 'page_title' => 'Laporan Keuangan'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{	
		
		$y = array('' => 'Tahun');
		$m = array('' => 'Bulan');
		$d = array('' => 'Tanggal');
		for($i = YEAR_START; $i <= date("Y"); $i++)
		{
			$y[str_pad($i,4,"0",STR_PAD_LEFT)] = str_pad($i,4,"0",STR_PAD_LEFT);
		}
		for($i = 1; $i <= 12; $i++)
		{
			$m[str_pad($i,2,"0",STR_PAD_LEFT)] = str_pad($i,2,"0",STR_PAD_LEFT);
		}
		for($i = 1; $i <= 31; $i++)
		{
			$d[str_pad($i,2,"0",STR_PAD_LEFT)] = str_pad($i,2,"0",STR_PAD_LEFT);
		}
		$options_date = array('y' => $y,'m' => $m,'d' => $d);
		$init = array(	'query' => 'SELECT * FROM(SELECT * FROM pengeluaran l UNION SELECT * FROM pemasukan m) AS lm ' ,
						'fields' => array(
											array(
													'name' => 'status',
													'label' => 'Jenis Mutasi',
													'id' => 'status',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'tanggal',
													'label' => 'Tanggal',
													'id' => 'tanggal',
													'value' => '',
													'type' => 'range_date_selectbox',
													'options_from' => $options_date,
													'options_to' => $options_date,
													'class' => 'input_date',
													'style' => 'width:auto;',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'sarana_id',
													'label' => 'Nama Fasilitas',
													'id' => 'sarana_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => false,
													'use_listing' => true,
													'table'	=> 'fasilitas',
													'select' => array('id AS value','nama_fasilitas AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'id',
													'rules' => 'required'
												),
											array(
													'name' => 'nama_sarana',
													'label' => 'Judul Laporan Biaya',
													'id' => 'nama_sarana',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'keterangan',
													'label' => 'Keterangan',
													'id' => 'keterangan',
													'value' => '',
													'type' => 'textarea',
													'use_search' => false,
													'use_listing' => true,
													'rules' => ''
												),
											array(
													'name' => 'jumlah',
													'label' => 'Jumlah',
													'id' => 'jumlah',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => true,
													'rules' => 'required',
													'list_style' => ' align="right" '
												)
										),
									'sort_order' => 'tanggal ASC',
									'bulk_options' => array('download_excel','download_pdf','printing'),
									'primary_key' => 'id'
					);
		$this->init = $init;
	}
		
	function _bulk_actions()
	{
		$action = $this->input->post("bulk_action");
		if(!empty($action))
		{
			$action;
		}
	}
		
	function _hook_show_bulk_allowed_is_ajax_set0($param = "")
	{
		return 0;
	}
		
	function _hook_show_bulk_allowed_is_ajax_set1($param = "")
	{
		return 1;
	}
		
	function _hook_create_listing_value_jumlah($param = "")
	{
		return 'Rp. ' . number_format($param,2,',','.');
	}
		
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
