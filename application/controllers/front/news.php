<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	var $init = array();
	
	public function index($paging = "")
	{
		$this->listing($paging);
	}
	
	public function detail($url = "")
	{
		$object_id = end(explode("_",$url));
		
		$this->db->where(array("id" => $object_id));
		$qnews = $this->db->get("berita");
		$news = $qnews->row_array();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('front/news/view',array('response' => '','detail' => $news,'page_title' => 'Berita'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
		{
			$this->db->order_by("id","DESC");
			$qnews = $this->db->get("berita");
			$news = $qnews->result_array();
			$this->load->view('front/news/listing',array(	'response' => '',
															'news' => $news,
															'page_title' => 'Berita'));
		}
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
