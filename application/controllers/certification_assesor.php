<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certification_assesor extends MY_Controller {

	var $init = array();
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('default/listing',array('response' => '','page_title' => 'Asesor Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
			
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'instructure/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response,'page_title' => 'Asesor Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title' => 'Asesor Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '','page_title' => 'Asesor Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/listing',array('response' => '','page_title' => 'Asesor Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'sertifikasi_asesor',
						'fields' => array(		/*array(
													'name' => 'jadwal_bintek_id',
													'label' => 'Bintek',
													'id' => 'jadwal_bintek_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'jadwal_bintek',
													'select' => array('jadwal_bintek_id AS value','judul_bintek AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'jadwal_bintek_id',
													'rules' => 'required'
												),*/
												array(
													'name' => 'nama',
													'label' => 'Nama',
													'id' => 'nama',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'nip',
													'label' => 'NIP',
													'id' => 'nip',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'gelar',
													'label' => 'Gelar',
													'id' => 'gelar',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'gol',
													'label' => 'Golongan',
													'id' => 'gol',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
														'name' => 'tempat_lahir_propinsi',
														'label' => 'Propinsi Tempat Lahir',
														'id' => 'tempat_lahir_propinsi',
														'value' => '',
														'type' => 'selectbox',
														'use_search' => true,
														'use_listing' => true,
														'table'	=> 'propinsi',
														'select' => array('kdprop AS value','nama AS label'),
														'options' => array('' => '---- Select Option ----'),
														'primary_key' => 'kdprop',
														'rules' => 'required'
													),
												array(
														'name' => 'tempat_lahir_kota',
														'label' => 'Kota Tempat Lahir',
														'id' => 'tempat_lahir_kota',
														'value' => '',
														'type' => 'selectbox',
														'use_search' => true,
														'use_listing' => true,
														'table'	=> 'kabupaten',
														'select' => array('kdkab AS value','nama AS label'),
														'options' => array('' => '---- Select Option ----'),
														'primary_key' => 'kdkab',
														'js_connect_to' => array(	'id_field_parent' => 'tempat_lahir_propinsi',
																					'table' => 'kabupaten A,propinsi B',
																					'select' => 'A.kdkab AS value,A.nama AS label',
																					'where' => " A.propinsi_id = B.propinsi_id ",
																					'foreign_key' => 'B.kdprop'
																				),
														'rules' => 'required'
													),
												array(
													'name' => 'tanggal_lahir',
													'label' => 'Tanggal Lahir',
													'id' => 'tanggal_lahir',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'jabatan',
													'label' => 'Jabatan',
													'id' => 'jabatan',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'status',
													'label' => 'Status',
													'id' => 'status',
													'value' => '',
													'type' => 'selectbox',
													'options' => array('' => '---- Select Option ----','Widyaiswara Kem. PU' => 'Widyaiswara Kem. PU','Instruktur' => 'Instruktur'),
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'instansi',
													'label' => 'Instansi',
													'id' => 'instansi',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'alamat_instansi',
													'label' => 'Alamat Instansi',
													'id' => 'alamat_instansi',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
														'name' => 'propinsi_instansi',
														'label' => 'Propinsi Instansi',
														'id' => 'propinsi_instansi',
														'value' => '',
														'type' => 'selectbox',
														'use_search' => true,
														'use_listing' => true,
														'table'	=> 'propinsi',
														'select' => array('kdprop AS value','nama AS label'),
														'options' => array('' => '---- Select Option ----'),
														'primary_key' => 'kdprop',
														'rules' => 'required'
													),
												array(
														'name' => 'kota_instansi',
														'label' => 'Kota Instansi',
														'id' => 'kota_instansi',
														'value' => '',
														'type' => 'selectbox',
														'use_search' => true,
														'use_listing' => true,
														'table'	=> 'kabupaten',
														'select' => array('kdkab AS value','nama AS label'),
														'options' => array('' => '---- Select Option ----'),
														'primary_key' => 'kdkab',
														'js_connect_to' => array(	'id_field_parent' => 'propinsi_instansi',
																					'table' => 'kabupaten A,propinsi B',
																					'select' => 'A.kdkab AS value,A.nama AS label',
																					'where' => " A.propinsi_id = B.propinsi_id ",
																					'foreign_key' => 'B.kdprop'
																				),
														'rules' => 'required'
													),
												array(
													'name' => 'telepon_instansi',
													'label' => 'Telp. Instansi',
													'id' => 'telepon_instansi',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'fax_instansi',
													'label' => 'Fax Instansi',
													'id' => 'fax_instansi',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'hp_instansi',
													'label' => 'HP. Instansi',
													'id' => 'hp_instansi',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'alamat_rumah',
													'label' => 'Alamat Rumah',
													'id' => 'alamat_rumah',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
														'name' => 'propinsi_rumah',
														'label' => 'Propinsi Rumah',
														'id' => 'propinsi_rumah',
														'value' => '',
														'type' => 'selectbox',
														'use_search' => true,
														'use_listing' => true,
														'table'	=> 'propinsi',
														'select' => array('kdprop AS value','nama AS label'),
														'options' => array('' => '---- Select Option ----'),
														'primary_key' => 'kdprop',
														'rules' => 'required'
													),
													array(
														'name' => 'kota_rumah',
														'label' => 'Kota Rumah',
														'id' => 'kota_rumah',
														'value' => '',
														'type' => 'selectbox',
														'use_search' => true,
														'use_listing' => true,
														'table'	=> 'kabupaten',
														'select' => array('kdkab AS value','nama AS label'),
														'options' => array('' => '---- Select Option ----'),
														'primary_key' => 'kdkab',
														'js_connect_to' => array(	'id_field_parent' => 'propinsi_rumah',
																					'table' => 'kabupaten A,propinsi B',
																					'select' => 'A.kdkab AS value,A.nama AS label',
																					'where' => " A.propinsi_id = B.propinsi_id ",
																					'foreign_key' => 'B.kdprop'
																				),
														'rules' => 'required'
													),
												array(
													'name' => 'telp_rumah',
													'label' => 'Telp. Rumah',
													'id' => 'telp_rumah',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'fax_rumah',
													'label' => 'Fax. Rumah',
													'id' => 'fax_rumah',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'email',
													'label' => 'Email',
													'id' => 'email',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required|email'
												),
												array(
													'name' => 'foto',
													'label' => 'foto',
													'id' => 'foto',
													'value' => '',
													'type' => 'file',
													'use_search' => false,
													'use_listing' => false,
													'config_upload' => array('upload_path' => getcwd().'/uploads/media/sertifikasi_asesor/','encrypt_name' => true,'allowed_types' =>  'gif|jpg|png'),
													'rules' => '',
													'list_style' => 'width="100" align="center"'
												),
												array(
													'name' => 'pengalaman_kerja',
													'label' => 'Pengalaman Kerja',
													'id' => 'pengalaman_kerja',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'pendidikan_tertinggi',
													'label' => 'PENDIDIKAN FORMAL TERTINGGI :',
													'id' => 'pendidikan_tertinggi',
													'value' => '',
													'type' => 'multiple',
													'multiple_cols' => array(array('title' => 'LEMBAGA PENDIDIKAN','type' => 'text','attr' => 'style="width:200px;"','name' => 'lem_dik'),array('title' => 'KOTA / NEGARA','type' => 'text','attr' => 'style="width:150px;"','name' => 'kot_neg'),array('title' => 'GELAR','type' => 'text','attr' => 'style="width:100px;"','name' => 'gelar'),array('title' => 'SPESIALISASI','type' => 'text','attr' => 'style="width:150px;"','name' => 'special')),
													'use_search' => false,
													'use_listing' => false,
													'rules' => ''
												),
												array(
													'name' => 'pelatihan_teknik',
													'label' => 'KURSUS ATAU PELATIHAN SELAIN PENDIDIKAN FORMAL :',
													'id' => 'pelatihan_teknik',
													'value' => '',
													'type' => 'multiple',
													'multiple_cols' => array(array('title' => 'URAIAN NAMA KURSUS','type' => 'textarea','name' => 'ur_nam_kur'),array('title' => 'KOTA/NEGARA','type' => 'text','name' => 'kot_neg'),array('title' => 'LAMA','type' => 'text','attr' => 'style="width:100px;"','name' => 'lama')),
													'use_search' => false,
													'use_listing' => false,
													'rules' => ''
												),
												array(
													'name' => 'spesialisasi_pelatihan',
													'label' => 'SPESIALISASI MATA PELAJARAN YANG DIAJARKAN :',
													'id' => 'spesialisasi_pelatihan',
													'value' => '',
													'type' => 'multiple',
													'multiple_cols' => array(array('title' => 'MATA PELAJARAN','type' => 'text','name' => 'mat_pel')),
													'use_search' => false,
													'use_listing' => false,
													'rules' => ''
												)
											),
											'primary_key' => 'asesor_id'
					);
		$this->init = $init;
	}
	
	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('response' => '','page_title' => 'Data Asesor'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		if(isset($param['foto']) and empty($param['foto']))
			unset($param['foto']);
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
