<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certification_aspects_evaluation extends MY_Controller {

	var $init = array();
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('certification/listing_certification_type',array('response' => '','page_title' => 'Aspek Penilaian (Unit Kompetensi)'));
		else
			$this->load->view('dashboard/dashboard');
			
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response,'page_title' => 'Aspek Penilaian (Unit Kompetensi)'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title' => 'Aspek Penilaian (Unit Kompetensi)'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '','page_title' => 'Aspek Penilaian (Unit Kompetensi)'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('certification/listing_certification_type',array('response' => '','page_title' => 'Aspek Penilaian (Unit Kompetensi)'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}

	public function element_kompetensi()
	{
		redirect("certification_element_aspects_evaluation");
	}
	
	public function daftar_pertanyaan()
	{
		redirect("certification_aspects_evaluation_question");
	}
	
	public function bukti_pendukung()
	{
		redirect("certification_aspects_evaluation_support");
	}
	
	public function unit_kompetensi()
	{
		redirect("certification_aspects_evaluation");
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'sertifikasi_kompetensi',
						'fields' => array(
											array(
													'name' => 'jenis_sertifikasi',
													'label' => 'Jenis Sertifikasi',
													'id' => 'jenis_sertifikasi',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'sertifikasi_jenis',
													'select' => array('kode_sertifikasi AS value','concat("[",kode_sertifikasi,"] - ",nama_sertifikasi) AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'kode_sertifikasi',
													'rules' => ''
												),
											/*
											array(
													'name' => 'parent_kompetensi',
													'label' => 'Parent Kompetensi',
													'id' => 'parent_kompetensi',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'sertifikasi_kompetensi',
													'select' => array('kode_kompetensi AS value','CONCAT(kode_kompetensi," - ", judul_kompetensi) AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'kompetensi_id',
													'rules' => ''
												),\
											*/
											array(
													'name' => 'kode_kompetensi',
													'label' => 'Kode Kompetensi',
													'id' => 'kode_kompetensi',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'judul_kompetensi',
													'label' => 'Judul Kompetensi',
													'id' => 'judul_kompetensi',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												)
										),
									'where' => ' AND parent_kompetensi = \'\' ',
									'primary_key' => 'kompetensi_id'
					);
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
