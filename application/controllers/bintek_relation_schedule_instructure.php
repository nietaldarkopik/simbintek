<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bintek_relation_schedule_instructure extends CI_Controller {

	var $init = array();
	
	public function index($object_id = "")
	{
		$this->listing($object_id);
	}
	
	public function delete($relation_id = "", $jadwal_bintek_id = "")
	{
		if($relation_id == ''){
			$this->listing($jadwal_bintek_id);
			die();
		}
		
		$this->db->delete('relation_bintek_instructure', array('relation_id' => $relation_id));		
		$response = "<p class='success'>Data berhasil dihapus</p>";
		$this->listing($jadwal_bintek_id);
		
	}	

	public function add($jadwal_bintek_id = "")
	{
		
		if($jadwal_bintek_id == "")
			redirect('bintek_schedule/listing');
		
		$data_jadwal_bintek = $this->db->get_where('jadwal_bintek', array('jadwal_bintek_id' => $jadwal_bintek_id))->row_array();
		if(empty($data_jadwal_bintek))
			redirect('bintek_schedule/listing');
		
		$response = '';
		$post_nonce = $this->input->post('nonce');
		$post_jadwal_bintek_id = $this->input->post('jadwal_bintek_id');
		$post_instructure_ids = $this->input->post('instructure_ids');
		if(!empty($post_nonce) AND !empty($post_jadwal_bintek_id)  AND !empty($post_instructure_ids) AND $post_nonce == $this->user_access->get_nonce()){
			foreach($post_instructure_ids as $idx => $instructure_id){
				if($instructure_id == '')
					continue;
					
				$post_data = array(
					'jadwal_bintek_id' => $post_jadwal_bintek_id,
					'instruktur_id' => $instructure_id,
				);	
				
				$check = $this->db->get_where('relation_bintek_instructure', $post_data)->num_rows();
				if($check == 0)
					$this->db->insert('relation_bintek_instructure', $post_data);
				
			}
			$response = "<p class='success'>Data berhasil disimpan</p>";
		}
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
		
		$this->db->order_by('nama');
		$data_instruktur = $this->db->get('bintek_instruktur')->result_array();
	
		$is_login = $this->user_access->is_login();
		if($is_login){	
			$this->load->view(
				'bintek/add_bintek_relation_schedule_instructure',
				array(
					'response' => $response, 
					'data_jadwal_bintek' => $data_jadwal_bintek, 
					'data_instruktur' => $data_instruktur, 
					'nonce' => $this->user_access->get_nonce(), 
					'page_title' => 'Tambah Data Instruktur Bintek '. $data_jadwal_bintek['judul_bintek'] .'('. $data_jadwal_bintek['kode_bintek'] .') '
				)
			);
		}else{
			$this->load->view('dashboard/dashboard');
		}
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	/*
	 * menampilkan list seluruh instruktur yang 
	 * terkait dengan jadwal bintek terpilih
	*/
	public function listing($jadwal_bintek_id = "")
	{
		if($jadwal_bintek_id == "")
			redirect('bintek_schedule/listing');
		
		//check if jadwal bintek is exists
		$data_jadwal_bintek = $this->db->get_where('jadwal_bintek', array('jadwal_bintek_id' => $jadwal_bintek_id))->row_array();
		if(empty($data_jadwal_bintek))
			redirect('bintek_schedule/listing');
		
		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
		
		//get data related member
		$this->db->select('*');
		$this->db->from('relation_bintek_instructure');
		$this->db->where('relation_bintek_instructure.jadwal_bintek_id', $jadwal_bintek_id);
		$this->db->join('bintek_instruktur', "relation_bintek_instructure.instruktur_id = bintek_instruktur.instruktur_id");
		$data_instruktur = $data = $this->db->get()->result_array();		
		
		$is_login = $this->user_access->is_login();
		if($is_login){
			$this->load->view(
				'bintek/listing_bintek_relation_schedule_instructure',
				array(
					'data_jadwal_bintek' => $data_jadwal_bintek, 
					'data_instruktur' => $data_instruktur, 
					'response' => @$response, 
					'object_id' => $jadwal_bintek_id, 
					'page_title' => 'Data Instruktur Bintek '. $data_jadwal_bintek['judul_bintek'] .'('. $data_jadwal_bintek['kode_bintek'] .') ')
				);	
		}else{
			$this->load->view('dashboard/dashboard');
		}
		
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	
	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/listing',array('response' => '','page_title' => 'Data Instruktur Bintek'));
		else
			$this->load->view('dashboard/dashboard');

			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		
	}


	public function download_excel($object_id)
	{
		if(empty($object_id))
			redirect('bintek_schedule/listing');
		
		//get all member that in current schedule
		$query = "
		SELECT * FROM bintek_instruktur, relation_bintek_instructure
		WHERE bintek_instruktur.instruktur_id = relation_bintek_instructure.instruktur_id
		AND relation_bintek_instructure.jadwal_bintek_id = '{$object_id}'
		ORDER BY bintek_instruktur.nama ASC
		";
		$data_jadwal = $this->db->get_where('jadwal_bintek', array('jadwal_bintek_id' => $object_id))->row_array();
		$data_instruktur = $this->db->query($query)->result_array();
		if(!empty($data_instruktur)){
			foreach($data_instruktur as $idx => $instruktur){
			 $data_instruktur[$idx]['detail_kota_lahir'] = $this->db->get_where('kabupaten', array('kabupaten_id' => $instruktur['tempat_lahir_kota']))->row_array();
			 $data_instruktur[$idx]['detail_propinsi_lahir'] = $this->db->get_where('propinsi', array('propinsi_id' => $instruktur['tempat_lahir_propinsi']))->row_array();

			 $data_instruktur[$idx]['detail_kota_instansi'] = $this->db->get_where('kabupaten', array('kabupaten_id' => $instruktur['kota_instansi']))->row_array();
			 $data_instruktur[$idx]['detail_propinsi_instansi'] = $this->db->get_where('propinsi', array('propinsi_id' => $instruktur['propinsi_instansi']))->row_array();
			
			 $data_instruktur[$idx]['detail_kota_rumah'] = $this->db->get_where('kabupaten', array('kabupaten_id' => $instruktur['kota_rumah']))->row_array();
			 $data_instruktur[$idx]['detail_propinsi_rumah'] = $this->db->get_where('propinsi', array('propinsi_id' => $instruktur['propinsi_rumah']))->row_array();
			
			}
		}
		
		
		$this->load->library("report");
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		$is_ajax = $this->input->post('is_ajax');
			$this->load->view('header');
			$this->load->view('topbar');
		if($is_login)			
			$output = $this->load->view(
				'bintek/report_instruktur_in_jadwal',
				array(
					'response' => 'Laporan Data Instruktur', 
					'data_instruktur' => $data_instruktur, 
					'data_jadwal' => $data_jadwal, 
				),
				true
			);
		
		$this->report->download_excel($output,date("ymdhis"));
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		exit;
	}
		
	public function download_pdf()
	{
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		if($is_login)			
			$output = $this->load->view('default/report',array('response' => '','page_title' => 'Laporan Data Materi'),true);
		
		$this->report->download_pdf($output,date("ymdhis"));
		exit;
	}
		
	public function printing()
	{
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('response' => '', 'page_title' => 'Laporan Data Materi'));
		
		echo '<script type="text/javascript">window.print();</script>';
	}

	function _bulk_actions()
	{
		$action = $this->input->post("bulk_action");
		if(!empty($action))
		{
			$action;
		}
	}
		
	function _hook_show_bulk_allowed_is_ajax_set0($param = "")
	{
		return 0;
	}
		
	function _hook_show_bulk_allowed_is_ajax_set1($param = "")
	{
		return 1;
	}
		
	public function _config($id_object = "")
	{
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
