<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bintek_material extends CI_Controller {

	var $init = array();
	function __construct()
	{
		parent::__construct();
		$this->_bulk_actions();
		$this->hook->add_action('hook_show_bulk_allowed_is_ajax',array($this,'_hook_show_bulk_allowed_is_ajax_set0'));
	}
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('default/listing',array('response' => '', 'page_title' => 'Materi'));
		else
			$this->load->view('dashboard/dashboard');
			
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response, 'page_title' => 'Materi'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response, 'page_title' => 'Materi'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '', 'page_title' => 'Materi'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/listing',array('response' => '', 'page_title' => 'Materi'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'bintek_material',
						'fields' => array(	
											/*
											array(
													'name' => 'jadwal_id',
													'label' => 'Jadwal Bintek',
													'id' => 'jadwal_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'jadwal_bintek',
													'select' => array('jadwal_bintek_id AS value','judul_bintek AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'jadwal_bintek_id',
													'rules' => 'required'
												),*/	
											array(
													'name' => 'bintek_id',
													'label' => 'Bintek',
													'id' => 'bintek_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'bintek',
													'select' => array('bintek_id AS value','concat(kode_bintek," - ",nama_bintek) AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'bintek_id',
													'rules' => 'required',
													'style' => 'width:200px;'
												),
											array(
													'name' => 'instructure_id',
													'label' => 'Instruktur',
													'id' => 'instruktur_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'bintek_instruktur',
													'select' => array('instruktur_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'instruktur_id',
													'js_connect_to' => array(	
															'id_field_parent' => 'jadwal_id',
															'table' => 'bintek_instruktur A,jadwal_bintek B, relation_bintek_instructure C',
															'select' => 'A.instruktur_id AS value,A.nama AS label',
															'where' => " A.instruktur_id = C.instruktur_id AND B.jadwal_bintek_id = C.jadwal_bintek_id ",
															'foreign_key' => 'B.jadwal_bintek_id'
													),
													'rules' => 'required'
												),
											array(
													'name' => 'material_name',
													'label' => 'Nama Materi',
													'id' => 'material_name',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'jam_pelajaran',
													'label' => 'Jam Pelajaran',
													'id' => 'jam_pelajaran',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
										),
									'bulk_options' => array('download_excel','download_pdf','printing'),
									'primary_key' => 'material_id'
					);
		$this->init = $init;
	}
	

	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('response' => '','page_title' => 'Data Materi'));
		else
			$this->load->view('dashboard/dashboard');

			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		
	}


	public function download_excel()
	{
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		$is_ajax = $this->input->post('is_ajax');
			$this->load->view('header');
			$this->load->view('topbar');
		if($is_login)			
			$output = $this->load->view('default/report',array('response' => 'Laporan Data Materi'),true);
		
		$this->report->download_excel($output,date("ymdhis"));
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		exit;
	}
		
	public function download_pdf()
	{
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		if($is_login)			
			$output = $this->load->view('default/report',array('response' => '','page_title' => 'Laporan Data Materi'),true);
		
		$this->report->download_pdf($output,date("ymdhis"));
		exit;
	}
		
	public function printing()
	{
		$this->load->library("report");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('response' => '', 'page_title' => 'Laporan Data Materi'));
		
		echo '<script type="text/javascript">window.print();</script>';
	}

	function _bulk_actions()
	{
		$action = $this->input->post("bulk_action");
		if(!empty($action))
		{
			$action;
		}
	}
		
	function _hook_show_bulk_allowed_is_ajax_set0($param = "")
	{
		return 0;
	}
		
	function _hook_show_bulk_allowed_is_ajax_set1($param = "")
	{
		return 1;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
