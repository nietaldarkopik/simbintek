<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Institution extends MY_Controller {

	var $init = array();
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('default/listing',array('response' => '','page_title'=> 'Lembaga'));
		else
			$this->load->view('dashboard/dashboard');
			
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response,'page_title'=> 'Lembaga'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title'=> 'Lembaga'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '','page_title'=> 'Lembaga'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/listing',array('response' => '','page_title'=> 'Lembaga'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'lembaga',
						'fields' => array(	
											array(
													'name' => 'kdlembaga',
													'label' => 'Kode Lembaga',
													'id' => 'kdlembaga',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required|is_unique[lembaga.kdlembaga]'
												),
											array(
													'name' => 'nmlembaga',
													'label' => 'Nama Lembaga',
													'id' => 'nmlembaga',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'alamat',
													'label' => 'Alamat',
													'id' => 'alamat',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'kdprop',
													'label' => 'Propinsi',
													'id' => 'kdprop',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'propinsi',
													'select' => array('kdprop AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'propinsi_id',
													'rules' => 'required'
												),
											array(
													'name' => 'kdkab',
													'label' => 'Kota/Kabupaten',
													'id' => 'kdkab',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'kabupaten',
													'select' => array('kdkab AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'kabupaten_id',
													'js_connect_to' => array(	'id_field_parent' => 'kdprop',
																				'table' => 'kabupaten A,propinsi B',
																				'select' => 'A.kdkab AS value,A.nama AS label',
																				'where' => " A.propinsi_id = B.propinsi_id ",
																				'foreign_key' => 'B.kdprop'
																			),
													'rules' => 'required'
												),
											array(
													'name' => 'email',
													'label' => 'Email',
													'id' => 'email',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required|valid_email'
												)
										),
									'primary_key' => 'lembaga_id'
					);
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		$this->load->helper('string');
		#$this->load->library('email');
		
		#$username = $param['kdprop'].$param['kdkab'].$param['kdlembaga'];
		$username = $param['email'];
		$password = random_string('alnum',8);
		
		$user = array(
					'user_name' => $username,
					'password' => md5($password),
					'email' => $param['email'],
					'user_level_id' => 3,
					'user_type' => 'lembaga',
					'status' => 'active'
					);
		$this->db->insert("users",$user);
		$param['user_id'] = $this->db->insert_id();
		
		/*
		//$config['protocol'] = 'smtp';
		//$config['smtp_host'] = 'smtp.gmail.com';
		//$config['smtp_user'] = 'nietaldarkopik@gmail.com';
		//$config['smtp_pass'] = 'adaajaada';
		//$config['smtp_port'] = '465';
		//$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$config['useragent'] = 'SIMBINTEK';

		#$this->email->initialize($config);
		
		#$this->email->from(ADMIN_EMAIL, ADMIN_NAME);
		#$this->email->to($param['email']);
		
		#$message = $this->load->view('templates/email_institution_user', array('username' => $username,'password' => $password), true);
		
		#$this->email->subject('User Akses SIMBINTEK');
		#$this->email->message($message);
		*/
		
		echo '<div style="margin:5px;background:#cccccc;border:1px blue solid;padding:10px;">
				Username : '.$username.'<br/>';
		echo "Password : ".$password .'</div>';
		#$this->email->send();
		#echo $this->email->print_debugger();
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		if(isset($param['user_id']) and !empty($param['user_id']))
		{
			$this->db->where(array("user_id" => $param['user_id']));
			$this->db->delete("users");
		}
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
