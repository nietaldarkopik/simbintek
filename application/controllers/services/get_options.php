<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Get_options extends CI_Controller {

	public function index()
	{
		$table = $this->input->post("table");
		$select = $this->input->post("select");
		$where = $this->input->post("where");
		$fk = $this->input->post("fk");
		$value = $this->input->post("value");
		$foreign_key = $this->input->post("foreign_key");
		
		if(empty($table))
			return "";
			
		$this->db->limit(999999);
		$this->db->select("$select", FALSE);
		$this->db->where("$foreign_key='".$fk."'", NULL, FALSE);
		if(!empty($where))
			$this->db->where($where, NULL, FALSE);
		$q = $this->db->get($table);
		$options = $q->result_array();
		
		echo $this->db->last_query();
		$output = "";
		if(is_array($options) and count($options) > 0)
		{
			foreach($options as $index => $option)
			{
				$output .= '<option value="'.$option['value'].'">'.$option['label'].'</option>'."\n";
			}
		}
		
		echo $output;
		die();
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
