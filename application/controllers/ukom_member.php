<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ukom_member extends MY_Controller {

	var $init = array();
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('default/listing',array('response' => '','page_title' => 'Peserta Ujikom'));
		else
			$this->load->view('dashboard/dashboard');
			
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'member_bintek/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response,'page_title' => 'Peserta Ujikom'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title' => 'Peserta Ujikom'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function register()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title' => 'Peserta Ujikom'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '','page_title' => 'Peserta Ujikom'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/listing',array('response' => '','page_title' => 'Peserta Ujikom'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'ujikom_peserta',
						'fields' => array(
											/*
												array(
													'name' => 'ujikom_id',
													'label' => 'Ujikom',
													'id' => 'ujikom_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'ujikom',
													'select' => array('ujikom_id AS value','judul_bintek AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'ujikom_id',
													'rules' => ''
												),
											*/
												array(
													'name' => 'lembaga',
													'label' => 'Lembaga	',
													'id' => 'lembaga',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'lembaga',
													'select' => array('lembaga_id AS value','concat("[",kdlembaga,"] - ",nmlembaga) AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'lembaga_id',
													'rules' => ''
												),
												array(
													'name' => 'nama',
													'label' => 'Nama',
													'id' => 'nama',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'tempat_lahir',
													'label' => 'Tempat Lahir',
													'id' => 'tempat_lahir',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'tanggal_lahir',
													'label' => 'Tanggal Lahir',
													'id' => 'tanggal_lahir',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'unit_kerja',
													'label' => 'Unit Kerja',
													'id' => 'unit_kerja',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => ''
												),
												array(
													'name' => 'pendidikan_terakhir',
													'label' => 'Pendidikan Terakhir',
													'id' => 'pendidikan_terakhir',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'jurusan',
													'label' => 'Jurusan',
													'id' => 'jurusan',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'status_kepegawaian',
													'label' => 'Status Kepegawaian',
													'id' => 'status_kepegawaian',
													'value' => '',
													'type' => 'selectbox',
													'options' => array('' => '---- Select Option ----','tetap' => 'Tetap','tidak tetap' => 'Tidak Tetap'),
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'nip',
													'label' => 'NIP',
													'id' => 'nip',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'no_ktp',
													'label' => 'No. KTP',
													'id' => 'no_ktp',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'jabatan',
													'label' => 'Jabatan',
													'id' => 'jabatan',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'pangkat',
													'label' => 'Pangkat',
													'id' => 'pangkat',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'golongan',
													'label' => 'Golongan',
													'id' => 'golongan',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'bidang_pekerjaan',
													'label' => 'Bidang Pekerjaan',
													'id' => 'bidang_pekerjaan',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'masa_kerja',
													'label' => 'Masa Kerja',
													'id' => 'masa_kerja',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'mulai_tahun',
													'label' => 'Mulai Tahun',
													'id' => 'mulai_tahun',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'alamat_kantor',
													'label' => 'Alamat Kantor',
													'id' => 'alamat_kantor',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'alamat_rumah',
													'label' => 'Alamat Rumah',
													'id' => 'alamat_rumah',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'telepon_kantor',
													'label' => 'Telepon Kantor',
													'id' => 'telepon_kantor',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'telepon_rumah',
													'label' => 'Telepon Rumah',
													'id' => 'telepon_rumah',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'hp',
													'label' => 'No. Handphone',
													'id' => 'hp',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'email',
													'label' => 'email',
													'id' => 'email',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'bank',
													'label' => 'Nama Bank',
													'id' => 'bank',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'no_rekening',
													'label' => 'No. Rekening',
													'id' => 'no_rekening',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'foto',
													'label' => 'foto',
													'id' => 'foto',
													'value' => '',
													'type' => 'file',
													'use_search' => false,
													'use_listing' => false,
													'config_upload' => array('upload_path' => getcwd().'/uploads/media/ujikom_member/','encrypt_name' => true,'allowed_types' =>  'gif|jpg|png'),
													'rules' => '',
													'list_style' => 'width="100" align="center"'
												),
												array(
													'name' => 'tanggal_registrasi',
													'label' => 'Tanggal Registrasi',
													'id' => 'tanggal_registrasi',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'status_registrasi',
													'label' => 'Status Registrasi',
													'id' => 'status_registrasi',
													'value' => '',
													'type' => 'selectbox',
													'options' => array('' => '---- Select Option ----','sudah' => 'Sudah','belum' => 'Belum'),
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												)
											),
									'primary_key' => 'peserta_ujikom_id'
					);
		$curr_user = $this->user_access->get_user_id();
		$curr_level = $this->user_access->get_level();
		if($curr_level == 3)
		{
			$this->db->where(array("user_id" => $curr_user));
			$q = $this->db->get("lembaga");
			$lembaga = $q->row_array();
			$lembaga_id = (isset($lembaga['lembaga_id']) and !empty($lembaga['lembaga_id']))?$lembaga['lembaga_id']:"-";
			$init['where'] = " AND lembaga = '".$lembaga_id."'";
			$init['where_operator'] = " AND ";
			if(isset($init['fields'][0]['name']) and $init['fields'][0]['name'] == 'lembaga')
			{
				unset($init['fields'][0]);
			}
			if(isset($init['fields'][25]['name']) and $init['fields'][25]['name'] == 'tanggal_registrasi')
			{
				unset($init['fields'][25]);
			}
			if(isset($init['fields'][26]['name']) and $init['fields'][26]['name'] == 'status_registrasi')
			{
				unset($init['fields'][26]);
			}
		}
		$this->init = $init;
	}
	
	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('response' => '','page_title' => 'Peserta uji Kompetensi'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	function _hook_do_add($param = "")
	{
		$curr_user = $this->user_access->get_user_id();
		$curr_level = $this->user_access->get_level();
		if($curr_level == 3)
		{
			$this->db->where(array("user_id" => $curr_user));
			$q = $this->db->get("lembaga");
			$lembaga = $q->row_array();
			$lembaga_id = (isset($lembaga['lembaga_id']) and !empty($lembaga['lembaga_id']))?$lembaga['lembaga_id']:"-";
			$param['lembaga'] = $lembaga_id;
		}
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		$curr_user = $this->user_access->get_user_id();
		$curr_level = $this->user_access->get_level();
		if($curr_level == 3)
		{
			$this->db->where(array("user_id" => $curr_user));
			$q = $this->db->get("lembaga");
			$lembaga = $q->row_array();
			$lembaga_id = (isset($lembaga['lembaga_id']) and !empty($lembaga['lembaga_id']))?$lembaga['lembaga_id']:"-";
			$param['lembaga'] = $lembaga_id;
		}
		if(isset($param['foto']) and empty($param['foto']))
			unset($param['foto']);
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
