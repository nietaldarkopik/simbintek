<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bintek_evaluation_member extends MY_Controller {

	var $init = array();
	
	public function index()
	{
		$this->_config();
		$this->init['fields'][2]['type'] = 'text';
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$this->hook->add_action('hook_create_listing_output',array($this,'_hook_create_listing_output'));
		$this->hook->add_action('hook_create_listing_rows',array($this,'_hook_create_listing_rows'));
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('bintek/listing_evaluation_member',array('response' => '', 'page_title' => 'Data Evaluasi Peserta'));
		else
			$this->load->view('dashboard/dashboard');
			
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response, 'page_title' => 'Data Evaluasi Peserta'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/add_evaluation_member',array('response' => $response, 'page_title' => 'Data Evaluasi Peserta'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	
	public function add_sertifikat()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/add_evaluation_member_sertifikat',array('response' => $response, 'page_title' => 'Data Evaluasi Peserta'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '', 'page_title' => 'Data Evaluasi Peserta'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->init['fields'][2]['type'] = 'text';
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$this->hook->add_action('hook_create_listing_output',array($this,'_hook_create_listing_output'));
		$this->hook->add_action('hook_create_listing_rows',array($this,'_hook_create_listing_rows'));
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_evaluation_member',array('response' => '', 'page_title' => 'Data Evaluasi Peserta'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function certificate()
	{
		$this->_config();
		$this->init['query'] = 'SELECT bintek_eval_member.*,bintek_certificates.no_certificate,bintek_certificates.ttd_tanggal FROM (`bintek_eval_member` LEFT JOIN bintek_certificates ON bintek_eval_member.jadwal_id = bintek_certificates.jadwal_id AND bintek_eval_member.eval_member_id = bintek_certificates.member_id AND bintek_certificates.certificate_type = "sertifikat") ';
		$this->init['fields'][] = 	array(
										'name' => 'no_certificate',
										'label' => 'No Sertifikat',
										'id' => 'no_certificate',
										'value' => '',
										'type' => 'text',
										'use_search' => true,
										'use_listing' => true,
										'rules' => 'required'
									);
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_listing_report_output',array($this,'_hook_create_listing_report_output'));
		$this->hook->add_action('hook_create_listing_report_rows',array($this,'_hook_create_listing_report_rows'));
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_certificate',array('response' => '', 'page_title' => 'Data Sertifikat'));
		else
			$this->load->view('dashboard/dashboard');
		
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function citation()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_evaluation_member',array('response' => '', 'page_title' => 'Data Evaluasi Peserta'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function grafic()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_evaluation_member',array('response' => '', 'page_title' => 'Data Evaluasi Peserta'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{	
		$unsur_penilaian = $this->db->get('bintek_material')->result_array();
		$column_multiple_field = array();
		foreach($unsur_penilaian as $idx => $up){
			$column_multiple_field[] = array(
				'key_option' => $up['material_id'],
				'label' => $up['material_name']
			);
		}
		
		$init = array(	'table' => 'bintek_eval_member',
						'fields' => array(
											array(
													'name' => 'jadwal_id',
													'label' => 'Bintek',
													'id' => 'jadwal_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'jadwal_bintek',
													'select' => array('jadwal_bintek_id AS value','judul_bintek AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'jadwal_bintek_id',
													'rules' => 'required'
											),
											array(
													'name' => 'member_id',
													'label' => 'Peserta',
													'id' => 'member_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'bintek_peserta',
													'select' => array('peserta_bintek_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'peserta_bintek_id',
													'js_connect_to' => array(	
															'id_field_parent' => 'jadwal_id',
															'table' => 'bintek_peserta A,jadwal_bintek B, relation_bintek_member C',
															'select' => 'A.peserta_bintek_id AS value,A.nama AS label',
															'where' => " A.peserta_bintek_id = C.member_id AND B.jadwal_bintek_id = C.jadwal_bintek_id ",
															'foreign_key' => 'B.jadwal_bintek_id'
													),
													'rules' => 'required'
												),
											//~ array(
													//~ 'name' => 'material_id',
													//~ 'label' => 'Materi',
													//~ 'id' => 'material_id',
													//~ 'value' => '',
													//~ 'type' => 'selectbox',
													//~ 'use_search' => true,
													//~ 'use_listing' => true,
													//~ 'table'	=> 'bintek_material',
													//~ 'select' => array('material_id AS value','material_name AS label'),
													//~ 'options' => array('' => '---- Select Option ----'),
													//~ 'primary_key' => 'material_id',
													//~ 'rules' => 'required'
												//~ ),
											array(
													'name' => 'nilai',
													'label' => 'Nilai',
													'id' => 'nilai',
													'value' => '',
													'type' => 'fixed_multiple_field_text',
													'columns' => $column_multiple_field,
													'use_search' => false,
													'use_listing' => true,
													'rules' => ''
												),
						),
						'bulk_options' => array('download_excel','download_pdf','printing'),
						'primary_key' => 'eval_member_id'
					);
		$this->init = $init;
	}
	
	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$query =  $this->data->get_query();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('query' => $query,'response' => '', 'page_title' => 'Data Evaluasi Peserta'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function download_excel()
	{
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		#$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
		
		$query =  $this->data->get_query();
		
		$is_login = $this->user_access->is_login();
		$output = "";
		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
		if($is_login)			
			$output = $this->load->view('bintek/report_evaluation_member_excel',array('query' => $query,'response' => '','page_title' => 'TABULASI HASIL EVALUASI PENGAJAR'),true);
			//$output = $this->load->view('default/report',array('response' => '','page_title' => 'TABULASI HASIL EVALUASI PENGAJAR'),true);
		
		$this->report->download_excel($output,date("ymdhis"));
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		exit;
	}
		
	public function download_pdf()
	{
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		if($is_login)			
			$output = $this->load->view('bintek/report_evaluation_member_excel',array('response' => '','page_title' => 'TABULASI HASIL EVALUASI PENGAJAR'),true);
			//$output = $this->load->view('default/report',array('response' => '','page_title' => 'TABULASI HASIL EVALUASI PENGAJAR'),true);
		
		$this->report->download_pdf($output,date("ymdhis"), 'L', '', 'A3');
		exit;
	}
		
	public function printing()
	{
		
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$output = $this->load->view('bintek/report_evaluation_member_excel',array('response' => '','page_title' => 'TABULASI HASIL EVALUASI PENGAJAR'),true);
			//$this->load->view('default/report',array('response' => '','page_title' => 'TABULASI HASIL EVALUASI PENGAJAR'));
		
		echo '<script type="text/javascript">window.print();</script>';
	}
	
	function _hook_report(){
		$init = $this->init;
		if(is_array($init['fields'])){
			foreach($init['fields'] as $idx => $field){
				if($field['name'] != 'foto') 
					$init['fields'][$idx]['use_listing'] = true;
			}
		}
		
		return $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
	
	function _hook_create_listing_report_rows($param = "")
	{
		$dom = new domDocument;
		$dom->loadHTML($param);
		if(!$dom) {
		}else{
			$tmp = $dom->getElementsByTagName("td");
			$tmp_json = (isset($tmp->item(3)->nodeValue))?$tmp->item(3)->nodeValue:"";
			$tmp_json = json_decode($tmp_json,true);
			$total = 0;
			$rata2 = 0;
			$predikat = '-';
			if(is_array($tmp_json))
			{
				foreach($tmp_json as $index => $col)
				{
					$total += $col['value'];
				}
				$rata2 = number_format($total/count($tmp_json),2);
				$predikat = $this->data->get_qualification($rata2);
			}
			
			$param =  '<td>' . $tmp->item(0)->nodeValue . '</td>';
			$param .= '<td>' . $tmp->item(1)->nodeValue . '</td>';
			$param .= '<td>' . $tmp->item(2)->nodeValue . '</td>';
			$param .= '<td align="center">' . $total . '</td>';
			$param .= '<td align="center">' . $rata2 . '</td>';
			$param .= '<td align="center">' . $predikat . '</td>';
			$param .= '<td>' . $tmp->item(4)->nodeValue . '</td>';
			
			
		}
		return $param;
	}
	
	function _hook_create_listing_report_output($param = "")
	{
		$param = str_replace('<th>Nilai</th>','<th>Total Nilai</th><th>Nilai Rata-Rata</th><th>Predikat</th>',$param);
		return $param;
	}
	
	function _hook_create_listing_rows($param = "")
	{
		$dom = new domDocument;
		$dom->loadHTML($param);
		$param = '';
		if(!$dom) {
		}else{
			$tr = $dom->getElementsByTagName("tr");
			foreach($tr as $the_tr)
			{
				$tmp = $the_tr->getElementsByTagName("td");
				$tmp_json = (isset($tmp->item(4)->nodeValue))?$tmp->item(4)->nodeValue:"";
				$tmp_json = json_decode($tmp_json,true);
				$total = 0;
				$rata2 = 0;
				$predikat = '-';
				if(is_array($tmp_json))
				{
					foreach($tmp_json as $index => $col)
					{
						$total += $col['value'];
					}
					$rata2 = number_format($total/count($tmp_json),2);
					$predikat = $this->data->get_qualification($rata2);
				}
				
				$param .= '<tr>';
				$param .=  $dom->saveXML($tmp->item(0));
				$param .= '<td>' . $tmp->item(1)->nodeValue . '</td>';
				$param .= '<td>' . $tmp->item(2)->nodeValue . '</td>';
				$param .= '<td>' . $tmp->item(3)->nodeValue . '</td>';
				$param .= '<td align="center">' . $total . '</td>';
				$param .= '<td align="center">' . $rata2 . '</td>';
				$param .= '<td align="center">' . $predikat . '</td>';
				$param .= $dom->saveXML($tmp->item(5));
				$param .= '</tr>';
			}
			
		}
		return $param;
	}
	
	function _hook_create_listing_output($param = "")
	{
		$param = str_replace('<th>Nilai</th>','<th>Total Nilai</th><th>Nilai Rata-Rata</th><th>Predikat</th>',$param);
		return $param;
	}
	
	public function unsur_penilaian()
	{
		redirect('/bintek_bintek_material');
	}
	
	function _bulk_actions()
	{
		$action = $this->input->post("bulk_action");
		if(!empty($action))
		{
			$action;
		}
	}
	
	function _hook_show_bulk_allowed_is_ajax_set0($param = "")
	{
		return 0;
	}
		
	function _hook_show_bulk_allowed_is_ajax_set1($param = "")
	{
		return 1;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
