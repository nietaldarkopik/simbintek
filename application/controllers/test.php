<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends MY_Controller {

	var $init = array();
	
	public function index()
	{
		$this->load->library('encrypt');
		if (function_exists('mcrypt_encrypt'))

		{

			$this->encrypt->set_cipher(MCRYPT_BLOWFISH);

			$this->encrypt->set_mode(MCRYPT_MODE_CFB);

		}
		$encryption_key = 'YloZESYTEJA4I1eJiHYfUpAsiRU$Uvo6';

		$hash 	= $this->encrypt->sha1('super' . '882e431a017ca329');
		$key 	= $this->encrypt->sha1($encryption_key . $hash);
		echo $this->encrypt->decode('wwr/GmkPNjpe7q7+RIXDdg==', substr($key, 0, 56));
		exit;
	}
	
	public function _chart()
	{
		echo $this->user_access->get_menus_allowed_structured();
		exit;
		$this->load->library('highcharts_lib');

		# Set the graph using methods chaining
		$this->highcharts_lib
		->set_graph_type('spline')
		->toggle_legend(TRUE)
		->display_shadow(TRUE)
		->set_xlabels_options('rotation', -45)
		->set_graph_title('')
		->set_yAxis(array(), 'Clicks')
		->add_serie('The first Serie', array())
		->add_serie('This is the second Serie', array(),'areaspline');
		
		$this->highcharts_lib->js_file = current_theme_url() . 'static/js/highchart/highcharts.src.js';
		# Set random data for the series
		for($i=1 ; $i <=365 ; $i++){
			$value = rand(100, 500);
			$value_second_serie = rand(50, 70);
			
			$this->highcharts_lib->push_serie_data('The first Serie', $value);
			$this->highcharts_lib->push_serie_data('This is the second Serie', $value_second_serie);

			// At the same time we set the axis label
			$this->highcharts_lib->push_xAxis_value($i);
		}

		# We can use debug to check the content of the options array
		#$this->highcharts_lib->debug();
		$s_graph = $this->highcharts_lib->render(800,400);
		
		echo '<script src="'.current_theme_url().'static/js/jquery-1.9.1.js"></script>';
		echo $s_graph;
		echo '<div id="highchart_id"></div>';
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
