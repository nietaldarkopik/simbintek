<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bintek_instructure extends CI_Controller {

	var $init = array();
	function __construct()
	{
		parent::__construct();
		$this->_bulk_actions();
		$this->hook->add_action('hook_show_bulk_allowed_is_ajax',array($this,'_hook_show_bulk_allowed_is_ajax_set0'));
	}
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('default/listing',array('response' => '', 'page_title' => 'Data Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
			
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'instructure/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response, 'page_title' => 'Data Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response, 'page_title' => 'Data Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '', 'page_title' => 'Data Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/listing',array('response' => '', 'page_title' => 'Data Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'bintek_instruktur',
						'fields' => array(	
												array(
													'name' => 'nama',
													'label' => 'Nama',
													'id' => 'nama',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'nip',
													'label' => 'NIP',
													'id' => 'nip',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'gelar',
													'label' => 'Gelar',
													'id' => 'gelar',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'gol',
													'label' => 'Golongan',
													'id' => 'gol',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'tempat_lahir_propinsi',
													'label' => 'Propinsi Tempat Lahir',
													'id' => 'tempat_lahir_propinsi',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => false,
													'use_listing' => false,
													'table'	=> 'propinsi',
													'select' => array('propinsi_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'propinsi_id',
													'rules' => 'required'
												),
											array(
													'name' => 'tempat_lahir_kota',
													'label' => 'Kota Tempat Lahir',
													'id' => 'tempat_lahir_kota',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => false,
													'use_listing' => false,
													'table'	=> 'kabupaten',
													'select' => array('kabupaten_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'kabupaten_id',
													'js_connect_to' => array(	
															'id_field_parent' => 'tempat_lahir_propinsi',
															'table' => 'kabupaten A, propinsi B',
															'select' => 'A.kabupaten_id AS value, A.nama AS label',
															'where' => " A.propinsi_id = B.propinsi_id ",
															'foreign_key' => 'B.propinsi_id'
													),
													'rules' => 'required'
												),
												array(
													'name' => 'tanggal_lahir',
													'label' => 'Tanggal Lahir',
													'id' => 'tanggal_lahir',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'jabatan',
													'label' => 'Jabatan',
													'id' => 'jabatan',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'status',
													'label' => 'Status',
													'id' => 'status',
													'value' => '',
													'type' => 'selectbox',
													'options' => array('' => '---- Select Option ----','Widyaiswara Kem. PU' => 'Widyaiswara Kem. PU','Instruktur' => 'Instruktur'),
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'instansi',
													'label' => 'Instansi',
													'id' => 'instansi',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'lembaga',
													'select' => array('lembaga_id AS value','nmlembaga AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'lembaga_id',
													'rules' => 'required'
												),
												array(
													'name' => 'alamat_instansi',
													'label' => 'Alamat Instansi',
													'id' => 'alamat_instansi',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'propinsi_instansi',
													'label' => 'Propinsi Instansi',
													'id' => 'propinsi_instansi',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => false,
													'use_listing' => false,
													'table'	=> 'propinsi',
													'select' => array('propinsi_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'propinsi_id',
													'rules' => 'required'
												),
												array(
													'name' => 'kota_instansi',
													'label' => 'Kota Instansi',
													'id' => 'kota_instansi',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => false,
													'use_listing' => false,
													'table'	=> 'kabupaten',
													'select' => array('kabupaten_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'kabupaten_id',
													'js_connect_to' => array(	
															'id_field_parent' => 'propinsi_instansi',
															'table' => 'kabupaten A, propinsi B',
															'select' => 'A.kabupaten_id AS value, A.nama AS label',
															'where' => " A.propinsi_id = B.propinsi_id ",
															'foreign_key' => 'B.propinsi_id'
													),
													'rules' => 'required'
												),
												array(
													'name' => 'telepon_instansi',
													'label' => 'Telp. Instansi',
													'id' => 'telepon_instansi',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'fax_instansi',
													'label' => 'Fax Instansi',
													'id' => 'fax_instansi',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'hp_instansi',
													'label' => 'HP. Instansi',
													'id' => 'hp_instansi',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'alamat_rumah',
													'label' => 'Alamat Rumah',
													'id' => 'alamat_rumah',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'propinsi_rumah',
													'label' => 'Propinsi Rumah',
													'id' => 'propinsi_rumah',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => false,
													'use_listing' => false,
													'table'	=> 'propinsi',
													'select' => array('propinsi_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'propinsi_id',
													'rules' => 'required'
												),
												array(
													'name' => 'kota_rumah',
													'label' => 'Kota Rumah',
													'id' => 'kota_rumah',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => false,
													'use_listing' => false,
													'table'	=> 'kabupaten',
													'select' => array('kabupaten_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'kabupaten_id',
													'js_connect_to' => array(	
															'id_field_parent' => 'propinsi_rumah',
															'table' => 'kabupaten A, propinsi B',
															'select' => 'A.kabupaten_id AS value, A.nama AS label',
															'where' => " A.propinsi_id = B.propinsi_id ",
															'foreign_key' => 'B.propinsi_id'
													),
													'rules' => 'required'
												),
												array(
													'name' => 'telp_rumah',
													'label' => 'Telp. Rumah',
													'id' => 'telp_rumah',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'fax_rumah',
													'label' => 'Fax. Rumah',
													'id' => 'fax_rumah',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'email',
													'label' => 'Email',
													'id' => 'email',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required|email'
												),
												array(
													'name' => 'pengalaman_kerja',
													'label' => 'Pengalaman Kerja',
													'id' => 'pengalaman_kerja',
													'value' => '',
													'type' => 'multiple',
													'multiple_cols' => array(array('title' => 'PENGALAMAN KERJA','type' => 'textarea','attr' => 'style="width:200px;"','name' => 'pengalaman_kerja'),array('title' => 'JABATAN','type' => 'text','attr' => 'style="width:150px;"','name' => 'jabatan'),array('title' => 'LAMA','type' => 'text','attr' => 'style="width:100px;"','name' => 'lama')),
													'use_search' => false,
													'use_listing' => false,
													'rules' => ''
												),
												array(
													'name' => 'pendidikan_tertinggi',
													'label' => 'PENDIDIKAN FORMAL TERTINGGI :',
													'id' => 'pendidikan_tertinggi',
													'value' => '',
													'type' => 'multiple',
													'multiple_cols' => array(array('title' => 'LEMBAGA PENDIDIKAN','type' => 'text','attr' => 'style="width:200px;"','name' => 'lem_dik'),array('title' => 'KOTA / NEGARA','type' => 'text','attr' => 'style="width:150px;"','name' => 'kot_neg'),array('title' => 'GELAR','type' => 'text','attr' => 'style="width:100px;"','name' => 'gelar'),array('title' => 'SPESIALISASI','type' => 'text','attr' => 'style="width:150px;"','name' => 'special')),
													'use_search' => false,
													'use_listing' => false,
													'rules' => ''
												),
												array(
													'name' => 'pelatihan_teknik',
													'label' => 'KURSUS ATAU PELATIHAN SELAIN PENDIDIKAN FORMAL :',
													'id' => 'pelatihan_teknik',
													'value' => '',
													'type' => 'multiple',
													'multiple_cols' => array(array('title' => 'URAIAN NAMA KURSUS','type' => 'textarea','name' => 'ur_nam_kur'),array('title' => 'KOTA/NEGARA','type' => 'text','name' => 'kot_neg'),array('title' => 'LAMA','type' => 'text','attr' => 'style="width:100px;"','name' => 'lama')),
													'use_search' => false,
													'use_listing' => false,
													'rules' => ''
												),
												array(
													'name' => 'spesialisasi_pelatihan',
													'label' => 'SPESIALISASI MATA PELAJARAN YANG DIAJARKAN :',
													'id' => 'spesialisasi_pelatihan',
													'value' => '',
													'type' => 'multiple',
													'multiple_cols' => array(array('title' => 'MATA PELAJARAN','type' => 'text','name' => 'mat_pel')),
													'use_search' => false,
													'use_listing' => false,
													'rules' => ''
												),
												array(
													'name' => 'foto',
													'label' => 'Foto',
													'id' => 'foto',
													'value' => '',
													'type' => 'file',
													'use_search' => false,
													'use_listing' => true,
													'config_upload' => array('upload_path' => getcwd().'/uploads/media/bintek_instructure/','encrypt_name' => true,'allowed_types' =>  'gif|jpg|png'),
													'rules' => '',
													'list_style' => 'width="100" align="center"'
												)
											),
											'bulk_options' => array('download_excel','download_pdf','printing'),
											'primary_key' => 'instruktur_id'
					);
		$this->init = $init;
	}
	

	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('response' => '','page_title' => 'Data Instruktur'));
		else
			$this->load->view('dashboard/dashboard');

			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		
	}


	public function download_excel()
	{
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_listing_report_value_spesialisasi_pelatihan',array($this,'_hook_create_listing_report_value_spesialisasi_pelatihan'));
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		$is_ajax = $this->input->post('is_ajax');
			$this->load->view('header');
			$this->load->view('topbar');
		if($is_login)			
			$output = $this->load->view('default/report',array('response' => 'Laporan Data Instruktur'),true);
		
		$this->report->download_excel($output,date("ymdhis"));
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		exit;
	}
		
	public function download_pdf()
	{
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		if($is_login)			
			$output = $this->load->view('default/report',array('response' => '','page_title' => 'Laporan Data Instruktur'),true);
		
		$this->report->download_pdf($output,date("ymdhis"), 'L', '', 'A3');
		exit;
	}
		
	public function printing()
	{
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('response' => '', 'page_title' => 'Laporan Data Instruktur'));
		
		echo '<script type="text/javascript">window.print();</script>';
	}

	function _bulk_actions()
	{
		$action = $this->input->post("bulk_action");
		if(!empty($action))
		{
			$action;
		}
	}
		
	function _hook_show_bulk_allowed_is_ajax_set0($param = "")
	{
		return 0;
	}
		
	function _hook_show_bulk_allowed_is_ajax_set1($param = "")
	{
		return 1;
	}
	


	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		if(isset($param['foto']) and empty($param['foto']))
			unset($param['foto']);
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
	
	//pelatihan_teknik,pengalaman_kerja,pendidikan_tertinggi
	function _hook_create_listing_report_value_spesialisasi_pelatihan($param = "")
	{
		#$param = str_replace('<li>','<br/>',$param);
		#$param = str_replace('</li>','',$param);
		#$param = strip_tags($param,'<br>');
		return $param;
	}
	
	function _hook_report(){
		$init = $this->init;
		if(is_array($init['fields'])){
			foreach($init['fields'] as $idx => $field){
				if($field['name'] != 'foto') 
				{
					$init['fields'][$idx]['use_listing'] = true;
				}else{
					$init['fields'][$idx]['use_listing'] = false;
				}
			}
		}
		
		return $init;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
