<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certification_results extends MY_Controller {

	var $init = array();
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('certification/listing',array('response' => '','page_title' => 'Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
			
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response,'page_title' => 'Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title' => 'Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '','page_title' => 'Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('certification/listing',array('response' => '','page_title' => 'Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function hasil($sertifikasi_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
		{			
			$data_materi = array();
			
			$material = $this->input->post("material");
			$nilai = $this->input->post("nilai");
			$bukti = $this->input->post("bukti");
			#$sertifikasi_id = $this->input->post("sertifikasi_id");
			$peserta_id = $this->input->post("peserta_id");
			$accessor_id = $this->input->post("accessor_id");
			$jenis_sertifikasi = $this->input->post("jenis_sertifikasi");
			
			if(is_array($material) and count($material) > 0)
			{
				foreach($material as $index => $value)
				{
					$this->db->where(array("sertifikasi_material" => $index));
					$q = $this->db->get("sertifikasi_material");
					if($q->num_rows() == 0)
						continue;
						
					$dt_materi = $q->row_array();
					$data_materi[] = array(
											'kompetensi_id' => $dt_materi['kompetensi_id'],
											'material_id' => $dt_materi['sertifikasi_material'],
											'kode_material' => $dt_materi['kode_material'],
											'judul_material' => $dt_materi['judul_material'],
											'peserta_id' => $peserta_id,
											'accessor_id' => $accessor_id,
											'nilai' => $nilai[$index],
											'bukti' => $bukti[$index],
											'jenis_sertifikasi' => $jenis_sertifikasi,
											'sertifikasi_id' => $sertifikasi_id
											);
				}
			}
			
			if(is_array($data_materi) and count($data_materi) > 0)
			{
				$this->db->where(array('sertifikasi_id' => $sertifikasi_id));
				$this->db->delete("sertifikasi_bukti_pendukung");
				
				foreach($data_materi as $index => $nilai_materi)
				{
					$this->db->insert("sertifikasi_bukti_pendukung",$nilai_materi);
				}
			}
			
			$this->db->where(array("sertifikasi_id" => $sertifikasi_id));
			$q_sertifikasi = $this->db->get("sertifikasi");
			$sertifikasi = $q_sertifikasi->row_array();
			
			$this->db->where(array("jenis_sertifikasi" => $sertifikasi['jenis_sertifikasi'],"parent_kompetensi" => ""));
			$kompetensi = $this->db->get("sertifikasi_kompetensi");
			$kompetensi = $kompetensi->result_array();
			
			if(is_array($kompetensi) and count($kompetensi) > 0)
			{
				foreach($kompetensi as $index => $kom)
				{
					$this->db->where(array("parent_kompetensi" => $kom['kode_kompetensi']));
					$subkompetensi = $this->db->get("sertifikasi_kompetensi");
					$subkompetensi = $subkompetensi->result_array();
					
					$kompetensi[$index]['sub'] = $subkompetensi;
				}
			}
			
			$this->load->view('certification/evaluasi_report',array('response' => '','page_title' => 'Hasil Penilaian Bukti Pendukung','kompetensi' => $kompetensi,'sertifikasi' => $sertifikasi));
		}
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function hasil_penilaian_mandiri($sertifikasi_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
		{			
			$data_materi = array();
			
			$material = $this->input->post("material");
			$nilai = $this->input->post("nilai");
			$bukti = $this->input->post("bukti");
			#$sertifikasi_id = $this->input->post("sertifikasi_id");
			$peserta_id = $this->input->post("peserta_id");
			$accessor_id = $this->input->post("accessor_id");
			$jenis_sertifikasi = $this->input->post("jenis_sertifikasi");
			
			if(is_array($material) and count($material) > 0)
			{
				foreach($material as $index => $value)
				{
					$this->db->where(array("sertifikasi_material" => $index));
					$q = $this->db->get("sertifikasi_material");
					if($q->num_rows() == 0)
						continue;
						
					$dt_materi = $q->row_array();
					$data_materi[] = array(
											'kompetensi_id' => $dt_materi['kompetensi_id'],
											'material_id' => $dt_materi['sertifikasi_material'],
											'kode_material' => $dt_materi['kode_material'],
											'judul_material' => $dt_materi['judul_material'],
											'peserta_id' => $peserta_id,
											'accessor_id' => $accessor_id,
											'nilai' => $nilai[$index],
											'bukti' => $bukti[$index],
											'jenis_sertifikasi' => $jenis_sertifikasi,
											'sertifikasi_id' => $sertifikasi_id
											);
				}
			}
			
			if(is_array($data_materi) and count($data_materi) > 0)
			{
				$this->db->where(array('sertifikasi_id' => $sertifikasi_id));
				$this->db->delete("sertifikasi_nilai_mandiri");
				
				foreach($data_materi as $index => $nilai_materi)
				{
					$this->db->insert("sertifikasi_nilai_mandiri",$nilai_materi);
				}
			}
			
			$this->db->where(array("sertifikasi_id" => $sertifikasi_id));
			$q_sertifikasi = $this->db->get("sertifikasi");
			$sertifikasi = $q_sertifikasi->row_array();
			
			$this->db->where(array("jenis_sertifikasi" => $sertifikasi['jenis_sertifikasi'],"parent_kompetensi" => ""));
			$kompetensi = $this->db->get("sertifikasi_kompetensi");
			$kompetensi = $kompetensi->result_array();
			
			if(is_array($kompetensi) and count($kompetensi) > 0)
			{
				foreach($kompetensi as $index => $kom)
				{
					$this->db->where(array("parent_kompetensi" => $kom['kode_kompetensi']));
					$subkompetensi = $this->db->get("sertifikasi_kompetensi");
					$subkompetensi = $subkompetensi->result_array();
					
					$kompetensi[$index]['sub'] = $subkompetensi;
				}
			}
			
			$this->load->view('certification/evaluasi_question_report',array('response' => '','page_title' => 'Hasil Penilaian Assesmen Mandiri','kompetensi' => $kompetensi,'sertifikasi' => $sertifikasi));
		}
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function penilaian($sertifikasi_id = "",$type = "asesor")
	{
		
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
		{
			
			$data_materi = array();
			
			$material = $this->input->post("material");
			$nilai = $this->input->post("nilai");
			$assesment = $this->input->post("assesment");
			#$sertifikasi_id = $this->input->post("sertifikasi_id");
			$peserta_id = $this->input->post("peserta_id");
			$accessor_id = $this->input->post("accessor_id");
			$jenis_sertifikasi = $this->input->post("jenis_sertifikasi");
			
			if(is_array($material) and count($material) > 0)
			{
				foreach($material as $index => $value)
				{
					$this->db->where(array("sertifikasi_material" => $index));
					$q = $this->db->get("sertifikasi_material_support");
					if($q->num_rows() == 0)
						continue;
						
					$dt_materi = $q->row_array();
					$data_materi[] = array(
											'kompetensi_id' => $dt_materi['kompetensi_id'],
											'material_id' => $dt_materi['sertifikasi_material'],
											'kode_material' => $dt_materi['kode_material'],
											'judul_material' => $dt_materi['judul_material'],
											'peserta_id' => $peserta_id,
											'accessor_id' => $accessor_id,
											'nilai' => $nilai[$index],
											'assesment' => $assesment[$index],
											'jenis_sertifikasi' => $jenis_sertifikasi,
											'sertifikasi_id' => $sertifikasi_id
											);
				}
			}
			
			if(is_array($data_materi) and count($data_materi) > 0)
			{
				$this->db->where(array('sertifikasi_id' => $sertifikasi_id));
				$this->db->delete("sertifikasi_bukti_pendukung");
				
				foreach($data_materi as $index => $nilai_materi)
				{
					$this->db->insert("sertifikasi_bukti_pendukung",$nilai_materi);
				}
			}
			
			$this->db->where(array("sertifikasi_id" => $sertifikasi_id));
			$q_sertifikasi = $this->db->get("sertifikasi");
			$sertifikasi = $q_sertifikasi->row_array();
			
			$this->db->where(array("jenis_sertifikasi" => $sertifikasi['jenis_sertifikasi'],"parent_kompetensi" => ""));
			$kompetensi = $this->db->get("sertifikasi_kompetensi");
			$kompetensi = $kompetensi->result_array();
			
			if(is_array($kompetensi) and count($kompetensi) > 0)
			{
				foreach($kompetensi as $index => $kom)
				{
					$this->db->where(array("parent_kompetensi" => $kom['kode_kompetensi']));
					$subkompetensi = $this->db->get("sertifikasi_kompetensi");
					$subkompetensi = $subkompetensi->result_array();
					
					$kompetensi[$index]['sub'] = $subkompetensi;
				}
			}
			
			$this->load->view('certification/evaluasi',array('response' => '','page_title' => 'Penilaian Sertifikasi','kompetensi' => $kompetensi,'sertifikasi' => $sertifikasi));
		}
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function penilaian_mandiri($sertifikasi_id = "",$type = "asesor")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
		{
			$data_materi = array();
			
			$material = $this->input->post("material");
			$nilai = $this->input->post("nilai");
			$bukti = $this->input->post("bukti");
			#$sertifikasi_id = $this->input->post("sertifikasi_id");
			$peserta_id = $this->input->post("peserta_id");
			$accessor_id = $this->input->post("accessor_id");
			$jenis_sertifikasi = $this->input->post("jenis_sertifikasi");
			
			if(is_array($material) and count($material) > 0)
			{
				foreach($material as $index => $value)
				{
					$this->db->where(array("sertifikasi_material" => $index));
					$q = $this->db->get("sertifikasi_material");
					if($q->num_rows() == 0)
						continue;
						
					$dt_materi = $q->row_array();
					$data_materi[] = array(
											'kompetensi_id' => $dt_materi['kompetensi_id'],
											'material_id' => $dt_materi['sertifikasi_material'],
											'kode_material' => $dt_materi['kode_material'],
											'judul_material' => $dt_materi['judul_material'],
											'peserta_id' => $peserta_id,
											'accessor_id' => $accessor_id,
											'nilai' => $nilai[$index],
											'bukti' => $bukti[$index],
											'jenis_sertifikasi' => $jenis_sertifikasi,
											'sertifikasi_id' => $sertifikasi_id
											);
				}
			}
			
			if(is_array($data_materi) and count($data_materi) > 0)
			{
				$this->db->where(array('sertifikasi_id' => $sertifikasi_id));
				$this->db->delete("sertifikasi_nilai_mandiri");
				
				foreach($data_materi as $index => $nilai_materi)
				{
					$this->db->insert("sertifikasi_nilai_mandiri",$nilai_materi);
				}
			}
			
			$this->db->where(array("sertifikasi_id" => $sertifikasi_id));
			$q_sertifikasi = $this->db->get("sertifikasi");
			$sertifikasi = $q_sertifikasi->row_array();
			
			$this->db->where(array("jenis_sertifikasi" => $sertifikasi['jenis_sertifikasi'],"parent_kompetensi" => ""));
			$kompetensi = $this->db->get("sertifikasi_kompetensi");
			$kompetensi = $kompetensi->result_array();
			
			if(is_array($kompetensi) and count($kompetensi) > 0)
			{
				foreach($kompetensi as $index => $kom)
				{
					$this->db->where(array("parent_kompetensi" => $kom['kode_kompetensi']));
					$subkompetensi = $this->db->get("sertifikasi_kompetensi");
					$subkompetensi = $subkompetensi->result_array();
					
					$kompetensi[$index]['sub'] = $subkompetensi;
				}
			}
			
			$this->load->view('certification/evaluasi_question',array('response' => '','page_title' => 'Penilaian Assesmen Mandiri','kompetensi' => $kompetensi,'sertifikasi' => $sertifikasi));
		}
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function _penilaian($sertifikasi_id = "",$type = "asesor")
	{
		redirect("certification_aspects_evaluation_support/penilaian/".$sertifikasi_id);
	}
	
	public function _penilaian_mandiri($sertifikasi_id = "",$type = "asesor")
	{
		redirect("certification_aspects_evaluation_question/penilaian_mandiri/".$sertifikasi_id);
	}
	
	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
		{			
			$data_materi = array();
			
			$material = $this->input->post("material");
			$nilai = $this->input->post("nilai");
			$bukti = $this->input->post("bukti");
			#$sertifikasi_id = $this->input->post("sertifikasi_id");
			$peserta_id = $this->input->post("peserta_id");
			$accessor_id = $this->input->post("accessor_id");
			$jenis_sertifikasi = $this->input->post("jenis_sertifikasi");
			
			if(is_array($material) and count($material) > 0)
			{
				foreach($material as $index => $value)
				{
					$this->db->where(array("sertifikasi_material" => $index));
					$q = $this->db->get("sertifikasi_material");
					if($q->num_rows() == 0)
						continue;
						
					$dt_materi = $q->row_array();
					$data_materi[] = array(
											'kompetensi_id' => $dt_materi['kompetensi_id'],
											'material_id' => $dt_materi['sertifikasi_material'],
											'kode_material' => $dt_materi['kode_material'],
											'judul_material' => $dt_materi['judul_material'],
											'peserta_id' => $peserta_id,
											'accessor_id' => $accessor_id,
											'nilai' => $nilai[$index],
											'bukti' => $bukti[$index],
											'jenis_sertifikasi' => $jenis_sertifikasi,
											'sertifikasi_id' => $sertifikasi_id
											);
				}
			}
			
			if(is_array($data_materi) and count($data_materi) > 0)
			{
				$this->db->where(array('sertifikasi_id' => $sertifikasi_id));
				$this->db->delete("sertifikasi_nilai_mandiri");
				
				foreach($data_materi as $index => $nilai_materi)
				{
					$this->db->insert("sertifikasi_nilai_mandiri",$nilai_materi);
				}
			}
			
			$this->db->where(array("sertifikasi_id" => $sertifikasi_id));
			$q_sertifikasi = $this->db->get("sertifikasi");
			$sertifikasi = $q_sertifikasi->row_array();
			
			$this->db->where(array("jenis_sertifikasi" => $sertifikasi['jenis_sertifikasi'],"parent_kompetensi" => ""));
			$kompetensi = $this->db->get("sertifikasi_kompetensi");
			$kompetensi = $kompetensi->result_array();
			
			if(is_array($kompetensi) and count($kompetensi) > 0)
			{
				foreach($kompetensi as $index => $kom)
				{
					$this->db->where(array("parent_kompetensi" => $kom['kode_kompetensi']));
					$subkompetensi = $this->db->get("sertifikasi_kompetensi");
					$subkompetensi = $subkompetensi->result_array();
					
					$kompetensi[$index]['sub'] = $subkompetensi;
				}
			}
			
			$this->load->view('certification/evaluasi_question_report',array('response' => '','page_title' => 'Hasil Penilaian Assesmen Mandiri','kompetensi' => $kompetensi,'sertifikasi' => $sertifikasi));
		}
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	
	public function certificate()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_evaluation_instructure',array('response' => '', 'page_title' => 'Evaluasi Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function citation()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_evaluation_instructure',array('response' => '', 'page_title' => 'Evaluasi Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function grafic()
	{
		
		$y = array('' => 'Tahun');
		$m = array('' => 'Bulan');
		$d = array('' => 'Tanggal');
		for($i = YEAR_START; $i <= date("Y"); $i++)
		{
			$y[str_pad($i,4,"0",STR_PAD_LEFT)] = str_pad($i,4,"0",STR_PAD_LEFT);
		}
		for($i = 1; $i <= 12; $i++)
		{
			$m[str_pad($i,2,"0",STR_PAD_LEFT)] = str_pad($i,2,"0",STR_PAD_LEFT);
		}
		for($i = 1; $i <= 31; $i++)
		{
			$d[str_pad($i,2,"0",STR_PAD_LEFT)] = str_pad($i,2,"0",STR_PAD_LEFT);
		}
		$options_date = array('y' => $y,'m' => $m,'d' => $d);
		$this->_config();
		$this->init['fields'][2] = array(
													'name' => 'grafik_data',
													'label' => 'Grafik Data',
													'id' => 'grafik_data',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'options' => array('perwilayah' => 'Perwilayah','perbintek' => 'Perjenis Sertifikasi','pertahun' => 'Pertahun'),
													'rules' => 'required'
												);
		$this->init['fields'][3] = array(
													'name' => 'tanggal',
													'label' => 'Tanggal',
													'id' => 'tanggal',
													'value' => '',
													'type' => 'range_year_selectbox',
													'options_from' => $options_date,
													'options_to' => $options_date,
													'class' => 'date_range',
													'style' => 'width:auto;',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												);
		unset($this->init['fields'][0]);
		unset($this->init['fields'][1]);
		unset($this->init['fields'][4]);
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->load->library('highcharts_lib');

		# Set the graph using methods chaining
		$this->highcharts_lib
		->set_graph_type('column')
		->toggle_legend(TRUE)
		->set_legend_option(array("layout" => "vertical",
													"align" => "right",
													"verticalAlign" => "middle",
													"borderWidth" => "1",
													"enabled" => false))
		->display_shadow(TRUE)
		->set_xlabels_options('rotation', -35)
		->set_xlabels_options('align', "right")
		->set_yAxis(array(), 'Jumlah');
		//->set_xAxis(array(), 'Tanggal');
		
		$query_both = "SELECT lembaga.*,sertifikasi.*,sertifikasi_peserta.tanggal_registrasi,count(sertifikasi_peserta.peserta_sertifikasi_id) total_peserta FROM sertifikasi_peserta,lembaga,sertifikasi WHERE sertifikasi_peserta.lembaga = lembaga.lembaga_id AND sertifikasi.peserta_id = sertifikasi_peserta.peserta_sertifikasi_id ";
		$filter = $this->data->get_filter("");

		$post_data = $filter;
		$filtered = false;
		
		if(isset($post_data['grafik_data']) and $post_data['grafik_data'] == 'perwilayah')
		{
			$filtered = true;
			$where = "";
			if(isset($post_data['tanggal']))
			{
				$year_start = (isset($post_data['tanggal']['from']['y']) and !empty($post_data['tanggal']['from']['y']))?$post_data['tanggal']['from']['y']:'';
				$year_end = (isset($post_data['tanggal']['to']['y']) and !empty($post_data['tanggal']['to']['y']))?$post_data['tanggal']['to']['y']:$year_start;
				$title_year = "";
				if($year_start.$year_end != "")
				{
					$where .= " AND (sertifikasi_peserta.tanggal_registrasi BETWEEN '".$year_start."-01-01' AND '".$year_end."-12-31') ";
					$title_year = $year_start;
					if($year_start != $year_end and $year_end != "")
						$title_year .= ' - ' . $year_end;
					
					if(!empty($title_year))
					{
						$title_year = ' Tahun '.$title_year;
					}
				}
				$this->highcharts_lib->set_graph_title('Grafik Peserta Perwilayah'.$title_year);
			}
			$que = $query_both . $where . ' GROUP BY kdkab ORDER BY nmlembaga ASC';
			$q_both = $this->db->query($que);
			$data_grafic = $q_both->result_array();
			
			$data_per_propinsi = array();
			foreach($data_grafic as $index => $peserta)
			{
				$nama = $this->master->get_value('propinsi','nama',array('kdprop' => $peserta['kdprop']));
				$data_per_propinsi[$nama] = $peserta;
			}
			
			$data_per_kabupaten = array();
			foreach($data_grafic as $index => $peserta)
			{
				$nama_prop = $this->master->get_value('propinsi','nama',array('kdprop' => $peserta['kdprop']));
				$data_per_propinsi[$nama_prop] = $nama_prop;
				$nama = $this->master->get_value('kabupaten','nama',array('kdkab' => $peserta['kdkab']));
				$data_per_kabupaten[$nama] = $peserta;
			}
			
			$series = array();
			foreach($data_per_kabupaten as $i => $v)
			{
				$series[$i] = $v['total_peserta'];
				$this->highcharts_lib->push_xAxis_value($i);
			}
				
			
			$this->highcharts_lib->add_serie('Kota/Kabupaten', $series,'',array('enabled' => true,
																							'rotation' => 0,
																							'color' => '#000000',
																							'align' => 'center'
																							));
		}
		
		if(isset($post_data['grafik_data']) and $post_data['grafik_data'] == 'perbintek')
		{
			$filtered = true;
			$where = "";
			if(isset($post_data['tanggal']))
			{
				$year_start = (isset($post_data['tanggal']['from']['y']) and !empty($post_data['tanggal']['from']['y']))?$post_data['tanggal']['from']['y']:'';
				$year_end = (isset($post_data['tanggal']['to']['y']) and !empty($post_data['tanggal']['to']['y']))?$post_data['tanggal']['to']['y']:$year_start;
				$title_year = "";
				if($year_start.$year_end != "")
				{
					$where .= " AND (tanggal BETWEEN '".$year_start."-01-01' AND '".$year_end."-12-31') ";
					$title_year = $year_start;
					if($year_start != $year_end and $year_end != "")
						$title_year .= ' - ' . $year_end;
					
					if(!empty($title_year))
					{
						$title_year = ' Tahun '.$title_year;
					}
				}
				$this->highcharts_lib->set_graph_title('Grafik Peserta Jenis Sertifikasi'.$title_year);
			}
			
			$query_both = "SELECT sertifikasi.*,COUNT(sertifikasi_id) total_peserta FROM sertifikasi WHERE 1 = 1 ";
			$que = $query_both . $where . ' GROUP BY jenis_sertifikasi ORDER BY jenis_sertifikasi ASC';

			$q_both = $this->db->query($que);
			$data_grafic = $q_both->result_array();
						
			$data_per_sertifikasi_jenis = array();
			foreach($data_grafic as $index => $peserta)
			{
				$nama = $this->master->get_value('sertifikasi_jenis','nama_sertifikasi',array('kode_sertifikasi' => $peserta['jenis_sertifikasi']));
				$data_per_sertifikasi_jenis[$nama] = $peserta;
			}
			
			$series = array();
			foreach($data_per_sertifikasi_jenis as $i => $v)
			{
				$series[$i] = $v['total_peserta'];
				$this->highcharts_lib->push_xAxis_value($i);
			}
				
			
			$this->highcharts_lib->add_serie('Kota/Kabupaten', $series,'',array('enabled' => true,
																							'rotation' => 0,
																							'color' => '#000000',
																							'align' => 'center'
																							));
		}
		
		if(isset($post_data['grafik_data']) and $post_data['grafik_data'] == 'pertahun')
		{
			$filtered = true;
			$where = "";
			if(isset($post_data['tanggal']))
			{
				$year_start = (isset($post_data['tanggal']['from']['y']) and !empty($post_data['tanggal']['from']['y']))?$post_data['tanggal']['from']['y']:'';
				$year_end = (isset($post_data['tanggal']['to']['y']) and !empty($post_data['tanggal']['to']['y']))?$post_data['tanggal']['to']['y']:$year_start;
				$title_year = "";
				if($year_start.$year_end != "")
				{
					$where .= " AND (tanggal BETWEEN '".$year_start."-01-01' AND '".$year_end."-12-31') ";
					$title_year = $year_start;
					if($year_start != $year_end and $year_end != "")
						$title_year .= ' - ' . $year_end;
					
					if(!empty($title_year))
					{
						$title_year = ' Tahun '.$title_year;
					}
				}
				$this->highcharts_lib->set_graph_title('Grafik Peserta Jenis Sertifikasi'.$title_year);
			}
			
			$query_both = "SELECT sertifikasi.*,(year(sertifikasi.tanggal)) tahun,COUNT(sertifikasi_id) total_peserta FROM sertifikasi WHERE 1 = 1 ";
			$que = $query_both . $where . ' GROUP BY tahun ORDER BY jenis_sertifikasi ASC';

			$q_both = $this->db->query($que);
			$data_grafic = $q_both->result_array();
						
			$data_per_sertifikasi_jenis = array();
			foreach($data_grafic as $index => $peserta)
			{
				$data_per_sertifikasi_jenis[$peserta['tahun']] = $peserta;
			}
			
			$series = array();
			foreach($data_per_sertifikasi_jenis as $i => $v)
			{
				$series[$i] = $v['total_peserta'];
				$this->highcharts_lib->push_xAxis_value($i);
			}
				
			
			$this->highcharts_lib->add_serie('Kota/Kabupaten', $series,'',array('enabled' => true,
																							'rotation' => 0,
																							'color' => '#000000',
																							'align' => 'center'
																							));
		}
		
		$s_graph = "";	
		if($filtered)
			$s_graph = $this->highcharts_lib->render("100%","400px");	
		
		#$this->highcharts_lib->debug();
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/grafic',array('action' => base_url().'certification_results/grafic/','data_grafic' => $s_graph,'response' => '', 'page_title' => 'Grafik Peserta'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'sertifikasi',
						'fields' => array(	
											array(
													'name' => 'peserta_id',
													'label' => 'Peserta',
													'id' => 'peserta_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'sertifikasi_peserta',
													'select' => array('peserta_sertifikasi_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'peserta_sertifikasi_id',
													'rules' => 'required'
												),
											array(
													'name' => 'accessor_id',
													'label' => 'Asesor',
													'id' => 'accessor_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'sertifikasi_asesor',
													'select' => array('asesor_id AS value','concat("[",nip,"] - ",nama) AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'asesor_id',
													'rules' => 'required'
												),
											array(
													'name' => 'jenis_sertifikasi',
													'label' => 'Jenis Sertifikasi',
													'id' => 'jenis_sertifikasi',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'sertifikasi_jenis',
													'select' => array('kode_sertifikasi AS value','nama_sertifikasi AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'kode_sertifikasi',
													'rules' => 'required'
												),
											array(
													'name' => 'tanggal',
													'label' => 'Tanggal',
													'id' => 'tanggal',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'status',
													'label' => 'Status',
													'id' => 'status',
													'value' => '',
													'type' => 'selectbox',
													'options' => array('' => '---- Select Option ----','lanjut' => 'Dilanjutkan','tidak lanjut' => 'Tidak dilanjutkan'),
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												)
										),
									'primary_key' => 'sertifikasi_id',
									'list_style' => ' style="width:260px;text-align:center;" '
					);
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
