<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bintek_member extends CI_Controller {

	var $init = array();
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('default/listing',array('response' => '','page_title' => 'Peserta Bintek'));
		else
			$this->load->view('dashboard/dashboard');
			
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'member_bintek/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response,'page_title' => 'Peserta Bintek'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title' => 'Peserta Bintek'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function register()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		$all_sessions = $this->session->all_userdata();
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title' => 'Peserta Bintek'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '','page_title' => 'Peserta Bintek'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/listing',array('response' => '','page_title' => 'Peserta Bintek'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'bintek_peserta',
						'fields' => array(
												array(
													'name' => 'lembaga',
													'label' => 'Lembaga',
													'id' => 'lembaga',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'lembaga',
													'select' => array('lembaga_id AS value','nmlembaga AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'lembaga_id',
													'rules' => 'required'
												),
												array(
													'name' => 'nama',
													'label' => 'Nama',
													'id' => 'nama',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'tempat_lahir',
													'label' => 'Tempat Lahir',
													'id' => 'tempat_lahir',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'tanggal_lahir',
													'label' => 'Tanggal Lahir',
													'id' => 'tanggal_lahir',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'unit_kerja',
													'label' => 'Unit Kerja',
													'id' => 'unit_kerja',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'pendidikan_terakhir',
													'label' => 'Pendidikan Terakhir',
													'id' => 'pendidikan_terakhir',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'jurusan',
													'label' => 'Jurusan',
													'id' => 'jurusan',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'status_kepegawaian',
													'label' => 'Status Kepegawaian',
													'id' => 'status_kepegawaian',
													'value' => '',
													'type' => 'selectbox',
													'options' => array('' => '---- Select Option ----','tetap' => 'Tetap','tidak tetap' => 'Tidak Tetap'),
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'nip',
													'label' => 'NIP',
													'id' => 'nip',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'no_ktp',
													'label' => 'No. KTP',
													'id' => 'no_ktp',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'jabatan',
													'label' => 'Jabatan',
													'id' => 'jabatan',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'pangkat',
													'label' => 'Pangkat',
													'id' => 'pangkat',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'golongan',
													'label' => 'Golongan',
													'id' => 'golongan',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'bidang_pekerjaan',
													'label' => 'Bidang Pekerjaan',
													'id' => 'bidang_pekerjaan',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'masa_kerja',
													'label' => 'Masa Kerja',
													'id' => 'masa_kerja',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'mulai_tahun',
													'label' => 'Mulai Tahun',
													'id' => 'mulai_tahun',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'alamat_kantor',
													'label' => 'Alamat Kantor',
													'id' => 'alamat_kantor',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'alamat_rumah',
													'label' => 'Alamat Rumah',
													'id' => 'alamat_rumah',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'telepon_kantor',
													'label' => 'Telepon Kantor',
													'id' => 'telepon_kantor',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'telepon_rumah',
													'label' => 'Telepon Rumah',
													'id' => 'telepon_rumah',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'hp',
													'label' => 'No. Handphone',
													'id' => 'hp',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'email',
													'label' => 'email',
													'id' => 'email',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'bank',
													'label' => 'Nama Bank',
													'id' => 'bank',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'no_rekening',
													'label' => 'No. Rekening',
													'id' => 'no_rekening',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
												array(
													'name' => 'foto',
													'label' => 'foto',
													'id' => 'foto',
													'value' => '',
													'type' => 'file',
													'use_search' => false,
													'use_listing' => false,
													'config_upload' => array('upload_path' => getcwd().'/uploads/media/bintek_member/','encrypt_name' => true,'allowed_types' =>  'gif|jpg|png'),
													'rules' => '',
													'list_style' => 'width="100" align="center"'
												),
											),
											
									'bulk_options' => array('download_excel','download_pdf','printing'),
									'primary_key' => 'peserta_bintek_id'
					);
		
		$curr_user = $this->user_access->get_user_id();
		$curr_level = $this->user_access->get_level();
		if($curr_level == 3)
		{
			$this->db->where(array("user_id" => $curr_user));
			$q = $this->db->get("lembaga");
			$lembaga = $q->row_array();
			$lembaga_id = (isset($lembaga['lembaga_id']) and !empty($lembaga['lembaga_id']))?$lembaga['lembaga_id']:"-";
			$init['where'] = " AND lembaga = '".$lembaga_id."'";
			if(isset($init['fields'][0]['name']) and $init['fields'][0]['name'] == 'lembaga')
			{
				unset($init['fields'][0]);
			}
		}
		$this->init = $init;
	}
	
	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		$query = $this->data->get_query();
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('query' => $query,'response' => '','page_title' => 'Data Peserta Bintek'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
		
	public function certificate()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_evaluation_instructure',array('response' => '', 'page_title' => 'Evaluasi Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function citation()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_evaluation_instructure',array('response' => '', 'page_title' => 'Evaluasi Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function grafic()
	{
		
		$y = array('' => 'Tahun');
		$m = array('' => 'Bulan');
		$d = array('' => 'Tanggal');
		for($i = YEAR_START; $i <= date("Y"); $i++)
		{
			$y[str_pad($i,4,"0",STR_PAD_LEFT)] = str_pad($i,4,"0",STR_PAD_LEFT);
		}
		for($i = 1; $i <= 12; $i++)
		{
			$m[str_pad($i,2,"0",STR_PAD_LEFT)] = str_pad($i,2,"0",STR_PAD_LEFT);
		}
		for($i = 1; $i <= 31; $i++)
		{
			$d[str_pad($i,2,"0",STR_PAD_LEFT)] = str_pad($i,2,"0",STR_PAD_LEFT);
		}
		$options_date = array('y' => $y,'m' => $m,'d' => $d);
		$this->_config();
		$this->init['fields'] = array();
		$this->init['fields'][2] = array(
													'name' => 'grafik_data',
													'label' => 'Grafik Data',
													'id' => 'grafik_data',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'options' => array('perwilayah' => 'Perwilayah','perbintek' => 'Perjenis Bintek','pertahun' => 'Pertahun'),
													'rules' => 'required'
												);
		$this->init['fields'][3] = array(
													'name' => 'tanggal',
													'label' => 'Tanggal',
													'id' => 'tanggal',
													'value' => '',
													'type' => 'range_year_selectbox',
													'options_from' => $options_date,
													'options_to' => $options_date,
													'class' => 'date_range',
													'style' => 'width:auto;',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												);
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->load->library('highcharts_lib');

		# Set the graph using methods chaining
		$this->highcharts_lib
		->set_graph_type('column')
		->toggle_legend(TRUE)
		->set_legend_option(array("layout" => "vertical",
													"align" => "right",
													"verticalAlign" => "middle",
													"borderWidth" => "1",
													"enabled" => false))
		->display_shadow(TRUE)
		->set_xlabels_options('rotation', -35)
		->set_xlabels_options('align', "right")
		->set_yAxis(array(), 'Jumlah');
		//->set_xAxis(array(), 'Tanggal');
		
		$query_both = "SELECT *,count(bintek_peserta.peserta_bintek_id) total_peserta FROM relation_bintek_member,jadwal_bintek,bintek_peserta,lembaga WHERE relation_bintek_member.jadwal_bintek_id = jadwal_bintek.jadwal_bintek_id AND bintek_peserta.peserta_bintek_id = relation_bintek_member.member_id AND lembaga.lembaga_id = bintek_peserta.lembaga ";
		$filter = $this->data->get_filter("");

		$post_data = $filter;
		$filtered = false;
		
		if(isset($post_data['grafik_data']) and $post_data['grafik_data'] == 'perwilayah')
		{
			$filtered = true;
			$where = "";
			if(isset($post_data['tanggal']))
			{
				$year_start = (isset($post_data['tanggal']['from']['y']) and !empty($post_data['tanggal']['from']['y']))?$post_data['tanggal']['from']['y']:'';
				$year_end = (isset($post_data['tanggal']['to']['y']) and !empty($post_data['tanggal']['to']['y']))?$post_data['tanggal']['to']['y']:$year_start;
				$title_year = "";
				if($year_start.$year_end != "")
				{
					$where .= " AND (relation_bintek_member.tanggal_registrasi BETWEEN '".$year_start."-01-01' AND '".$year_end."-12-31') ";
					$title_year = $year_start;
					if($year_start != $year_end and $year_end != "")
						$title_year .= ' - ' . $year_end;
					
					if(!empty($title_year))
					{
						$title_year = ' Tahun '.$title_year;
					}
				}
				$this->highcharts_lib->set_graph_title('Grafik Peserta Perwilayah'.$title_year);
			}
			$que = $query_both . $where . ' GROUP BY kdkab ORDER BY nmlembaga ASC';

			$q_both = $this->db->query($que);
			$data_grafic = $q_both->result_array();
			
			$data_per_propinsi = array();
			foreach($data_grafic as $index => $peserta)
			{
				$nama = $this->master->get_value('propinsi','nama',array('kdprop' => $peserta['kdprop']));
				$data_per_propinsi[$nama] = $peserta;
			}
			
			$data_per_kabupaten = array();
			foreach($data_grafic as $index => $peserta)
			{
				$nama_prop = $this->master->get_value('propinsi','nama',array('kdprop' => $peserta['kdprop']));
				$data_per_propinsi[$nama_prop] = $nama_prop;
				$nama = $this->master->get_value('kabupaten','nama',array('kdkab' => $peserta['kdkab']));
				$data_per_kabupaten[$nama] = $peserta;
			}
			
			$series = array();
			foreach($data_per_kabupaten as $i => $v)
			{
				$series[$i] = $v['total_peserta'];
				$this->highcharts_lib->push_xAxis_value($i);
			}
				
			
			$this->highcharts_lib->add_serie('Kota/Kabupaten', $series,'',array('enabled' => true,
																							'rotation' => 0,
																							'color' => '#000000',
																							'align' => 'center'
																							));
		}
		
		if(isset($post_data['grafik_data']) and $post_data['grafik_data'] == 'perbintek')
		{
			$filtered = true;
			$where = "";
			if(isset($post_data['tanggal']))
			{
				$year_start = (isset($post_data['tanggal']['from']['y']) and !empty($post_data['tanggal']['from']['y']))?$post_data['tanggal']['from']['y']:'';
				$year_end = (isset($post_data['tanggal']['to']['y']) and !empty($post_data['tanggal']['to']['y']))?$post_data['tanggal']['to']['y']:$year_start;
				$title_year = "";
				if($year_start.$year_end != "")
				{
					$where .= " AND (relation_bintek_member.tanggal_registrasi BETWEEN '".$year_start."-01-01' AND '".$year_end."-12-31') ";
					$title_year = $year_start;
					if($year_start != $year_end and $year_end != "")
						$title_year .= ' - ' . $year_end;
					
					if(!empty($title_year))
					{
						$title_year = ' Tahun '.$title_year;
					}
				}
				$this->highcharts_lib->set_graph_title('Grafik Peserta Jenis Bintek'.$title_year);
			}
			
			$query_both = "SELECT *,count(bintek_peserta.peserta_bintek_id) total_peserta FROM relation_bintek_member,jadwal_bintek,bintek_peserta,lembaga WHERE relation_bintek_member.jadwal_bintek_id = jadwal_bintek.jadwal_bintek_id AND bintek_peserta.peserta_bintek_id = relation_bintek_member.member_id AND lembaga.lembaga_id = bintek_peserta.lembaga ";
			$que = $query_both . $where . ' GROUP BY kode_bintek ORDER BY kode_bintek ASC';

			$q_both = $this->db->query($que);
			$data_grafic = $q_both->result_array();
						
			$data_per_bintek_jenis = array();
			foreach($data_grafic as $index => $peserta)
			{
				$nama = $this->master->get_value('bintek','nama_bintek',array('kode_bintek' => $peserta['kode_bintek']));
				$data_per_bintek_jenis[$nama] = $peserta;
			}
			
			$series = array();
			foreach($data_per_bintek_jenis as $i => $v)
			{
				$series[$i] = $v['total_peserta'];
				$this->highcharts_lib->push_xAxis_value($i);
			}
				
			
			$this->highcharts_lib->add_serie('Kota/Kabupaten', $series,'',array('enabled' => true,
																							'rotation' => 0,
																							'color' => '#000000',
																							'align' => 'center'
																							));
		}
		
		if(isset($post_data['grafik_data']) and $post_data['grafik_data'] == 'pertahun')
		{
			$filtered = true;
			$where = "";
			if(isset($post_data['tanggal']))
			{
				$year_start = (isset($post_data['tanggal']['from']['y']) and !empty($post_data['tanggal']['from']['y']))?$post_data['tanggal']['from']['y']:'';
				$year_end = (isset($post_data['tanggal']['to']['y']) and !empty($post_data['tanggal']['to']['y']))?$post_data['tanggal']['to']['y']:$year_start;
				$title_year = "";
				if($year_start.$year_end != "")
				{
					$where .= " AND (relation_bintek_member.tanggal_registrasi BETWEEN '".$year_start."-01-01' AND '".$year_end."-12-31') ";
					$title_year = $year_start;
					if($year_start != $year_end and $year_end != "")
						$title_year .= ' - ' . $year_end;
					
					if(!empty($title_year))
					{
						$title_year = ' Tahun '.$title_year;
					}
				}
				$this->highcharts_lib->set_graph_title('Grafik Peserta Jenis Sertifikasi'.$title_year);
			}
			
			$query_both = "SELECT *,(year(relation_bintek_member.tanggal_registrasi)) tahun,count(bintek_peserta.peserta_bintek_id) total_peserta FROM relation_bintek_member,jadwal_bintek,bintek_peserta,lembaga WHERE relation_bintek_member.jadwal_bintek_id = jadwal_bintek.jadwal_bintek_id AND bintek_peserta.peserta_bintek_id = relation_bintek_member.member_id AND lembaga.lembaga_id = bintek_peserta.lembaga ";
			$que = $query_both . $where . ' GROUP BY tahun ORDER BY kode_bintek ASC';

			$q_both = $this->db->query($que);
			$data_grafic = $q_both->result_array();
						
			$data_per_bintek_jenis = array();
			foreach($data_grafic as $index => $peserta)
			{
				$data_per_bintek_jenis[$peserta['tahun']] = $peserta;
			}
			
			$series = array();
			foreach($data_per_bintek_jenis as $i => $v)
			{
				$series[$i] = $v['total_peserta'];
				$this->highcharts_lib->push_xAxis_value($i);
			}
				
			
			$this->highcharts_lib->add_serie('Kota/Kabupaten', $series,'',array('enabled' => true,
																							'rotation' => 0,
																							'color' => '#000000',
																							'align' => 'center'
																							));
		}
		
		$s_graph = "";	
		if($filtered)
			$s_graph = $this->highcharts_lib->render("100%","400px");	
		
		#$this->highcharts_lib->debug();
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/grafic',array('action' => base_url().'bintek_member/grafic/','data_grafic' => $s_graph,'response' => '', 'page_title' => 'Grafik Peserta'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	function _bulk_actions()
	{
		$action = $this->input->post("bulk_action");
		if(!empty($action))
		{
			$action;
		}
	}
		
	function _hook_show_bulk_allowed_is_ajax_set0($param = "")
	{
		return 0;
	}
		
	function _hook_show_bulk_allowed_is_ajax_set1($param = "")
	{
		return 1;
	}
		
	function _hook_do_add($param = "")
	{
		$curr_user = $this->user_access->get_user_id();
		$curr_level = $this->user_access->get_level();
		if($curr_level == 3)
		{
			$this->db->where(array("user_id" => $curr_user));
			$q = $this->db->get("lembaga");
			$lembaga = $q->row_array();
			$lembaga_id = (isset($lembaga['lembaga_id']) and !empty($lembaga['lembaga_id']))?$lembaga['lembaga_id']:"-";
			$param['lembaga'] = $lembaga_id;
		}
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		$curr_user = $this->user_access->get_user_id();
		$curr_level = $this->user_access->get_level();
		if($curr_level == 3)
		{
			$this->db->where(array("user_id" => $curr_user));
			$q = $this->db->get("lembaga");
			$lembaga = $q->row_array();
			$lembaga_id = (isset($lembaga['lembaga_id']) and !empty($lembaga['lembaga_id']))?$lembaga['lembaga_id']:"-";
			$param['lembaga'] = $lembaga_id;
		}
		if(isset($param['foto']) and empty($param['foto']))
			unset($param['foto']);
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}

		
	public function download_excel()
	{
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		$is_ajax = $this->input->post('is_ajax');
		
		
		$query = $this->data->get_query();
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
		if($is_login)			
			$output = $this->load->view('bintek/report_member',array('query' => $query,'response' => '','page_title' => 'DATA-DATA PESERTA'),true);

			//$output = $this->load->view('default/report',array('response' => '','page_title' => 'Peserta Bintek'),true);
		
		$this->report->download_excel($output,date("ymdhis"));
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		exit;
	}
		
	public function download_pdf()
	{
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		if($is_login)			
			$output = $this->load->view('bintek/report_member',array('response' => '','page_title' => 'DATA-DATA PESERTA'),true);
		
		$this->report->download_pdf($output,date("ymdhis"), 'L', '', 'A3');
		exit;
	}
		
	public function printing()
	{
		
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/report_member',array('response' => '','page_title' => 'DATA-DATA PESERTA'));
		
		echo '<script type="text/javascript">window.print();</script>';
	}
	
	function _hook_report(){
		$init = $this->init;
		if(is_array($init['fields'])){
			foreach($init['fields'] as $idx => $field){
				if($field['name'] != 'foto') 
					$init['fields'][$idx]['use_listing'] = true;
			}
		}
		
		return $init;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
