<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ukom_certificate extends MY_Controller {

	var $init = array();
	var $data_evaluation = array();
	var $data_member = array();
	var $data_schedule = array();
	var $data_sertifikat = array();
	var $no_id = 1;
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_listing_output',array($this,'_hook_create_listing_output'));
		$this->hook->add_action('hook_create_listing_rows',array($this,'_hook_create_listing_rows'));
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('bintek/listing_certificate',array('response' => '', 'page_title' => 'Data Sertifikat'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function _delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_field_output_nilai',array($this,'_hook_create_form_field_output_nilai'));
		$this->hook->add_action('hook_create_form_field_output_jadwal_id',array($this,'_hook_create_form_field_output_jadwal_id'));
		$this->hook->add_action('hook_create_form_field_output_member_id',array($this,'_hook_create_form_field_output_member_id'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response, 'page_title' => 'Data Sertifikat'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/add_certificate',array('response' => $response, 'page_title' => 'Data Sertifikat'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		$this->db->where(array("name" => 'ukom_sertifikat_depan'));
		$data = $this->db->get("setting_certificates");
		$data = $data->row_array();
		$data['data'] = (isset($data['data']))?json_decode($data['data'],true):array();
		$data['edit_mode'] = false;
		$output2 = "";
		
		$q_evaluation = $this->db->query('SELECT ujikom.*,ujikom_certificates.* FROM (`ujikom` LEFT JOIN ujikom_certificates ON  ujikom.ujikom_id = ujikom_certificates.member_eval_id  AND ujikom.peserta_id = ujikom_certificates.id_member AND ujikom_certificates.certificate_type = "sertifikat") WHERE ujikom_id = "'.$object_id.'"');
		
		$this->data_evaluation = $q_evaluation->row_array();
		
		$this->data_evaluation['member_id'] = (isset($this->data_evaluation['member_id']))?$this->data_evaluation['member_id']:'';
		$this->db->where(array('peserta_ujikom_id' => $this->data_evaluation['member_id']));
		$q_member = $this->db->get('ujikom_peserta');
		$this->data_member = $q_member->row_array();
		
		$this->data_evaluation['member_id'] = (isset($this->data_evaluation['member_id']))?$this->data_evaluation['member_id']:'';
		$this->db->where(array('jadwal_bintek_id' => $this->data_evaluation['id_jadwal']));
		$q_schedule = $this->db->get('jadwal_bintek');
		$this->data_schedule = $q_schedule->row_array();
		
		$data_sertifikat = array_merge($this->data_member,$this->data_schedule,$this->data_evaluation);
		$tanggal_bintek = (isset($data_sertifikat['tglawal']))?$this->data->human_date($data_sertifikat['tglawal']):'';
		$tanggal_bintek .= (isset($data_sertifikat['tglakhir']))?' s/d ' . $this->data->human_date($data_sertifikat['tglakhir']):'';
		$lembaga = (isset($data_sertifikat['lembaga']))?$data_sertifikat['lembaga']:'';
		$lembaga = $this->data->get_value("lembaga","nmlembaga as label", "lembaga_id = '".$lembaga."'","");
		
		$hasil_sertifikasi = (isset($data_sertifikat['avg_nilai']))?$data_sertifikat['avg_nilai']:'';
		$hasil_sertifikasi = ($hasil_sertifikasi >= 60)?'LULUS':'TIDAK LULUS';
		$this->data_sertifikat = array(
										 'nomor_sertifikat' => (isset($data_sertifikat['no_certificate']))?$data_sertifikat['no_certificate']:'',
										 'nama_peserta' => (isset($data_sertifikat['nama']))?$data_sertifikat['nama']:'',
										 'nik_peserta' => (isset($data_sertifikat['nip']))?$data_sertifikat['nip']:'',
										 'tempat_lahir_peserta' => (isset($data_sertifikat['tempat_lahir']))?$data_sertifikat['tempat_lahir']:'',
										 'tanggal_lahir_peserta' => (isset($data_sertifikat['tanggal_lahir']))?$this->data->human_date($data_sertifikat['tanggal_lahir']):'',
										 'pangkat_peserta' => (isset($data_sertifikat['pangkat']))?$data_sertifikat['pangkat']:'',
										 'golongan_peserta' => (isset($data_sertifikat['golongan']))?$data_sertifikat['golongan']:'',
										 'jabatan_peserta' => (isset($data_sertifikat['jabatan']))?$data_sertifikat['jabatan']:'',
										 'instansi_peserta' => $lembaga,
										 'hasil_sertifikasi' => $hasil_sertifikasi,
										 'kualifikasi_sertifikasi' => (isset($data_sertifikat['predikat']))?$data_sertifikat['predikat']:'',
										 'ttd_tanggal_sertifikat' => (isset($data_sertifikat['ttd_tanggal']))?$this->data->human_date($data_sertifikat['ttd_tanggal']):'',
										 'nama_bintek' => (isset($data_sertifikat['judul_bintek']))?$data_sertifikat['judul_bintek']:'',
										 'penyelenggara_bintek' => (isset($data_sertifikat['penyelenggara']))?$data_sertifikat['penyelenggara']:'',
										 'tanggal_bintek' => $tanggal_bintek,
										 'tempat_bintek' => (isset($data_sertifikat['tempat']))?$data_sertifikat['tempat']:'',
										 'photo' => (isset($data_sertifikat['foto']))?$data_sertifikat['foto']:''
									);
		if(empty($is_ajax))
		{
			$output2 .= $this->load->view('header','',true);
			$output2 .= $this->load->view('topbar','',true);
		}
		
		$is_login = $this->user_access->is_login();
		$output = "";
		$response = "";

		if($is_login)			
			$output = $this->load->view('templates/'.$data['name'],array('data' => $data,'response' => $response,'page_title' => 'View Sertifikat'),true);
		else
		{
			$output2 .= $this->load->view('dashboard/dashboard','',true);
		}
		
		
		$output = preg_replace_callback('/\{\$(.*?)\$\}/s', 'self::_inject_cb2', $output);
		$output = preg_replace_callback('/\{\%(.*?)\%\}/s', 'self::_inject_cb', $output);
		#$output = preg_match_all('/\{\$(.*?)\$\}/s', $output,$matches);
		#$keys = $matches[1];
		
		$output2 .= $output;
		if(empty($is_ajax))
		{
			$output2 .= $this->load->view('bottombar','',true);
			$output2 .= $this->load->view('footer','',true);
		}
		echo $output2;
	}
		
	function _inject_cb($match) {
		$key = (isset($match[1]))?$match[1]:$match[0];
		$key = trim($key,'{%}');
		$match = $match[0];
		$this->no_id = $this->no_id+1;
		$match = str_replace(array('{%','%}'),array('<span class="no_text_editor" id="text_editor_'.$this->no_id.'" key="'.$key.'">','</span>'),$match);
		return $match;
	}
	function _inject_cb2($match) {
		$key = (isset($match[1]))?$match[1]:$match[0];
		$key = trim($key,'{$}');
		$match = $match[0];
		#$match = str_replace(array('{$','$}'),array('<span class="text_editor" id="text_editor_'.$this->no_id.'" key="'.$key.'">','</span>'),$match);
		if(isset($this->data_sertifikat[$key]))
			$match = str_replace('{$'.$key.'$}',$this->data_sertifikat[$key],$match);
		return $match;
	}
	
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_listing_output',array($this,'_hook_create_listing_output'));
		$this->hook->add_action('hook_create_listing_rows',array($this,'_hook_create_listing_rows'));
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_certificate',array('response' => '', 'page_title' => 'Data Sertifikat'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
		
	public function citation()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_certificate',array('response' => '', 'page_title' => 'Data Sertifikat'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function grafic()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_certificate',array('response' => '', 'page_title' => 'Data Sertifikat'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{	
		$unsur_penilaian = $this->db->get('ujikom_kompetensi')->result_array();
		$column_multiple_field = array();
		foreach($unsur_penilaian as $idx => $up){
			$column_multiple_field[] = array(
				'key_option' => $up['kompetensi_id'],
				'label' => $up['judul_kompetensi']
			);
		}
		
		$init = array(	'query' => 'SELECT ujikom.*,ujikom_certificates.no_certificate,ujikom_certificates.ttd_tanggal FROM (`ujikom` LEFT JOIN ujikom_certificates ON  ujikom.ujikom_id = ujikom_certificates.member_eval_id  AND ujikom.peserta_id = ujikom_certificates.id_member AND ujikom_certificates.certificate_type = "sertifikat") ',
						'table' => 'ujikom_certificates RIGHT JOIN ujikom ON  ujikom.ujikom_id = ujikom_certificates.member_eval_id AND ujikom.peserta_id = ujikom_certificates.id_member AND ujikom_certificates.certificate_type = "sertifikat"',
						'fields' => array(
											array(
													'name' => 'peserta_id',
													'label' => 'Peserta',
													'id' => 'peserta_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'ujikom_peserta',
													'select' => array('peserta_ujikom_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'peserta_ujikom_id',
													'js_connect_to' => array(	
															'id_field_parent' => 'jadwal_id',
															'table' => 'ujikom_peserta A,jadwal_bintek B, relation_ujikom_peserta C',
															'select' => 'A.peserta_ujikom_id AS value,A.nama AS label',
															'where' => " A.peserta_ujikom_id = C.peserta_ujikom_id AND B.jadwal_bintek_id = C.jadwal_bintek_id ",
															'foreign_key' => 'B.jadwal_bintek_id'
													),
													'rules' => 'required'
												),
											array(
													'name' => 'nilai',
													'label' => 'Nilai',
													'id' => 'nilai',
													'value' => '',
													'type' => 'text',
													'columns' => $column_multiple_field,
													'use_search' => false,
													'use_listing' => true,
													'rules' => ''
												),
											array(
												'name' => 'no_certificate',
												'label' => 'No Sertifikat',
												'id' => 'no_certificate',
												'value' => '',
												'type' => 'text',
												'use_search' => true,
												'use_listing' => true,
												'rules' => 'required'
											),
											array(
												'name' => 'ttd_tanggal',
												'label' => 'Tanggal Tanda Tangan',
												'id' => 'ttd_tanggal',
												'value' => '',
												'type' => 'text',
												'class' => 'input_date',
												'use_search' => false,
												'use_listing' => false,
												'rules' => 'required'
											)
						),
						'primary_key' => 'ujikom_id'
					);
		$this->init = $init;
	}
	
	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_listing_output',array($this,'_hook_create_listing_output'));
		$this->hook->add_action('hook_create_listing_rows',array($this,'_hook_create_listing_rows'));

		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('ujikom/listing_certificate',array('response' => '', 'page_title' => 'Data Hasil Uji Kompetensi'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		$param[$this->data->primary_key] = $this->data->primary_key_value;		
		$data = array(	'id_member' => $param['peserta_id'],
						'total' => $param['total'],
						'avg_nilai' => $param['avg_nilai'],
						'predikat' => $param['predikat'],
						'no_certificate' => $param['no_certificate'],
						'ttd_tanggal' => $param['ttd_tanggal'],
						'member_eval_id' => $param['ujikom_id'],
						'certificate_type' => 'sertifikat');
		
		$where = array(	'id_member' => $param['peserta_id'],
						'member_eval_id' => $param['ujikom_id']
					   );
		$this->db->where($where);
		$q_check_bintek_sert = $this->db->get("ujikom_certificates");
		if($q_check_bintek_sert->num_rows() > 0)
		{
			$this->db->where($where);
			$this->db->update("ujikom_certificates",$data);
		}else{
			$this->db->insert("ujikom_certificates",$data);
		}
		
		$param = array('peserta_id' => $param['peserta_id']);
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
	
	function _hook_create_listing_rows($param = "")
	{
		$dom = new domDocument;
		$dom->loadHTML($param);
		$param = '';
		if(!$dom) {
		}else{
			$tr = $dom->getElementsByTagName("tr");
			foreach($tr as $the_tr)
			{
				$tmp = $the_tr->getElementsByTagName("td");
				$tmp_json = (isset($tmp->item(3)->nodeValue))?$tmp->item(3)->nodeValue:"";
				$tmp_json = json_decode($tmp_json,true);
				$total = 0;
				$rata2 = 0;
				$predikat = '-';
				if(is_array($tmp_json))
				{
					foreach($tmp_json as $index => $col)
					{
						$total += $col['value'];
					}
					$rata2 = number_format($total/count($tmp_json),2);
					$predikat = $this->data->get_qualification($rata2);
				}
				
				$param .=  '<tr>';
				$param .=  $dom->saveXML($tmp->item(0));
				$param .= '<td>' . $tmp->item(1)->nodeValue . '</td>';
				$param .= '<td>' . $tmp->item(2)->nodeValue . '</td>';
				$param .= '<td align="center">' . $total . '</td>';
				$param .= '<td align="center">' . $rata2 . '</td>';
				$param .= '<td align="center">' . $predikat . '</td>';
				$param .= '<td>' . $tmp->item(4)->nodeValue . '</td>';
				$param .= $dom->saveXML($tmp->item(5));
				$param .=  '</tr>';
			}
			
		}
		return $param;
	}
	
	function _hook_create_listing_output($param = "")
	{
		$param = str_replace('<th>Nilai</th>','<th>Total Nilai</th><th>Nilai Rata-Rata</th><th>Predikat</th>',$param);
		return $param;
	}
	
	function _hook_create_form_field_output_nilai($param = "")
	{
		$dom = new domDocument;
		$dom->loadHTML($param);
		if(!$dom) {
		}else{
			$tmp = $dom->getElementsByTagName("input");
			$value = $tmp->item(0);
			$tmp_json = $value->getAttribute('value');
			$tmp_json = json_decode($tmp_json,true);
			$total = 0;
			$rata2 = 0;
			$predikat = '-';
			if(is_array($tmp_json))
			{
				foreach($tmp_json as $index => $col)
				{
					$total += $col['value'];
				}
				$rata2 = number_format($total/count($tmp_json),2);
				$predikat = $this->data->get_qualification($rata2);
			}
			
			$param = '<label for="nilai">Total Nilai</label><span class="value_view"> : <input type="hidden" name="data[total]" value="'.$total.'" id="total" maxlength="" size="" style="padding:5px;" class="input_text ">'.$total.'</span><br class="fclear">';
			$param .= '<label for="nilai">Nilai Rata-rata</label><span class="value_view"> : <input type="hidden" name="data[avg_nilai]" value="'.$rata2.'" id="avg_nilai" maxlength="" size="" style="padding:5px;" class="input_text ">'.$rata2.'</span><br class="fclear">';
			$param .= '<label for="nilai">Predikat</label><span class="value_view"> : <input type="hidden" name="data[predikat]" value="'.$predikat.'" id="predikat" maxlength="" size="" style="padding:5px;" class="input_text ">'.$predikat.'</span><br class="fclear">';
			
			
		}
		return $param;
	}
	
	function _hook_create_form_field_output_jadwal_id($param = "")
	{
		$dom = new domDocument;
		$dom->loadHTML($param);
		if(!$dom) {
		}else{
			$tmp = $dom->getElementsByTagName("option");
			$value = '';
			$text = '';
			for ($i = 0; $i < $tmp->length; $i++ ) 
			{
			  if ($tmp->item($i)->hasAttribute('selected') && $tmp->item($i)->getAttribute('selected') === "selected") 
			  {
					$value = $tmp->item($i)->getAttribute('value');
					$text = $tmp->item($i)->nodeValue;
			  }
			}
			
			$param = '<label for="nilai">Bintek</label><span class="value_view"> : <input type="hidden" name="data[jadwal_id]" value="'.$value.'" id="jadwal_id" maxlength="" size="" style="padding:5px;" class="input_text ">'.$text.'</span><br class="fclear">';
		}
		return $param;
	}
	
	function _hook_create_form_field_output_member_id($param = "")
	{
		$dom = new domDocument;
		$dom->loadHTML($param);
		if(!$dom) {
		}else{
			$tmp = $dom->getElementsByTagName("option");
			$value = '';
			$text = '';
			for ($i = 0; $i < $tmp->length; $i++ ) 
			{
			  if ($tmp->item($i)->hasAttribute('selected') && $tmp->item($i)->getAttribute('selected') === "selected") 
			  {
					$value = $tmp->item($i)->getAttribute('value');
					$text = $tmp->item($i)->nodeValue;
			  }
			}
			
			$param = '<label for="nilai">Peserta</label><span class="value_view"> : <input type="hidden" name="data[member_id]" value="'.$value.'" id="member_id" maxlength="" size="" style="padding:5px;" class="input_text ">'.$text.'</span><br class="fclear">';
		}
		return $param;
	}
	
	public function unsur_penilaian()
	{
		redirect('/bintek_ujikom_kompetensi');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
