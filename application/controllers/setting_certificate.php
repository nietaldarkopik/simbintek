<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting_certificate extends MY_Controller {

	var $init = array();
	var $no_id = 0;
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_show_bulk_allowed_output',array($this,'_hook_show_bulk_allowed_output'));
		$this->hook->add_action('hook_create_listing_action',array($this,'_hook_create_listing_action'));
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('bintek/listing_certificate',array('response' => '','page_title' => 'Sertifikat/Surat Keterangan'));
		else
			$this->load->view('dashboard/dashboard');
			
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function _delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'city/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		
		$key = $this->input->post("key");
		$content = $this->input->post("content");
		
		if(!empty($key))
		{
			$this->db->where(array("id" => $object_id));
			$data = $this->db->get("setting_certificates");
			$data = $data->row_array();
			$data['data'] = (isset($data['data']))?json_decode($data['data'],true):array();
			$data['data'][$key] = str_replace("_and_","&",$content);
			$data['data'] = json_encode($data['data']);
			
			$this->db->where(array("id" => $object_id));
			$q = $this->db->update("setting_certificates",$data);
			echo ($q)?1:0;
			exit;
		}
		
		$files = (isset($_FILES))?$_FILES:"";
		if(!empty($files))
		{
			$this->db->where(array("id" => $object_id));
			$data = $this->db->get("setting_certificates");
			$data = $data->row_array();
			$data['data'] = (isset($data['data']))?json_decode($data['data'],true):array();
			$background_name = (isset($data['data']['background']))?$data['data']['background']:"";
			
			$config['upload_path'] = FCPATH.'uploads/media/sertifikat/';
			$config['allowed_types'] = 'gif|jpg|png';
			#$config['max_size']	= '100';
			#$config['max_width']  = '1024';
			#$config['max_height']  = '768';
			$config['file_name']  = date("ymdhis").rand(0,1000);
			$config['remove_spaces']  = true;
			$config['encrypt_name']  = true;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('background'))
			{
				$error = $this->upload->display_errors();
				echo $error;
				exit;
			}
			else
			{
				$data_upload = $this->upload->data();
				if(file_exists($config['upload_path'].$background_name))
				{
					@unlink($config['upload_path'].$background_name);
				}
				$data['data']['background'] = $data_upload['file_name'];
				$data['data'] = json_encode($data['data']);
				
				$this->db->where(array("id" => $object_id));
				$q = $this->db->update("setting_certificates",$data);
				echo $data_upload['file_name'];
				exit;
			}
			echo 0;
			exit;
		}
		
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_show_bulk_allowed',array($this,'_hook_show_bulk_allowed_is_ajax_set0'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		$this->db->where(array("id" => $object_id));
		$data = $this->db->get("setting_certificates");
		$data = $data->row_array();
		$data['data'] = (isset($data['data']))?json_decode($data['data'],true):array();

		$output2 = "";
		if(empty($is_ajax))
		{
			$output2 .= $this->load->view('header','',true);
			$output2 .= $this->load->view('topbar','',true);
		}
	
		
		$is_login = $this->user_access->is_login();
		$output = "";
		if($is_login)			
			$output = $this->load->view('templates/'.$data['name'],array('data' => $data,'response' => $response,'page_title' => 'Edit Sertifikat/Surat Keterangan'),true);
		else
		{
			$output2 .= $this->load->view('dashboard/dashboard','',true);
		}
		
		$output = preg_replace_callback('/\{\%(.*?)\%\}/s', 'self::_inject_cb', $output);
		$output2 .= $output;
		if(empty($is_ajax))
		{
			$output2 .= $this->load->view('bottombar','',true);
			$output2 .= $this->load->view('footer','',true);
		}
		echo $output2;
	}
		
	public function _add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_show_bulk_allowed',array($this,'_hook_show_bulk_allowed_is_ajax_set0'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		$output2 = "";
		if(empty($is_ajax))
		{
			$output2 .= $this->load->view('header','',true);
			$output2 .= $this->load->view('topbar','',true);
		}
	
		
		$is_login = $this->user_access->is_login();
		$output = "";
		if($is_login)			
			$output = $this->load->view('templates/bintek_sertifikat_depan',array('response' => $response,'page_title' => 'Add Sertifikat/Surat Keterangan'),true);
		else
		{
			$output2 .= $this->load->view('dashboard/dashboard','',true);
		}
		
		#$output = preg_replace_callback('/\{\%\<p(.*)\<\/p\>\%\}/s', 'self::_inject_cb_tinymce', $output);
		$output = preg_replace_callback('/\{\%(.*?)\%\}/s', 'self::_inject_cb', $output);
		$output2 .= $output;
		if(empty($is_ajax))
		{
			$output2 .= $this->load->view('bottombar','',true);
			$output2 .= $this->load->view('footer','',true);
		}
		echo $output2;
	}
	
	function _inject_cb_tinymce($match) {
		global $no_id;
		$no_id = (empty($no_id))?1:$no_id;
		$key = (isset($match[1]))?$match[1]:$match[0];
		$key = trim($key,'{%}');
		$match = $match[0];
		$match = str_replace(array('{%','%}'),array('<span class="text_editor tinymce" id="text_editor_'.$no_id.'" key="'.$key.'">','</span>'),$match);
		$no_id++;
		return $match;
	}
	
	function _inject_cb($match) {
		$key = (isset($match[1]))?$match[1]:$match[0];
		$key = trim($key,'{%}');
		$match = $match[0];
		$this->no_id = $this->no_id+1;
		$match = str_replace(array('{%','%}'),array('<span class="text_editor" id="text_editor_'.$this->no_id.'" key="'.$key.'">','</span>'),$match);
		return $match;
	}
	
	public function _view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '','page_title' => 'Detail Sertifikat/Surat Keterangan'));
		else
			$this->load->view('dashboard/dashboard');
		
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_show_bulk_allowed_output',array($this,'_hook_show_bulk_allowed_output'));
		$this->hook->add_action('hook_create_listing_action',array($this,'_hook_create_listing_action'));
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_certificate',array('response' => '','page_title' => 'Sertifikat/Surat Keterangan'));
		else
			$this->load->view('dashboard/dashboard');
		
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'setting_certificates',
						'fields' => array(	
											array(
													'name' => 'title',
													'label' => 'Nama Sertifikat',
													'id' => 'title',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'data',
													'label' => 'Nama Kota/Kabupaten',
													'id' => 'data',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												)
										),
									'panel_function' => array('edit'),
									'bulk_options' => array('view'),
									'primary_key' => 'id',
									'list_style' => 'width="170"'
					);
		$this->init = $init;
	}
	
	function _hook_show_bulk_allowed_is_ajax_set0($param = "")
	{
		return 0;
	}
	
	function _hook_create_listing_action($param = "")
	{
		$param = str_replace("a_ajax","",$param);
		return $param;
	}
	
	function _hook_show_bulk_allowed_output($param = "")
	{
		return "";
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
