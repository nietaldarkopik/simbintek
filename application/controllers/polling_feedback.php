<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Polling_feedback extends MY_Controller {

	var $init = array();
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
				
		$this->load->view('header');
		$this->load->view('topbar');
		$this->load->view('default/listing');
		#$this->load->view('rightbar');
		$this->load->view('bottombar');
		$this->load->view('footer');
	}
	
	public function edit()
	{
		$this->load->view('header');
		$this->load->view('default/edit');
		$this->load->view('footer');
	}
	
	public function add()
	{
		$this->load->view('header');
		$this->load->view('default/add');
		$this->load->view('footer');
	}
	
	public function view()
	{
		$this->load->view('header');
		$this->load->view('default/view');
		$this->load->view('footer');
	}
	
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		#$this->load->view('header');
		$this->load->view('default/listing');
		#$this->load->view('footer');
	}
	
	public function _config($id_object = "")
	{
		$init = array(	'table' => 'users',
						'fields' => array(	
											array(
													'name' => 'user_name',
													'label' => 'Username',
													'id' => 'user_name',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true
												),
											array(
													'name' => 'email',
													'label' => 'Email',
													'id' => 'user_name',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true
												),
											array(
													'name' => 'user_level_id',
													'label' => 'User Level',
													'id' => 'user_level_id',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'user_levels',
													'select' => array('user_level_id AS value','user_level_name AS label'),
													'primary_key' => 'user_level_id'
												),
											array(
													'name' => 'status',
													'label' => 'Status',
													'id' => 'status',
													'value' => '',
													'type' => 'selectbox',
													'options' => array('' => '---- Select Option ----','active' => 'Active','not active' => 'Not Active'),
													'use_search' => true,
													'use_listing' => true
												)
										)
					);
		$this->init = $init;
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
