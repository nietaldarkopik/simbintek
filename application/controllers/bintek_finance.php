<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bintek_finance extends CI_Controller {

	var $init = array();
	
	public function index()
	{
		$this->_config();
				
		//add total field
		$this->init['fields'][] = array(
													'name' => 'total',
													'label' => 'Total',
													'id' => 'total',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => true,
													'rules' => 'required number'
												);
		
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('default/listing',array('response' => '', 'page_title' => 'Pendanaan'));
		else
			$this->load->view('dashboard/dashboard');
			
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response, 'page_title' => 'Pendanaan'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response, 'page_title' => 'Pendanaan'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
				
		//add total field
		$this->init['fields'][] = array(
													'name' => 'total',
													'label' => 'Total',
													'id' => 'total',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => true,
													'rules' => 'required number'
												);
		
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '', 'page_title' => 'Pendanaan'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		
		//add total field
		$this->init['fields'][] = array(
													'name' => 'total',
													'label' => 'Total',
													'id' => 'total',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => true,
													'rules' => 'required number'
												);		
		
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/listing',array('response' => '', 'page_title' => 'Pendanaan'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'pendanaan',
						'fields' => array(	
											array(
													'name' => 'jadwal_bintek_id',
													'label' => 'Jadwal Bintek',
													'id' => 'jadwal_bintek_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'jadwal_bintek',
													'select' => array('jadwal_bintek_id AS value','judul_bintek AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'jadwal_bintek_id',
													'rules' => 'required'
												),
											array(
													'name' => 'peserta_bintek_id',
													'label' => 'Peserta Bintek',
													'id' => 'peserta_bintek_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'bintek_peserta',
													'select' => array('peserta_bintek_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'peserta_bintek_id',
													'js_connect_to' => array(	
															'id_field_parent' => 'jadwal_bintek_id',
															'table' => 'bintek_peserta A,jadwal_bintek B, relation_bintek_member C',
															'select' => 'A.peserta_bintek_id AS value, A.nama AS label',
															'where' => " A.peserta_bintek_id = C.member_id AND B.jadwal_bintek_id = C.jadwal_bintek_id ",
															'foreign_key' => 'B.jadwal_bintek_id'
													),
													'rules' => 'required'
												),
											array(
													'name' => 'uang_makan',
													'label' => 'Uang Makan',
													'id' => 'uang_makan',
													'value' => '',
													'type' => 'text',
													'use_listing' => true,
													'rules' => 'required number'
												),
											array(
													'name' => 'uang_transport',
													'label' => 'Uang Transport',
													'id' => 'uang_transport',
													'value' => '',
													'type' => 'text',
													'use_listing' => true,
													'rules' => 'required number'
												),
											array(
													'name' => 'tanggal',
													'label' => 'Tanggal',
													'id' => 'tanggal',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
										),
									'bulk_options' => array('download_excel','download_pdf','printing'),
									'primary_key' => 'pendanaan_id'
					);
		$this->init = $init;
	}
	
	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		$query = $this->data->get_query();
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('query' => $query,'response' => '', 'page_title' => 'Pendanaan'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	

	public function download_excel()
	{
		$this->load->library("report");
		$this->_config();
		
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		$is_ajax = $this->input->post('is_ajax');
			$this->load->view('header');
			$this->load->view('topbar');
		if($is_login)			
			$output = $this->load->view('default/report',array('response' => 'Laporan Data Instruktur'),true);
		
		$this->report->download_excel($output,date("ymdhis"));
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		exit;
	}
		
	public function download_pdf()
	{
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		if($is_login)			
			$output = $this->load->view('default/report',array('response' => '','page_title' => 'Laporan Data ATK'),true);
		
		$this->report->download_pdf($output,date("ymdhis"));
		exit;
	}
		
	public function printing()
	{
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('response' => '', 'page_title' => 'Laporan Data Instruktur'));
		
		echo '<script type="text/javascript">window.print();</script>';
	}

	function _bulk_actions()
	{
		$action = $this->input->post("bulk_action");
		if(!empty($action))
		{
			$action;
		}
	}
	
	function _hook_show_bulk_allowed_is_ajax_set0($param = "")
	{
		return 0;
	}
		
	function _hook_show_bulk_allowed_is_ajax_set1($param = "")
	{
		return 1;
	}

	
	function _hook_report(){
		$init = $this->init;
		if(is_array($init['fields'])){
			foreach($init['fields'] as $idx => $field){
				$init['fields'][$idx]['use_listing'] = true;
			}
		}
		
		return $init;
	}	
	
	function _hook_do_add($param = "")
	{
		//add total to it
		$param['total'] = $param['uang_makan'] + $param['uang_transport'];
		return $param;
		
	}
	
	function _hook_do_edit($param = "")
	{
		//add total to it
		$param['total'] = $param['uang_makan'] + $param['uang_transport'];
		return $param;		
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
