<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Log extends MY_Controller {

	var $init = array();
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('default/listing',array('response' => '','page_title' => 'Log Manager'));
		else
			$this->load->view('dashboard/dashboard');
			
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}
	
	public function clear()
	{	
		$this->db->truncate('log'); 
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response,'page_title' => 'Log Manager'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title' => 'Log Manager'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '','page_title' => 'Log Manager'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/listing',array('response' => '','page_title' => 'Log Manager'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'log',
						'sort_order' => 'id DESC',
						'fields' => array(	
											array(
													'name' => 'user_id',
													'label' => 'User',
													'id' => 'user_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'users',
													'select' => array('user_id AS value','user_name AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'user_id',
													'rules' => 'required'
												),
											array(
													'name' => 'datetime',
													'label' => 'Datetime',
													'id' => 'datetime',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'url',
													'label' => 'URL',
													'id' => 'url',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'access',
													'label' => 'Access',
													'id' => 'access',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'request',
													'label' => 'Request',
													'id' => 'request',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'user_agent',
													'label' => 'User Agent',
													'id' => 'user_agent',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'ip_address',
													'label' => 'Ip Address',
													'id' => 'ip_address',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'browser',
													'label' => 'Browser',
													'id' => 'browser',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												)
										),
									'primary_key' => 'id'
					);
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
