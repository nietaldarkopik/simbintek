<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bintek_evaluation_instructure extends MY_Controller {

	var $init = array();
	
	public function index()
	{
		$this->listing();
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response, 'page_title' => 'Evaluasi Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/add_evaluation_instructure',array('response' => $response, 'page_title' => 'Evaluasi Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '', 'page_title' => 'Evaluasi Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_listing_output',array($this,'_hook_create_listing_output'));
		$this->hook->add_action('hook_create_listing_rows',array($this,'_hook_create_listing_rows'));

		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('bintek/listing_evaluation_instructure',array('response' => '', 'page_title' => 'Evaluasi Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{	
		$unsur_penilaian = $this->db->get('unsur_penilaian_instruktur')->result_array();
		$column_multiple_field = array();
		foreach($unsur_penilaian as $idx => $up){
			$column_multiple_field[] = array(
				'key_option' => $up['id_unsur_penilaian'],
				'label' => $up['unsur_penilaian'],
				'options'  => array(10, 20, 40, 80, 100)
			);
		}
		
		$init = array(	
						'table' => 'bintek_eval_instructure',
						'fields' => array(
											array(
													'name' => 'jadwal_id',
													'label' => 'Bintek',
													'id' => 'jadwal_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'jadwal_bintek',
													'select' => array('jadwal_bintek_id AS value','judul_bintek AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'jadwal_bintek_id',
													'rules' => 'required'
											),
											array(
													'name' => 'instructure_id',
													'label' => 'Instruktur',
													'id' => 'instructure_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'bintek_instruktur',
													'select' => array('instruktur_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'instruktur_id',
													'js_connect_to' => array(	
															'id_field_parent' => 'jadwal_id',
															'table' => 'bintek_instruktur A,jadwal_bintek B, relation_bintek_instructure C',
															'select' => 'A.instruktur_id AS value,A.nama AS label',
															'where' => " A.instruktur_id = C.instruktur_id AND B.jadwal_bintek_id = C.jadwal_bintek_id ",
															'foreign_key' => 'B.jadwal_bintek_id'
													),
													'rules' => 'required'
												),
											array(
													'name' => 'nilai',
													'label' => 'Nilai',
													'id' => 'nilai',
													'value' => '',
													'type' => 'fixed_multiple_field_text',
													'columns' => $column_multiple_field,
													'use_search' => false,
													'use_listing' => true,
													'rules' => ''
												),
						),
						
						'sort_order' => 'jadwal_id ASC',
						'bulk_options' => array('download_excel','download_pdf','printing'),
						'primary_key' => 'eval_instructure_id'
					);
		$this->init = $init;
	}
	
	public function report()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/report',array('response' => '', 'page_title' => 'Evaluasi Instruktur'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
		if(empty($is_ajax))
		{
			$this->load->view('bottombar');
			$this->load->view('footer');
		}
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
	
	public function unsur_penilaian()
	{
		redirect('/bintek_unsur_penilaian_instruktur');
	}
	
	
	function _bulk_actions()
	{
		$action = $this->input->post("bulk_action");
		if(!empty($action))
		{
			$action;
		}
	}
		
	function _hook_show_bulk_allowed_is_ajax_set0($param = "")
	{
		return 0;
	}
		
	function _hook_show_bulk_allowed_is_ajax_set1($param = "")
	{
		return 1;
	}
		
	public function download_excel()
	{
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		$is_ajax = $this->input->post('is_ajax');
		$query = $this->data->get_query();
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
		if($is_login)			
			$output = $this->load->view('bintek/report_evaluation_instruktur_excel',array('query' => $query,'response' => '','page_title' => 'TABULASI HASIL EVALUASI PENGAJAR'),true);
			//$output = $this->load->view('default/report',array('response' => '','page_title' => 'TABULASI HASIL EVALUASI PENGAJAR'),true);
		
		$this->report->download_excel($output,date("ymdhis"));
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
		exit;
	}
		
	public function download_pdf()
	{
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		$output = "";
		if($is_login)			
			$output = $this->load->view('bintek/report_evaluation_instruktur_excel',array('response' => '','page_title' => 'TABULASI HASIL EVALUASI PENGAJAR'),true);
			//$output = $this->load->view('default/report',array('response' => '','page_title' => 'TABULASI HASIL EVALUASI PENGAJAR'),true);
		
		$this->report->download_pdf($output,date("ymdhis"), 'L', '', 'A3');
		exit;
	}
		
	public function printing()
	{
		
		$this->load->library("report");
		$this->_config();
		$init = $this->_hook_report();
		$this->data->init($init);
		$this->data->set_filter();
		$is_ajax = $this->input->post('is_ajax');
	
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$output = $this->load->view('bintek/report_evaluation_instruktur_excel',array('response' => '','page_title' => 'TABULASI HASIL EVALUASI PENGAJAR'),true);
			//$this->load->view('default/report',array('response' => '','page_title' => 'TABULASI HASIL EVALUASI PENGAJAR'));
		
		echo '<script type="text/javascript">window.print();</script>';
	}
	
	function _hook_report(){
		$init = $this->init;
		if(is_array($init['fields'])){
			foreach($init['fields'] as $idx => $field){
				if($field['name'] != 'foto') 
					$init['fields'][$idx]['use_listing'] = true;
			}
		}
		
		return $init;
	}
	

	function _hook_create_listing_report_output($param = "")
	{
		$param = str_replace('<th>Nilai</th>','<th>Total Nilai</th><th>Nilai Rata-Rata</th><th>Predikat</th>',$param);
		return $param;
	}
	
	function _hook_create_listing_rows($param = "")
	{
		$param = str_replace(" & ", " &amp; ", $param);
		$dom = new domDocument;
		$dom->loadHTML($param);
		$param = '';
		if(!$dom) {
		}else{
			$tr = $dom->getElementsByTagName("tr");
			foreach($tr as $the_tr)
			{
				$cur_td = $the_tr->getElementsByTagName("td");
				$list = $cur_td->item(4)->getElementsByTagName("li");
				$total_nilai = 0;
				$count_data = 0;
				if(!empty($list)){
					foreach($list as $li){
						$nilai = (isset($li->getElementsByTagName("span")->item(1)->nodeValue)) ? $li->getElementsByTagName("span")->item(1)->nodeValue : 0;
						$total_nilai += $nilai;
						$count_data++;
					}
				}
				$rata_rata = $total_nilai / $count_data;
				
				
				$param .= '<tr>';
				$param .=  $dom->saveXML($cur_td->item(0));
				$param .= '<td>' . $cur_td->item(1)->nodeValue . '</td>';
				$param .= '<td>' . $cur_td->item(2)->nodeValue . '</td>';
				$param .= '<td>' . $cur_td->item(3)->nodeValue . '</td>';
				$param .= '<td align="center">' . $total_nilai . '</td>';
				$param .= '<td align="center">' . $rata_rata . '</td>';
				$param .= $dom->saveXML($cur_td->item(5));
				$param .= '</tr>';
			}
			
		}
		return $param;
	}
	
	function _hook_create_listing_output($param = "")
	{
		$param = str_replace('<th>Nilai</th>','<th>Total Nilai</th><th>Nilai Rata-Rata</th>',$param);
		return $param;
	}	
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
