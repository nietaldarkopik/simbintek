<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certification extends MY_Controller {

	var $init = array();
	
	public function index()
	{
		$this->_config();
		$this->init['fields'][4]['type'] = 'text';
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$this->hook->add_action('hook_create_listing_output',array($this,'_hook_create_listing_output'));
		$this->hook->add_action('hook_create_listing_rows',array($this,'_hook_create_listing_rows'));
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('default/listing',array('response' => '','page_title' => 'Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
			
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config($object_id);
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response,'page_title' => 'Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		unset($this->init['fields'][4]);
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title' => 'Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config($object_id);
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '','page_title' => 'Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->init['fields'][4]['type'] = 'text';
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$this->hook->add_action('hook_create_listing_output',array($this,'_hook_create_listing_output'));
		$this->hook->add_action('hook_create_listing_rows',array($this,'_hook_create_listing_rows'));
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/listing',array('response' => '','page_title' => 'Sertifikasi'));
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{			
		$column_multiple_field = array();
		if(!empty($id_object))
		{
			$this->db->where(array('sertifikasi_id' => $id_object));
			$q_default = $this->db->get("sertifikasi");
			$sertifikasi = $q_default->row_array();
			
			$this->db->where(array('jenis_sertifikasi' => $sertifikasi['jenis_sertifikasi']));
			$unsur_penilaian = $this->db->get('sertifikasi_kompetensi')->result_array();
			foreach($unsur_penilaian as $idx => $up)
			{
				$column_multiple_field[] = array(
					'key_option' => $up['kompetensi_id'],
					'label' => $up['judul_kompetensi']
				);
			}
		}
		$init = array(	'table' => 'sertifikasi',
						'fields' => array(	
											array(
													'name' => 'peserta_id',
													'label' => 'Peserta',
													'id' => 'peserta_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'sertifikasi_peserta',
													'select' => array('peserta_sertifikasi_id AS value','nama AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'peserta_sertifikasi_id',
													'rules' => 'required'
												),
											array(
													'name' => 'accessor_id',
													'label' => 'Asesor',
													'id' => 'accessor_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => false,
													'table'	=> 'sertifikasi_asesor',
													'select' => array('asesor_id AS value','concat("[",nip,"] - ",nama) AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'asesor_id',
													'rules' => 'required'
												),
											array(
													'name' => 'jenis_sertifikasi',
													'label' => 'Jenis Sertifikasi',
													'id' => 'jenis_sertifikasi',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'sertifikasi_jenis',
													'select' => array('kode_sertifikasi AS value','nama_sertifikasi AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'kode_sertifikasi',
													'rules' => 'required'
												),
											array(
													'name' => 'tanggal',
													'label' => 'Tanggal',
													'id' => 'tanggal',
													'value' => '',
													'type' => 'text',
													'class' => 'input_date',
													'use_search' => true,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'nilai',
													'label' => 'Nilai',
													'id' => 'nilai',
													'value' => '',
													'type' => 'fixed_multiple_field_text',
													'columns' => $column_multiple_field,
													'use_search' => false,
													'use_listing' => true,
													'rules' => ''
												),
											array(
													'name' => 'status',
													'label' => 'Status',
													'id' => 'status',
													'value' => '',
													'type' => 'selectbox',
													'options' => array('' => '---- Select Option ----','lanjut' => 'Dilanjutkan','tidak lanjut' => 'Tidak dilanjutkan'),
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												)
										),
									'primary_key' => 'sertifikasi_id'
					);
		$this->init = $init;
	}
	
	function _hook_create_listing_rows($param = "")
	{
		$dom = new domDocument;
		$dom->loadHTML($param);
		$param = '';
		if(!$dom) {
		}else{
			$tr = $dom->getElementsByTagName("tr");
			foreach($tr as $the_tr)
			{
				$tmp = $the_tr->getElementsByTagName("td");
				$tmp_json = (isset($tmp->item(4)->nodeValue))?$tmp->item(4)->nodeValue:"";
				$tmp_json = json_decode($tmp_json,true);
				$total = 0;
				$rata2 = 0;
				$predikat = '-';
				if(is_array($tmp_json))
				{
					foreach($tmp_json as $index => $col)
					{
						$total += $col['value'];
					}
					$rata2 = number_format($total/count($tmp_json),2);
					$predikat = $this->data->get_qualification($rata2);
				}
				
				$param .= '<tr>';
				$param .=  $dom->saveXML($tmp->item(0));
				$param .= '<td>' . $tmp->item(1)->nodeValue . '</td>';
				$param .= '<td>' . $tmp->item(2)->nodeValue . '</td>';
				$param .= '<td>' . $tmp->item(3)->nodeValue . '</td>';
				#$param .= '<td align="center">' . $total . '</td>';
				$param .= '<td align="center">' . $rata2 . '</td>';
				#$param .= '<td align="center">' . $predikat . '</td>';
				$param .= '<td>' . $tmp->item(5)->nodeValue . '</td>';
				$param .= $dom->saveXML($tmp->item(6));
				$param .= '</tr>';
			}
			
		}
		return $param;
	}
	
	function _hook_create_listing_output($param = "")
	{
		$param = str_replace('<th>Nilai</th>','<th>Nilai Rata-Rata</th>',$param);
		return $param;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
