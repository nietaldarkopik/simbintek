<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certification_aspects_evaluation_support extends MY_Controller {

	var $init = array();
	
	public function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
				
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('certification/listing_certification_type',array('response' => '','page_title' => 'Aspek Penilaian (Bukti Pendukung)'));
		else
			$this->load->view('dashboard/dashboard');
			
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	public function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/edit',array('response' => $response,'page_title' => 'Aspek Penilaian (Bukti Pendukung)'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/add',array('response' => $response,'page_title' => 'Aspek Penilaian (Bukti Pendukung)'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	
	public function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('default/view',array('response' => '','page_title' => 'Aspek Penilaian (Bukti Pendukung)'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('certification/listing_certification_type',array('response' => '','page_title' => 'Aspek Penilaian (Bukti Pendukung)'));
		else
			$this->load->view('dashboard/dashboard');
		
			#$this->load->view('rightbar');
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
		
	public function element_kompetensi()
	{
		redirect("certification_element_aspects_evaluation");
	}
	
	public function daftar_pertanyaan()
	{
		redirect("certification_aspects_evaluation_question");
	}
	
	public function bukti_pendukung()
	{
		redirect("certification_aspects_evaluation_support");
	}
	
	public function unit_kompetensi()
	{
		redirect("certification_aspects_evaluation");
	}
	
	public function penilaian($sertifikasi_id = "",$type = "asesor")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('header');
			$this->load->view('topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)
		{
			
			$this->db->where(array("sertifikasi_id" => $sertifikasi_id));
			$q_sertifikasi = $this->db->get("sertifikasi");
			$sertifikasi = $q_sertifikasi->row_array();
			
			$this->db->where(array("jenis_sertifikasi" => $sertifikasi['jenis_sertifikasi'],"parent_kompetensi" => ""));
			$kompetensi = $this->db->get("sertifikasi_kompetensi");
			$kompetensi = $kompetensi->result_array();
			
			if(is_array($kompetensi) and count($kompetensi) > 0)
			{
				foreach($kompetensi as $index => $kom)
				{
					$this->db->where(array("parent_kompetensi" => $kom['kode_kompetensi']));
					$subkompetensi = $this->db->get("sertifikasi_kompetensi");
					$subkompetensi = $subkompetensi->result_array();
					
					$kompetensi[$index]['sub'] = $subkompetensi;
				}
			}
			
			$this->load->view('certification/evaluasi',array('response' => '','page_title' => 'Penilaian Sertifikasi','kompetensi' => $kompetensi));
		}
		else
			$this->load->view('dashboard/dashboard');
		
			$this->load->view('bottombar');
		if(empty($is_ajax))
		{
			$this->load->view('footer');
		}
	}
	
	public function _config($id_object = "")
	{			
		$init = array(	'table' => 'sertifikasi_material_support',
						'fields' => array(
											array(
													'name' => 'sertifikasi_jenis_id',
													'label' => 'Jenis Sertifikasi',
													'id' => 'sertifikasi_jenis_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'sertifikasi_jenis',
													'select' => array('kode_sertifikasi AS value','concat("[",kode_sertifikasi,"] - ",nama_sertifikasi) AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'kode_sertifikasi',
													'rules' => ''
												),
											array(
													'name' => 'kompetensi_id',
													'label' => 'Unit Kompetensi',
													'id' => 'kompetensi_id',
													'value' => '',
													'type' => 'selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'sertifikasi_kompetensi',
													'select' => array('kode_kompetensi AS value','CONCAT(kode_kompetensi," - ", judul_kompetensi) AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'kompetensi_id',
													'rules' => '',
													'js_connect_to' => array(	'id_field_parent' => 'sertifikasi_jenis_id',
																				'table' => 'sertifikasi_kompetensi A,sertifikasi_jenis B',
																				'select' => 'A.kode_kompetensi AS value,concat(\'[\',A.kode_kompetensi,\'] - \',A.judul_kompetensi) AS label ',
																				'where' => ' A.jenis_sertifikasi = B.kode_sertifikasi AND A.parent_kompetensi = \'\' ',
																				'foreign_key' => 'B.kode_sertifikasi'
																			)
												),
											array(
													'name' => 'kode_material',
													'label' => 'Kode Pertanyaan',
													'id' => 'kode_material',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'judul_material',
													'label' => 'Pertanyaan',
													'id' => 'judul_material',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												)
										),
									'primary_key' => 'sertifikasi_material'
					);
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
