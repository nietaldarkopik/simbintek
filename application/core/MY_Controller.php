<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class MY_Controller extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		$user_id = $this->user_access->current_user_id;
		$controller = $this->uri->segment(1);
		$function = $this->uri->segment(2,"index");
		
		if($this->uri->segment(1) != "dashboard" and $this->uri->segment(1) != "")
		{
			if(!$this->user_access->is_page_allowed($user_id,$controller,$function))
				show_error('Anda tidak memiliki ijin untuk mengakses halaman ini',500,'404 Page Not Found');
		}
		
    }

}
// END Controller class

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */