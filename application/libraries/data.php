<?php

class Data{
	var $query;
	var $object_name;
	var $method_name;
	var $table;
	var $fields;
	var $files;
	var $filters;
	var $CI;
	var $config = array();
	var $is_pagination_init = false;
	var $primary_key = "";
	var $primary_key_value = "";
	var $paging_config = "";
	var $the_config = "";
	var $controllers = array();
	var $methods = array();
	var $controller_methods = array();
	var $data_rows = array();
	
	function Data(){
		$this->CI =& get_instance();
	}
	
	function init($config = array())
	{
		$object_name = $this->CI->uri->segment(1);
		$method_name = $this->CI->uri->segment(2);
		$this->object_name = (isset($config['object_name']))?$config['object_name']:$object_name;
		$this->method_name = (isset($config['method_name']))?$config['method_name']:$method_name;
		$this->primary_key = (isset($config['primary_key']))?$config['primary_key']:"";
		$this->the_config = $config;
		
		$this->table = (isset($config['table']))?$config['table']:"";
		$fields = array();
		$query = "";
		
		if($this->table != "")
		{
			$q = $this->CI->db->get($this->table);
			$query = $this->CI->db->last_query();
		}
		
		$this->query = (isset($config['query']))?$config['query']:$query;
		$this->query = $this->query . " where 1 = 1";
		if($this->query != "")
		{		
			$q = $this->CI->db->query($this->query);
			$fields = $q->list_fields();
		}
		$this->fields = (isset($config['fields']))?$config['fields']:$fields;
		
		$filters = (isset($config['filters']))?$config['filters']:$this->get_filter();
		$t_filter = "";
		if(!empty($filters) and is_array($filters) and count($filters) > 0)
		{
			$t_filter = " 1 = 1 ";
			foreach($filters as $index => $filter)
			{
				$t_filter .= $filter;
			}
		}else{
			$t_filter = $filters;
		}
		$this->filters = $t_filter;
		
	}
	
	function init_pagination($config = array())
	{
		$function = $this->CI->uri->segment(2);
		if($function == 'listing' or $function == 'index')
		{
			$function = "";
		}else{
			$function = "/".$function;
		}
		$this->CI->load->library("pagination");
		$this->paging_config = array();
		$this->paging_config['base_url'] = base_url().$this->CI->uri->segment(1).$function.'/listing/';
		$this->paging_config['total_rows'] = $this->get_total_data();
		$this->paging_config['per_page'] = 10; 
		$this->paging_config['uri_segment'] = 3;
		$this->paging_config['num_links'] = 2;
		$this->paging_config['use_page_numbers'] = FALSE;
		$this->paging_config['page_query_string'] = FALSE;
		$this->paging_config['full_tag_open'] = '<div id="pager">';
		$this->paging_config['full_tag_close'] = '</div>';
		$this->paging_config['first_link'] = 'First';
		$this->paging_config['first_tag_open'] = '<div style="padding:5px;float:left;">';
		$this->paging_config['first_tag_close'] = '</div>';
		$this->paging_config['last_link'] = 'Last';
		$this->paging_config['last_tag_open'] = '<div style="padding:5px;float:left;">';
		$this->paging_config['last_tag_close'] = '</div>';
		$this->paging_config['next_link'] = '&gt;';
		$this->paging_config['next_tag_open'] = '<div style="padding:5px;float:left;">';
		$this->paging_config['next_tag_close'] = '</div>';
		$this->paging_config['prev_link'] = '&lt;';
		$this->paging_config['prev_tag_open'] = '<div style="padding:5px;float:left;">';
		$this->paging_config['prev_tag_close'] = '</div>';
		$this->paging_config['cur_tag_open'] = '<div style="padding:5px;float:left;"><b><a href="javascript:void(0);">';
		$this->paging_config['cur_tag_close'] = '</a></b></div>';
		$this->paging_config['num_tag_open'] = '<div style="padding:5px;float:left;">';
		$this->paging_config['num_tag_close'] = '</div>';
		
		$config = array_merge($this->paging_config,$config);
		$this->paging_config = $config;
		$this->is_pagination_init = true;
	}
	
	function create_pagination($config = array())
	{
		if($this->is_pagination_init == false or (is_array($config) and count($config) > 0))
			$this->init_pagination($config);
			
		$this->CI->pagination->initialize($this->paging_config); 
		return $this->CI->pagination->create_links();
	}
	
	function create_view($config = array())
	{
		$method_name = (!empty($this->method_name))?$this->method_name:'listing';
		$t_nonce = $this->CI->user_access->get_nonce();
		#$action = (isset($config['action']))?$config['action']:base_url().$this->object_name.'/'.$method_name;
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		
		$form_title = $this->CI->hook->do_action('hook_create_view_form_title','View Data');
		$output = '
			<form id="form" action="#" method="post" class="form">
				<fieldset id="edit_form_fieldset">
				<legend>'.$form_title.'</legend>';
						
				if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
				{
					foreach($this->fields as $index => $field)
					{
							$field['parent_name'] = 'data';
							$label = (isset($config['primary_key']))?$config['primary_key']:$this->primary_key;
							
							if(empty($label))
							{
								$label = 'label';
							}
						
							$where = array($label => "'".$this->primary_key_value."'");
							$value = $this->get_value($this->table,$field['name'] . " label",$where,"");
							$field['value'] = $value;
							
							$output .= $this->create_form_view($field);
					}
				}
		
		$output .= '	<br class="fclear"/>
					</fieldset>
				</form>';
		
		return $output;
	}
	
	
	function create_form($config = array())
	{
		$method_name = (!empty($this->method_name))?$this->method_name:'listing';
		$t_nonce = $this->CI->user_access->get_nonce();
		#$action = (isset($config['action']))?$config['action']:base_url().$this->object_name.'/'.$method_name;
		$action = (isset($config['action']))?$config['action']:current_url();
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		$form_title = $this->CI->hook->do_action('hook_create_form_form_title','Form Data');
		$output = '
			<form id="form" action="'.$action.'" method="post" class="form" enctype="multipart/form-data">
				<fieldset id="edit_form_fieldset">
				<legend>'.$form_title.'</legend>';
						
				if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
				{
					foreach($this->fields as $index => $field)
					{
							$field['parent_name'] = 'data';
							$label = (isset($config['primary_key']))?$config['primary_key']:"";
							
							if(empty($label))
							{
								$label = 'label';
							}
						
							$where = array($label => "'".$this->primary_key_value."'");
							$value = $this->get_value($this->table,$field['name'] . " label",$where,"");
							$field['value'] = $value;
							$output .= $this->create_form_field($field);
					}
				}
		
		
		$ajax_target = $this->CI->hook->do_action('hook_create_form_ajax_target','#main_content .col1');
		$button_action = $this->CI->hook->do_action('hook_create_form_button_action','<input type="submit" value="Simpan" />');
		$output .= '	<br class="fclear"/>
									<label>&nbsp;</label>
									<input type="hidden" name="nonce" value="'.$nonce.'"/> 
									<input type="hidden" name="ajax_target" value="'.$ajax_target.'"/> 
									<input type="hidden" name="is_ajax" value="1"/> 
									<span class="value_view">
										&nbsp;
										&nbsp;
										'.$button_action.'
									</span>
							</fieldset>
						</form>';
		
		return $output;
	}
	
	function create_form_filter($config = array())
	{
		$t_nonce = $this->CI->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:base_url().$this->object_name.'/';
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		
		$filter = $this->get_filter();
		
		$form_title = $this->CI->hook->do_action('hook_create_form_filter_form_title','Search Form');
		$output = '
			<form id="form_filter" action="'.$action.'" method="post" class="form">
				<fieldset id="search_form_fieldset">
				<legend>'.$form_title.'</legend>';
						
				if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
				{
					foreach($this->fields as $index => $field)
					{
						if(isset($field['use_search']) and $field['use_search'] == true)
						{
							$value = (isset($filter[$field['name']]))?$filter[$field['name']]:"";
							$field['parent_name'] = 'data_filter';
							if(isset($field['type']) and $field['type'] == 'textarea')
								$field['type'] = 'text';
							$output .= $this->create_form_field($field,$value);
						}
					}
				}
		
		$ajax_target = $this->CI->hook->do_action('hook_create_form_filter_ajax_target','#main_content .col1');
		$button_action = $this->CI->hook->do_action('hook_create_form_filter_button_action','<input type="submit" value="Search" />');

		$output .= '
							<label>&nbsp;</label>
							<input type="hidden" name="nonce" value="'.$nonce.'"/> 
							<input type="hidden" name="ajax_target" value="'.$ajax_target.'"/> 
							<input type="hidden" name="is_ajax" value="1"/> 
							<span class="value_view">
								&nbsp;&nbsp;
								'.$button_action.'
							</span>
					</fieldset>	
					<br class="fclear"/>
				</form>';
		
		return $output;
	}
	
	function create_form_view($field = array(),$value = "")
	{
		$output = "";
		$type = (isset($field['type']))?$field['type']:'text';
		
		switch($type)
		{			
			case 'selectbox':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']))?$field['value']:$value;
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"";
				$options = (isset($field['options']))?$field['options']:"";
				$parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
				$primary_key = (isset($field['primary_key']))?$field['primary_key']:"";
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				
				$t_attribute = "";
				if(!empty($id))
					$t_attribute .= ' id="'.$id.'" ';
				if(!empty($style))
					$t_attribute .= ' style="'.$style.'" ';
				if(!empty($class))
					$t_attribute .= ' class="'.$class.'" ';
					
				$attribute = (isset($field['attribute']))?$field['attribute'] . $t_attribute:$t_attribute;
				$query = (isset($field['query']))?$field['query']:"";
				
				if(!empty($query))
				{
					$q = $this->CI->db->query($query);
					if($q->num_rows() > 0)
					{
						$t_options = $q->row_array();
						if(isset($t_options['value']))
						{
							$t_label = (isset($t_options['label']))?$t_options['label']:$t_options['value'];
							$value = $t_label;
						}
					}
				}elseif(isset($field['table']) and !empty($field['table']))
				{
					if(!empty($primary_key))
						$this->CI->db->where(array($primary_key => $value));
						
					$this->CI->db->select($field['select']);
					$q = $this->CI->db->get($field['table']);
					
					if($q->num_rows() > 0)
					{
						$t_options = $q->row_array();
						if(isset($t_options['value']))
						{
							$t_label = (isset($t_options['label']))?$t_options['label']:$t_options['value'];
							$value = $t_label;
						}
					}
				
				}
				
				$label = $this->CI->hook->do_action('hook_create_form_view_label_'.$name,$label);
				$value = $this->CI->hook->do_action('hook_create_form_view_value_'.$name,$value);

				$output .= '<label for="'. $name .'">'. $label .'</label>';
				$output .= '<span class="value_view"> : '. $value.'</span>' . '<br class="fclear"/>';
			break;			
			
			
			case 'multiple':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and !empty($field['value']))?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = 'input_multiple';
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$multiple_cols = (isset($field['multiple_cols']))?$field['multiple_cols']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				
				$multiple_cols_output = "";
				
				if(is_array($multiple_cols) and count($multiple_cols) > 0)
				{
					$value2 = json_decode($value,true);
					$value = array();
					if(is_array($value2) and count($value2) > 0)
					{
						$no = 0;
						foreach($value2 as $n => $v)
						{
							$val = array();
							foreach($v as $n2 => $v2)
							{
								$value[$n2][$n] = $v2;
							}
						}
					}
					
					$multiple_cols_output = "<table border='1' class='input_multiple' id='tbl_multiple_".$name."'><tr>";
					$multiple_cols_output .= '<td>No</td>';
					foreach($multiple_cols as $index => $col)
					{
						$multiple_cols_output .= '<td>'.$col['title'].'</td>';
					}
					$multiple_cols_output .= "</tr>";
					
					$multiple_cols_output .= '<tr class="master" style="display:none;"><td>1</td>';

					if(is_array($value) and count($value) > 0)
					{
						$no = 0;
						foreach($value as $i => $vals)
						{
							if(implode("",$vals) == "")
								continue;
							
							$no++;
							$multiple_cols_output .= '<tr class="master2"><td>' . ($no) .'</td>';
							foreach($multiple_cols as $index => $col)
							{
								$input_multiple = "";
								$attr = (isset($col['attr']))?$col['attr']:"";
								$sname = (isset($col['name']))?$col['name']:"";
								$val = (isset($vals[$sname]))?$vals[$sname]:"";
								$multiple_cols_output .= '<td>'.$val.'</td>';
							}
						}
					}
					$multiple_cols_output .= "</tr>";
					$multiple_cols_output .= "</table>";
				}
							
				$desc = "";
				$label = $this->CI->hook->do_action('hook_create_form_view_label_'.$name,$label);
				$multiple_cols_output = $this->CI->hook->do_action('hook_create_form_view_value_'.$name,$multiple_cols_output);

				$output .= '<label for="'. $name .'" class="label_input_multiple">'. $label .'</label>';
				$output .= '<span class="value_view" class="span_input_multiple">'. $multiple_cols_output . '</span>' . $desc . '<br class="fclear"/>';
			break;
			
			
			case 'fixed_multiple_field':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and !empty($field['value']))?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = 'input_multiple';
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$columns = (isset($field['columns']))?$field['columns']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				
				$columns_output = "";
				
				if(is_array($columns) and count($columns) > 0)
				{
					
					$columns_output = "<table class='input_fixed_multiple' id='tbl_fixed_multiple_".$name."'><tr>";
					
					$decoded_value = json_decode($value, true);		
					foreach($columns as $index => $col)
					{
						$columns_output .= '<tr>';
						$columns_output .= '<td>'.$col['label'].'</td>';
						
						$option_val = '';
						foreach($col['options'] as $opt){
							//if($this->is_option_selected($decoded_value, $col['label'], $opt))
							if($this->is_option_selected($decoded_value, $col['key_option'], $opt))
								$option_val = $opt;
						}
						
						$columns_output .= '<td>'. $option_val. '</td>';
					}
					$columns_output .= "</tr>";
					

					$columns_output .= "</table>";
				}
							
				$desc = "";
				$output .= '<br /><br />';
				
				$label = $this->CI->hook->do_action('hook_create_form_view_label_'.$name,$label);
				$columns_output = $this->CI->hook->do_action('hook_create_form_view_value_'.$name,$columns_output);

				$output .= '<label for="'. $name .'" class="label_input_multiple">'. $label .'</label>';
				$output .= '<span class="value_view" class="span_input_multiple">'. $columns_output . '</span>' . $desc . '<br class="fclear"/>';
			break;
			case 'fixed_multiple_field_text':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and !empty($field['value']))?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = 'input_multiple';
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$columns = (isset($field['columns']))?$field['columns']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				
				$columns_output = "";
				
				if(is_array($columns) and count($columns) > 0)
				{
					
					$columns_output = "<table class='input_fixed_multiple' id='tbl_fixed_multiple_".$name."'>";
					
					$decoded_value = json_decode($value, true);		
					$values = array();
					if(is_array($decoded_value))
					{
						foreach($decoded_value as $i => $v)
						{
							$values[$v['label']] = $v['value'];
						}
					}
					$total = 0;
					foreach($columns as $index => $col)
					{
						$columns_output .= '<tr>';
						$columns_output .= '<td>'.$col['label'].'</td>';
						
						$option_val = (isset($values[$col['key_option']]))?$values[$col['key_option']]:0;						
						$columns_output .= '<td align="right">'. $option_val. '</td>';
						$columns_output .= "</tr>";
						$total += $option_val;
					}
					$columns_output .= '<tr>';
					$columns_output .= '	<td align="right"><strong>Total</strong></td>';
					$columns_output .= '	<td align="right"><strong>'. $total. '</strong></td>';
					$columns_output .= '</tr>';
					$columns_output .= '<tr>';
					$columns_output .= '	<td align="right"><strong>Rata-rata</strong></td>';
					$columns_output .= '	<td align="right"><strong>'. number_format($total/count($columns),2). '</strong></td>';
					$columns_output .= "</tr>";
					$columns_output .= '<tr>';
					$columns_output .= '	<td align="right"><strong>Predikat</strong></td>';
					$columns_output .= '	<td align="right"><strong>'. $this->get_qualification($total/count($columns)). '</strong></td>';
					$columns_output .= "</tr>";
					$columns_output .= "</table>";
				}
							
				$desc = "";
				$output .= '<br /><br />';
				
				$label = $this->CI->hook->do_action('hook_create_form_view_label_'.$name,$label);
				$columns_output = $this->CI->hook->do_action('hook_create_form_view_value_'.$name,$columns_output);

				$output .= '<label for="'. $name .'" class="label_input_multiple">'. $label .'</label>';
				$output .= '<span class="value_view" class="span_input_multiple">'. $columns_output . '</span>' . $desc . '<br class="fclear"/>';
			break;
			case 'text':
			case 'password':
			case 'hidden':
			case 'date':
			case 'tinymce':
			case 'tinymce_simple':
			default :
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']))?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"";
				$parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				
				$class .= " input_text";
								
				$value = ($type == "password")?"&nbsp;":$value;
				$data = array(
				  'name'        => $input_name,
				  'id'          => $id,
				  'value'       => $value,
				  'maxlength'   => $maxlength,
				  'size'        => $size,
				  'style'       => $style,
				  'type'       => $type,
				  'class'		=> $class
				);
	
				if($type == 'file')
				{
					$path = (	isset($field['config_upload']) and 
										isset($field['config_upload']['upload_path']) and 
										!empty($field['config_upload']['upload_path']))
									?$field['config_upload']['upload_path']
									:"";
					$path_value = $path . $value;
					$is_image = $this->is_image($path_value);
					if($is_image)
					{
						$path = str_replace(getcwd().'/',base_url(),$path_value);
						$value = $this->show_image($path,'height="50"');
					}
				}
				if($field['type'] == 'date' or $field['type'] == 'datetime')
				{
					$value = $this->human_date($value,false,false);
				}
				
				$label = $this->CI->hook->do_action('hook_create_form_view_label_'.$name,$label);
				$value = $this->CI->hook->do_action('hook_create_form_view_value_'.$name,$value);

				$output .= '<label for="'. $name .'">'. $label .'</label>';
				$output .= '<span class="value_view"> : '. $value.'</span>' . '<br class="fclear"/>';
			break;
		}
		
		return $output;
	}
	
	
	function create_form_field($field = array(),$value = "")
	{
		$output = "";
		$type = (isset($field['type']))?$field['type']:'text';

		switch($type)
		{			
			case 'selectbox':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"";
				$options = (isset($field['options']))?$field['options']:"";
				$parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
				$rules = (isset($field['rules']))?$field['rules']:"";
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				
				$t_attribute = "";
				if(!empty($id))
					$t_attribute .= ' id="'.$id.'" ';
				if(!empty($style))
					$t_attribute .= ' style="'.$style.'" ';
				if(!empty($class))
					$t_attribute .= ' class="'.$class.' '. ((is_array($rules))?implode(' ',$rules):$rules) .'" ';
					
				$attribute = (isset($field['attribute']))?$field['attribute'] . $t_attribute:$t_attribute;
				$query = (isset($field['query']))?$field['query']:"";
				
				if(!empty($query))
				{
					$q = $this->CI->db->query($query);
					if($q->num_rows() > 0)
					{
						$t_options = $q->result_array();
						foreach($t_options as $index => $row)
						{
							if(isset($row['value']))
							{
								$t_label = (isset($row['label']))?$row['label']:$row['value'];
								$options[$row['value']] = $t_label;
							}
						}
					}
				}elseif(isset($field['table']) and !empty($field['table']))
				{
					$this->CI->db->order_by("value","ASC");
					$this->CI->db->select($field['select']);
					$q = $this->CI->db->get($field['table']);
					
					if($q->num_rows() > 0)
					{
						$t_options = $q->result_array();
						foreach($t_options as $index => $row)
						{
							if(isset($row['value']))
							{
								$t_label = (isset($row['label']))?$row['label']:$row['value'];
								$options[$row['value']] = $t_label;
							}
						}
					}
				
				}
				
				$label = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$value = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$value);
				
				$output .= '<label for="'. $name .'">'. $label .'</label>';
				$output .= '<span class="value_view"> : '. form_dropdown($input_name, $options, $value ,$attribute) . form_error($input_name) . '</span><br class="fclear"/>';
				if(isset($field['js_connect_to']) and !empty($field['js_connect_to']))
				{
				
					$table 	= $field['js_connect_to']['table'];
					$where 	= $field['js_connect_to']['where'];
					$select	= $field['js_connect_to']['select'];
					$value 	= $field['primary_key'];
					$foreign_key 	= $field['js_connect_to']['foreign_key'];
					
					$param = "foreign_key=$foreign_key&table=$table&where=$where&select=$select&value=$value";
					$param = urldecode($param);
					
					$output .= '<script type="text/javascript">
									jQuery(document).ready(function(){
										jQuery("#'.$field['js_connect_to']['id_field_parent'].'").on("change",function(){
											var foreign_key = jQuery(this).val();
											jQuery.ajax({
												url: "'.base_url().'services/get_options",
												type: "post",
												data: "'.$param.'&fk="+foreign_key,
												success: function(msg){
													var first_row = jQuery("#'.$id.'").find("option").eq(0).clone();
													jQuery("#'.$id.'").html("");
													jQuery("#'.$id.'").append(jQuery(first_row));
													jQuery("#'.$id.'").append(msg);
												}
											});
										});
									});
								</script>';
				}
			break;			
			
			case 'multiple':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = 'input_multiple';
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$multiple_cols = (isset($field['multiple_cols']))?$field['multiple_cols']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				
				$multiple_cols_output = "";
				
				if(is_array($multiple_cols) and count($multiple_cols) > 0)
				{
					$value2 = json_decode($value,true);
					$value = array();
					if(is_array($value2) and count($value2) > 0)
					{
						foreach($value2 as $n => $v)
						{
							$no = 0;
							$val = array();
							foreach($v as $n2 => $v2)
							{
								$value[$no][$n] = $v2;
								$no++;
							}
						}
					}
					
					$multiple_cols_output = "<table border='1' class='input_multiple' id='tbl_multiple_".$name."'><tr>";
					$multiple_cols_output .= '<td>No</td>';
					foreach($multiple_cols as $index => $col)
					{
						$multiple_cols_output .= '<td>'.$col['title'].'</td>';
					}
					$multiple_cols_output .= '<td>Action</td>';
					$multiple_cols_output .= '</tr>';
					$multiple_cols_output .= '<tr class="master" style="display:none;"><td>1</td>';
					foreach($multiple_cols as $index => $col)
					{
						$input_multiple = "";
						$attr = (isset($col['attr']))?$col['attr']:"";
						$name = (isset($col['name']))?$col['name']:"";
						$val = (isset($value[$name][$index]))?$value[$name][$index]:"";
						if($col['type'] == 'text')
						{
							$input_multiple = '<input type="text" '.$attr.' name="'.$input_name.'['.$name.'][]" value="'.$val.'"/>';
						}else{
							$input_multiple = '<textarea '.$attr.' name="'.$input_name.'['.$name.'][]">'.$val.'</textarea>';
						}
						$multiple_cols_output .= '<td>'.$input_multiple.'</td>';
					}						
					$multiple_cols_output .= '<td><a href="javascript:void(0);" class="remove_row_input_multiple">delete</a></td>';
					$multiple_cols_output .= '</tr>';
					
					if(is_array($value) and count($value) > 0)
					{
						$no = 0;
						foreach($value as $i => $vals)
						{
							if(implode("",$vals) == "")
								continue;
							
							$no++;
							$multiple_cols_output .= '<tr class="master2"><td>' . ($no) .'</td>';
							foreach($multiple_cols as $index => $col)
							{
								$input_multiple = "";
								$attr = (isset($col['attr']))?$col['attr']:"";
								$sname = (isset($col['name']))?$col['name']:"";
								$val = (isset($vals[$sname]))?$vals[$sname]:"";
								if($col['type'] == 'text')
								{
									$input_multiple = '<input type="text" '.$attr.' name="'.$input_name.'['.$sname.'][]" value="'.$val.'"/>';
								}else{
									$input_multiple = '<textarea '.$attr.' name="'.$input_name.'['.$sname.'][]">'.$val.'</textarea>';
								}
								$multiple_cols_output .= '<td>'.$input_multiple.'</td>';
							}
							$multiple_cols_output .= '<td><a href="javascript:void(0);" class="remove_row_input_multiple">delete</a></td>';
							$multiple_cols_output .= '</tr>';
						}
					}
					
					$multiple_cols_output .= "<tr>
												<td colspan='". (count($multiple_cols)+2) ."'>
													<a href='javascript:void(0);' class='add_row_input_multiple'>add new</a>
												</td>
											  </tr>
											</table>";
				}
							
				$desc = "";
				$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$multiple_cols_output  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$multiple_cols_output);

				$output .= '<label for="'. $name .'" class="label_input_multiple">'. $label .'</label>';
				$output .= '<span class="value_view" class="span_input_multiple">'. $multiple_cols_output . '</span>' . $desc . '<br class="fclear"/>';
			break;
			
			case 'fixed_multiple_field':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = 'input_multiple';
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$columns = (isset($field['columns']))?$field['columns']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				
				$columns_output = "";
				
				if(is_array($columns) and count($columns) > 0)
				{
					
					$columns_output = "<table class='input_fixed_multiple' id='tbl_fixed_multiple_".$name."'><tr>";
					
					$decoded_value = json_decode($value, true);		
					foreach($columns as $index => $col)
					{
						$columns_output .= '<tr>';
						$columns_output .= '<td>'.$col['label'].'</td>';
						
						$option_val = '';
						foreach($col['options'] as $opt){
							if($this->is_option_selected($decoded_value, $col['key_option'], $opt))
								$option_val .= "<option selected='selected'>{$opt}</option>\n";
							else
								$option_val .= "<option>{$opt}</option>\n";
						}
						
						$columns_output .= '<td>
																		<input type="hidden" name="fixed_multiple_data['.$name.']['.$index.'][label]" value="'.$col['key_option'].'" />
																		<select name="fixed_multiple_data['.$name.']['.$index.'][value]" >
																			'. $option_val. '
																		</select>
																</td>';
					}
					$columns_output .= "</tr>";
					

					$columns_output .= "</table>";
				}
							
				$desc = "";
				$output .= '<br /><br />';
				
				$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$columns_output  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$columns_output);

				$output .= '<label for="'. $name .'" class="label_input_multiple">'. $label .'</label>';
				$output .= '<span class="value_view" class="span_input_multiple">'. $columns_output . '</span>' . $desc . '<br class="fclear"/>';
			break;
			case 'fixed_multiple_field_text':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = 'input_multiple';
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$columns = (isset($field['columns']))?$field['columns']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				
				$columns_output = "";
				
				if(is_array($columns) and count($columns) > 0)
				{
					
					$columns_output = "<table class='input_fixed_multiple' id='tbl_fixed_multiple_".$name."'><tr>";
					
					$decoded_value = json_decode($value, true);			
					$values = array();
					if(is_array($decoded_value))
					{
						foreach($decoded_value as $i => $v)
						{
							$values[$v['label']] = $v['value'];
						}
					}
					foreach($columns as $index => $col)
					{
						$columns_output .= '<tr>';
						$columns_output .= '<td>'.$col['label'].'</td>';
						
						$option_val = (isset($values[$col['key_option']]))?$values[$col['key_option']]:0;
						$columns_output .= '<td>
													<input type="hidden" name="fixed_multiple_data['.$name.']['.$index.'][label]" value="'.$col['key_option'].'" />
													<input type="text" name="fixed_multiple_data['.$name.']['.$index.'][value]" value="'.$option_val.'">
											</td>';
					}
					$columns_output .= "</tr>";
					

					$columns_output .= "</table>";
				}
							
				$desc = "";
				$output .= '<br /><br />';
				
				$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$columns_output  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$columns_output);

				$output .= '<label for="'. $name .'" class="label_input_multiple">'. $label .'</label>';
				$output .= '<span class="value_view" class="span_input_multiple">'. $columns_output . '</span>' . $desc . '<br class="fclear"/>';
			break;
			case 'range_date_selectbox':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				$options_to = (isset($field['options_to']))?$field['options_to']:array('y' => array(),'m' => array(),'d' => array());
				$options_from = (isset($field['options_from']))?$field['options_from']:array('y' => array(),'m' => array(),'d' => array());
				
				$data = array(
				  'name'        => $input_name,
				  'id'          => $id,
				  'value'       => $value,
				  'maxlength'   => $maxlength,
				  'size'        => $size,
				  'style'       => $style,
				  'type'       => $type,
				  'class'		=> $class
				);
				
				$desc = "";
				$error = form_error($input_name);
				
				$options_from_y = '';
				$options_from_m = '';
				$options_from_d = '';
				$options_to_y = '';
				$options_to_m = '';
				$options_to_d = '';
				
				if(is_array($options_from['y']) and count($options_from['y']) > 0)
				{
					foreach($options_from['y'] as $i => $x)
					{
						$options_from_y .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_from['m']) and count($options_from['m']) > 0)
				{
					foreach($options_from['m'] as $i => $x)
					{
						$options_from_m .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_from['d']) and count($options_from['d']) > 0)
				{
					foreach($options_from['d'] as $i => $x)
					{
						$options_from_d .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_to['y']) and count($options_to['y']) > 0)
				{
					foreach($options_to['y'] as $i => $x)
					{
						$options_to_y .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_to['m']) and count($options_to['m']) > 0)
				{
					foreach($options_to['m'] as $i => $x)
					{
						$options_to_m .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_to['d']) and count($options_to['d']) > 0)
				{
					foreach($options_to['d'] as $i => $x)
					{
						$options_to_d .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				$input_form = '	<select name="'.$input_name.'[from][y]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_from_y.'
												</select>
												<select name="'.$input_name.'[from][m]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_from_m.'
												</select>
												<select name="'.$input_name.'[from][d]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_from_d.'
												</select>
												-
												<select name="'.$input_name.'[to][y]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_to_y.'
												</select>
												<select name="'.$input_name.'[to][m]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_to_m.'
												</select>
												<select name="'.$input_name.'[to][d]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_to_d.'
												</select>
											';
				
				#$input_from = form_dropdown($data['name'].'[from]', $options_from['y'], $value_from ,$attribute);
				
				$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$output .= '<label for="'. $name .'">'. $label .'</label>';
				if($type == "textarea")
				{
					$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$input_form);
					$output .= '<span class="value_view"> : '. $form . $error . '</span>' . $desc . '<br class="fclear"/>';
				}else{
					$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$input_form);
					$output .= '<span class="value_view"> : '. $form . $error . '</span>' . $desc . '<br class="fclear"/>';
				}
			break;
			case 'range_year_selectbox':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				$options_to = (isset($field['options_to']))?$field['options_to']:array('y' => array(),'m' => array(),'d' => array());
				$options_from = (isset($field['options_from']))?$field['options_from']:array('y' => array(),'m' => array(),'d' => array());
				
				$data = array(
				  'name'        => $input_name,
				  'id'          => $id,
				  'value'       => $value,
				  'maxlength'   => $maxlength,
				  'size'        => $size,
				  'style'       => $style,
				  'type'       => $type,
				  'class'		=> $class
				);
				
				$desc = "";
				$error = form_error($input_name);
				
				$options_from_y = '';
				$options_from_m = '';
				$options_from_d = '';
				$options_to_y = '';
				$options_to_m = '';
				$options_to_d = '';
				
				if(is_array($options_from['y']) and count($options_from['y']) > 0)
				{
					foreach($options_from['y'] as $i => $x)
					{
						$options_from_y .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_from['m']) and count($options_from['m']) > 0)
				{
					foreach($options_from['m'] as $i => $x)
					{
						$options_from_m .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_from['d']) and count($options_from['d']) > 0)
				{
					foreach($options_from['d'] as $i => $x)
					{
						$options_from_d .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_to['y']) and count($options_to['y']) > 0)
				{
					foreach($options_to['y'] as $i => $x)
					{
						$options_to_y .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_to['m']) and count($options_to['m']) > 0)
				{
					foreach($options_to['m'] as $i => $x)
					{
						$options_to_m .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_to['d']) and count($options_to['d']) > 0)
				{
					foreach($options_to['d'] as $i => $x)
					{
						$options_to_d .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				$input_form = '	<select name="'.$input_name.'[from][y]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_from_y.'
												</select>
												-
												<select name="'.$input_name.'[to][y]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_to_y.'
												</select>
											';
				
				#$input_from = form_dropdown($data['name'].'[from]', $options_from['y'], $value_from ,$attribute);
				
				$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$output .= '<label for="'. $name .'">'. $label .'</label>';
				if($type == "textarea")
				{
					$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$input_form);
					$output .= '<span class="value_view"> : '. $form . $error . '</span>' . $desc . '<br class="fclear"/>';
				}else{
					$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$input_form);
					$output .= '<span class="value_view"> : '. $form . $error . '</span>' . $desc . '<br class="fclear"/>';
				}
			break;
			case 'text':
			case 'password':
			case 'hidden':
			case 'date':
			case 'file':
			case 'textarea':
			case 'tinymce':
			case 'tinymce_simple':
			default :
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				
				$data = array(
				  'name'        => $input_name,
				  'id'          => $id,
				  'value'       => ($type == "password")?"":$value,
				  'maxlength'   => $maxlength,
				  'size'        => $size,
				  'style'       => $style,
				  'type'       => $type,
				  'class'		=> $class
				);
				
				$desc = "";
				$error = form_error($input_name);
				if($type == "file")
				{
					$this->CI->load->library('upload');
					if(isset($field['config_upload']) and isset($field['config_upload']['allowed_types']) and !empty($field['config_upload']['allowed_types']))
					{
						$format_file = str_replace("|",",",$field['config_upload']['allowed_types']);
						$desc .= "<span class='desc_field'><i>Format file : (".$format_file.")</i></span>";
					}
					$error = $this->CI->upload->display_errors('<p class="error">','</p>');
				}

				$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$output .= '<label for="'. $name .'">'. $label .'</label>';
				if($type == "textarea")
				{
					$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_textarea($data));
					$output .= '<span class="value_view"> : '. $form . $error . '</span>' . $desc . '<br class="fclear"/>';
				}else{
					$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_input($data));
					$output .= '<span class="value_view"> : '. $form . $error . '</span>' . $desc . '<br class="fclear"/>';
				}
			break;
		}
		
		$output  = $this->CI->hook->do_action('hook_create_form_field_output_'.$name,$output);
		return $output;
	}
	
	function get_value($table = "",$field = "", $where = "",$default = "")
	{
		if(empty($table) or empty($field) or empty($where))
			return $default;
			
		$this->CI->db->where($where,"",FALSE);
		$this->CI->db->select($field,FALSE);
		$query = $this->CI->db->get($table);
		$data = $query->row_array();
		
		return (isset($data['label']))?$data['label']:$default;
	}
	
	
	function create_listing($config = array())
	{
		if($this->is_pagination_init == false or (is_array($config) and count($config) > 0))
			$this->init_pagination($config);
		
		$t_nonce = $this->CI->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:'search';
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		$panel_function = (isset($config['panel_function']))?$config['panel_function']:array("edit","view","delete");
		
		$action = $this->show_panel_allowed("","",$panel_function);
		
		$col = "";				
		if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
		{
			if(!empty($action))
			{
				$col .= '
						<th align="center" valign="middle" class="no-print bulk_data_all_th" width="5"><input type="checkbox" name="bulk_data_all" value="1" class="bulk_data_all"/></th>';
			}
			$col .= '<th>No</th>';
			foreach($this->fields as $index => $field)
			{
				if(isset($field['use_listing']) and $field['use_listing'] == true)
				{
					$label = (isset($field['label']))?$field['label']:$field;
					$name = (isset($field['name']))?$field['name']:$field;
					#$col .= '<th><a href="#">ID<img src="static/img/icons/arrow_down_mini.gif" width="16" height="16" align="absmiddle" /></a></th>';
					$col .= '
							<th>' . $label . '</th>';
				}
			}
			if(!empty($action))
				$col .= '<th class="action_menu_col">Action</th>';
		}
		
		$col  = $this->CI->hook->do_action('hook_create_listing_cols',$col);
		
		$list = "";	
		$is_listing = $this->CI->uri->segment($this->paging_config['uri_segment']-1,'');
		
		$start_page = 0;
		if($is_listing == 'listing')
		$start_page = $this->CI->uri->segment($this->paging_config['uri_segment'],0);
		
		$start_page  = $this->CI->hook->do_action('hook_create_listing_start_page',$start_page);
		
		$sql = $this->get_query() . ' LIMIT ' . $start_page . ',' . $this->paging_config['per_page'];
		$query = $this->CI->db->query($sql);

		if($query->num_rows() > 0)
		{
			$data_rows = $query->result_array();
			$this->data_rows = $data_rows;
			foreach($data_rows as $index => $data_row)
			{
				if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
				{
					$tmp_list = "";
					$tmp_list.= '
								<tr>';
					if(!empty($action))
					{
						$id_bulk_data = (isset($data_row[$this->primary_key]))?$data_row[$this->primary_key]:"";
						$tmp_list.= '
									<td align="center" valign="middle" class="no-print"><input type="checkbox" name="bulk_data[]" value="'.$id_bulk_data.'" class="bulk_data"/></td>';
					}
						$tmp_list.= '
									<td align="center">' . ++$start_page .'</td>';
					foreach($this->fields as $index_field => $field)
					{
						if(isset($field['use_listing']) and $field['use_listing'] == true)
						{
							$name = (isset($field['name']))?$field['name']:$field;
							$value = (isset($data_row[$name]))?$data_row[$name]:"";
							
							if(isset($field['table']) and !empty($field['table']))
							{
								$label = (isset($field['primary_key']))?$field['primary_key']:"";
								if(empty($label))
								{
									$label = 'label';
								}
								$where = "";
								if(isset($field['value']) and $label)
									$where = array($label => "'".$value."'");
									
								$value = $this->get_value($field['table'],$field['select'],$where,$value);
								
								if($field['type'] == 'file')
								{
									$path = (	isset($field['config_upload']) and 
														isset($field['config_upload']['upload_path']) and 
														!empty($field['config_upload']['upload_path']))
													?$field['config_upload']['upload_path']
													:"";
									$path_value = $path . $value;
									$is_image = $this->is_image($path_value);
									if($is_image)
									{
										$path = str_replace(getcwd().'/',base_url(),$path_value);
										$value = $this->show_image($path,'height="50"');
									}
								}
								
								if($field['type'] == 'date' or $field['type'] == 'datetime')
								{
									$value = $this->human_date($value,false,false);
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->CI->hook->do_action('hook_create_listing_value_'.$name,$value);
								$tmp_list.= '
											<td '.$list_style.'>' . $value . '</td>';
							}else{
								
								if($field['type'] == 'file')
								{
									$path = (	isset($field['config_upload']) and 
														isset($field['config_upload']['upload_path']) and 
														!empty($field['config_upload']['upload_path']))
													?$field['config_upload']['upload_path']
													:"";
									$path_value = $path . $value;
									$is_image = $this->is_image($path_value);
									if($is_image)
									{
										$path = str_replace(getcwd().'/',base_url(),$path_value);
										$value = $this->show_image($path,'height="50"');
									}
								}
								if($field['type'] == 'date' or $field['type'] == 'datetime')
								{
									$value = $this->human_date($value,false,false);
								}
								
								if($field['type'] == 'fixed_multiple_field' || $field['type'] == 'fixed_multiple_field_text'){
									$decoded_value = json_decode($value, true);
									
									if(is_array($decoded_value) and count($decoded_value) > 0){
										$value = "<ul style='padding:10px;'>";
										foreach($decoded_value as $idx => $dv){
											$label = $this->get_label_value($dv['label'], $field['columns']);
											$value .= "<li><span>{$label} :</span> <span class='value_data'>{$dv['value']}</span> </li>";
										}
										$value .= "</ul>";
									}
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->CI->hook->do_action('hook_create_listing_value_'.$name,$value);
								$tmp_list.= '
											<td '.$list_style.'>' . $value . '</td>';
							}
						}
					}
					if(!empty($action))
					{
						$action = $this->show_panel_allowed("","",$panel_function,$data_row[$this->primary_key]);
						$list_style = (isset($this->the_config['list_style']))?$this->the_config['list_style']:"class='rows_action'";
						$action  = $this->CI->hook->do_action('hook_create_listing_action',$action);
						$tmp_list.= '<td '.$list_style.'>'. $action .'</td>';
					}
					$tmp_list.= '
								</tr>';
					$list  .= $this->CI->hook->do_action('hook_create_listing_rows',$tmp_list);
				}
			}
		}
				
        $output = '<table width="100%">
						<thead>
							<tr>
                            	' . $col . '
                            </tr>
						</thead>';
		$output .= 	'				
						<tbody>
							'. $list .'
						</tbody>
					</table>';		
		
		$output  = $this->CI->hook->do_action('hook_create_listing_output',$output);
		return $output;
	}
	
	function create_listing_report($config = array())
	{
		if($this->is_pagination_init == false or (is_array($config) and count($config) > 0))
			$this->init_pagination($config);
		
		$t_nonce = $this->CI->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:'search';
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		$panel_function = (isset($config['panel_function']))?$config['panel_function']:array("edit","view","delete");
		
		$action = $this->show_panel_allowed("","",$panel_function);
		
		$col = "";				
		if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
		{
			$col .= '<th width="5%">No</th>';
			foreach($this->fields as $index => $field)
			{
				if(isset($field['use_listing']) and $field['use_listing'] == true)
				{
					$label = (isset($field['label']))?$field['label']:$field;
					$name = (isset($field['name']))?$field['name']:$field;
					#$col .= '<th><a href="#">ID<img src="static/img/icons/arrow_down_mini.gif" width="16" height="16" align="absmiddle" /></a></th>';
					$col .= '<th>' . $label . '</th>';
				}
			}
		}
		
		$col  = $this->CI->hook->do_action('hook_create_listing_report_cols',$col);
		
		$list = "";	
		$is_listing = $this->CI->uri->segment($this->paging_config['uri_segment']-1,'');
		
		$start_page = 0;
		if($is_listing == 'listing')
		$start_page = $this->CI->uri->segment($this->paging_config['uri_segment'],0);
		
		$start_page  = $this->CI->hook->do_action('hook_create_listing_report_start_page',$start_page);
		
		$sql = $this->get_query() . ' LIMIT ' . $start_page . ',' . $this->paging_config['per_page'];
		$query = $this->CI->db->query($sql);

		if($query->num_rows() > 0)
		{
			$data_rows = $query->result_array();
			
			foreach($data_rows as $index => $data_row)
			{
				if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
				{
					$tmp_list = "";
					$tmp_list.= '
								<tr>';
						$tmp_list.= '
									<td align="center" width="5%">' . ++$start_page .'</td>
								';
					foreach($this->fields as $index_field => $field)
					{
						if(isset($field['use_listing']) and $field['use_listing'] == true)
						{
							$name = (isset($field['name']))?$field['name']:$field;
							$value = (isset($data_row[$name]))?$data_row[$name]:"";
							$multiple_cols = (isset($field['multiple_cols']))?$field['multiple_cols']:"";
							
							if(isset($field['table']) and !empty($field['table']))
							{
								$label = (isset($field['primary_key']))?$field['primary_key']:"";
								if(empty($label))
								{
									$label = 'label';
								}
								$where = "";
								if(isset($field['value']) and $label)
									$where = array($label => "'".$value."'");
									
								$value = $this->get_value($field['table'],$field['select'],$where,$value);
								
								if($field['type'] == 'file')
								{
									$path = (	isset($field['config_upload']) and 
														isset($field['config_upload']['upload_path']) and 
														!empty($field['config_upload']['upload_path']))
													?$field['config_upload']['upload_path']
													:"";
									$path_value = $path . $value;
									$is_image = $this->is_image($path_value);
									if($is_image)
									{
										$path = str_replace(getcwd().'/',base_url(),$path_value);
										$value = $this->show_image($path,'height="50"');
									}
								}
								
								if($field['type'] == 'date' or $field['type'] == 'datetime')
								{
									$value = $this->human_date($value,false,false);
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->CI->hook->do_action('hook_create_listing_report_value_'.$name,$value);
								$tmp_list.= '
											<td '.$list_style.'>' . $value . '</td>';
							}else{
								
								if($field['type'] == 'file')
								{
									$path = (	isset($field['config_upload']) and 
														isset($field['config_upload']['upload_path']) and 
														!empty($field['config_upload']['upload_path']))
													?$field['config_upload']['upload_path']
													:"";
									$path_value = $path . $value;
									$is_image = $this->is_image($path_value);
									if($is_image)
									{
										$path = str_replace(getcwd().'/',base_url(),$path_value);
										$value = $this->show_image($path,'height="50"');
									}
								}
								if($field['type'] == 'date' or $field['type'] == 'datetime')
								{
									$value = $this->human_date($value,false,false);
								}
								
								if($field['type'] == 'fixed_multiple_field' || $field['type'] == 'fixed_multiple_field_text'){
									$decoded_value = json_decode($value, true);
									
									if(is_array($decoded_value) and count($decoded_value) > 0){
										$value = "<ul style='padding:10px;'>";
										foreach($decoded_value as $idx => $dv){
											$label = $this->get_label_value($dv['label'], $field['columns']);
											$value .= "<li><span>{$label} :</span> {$dv['value']} </li>";
										}
										$value .= "</ul>";
									}
								}
								
								if($field['type'] == 'multiple'){
									$decoded_value = json_decode($value, true);
									if(is_array($multiple_cols) and count($multiple_cols) > 0)
									{
										$value2 = json_decode($value,true);
										$value = array();
										if(is_array($value2) and count($value2) > 0)
										{
											$no = 0;
											foreach($value2 as $n => $v)
											{
												$val = array();
												foreach($v as $n2 => $v2)
												{
													$value[$n2][$n] = $v2;
												}
											}
										}
										
										$multiple_cols_output = '';

										if(is_array($value) and count($value) > 0)
										{
											$no = 0;
											foreach($value as $i => $vals)
											{
												if(implode("",$vals) == "")
													continue;
												
												$no++;
												foreach($multiple_cols as $index => $col2)
												{
													$multiple_cols_output .= $no;
													$input_multiple = "";
													$attr = (isset($col2['attr']))?$col2['attr']:"";
													$sname = (isset($col2['name']))?$col2['name']:"";
													$val = (isset($vals[$sname]))?$vals[$sname]:"";
													$multiple_cols_output .=  ' | ' . $col2['title'] . ' : ' . $val;
												}
												$multiple_cols_output .= "<br>";
											}
										}
										$value = $multiple_cols_output;
									}
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->CI->hook->do_action('hook_create_listing_report_value_'.$name,$value);
								$tmp_list.= '
											<td '.$list_style.'>' . $value . '</td>';
							}
						}
					}
					$tmp_list.= '
								</tr>';
					$list  .= $this->CI->hook->do_action('hook_create_listing_report_rows',$tmp_list);
				}
			}
		}
				
        $output = '<table width="100%" cellpadding="0" cellspacing="0" border="1">
						<thead>
							<tr>
                            	' . $col . '
                            </tr>
						</thead>';
		$output .= 	'				
						<tbody>
							'. $list .'
						</tbody>
					</table>';		
		
		$output  = $this->CI->hook->do_action('hook_create_listing_report_output',$output);
		return $output;
	}
	
	function get_total_data($query = "")
	{
		$query = (isset($query) and !empty($query))?$query:$this->get_query();
		if(!empty($query))
		{
			$q = $this->CI->db->query($query);
			return $q->num_rows();
		}
		return 0;
	}
	
	function set_filter($sess_keyname = "")
	{
		if(empty($sess_keyname))
			$sess_keyname = $this->object_name.'_data_filter';
			
		$data_filter = $this->CI->input->post('data_filter');
		if(is_array($data_filter) and count($data_filter) > 0)
		{
			$this->CI->session->set_userdata($sess_keyname,"");
			$this->CI->session->set_userdata($sess_keyname,$data_filter);
		}		
	}
	
	function get_filter($sess_keyname = "",$to_string = false)
	{
		if(empty($sess_keyname))
			$sess_keyname = $this->object_name.'_data_filter';
		
		$filter = $this->CI->session->userdata($sess_keyname);
		
		if($to_string)
		{
			if(is_array($filter) and count($filter) > 0)
			{
				$tmp_filter = "";
				foreach($filter as $index => $fil)
				{
					$field = $this->get_config_field_by("name",$index);
					if($field['type'] == 'selectbox')
					{
						if($fil != "")
							$tmp_filter .= ' AND '.$index.' LIKE "'.$fil.'"';
					}elseif($field['type'] == 'range_date_selectbox')
					{
						if($fil != "")
						{
							$start_date = implode("-",$fil['from']);
							$end_date = implode("-",$fil['to']);
							if(empty($end_date))
								$end_date = $start_date;
								
							$tmp_filter .= ' AND ('.$index.' BETWEEN "'.$start_date.'" AND "'.$end_date.'")';
						}
					}elseif($field['type'] == 'range_year_selectbox')
					{
						if($fil != "")
						{
							$start_date = implode("-",$fil['from']);
							$end_date = implode("-",$fil['to']);
							if(empty($end_date))
								$end_date = $start_date;
								
							$tmp_filter .= ' AND ('.$index.' BETWEEN "'.$start_date.'-01-01" AND "'.$end_date.'-12-31")';
						}
					}else{
						if(!empty($fil))
						$tmp_filter .= ' AND '.$index.' LIKE "%'.$fil.'%"';
					}
				}
				$filter = $tmp_filter;
			}
		}
		
		return $filter;
	}
	
	function get_sort_order()
	{	
		$order_by = (isset($this->the_config['sort_order']))?$this->the_config['sort_order']:"";
		return $order_by;
	}
	
	function get_query($query = "",$sess_keyname = "")
	{
		$filter = $this->get_filter($sess_keyname,true);
		$query = (isset($query) and !empty($query))?$query:$this->query;		
		$append_where = (isset($this->the_config['where']))?$this->the_config['where']:"";
		$append_where_operator = (isset($this->the_config['where_operator']))?$this->the_config['where_operator']:"AND";
		if(!empty($append_where))
		{
			if(is_array($append_where) and count($append_where) > 0)
			{
				$append_where_operator = ' ' . $append_where_operator . ' ';
				$tmp_append_where = "";
				foreach($append_where as $i => $w)
				{
					if(!empty($w) and is_array($w) and count($w) > 0)
					{
						$tmp_append_where .= implode($append_where_operator,$w);
					}
				}
				$append_where = $tmp_append_where;
			}
			if(!empty($append_where))
			{
				$append_where = ' '.$append_where.' ';
			}
		}
		$query = $query . $append_where . $filter  ;
		$query .= ($this->get_sort_order() != "")?" ORDER BY " . $this->get_sort_order():"";
		return $query;
	}
	
	function clear_filter($sess_keyname = "")
	{
		if(empty($sess_keyname))
			$sess_keyname = $this->object_name.'_data_filter';
		$this->CI->session->set_userdata($sess_keyname,'');
		$this->CI->session->unset_userdata($sess_keyname);
	}
	
	function show_panel_allowed($user_id = "",$controller = "",$panels = array(),$primary_key = "",$ul = true)
	{
		$panels_allowed = array();
		$controller = (!empty($controller))?$controller:$this->object_name;
		
		if(is_array($panels) and count($panels) > 0)
		{
			foreach($panels as $index => $panel)
			{
				$is_allowed = $this->CI->user_access->is_process_allowed($user_id,$controller,$panel);
				if($is_allowed)
				{
					$panels_allowed[] = $panel;
				}
			}
		}
		
		if((is_array($panels_allowed) and count($panels_allowed) == 0) or empty($panels_allowed))
			return "";
		
		$panels_output = "";
		foreach($panels_allowed as $index => $panel)
		{
			if($ul)
			{
				
				$tmp_panels_output  = '<li><a href="'. $this->CI->user_access->get_url($controller,$panel) . '/' . $primary_key .'" class="'.$panel.' a_ajax">' . str_replace("_"," ",$panel) . '</a></li>';
				$tmp_panels_output  = $this->CI->hook->do_action('hook_show_panel_allowed_panel_' . $controller . '_' . $panel,$tmp_panels_output);
				$panels_output .= $tmp_panels_output;
			}
			else
			{
				
				$tmp_panels_output  = '<a href="'. $this->CI->user_access->get_url($controller,$panel) . '/' . $primary_key .'" class="'.$panel.' a_ajax">' . str_replace("_"," ",$panel) . '</a>';
				$tmp_panels_output  = $this->CI->hook->do_action('hook_show_panel_allowed_panel_' . $controller . '_' . $panel,$tmp_panels_output);
				$panels_output .= $tmp_panels_output;
			}
		}
		
		$output = '
				<ul class="action_menu">
					' . $panels_output . '
				</ul>
				';
		if(!$ul)
			$output = $panels_output;
		
		$output  = $this->CI->hook->do_action('hook_show_panel_allowed_output',$output);
		return $output;
	}
	
	function show_bulk_allowed($user_id = "",$controller = "",$panels = array(),$primary_key = "",$ul = true)
	{
		$panels_allowed = array();
		$controller = (!empty($controller))?$controller:$this->object_name;
		
		if(is_array($panels) and count($panels) > 0)
		{
			foreach($panels as $index => $panel)
			{
				$is_allowed = $this->CI->user_access->is_process_allowed($user_id,$controller,$panel);
				if($is_allowed)
				{
					$panels_allowed[] = $panel;
				}
			}
		}
		
		if((is_array($panels_allowed) and count($panels_allowed) == 0) or empty($panels_allowed))
			return "";
		
		$panels_output = "";
		foreach($panels_allowed as $index => $panel)
		{
			if($ul)
			{
				
				$tmp_panels_output  = '<option value="'. $controller . '/' . $panel . '">' . str_replace("_"," ",$panel) . '</option>';
				$tmp_panels_output  = $this->CI->hook->do_action('hook_show_bulk_allowed_panel_' . $controller . '_' . $panel,$tmp_panels_output);
				$panels_output .= $tmp_panels_output;
			}
			else
			{
				
				$tmp_panels_output  = '<optin value="'. $panel . '">' . str_replace("_"," ",$panel) . '</option>';
				$tmp_panels_output  = $this->CI->hook->do_action('hook_show_bulk_allowed_panel_' . $controller . '_' . $panel,$tmp_panels_output);
				$panels_output .= $tmp_panels_output;
			}
		}
		
		$t_nonce = $this->CI->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:current_url();
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		
		$is_ajax = $this->CI->hook->do_action('hook_show_bulk_allowed_is_ajax','');
		$ajax_target = $this->CI->hook->do_action('hook_show_bulk_allowed_ajax_target','#main_content .col1');
		$button_action = $this->CI->hook->do_action('hook_show_bulk_allowed_button_action','<button type="submit" name="do_bulk_action" value="GO"/>');
		$action = $this->CI->hook->do_action('hook_show_bulk_allowed_action_url',$action);
		$form_target = $this->CI->hook->do_action('hook_show_bulk_allowed_form_target','_self');
		$form_class = $this->CI->hook->do_action('hook_show_bulk_allowed_form_class','form_bulk no_ajax');

		$output = '
				<form method="post" action="'.$action.'" class="'.$form_class.'" target="'.$form_target.'">
					<select name="bulk_action">
						<option value="" align="center"> Choose Action </option>
						' . $panels_output . '
					</select>
					<input type="hidden" name="nonce" value="'.$nonce.'"/> 
					<input type="hidden" name="ajax_target" value="'.$ajax_target.'"/> 
					<input type="hidden" name="is_ajax" value="'.$is_ajax.'"/> 
					<input type="hidden" name="bulk_data" value="" id="bulk_data"/> 
					'.$button_action.'
				</form>
				';
		if(!$ul)
			$output = $panels_output;
		
		$output  = $this->CI->hook->do_action('hook_show_bulk_allowed_output',$output);
		return $output;
	}

	function add($data = array(),$config = array())
	{
		$nonce = $this->CI->input->post("nonce");
		if(!empty($nonce))
		{
			$this->CI->load->library('upload');
			$this->CI->load->library('form_validation');
			$data = (is_array($data) and count($data) > 0)?$data:$this->CI->input->post('data');
			$input_multiple = $this->CI->input->post('input_multiple');
			$fixed_multiple_data = $this->CI->input->post('fixed_multiple_data');
			$the_config = $config;
			$config = array();
			
			$type_files = "";
			$rules_files = "";
			if(is_array($the_config) and count($the_config) > 0)
			{
				foreach($the_config as $index => $con)
				{
					$config[] = array(
										'field' => 'data['.$con['name'].']',
										'label' => $con['label'],
										'rules' => $con['rules']
									 );
					if($con['type'] == 'file')
					{
						$type_files[$con['name']] = array("config" => $con['config_upload']);
						$rules_files[$con['name']] = $con['rules'];
					}
				}
			}
			
			$method_name = (!empty($this->method_name))?$this->method_name:'listing';
			$t_nonce = $this->CI->user_access->get_nonce();
			#$action = (isset($config['action']))?$config['action']:base_url().$this->object_name.'/'.$method_name;
			$action = (isset($config['action']))?$config['action']:current_url();
			$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
			
			
			$this->CI->form_validation->set_error_delimiters('<p class="error">', '</p>');
			$this->CI->form_validation->set_rules($config);
			$this->CI->form_validation->run();
			
			$this->CI->lang->load('upload', $this->CI->config->item('language'));
			
			$errors = validation_errors();
			if(is_array($type_files) and count($type_files) > 0 and empty($errors))
			{
				$error_empty_file = $this->CI->lang->line('upload_no_file_selected');
				$this->files = $this->reconf_file_upload();
				$upload = array();
				foreach($type_files as $input_name => $file)
				{
					$upload = $this->upload($input_name,$file['config']);	

					if(isset($upload['error']))
					{
						if($upload['error'] == $error_empty_file and $rules_files[$input_name] == "required")
						{
							$errors .= $upload['error'] . $upload['error'] ." == " . $error_empty_file . " and " . $rules_files[$input_name] . " == 'required'";
						}
					}else{
						$data[$input_name] = $upload['success']['file_name'];
					}
				}
				
			}
			
			if(!empty($errors))
				return false;
			
			if(is_array($input_multiple) and count($input_multiple) > 0)
			{
				foreach($input_multiple as $index => $mul)
				{
					if(isset($mul[0]))
						unset($mul[0]);
						
					if(is_array($mul) and count($mul) > 0)
					{
						$data[$index] = json_encode($mul);
					}
				}
			}
			
			if(is_array($fixed_multiple_data) and count($fixed_multiple_data))
			{
				foreach($fixed_multiple_data as $index => $mul)
				{
							
					if(is_array($mul) and count($mul) > 0)
					{
						$data[$index] = json_encode($mul);
					}
				}
				
			}
			
			if(count($data) > 0 and is_array($data))
			{
				$data  = $this->CI->hook->do_action('hook_do_add',$data);
				$query = $this->CI->db->insert($this->table,$data);
				if($query)
				{
					return "<p class='success'>Data berhasil disimpan</p>";
				}else{
					return "<p class='error'>Data gagal disimpan</p>";
				}
				
			}
		}
	}

	function edit($data = array(),$config = array())
	{
		$nonce = $this->CI->input->post("nonce");
		if(!empty($nonce))
		{
			$this->CI->load->library('upload');
			$this->CI->load->library('form_validation');
			$data = (is_array($data) and count($data) > 0)?$data:$this->CI->input->post('data');
			$input_multiple = $this->CI->input->post('input_multiple');
			$fixed_multiple_data = $this->CI->input->post('fixed_multiple_data');
			$the_config = $config;
			$config = array();
			
			$type_files = "";
			if(is_array($the_config) and count($the_config) > 0)
			{
				foreach($the_config as $index => $con)
				{
					$con['rules'] = explode("|",$con['rules']);
					if(is_array($con['rules']) and count($con['rules']) > 0)
					{
						$tmp_rules = array();
						foreach($con['rules'] as $idx => $rul)
						{
							if(strpos($rul,'is_unique') === false)
							{
								$tmp_rules[] = $rul;
							}else{
								$field = str_replace(array('is_unique','[',']'),array('','',''),$rul);
								$field = explode('.',$field);
								$table = (isset($field[1]))?$field[0]:$this->table;
								$field = (isset($field[1]))?$field[1]:$field[0];
								$get_current_value = $this->CI->master->get_value($table,$field,array($this->primary_key => $this->primary_key_value));
								echo "$get_current_value != ".$data[$con['name']];
								if(isset($data[$con['name']]) and $get_current_value != $data[$con['name']])
								{
									$tmp_rules[] = $rul;
								}
							}
						}
						$con['rules'] = implode("|",$tmp_rules);
					}
					$config[] = array(
										'field' => 'data['.$con['name'].']',
										'label' => $con['label'],
										'rules' => $con['rules']
									 );
					if($con['type'] == 'file')
					{
						$type_files[$con['name']] = array("config" => $con['config_upload']);
					}
				}
			}
			
			$method_name = (!empty($this->method_name))?$this->method_name:'listing';
			$t_nonce = $this->CI->user_access->get_nonce();
			#$action = (isset($config['action']))?$config['action']:base_url().$this->object_name.'/'.$method_name;
			$action = (isset($config['action']))?$config['action']:current_url();
			$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
			
			
			$this->CI->form_validation->set_error_delimiters('<p class="error">', '</p>');
			$this->CI->form_validation->set_rules($config);
			$this->CI->form_validation->run();
			
			$this->CI->lang->load('upload', $this->CI->config->item('language'));
			
			$errors = validation_errors();
			if(is_array($type_files) and count($type_files) > 0 and empty($errors))
			{
				$error_empty_file = $this->CI->lang->line('upload_no_file_selected');
				$this->reconf_file_upload();
				$upload = array();
				foreach($type_files as $input_name => $file)
				{
					$upload = $this->upload($input_name,$file['config']);
					if(isset($upload['error']))
					{
						if($upload['error'] == $error_empty_file and $rules_files[$input_name] == "required")
						{
							$errors .= $upload['error'] . $upload['error'] ." == " . $error_empty_file . " and " . $rules_files[$input_name] . " == 'required'";
						}
					}else{
						if(isset($upload['success']['file_name']) and !empty($upload['success']['file_name']))
							$data[$input_name] = $upload['success']['file_name'];
					}
				}
			}
			
			if(!empty($errors))
				return false;
			
			if(is_array($input_multiple) and count($input_multiple) > 0)
			{
				foreach($input_multiple as $index => $mul)
				{
					if(isset($mul[0]))
						unset($mul[0]);
						
					if(is_array($mul) and count($mul) > 0)
					{
						$data[$index] = json_encode($mul);
					}
				}
			}
			
			
			if(is_array($fixed_multiple_data) and count($fixed_multiple_data))
			{
				foreach($fixed_multiple_data as $index => $mul)
				{
							
					if(is_array($mul) and count($mul) > 0)
					{
						$data[$index] = json_encode($mul);
					}
				}
				
			}
			
			if(count($data) > 0 and is_array($data))
			{
				$data  = $this->CI->hook->do_action('hook_do_edit',$data);
				$this->CI->db->where($this->primary_key,$this->primary_key_value);
				$query = $this->CI->db->update($this->table,$data);
				if($query)
				{
					return '<p class="success">Data berhasil dirubah</p>';
				}else{
					return '<p class="error">Data gagal dirubah</p>';
				}
				
			}
		}
	}

	function delete($data = array(),$config = array())
	{
		$nonce = $this->CI->input->post("nonce");
		$bulk_data = $this->CI->input->post("bulk_data");
		if(!empty($bulk_data))
		{
			if(!is_array($bulk_data))
			{
				$bulk_data = explode(",",$bulk_data);
			}
		}
		#if(!empty($nonce))
		#{
				$t_nonce = $this->CI->user_access->get_nonce();
				$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
					
				$the_config = $config;
				$type_files = "";
				if(is_array($the_config) and count($the_config) > 0)
				{
					foreach($the_config as $index => $con)
					{
						$config[] = array(
											'field' => 'data['.$con['name'].']',
											'label' => $con['label'],
											'rules' => $con['rules']
										 );
						if($con['type'] == 'file')
						{
							$type_files[$con['name']] = array("config" => $con['config_upload']);
						}
					}
				}
				
				$query = false;
				
				if(!empty($bulk_data) and is_array($bulk_data) and count($bulk_data) > 0)
				{
					foreach($bulk_data as $index => $idx_value)
					{
						if(!empty($idx_value))
						{
							$this->primary_key_value = $idx_value;
							if(is_array($type_files) and count($type_files) > 0)
							{
								$this->CI->db->where($this->primary_key,$this->primary_key_value);
								$query = $this->CI->db->get($this->table);
								$the_data = $query->row_array();
								foreach($type_files as $index => $row)
								{
									$config_upload = $row['config'];
									if(isset($the_data[$index]) and !empty($the_data[$index]) and file_exists($config_upload['upload_path'] . $the_data[$index]))
									{
										@unlink($config_upload['upload_path'] . $the_data[$index]);
									}
								}
							}
							
							$data  = $this->CI->hook->do_action('hook_do_delete',$this);
							$this->CI->db->where($this->primary_key,$this->primary_key_value);
							$query = $this->CI->db->delete($this->table);
						}
					}
				
				}
				else
				{
					if(is_array($type_files) and count($type_files) > 0)
					{
						$this->CI->db->where($this->primary_key,$this->primary_key_value);
						$query = $this->CI->db->get($this->table);
						$the_data = $query->row_array();
						foreach($type_files as $index => $row)
						{
							$config_upload = $row['config'];
							if(isset($the_data[$index]) and !empty($the_data[$index]) and file_exists($config_upload['upload_path'] . $the_data[$index]))
							{
								@unlink($config_upload['upload_path'] . $the_data[$index]);
							}
						}
					}
					
					$data  = $this->CI->hook->do_action('hook_do_delete',$this);
					$this->CI->db->where($this->primary_key,$this->primary_key_value);
					$query = $this->CI->db->delete($this->table);
				}
				
				if($query)
				{
					return "Data berhasil dihapus";
				}else{
					return "Data gagal dihapus";
				}
		#}
	}
	
	function set_message_error()
	{
		
			$this->CI->form_validation->set_message('required', 'Your custom message here');
	}

	function upload($input_name = "",$config = array())
	{
		if(empty($input_name))
			return false;
			
		$tconfig = array();
		$tconfig['upload_path'] = './uploads/others/';
		
		$config = array_merge($tconfig,$config);
		
		$this->CI->upload->initialize($config);
		
		$data = "";
		if ( ! $this->CI->upload->do_upload($input_name))
		{
			$data = array('error' => $this->CI->upload->display_errors());
		}
		else
		{
			$data = array('success' => $this->CI->upload->data());
		}
		return $data;
	}
	
	function reconf_file_upload()
	{
		$_FILES = (isset($_FILES) and count($_FILES) > 0)?$_FILES:array();
		$files = (isset($_FILES) and count($_FILES) > 0)?$_FILES:array();
		$the_files = array();
			
		if(is_array($files) and count($files) > 0)
		{
				foreach($files as $idx => $file)
				{
					foreach($file as $index => $f)
					{
						foreach($f as $i => $f2)
						{
							$the_files[$i][$index]= $f2;
						}  
					}
				}
			}
			
			if(is_array($the_files) and count($the_files) > 0)
			{
				foreach($the_files as $index => $data)
				{
					$_FILES[$index] = $data;
				}
			}
			
		return $_FILES; 
	}
	
	function show_image($url = "",$attribute = "")
	{
		$output = '<img src="'.$url.'" '.$attribute.' style="padding:5px;border:1px green solid;text-align:center;margin:5px auto;"/>';
		
		$output  = $this->CI->hook->do_action('hook_show_image_output',$output);
		return $output;
	}
	
	function is_image($path)
	{
			$result = false;

			if (is_file($path) === true)
			{
					if (function_exists('finfo_open') === true)
					{
							$finfo = finfo_open(FILEINFO_MIME_TYPE);

							if (is_resource($finfo) === true)
							{
									$result = finfo_file($finfo, $path);
							}

							finfo_close($finfo);
					}

					else if (function_exists('mime_content_type') === true)
					{
							$result = preg_replace('~^(.+);.*$~', '$1', mime_content_type($path));
					}

					else if (function_exists('exif_imagetype') === true)
					{
							$result = image_type_to_mime_type(exif_imagetype($path));
					}else{
						$type =array('jpg','gif','png');

							foreach($type as $val){
								$ext = pathinfo($path, PATHINFO_EXTENSION);
								$ext = strtolower($ext);
								if(in_array($ext,$type))
								{
									$result = true;
								}
							}
					}
			}

			return $result;
	}
	
	function hari($hari = "")
	{
		$hari2 = array(	"1" => "Senin",
						"2" => "Selasa",
						"3" => "Rabu",
						"4" => "Kamis",
						"5" => "Jum'at",
						"6" => "Sabtu",
						"7" => "Minggu"
						);
		$str_hari = (isset($hari2[$hari]))?$hari2[$hari]:"";
		return $str_hari;
	}
	
	function bulan($int_bulan = "")
	{
		$bulan = array(	"1" => "Januari",
						"2" => "Februari",
						"3" => "Maret",
						"4" => "April",
						"5" => "Mei",
						"6" => "Juni",
						"7" => "Juli",
						"8" => "Agustus",
						"9" => "September",
						"10" => "Oktober",
						"11" => "November",
						"12" => "Desember"
						);
		$str_bulan = (isset($bulan[$int_bulan]))?$bulan[$int_bulan]:"";
		return $str_bulan;
	}
	
	function human_date($the_date = "",$the_time = false, $the_day = false)
	{
		$output = '';
		
		if(!empty($the_date))
		{
			$date = explode(" ",$the_date);
			$time = (isset($date[1]))?$date[1]:"";
			$date = explode("-",$date[0]);
			
			$day  = strtotime($the_date);
			$day  = date("N",$day);
			$day  = $this->hari($day);
			
			$month = (isset($date[1]))?$this->bulan((int)$date[1]):"";
			if($the_day)
				$output .= $day.", ";
			
			$output .= (isset($date[2]))?$date[2]." ":"";
			$output .= (!empty($month))?$month." ":"";
			$output .= (isset($date[0]))?$date[0]." ":"";
			
			if($the_time)
				$output .= $time;
		}
		
		return $output;
	}
	
	function get_config_field_by($by = "name",$value = "")
	{
		$fields = $this->fields;
		if(is_array($fields) and count($fields) > 0)
		{
			foreach($fields as $index => $field)
			{
				if($field[$by] == $value)
					return $field;
			}
		}
		
		return false;
	}
		
	function _get_controllers($type = "all")
	{  
		$all    = array();
		$methods    = array();

		$dir            = APPPATH.'/controllers/';
		$files          = scandir($dir);

		$controller_files = array_filter($files, function($filename) {
			return (substr(strrchr($filename, '.'), 1)=='php') ? true : false;
		});
		
		$allmethods = array();
		foreach ($controller_files as $filename)
		{
			require_once('./application/controllers/'.$filename);

			$classname = ucfirst(substr($filename, 0, strrpos($filename, '.')));
			#$controller = new $classname();
			$t_methods = get_class_methods($classname);
			$methods = array();
			foreach ($t_methods as $index => $method)
			{
				if((strpos($method,'_') <= 0 and strpos($method,'_') === 0) or $method == 'get_instance')
				{
					#unset($methods[$index]);
				}else{
					$methods[$method] = strtolower($method);
					$allmethods[strtolower($classname)][$method] = strtolower($method);
				}
			}
			
			#unset($controller);
			
			$controller_info = array(
				'filename' => $filename,
				'class_name' => strtolower($classname),
				'methods'  => $methods
			);
			
			$all[strtolower($classname)] = strtolower($classname);
			$controller_methods[strtolower($classname)] = $controller_info;
		}
				
		$this->controllers =  $all;
		$this->methods =  $allmethods;
		$this->controller_methods =  $controller_methods;
	}
	
	function excerpt($content = "",$length = 300)
	{
		$this->CI->load->helper('text');
		$content = strip_tags($content);
		$content = character_limiter($content,$length);
		$content  = $this->CI->hook->do_action('hook_excerpt_content',$content);
		return $content;
	}
	
	function is_option_selected($decoded_value, $keyoption, $opt){
		if(!is_array($decoded_value))
			return false;
		
		if(count($decoded_value) == 0)
			return false;
		
		foreach($decoded_value as $idx => $dv){
			if($dv['label'] == $keyoption AND $dv['value'] == $opt)
				return true;
		}
		
		return false;
	}
	
	function get_label_value($label_id, $columns){
		if(!is_array($columns)) return $label_id;
		
		foreach($columns as $idx => $column){
			if($column['key_option'] == $label_id)
				return $column['label'];
		}
		
		return $label_id;
	}
	
	function sum_data($data){
		//type data is json
		$data = json_decode($data, true);
		if(empty($data))
			return 0;
			
		$sum = 0;
		foreach($data as $idx => $dt){
			if(isset($dt['value']))
				$sum += $dt['value'];
		}
		
		return $sum;
	}
	
	function get_average_data($data, $total_data = ''){
		//type data is json
		$data = json_decode($data, true);
		if(empty($data))
			return 0;
		
		if(empty($total_data))
			$total_data = count($data);
			
		$sum = 0;
		foreach($data as $idx => $dt){
			if(isset($dt['value']))
				$sum += $dt['value'];
		}
		
		$average = $sum / $total_data;
		
		return $average;
	}
	
	function get_qualification($nilai = 0)
	{
		$output = "";
		if($nilai < 60)
			$output = "Tidak Lulus";
			
		if($nilai >= 60 and $nilai <= 69)
			$output = "Cukup";
			
		if($nilai >= 70 and $nilai <= 84)
			$output = "Baik";
			
		if($nilai >= 85)
			$output = "Baik Sekali";
		
		return $output;
	}
}
