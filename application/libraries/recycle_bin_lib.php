<?php

class Recycle_bin_lib{
	var $CI;
	
	function Recycle_bin_lib(){
		$this->CI =& get_instance();
		$this->init();
	}
	
	function init(){
		$this->CI->load->library('user_agent');
		$this->CI->load->library("user_access");
		
		$this->insert();
	}
	
	function current_access()
	{
		$data = 	array(	"user_id" => $this->CI->user_access->current_user_id,
							"datetime" => date("Y-m-d h:i:s"),
							"url" => current_url(),
							"access" => $this->CI->router->class . '/' . $this->CI->router->method,
							"request" => (isset($_REQUEST))?json_encode($_REQUEST):json_encode(array()),
							"ip_address" => $this->CI->input->ip_address(),
							"browser" => $this->CI->agent->browser(),
							"user_agent" => $this->CI->agent->agent_string()
						);
		return $data;
	}
	
	function insert()
	{
		$data = $this->current_access();
		#$this->CI->db->insert("log",$data);
	}
}
