<?php

class User_access{
	var $current_user_id;
	var $current_user_name;
	var $current_user_level;
	var $CI;
	
	function User_access(){
		$this->CI =& get_instance();
		$user_id = $this->CI->session->userdata("user_id");
		$user_name = $this->CI->session->userdata("user_name");
		$this->current_user_id = $user_id;
		$this->current_user_name = $this->get_user_name($user_id);
		$this->current_user_level = $this->get_level($user_id);
		
		$is_login = $this->is_login();
		if(!$is_login and $this->CI->uri->segment(1) != "dashboard" and $this->CI->uri->segment(2) != "login")
		{
			header("location: ".base_url()."dashboard/login");
		}
	}
	
	/* Untuk mengecek page apakah diperbolehkan di akses atau tidak. 
	 * Isi parameter untuk spesifik user
	 * kosongkan untuk current user
	 * Param : user_id,controller,function
	 * Return : true/false
	*/
	function is_page_allowed($user_id = "",$controller = "",$function = "index")
	{
		if(empty($controller))
			return false;
			
		$function = (empty($function))?$controller:$function;
		$roles = $this->get_roles($user_id,$controller,$function);
		
		if(is_array($roles) and count($roles) > 0)
			return true;
		
		return false;
	}
	
	/* Untuk mengecek menu apakah diperbolehkan di akses atau tidak. 
	 * Isi parameter untuk spesifik user
	 * kosongkan untuk current user
	 * Param : user id
	 * Return : true/false
	*/
	function is_menu_allowed($user_id = "",$controller = "")
	{
		if(empty($controller))
			return false;
			
		$roles = $this->get_roles($user_id,$controller,'index');
		
		if(is_array($roles) and count($roles) > 0)
			return true;
		
		return false;
	}
	
	/* Untuk mengecek proses (update,delete,add dll) apakah diperbolehkan di akses atau tidak
	 * Isi parameter untuk spesifik user
	 * kosongkan untuk current user
	 * Param : user id
	 * Return : true/false
	*/
	function is_process_allowed($user_id = "",$controller = "",$function = "")
	{
		if(empty($controller))
			return false;
			
		if(empty($function))
			return false;
			
		$function = (empty($function))?$controller:$function;
		$roles = $this->get_roles($user_id,$controller,$function);
		
		if(is_array($roles) and count($roles) > 0)
			return true;
		
		return false;
		
	}
	
	/* Untuk menampilkan semua menu yang diperbolehkan di akses
	 * Isi parameter untuk spesifik user
	 * kosongkan untuk current user
	 * Param : user id
	 * Return : true/false
	*/
	function get_menus_allowed($user_id = "",$where = "")
	{
		$user_level = $this->get_level($user_id);
		$query = $this->CI->db->query("SELECT * FROM user_level_roles,menu WHERE ((user_level_roles.controller = menu.controller AND user_level_roles.function = menu.function AND user_level_roles.user_level_id = '$user_level') or menu.controller = '#') $where GROUP BY menu.controller,menu.function,menu.menu_name ORDER BY sort_order ASC");
		$menus = $query->result_array();		
		return $menus;
	}
		
	function mapTree($dataset = array(),$parent = 0) {
		$tree = array();
		
		if(isset($dataset) and is_array($dataset) and count($dataset) > 0)
		{
			foreach($dataset as $index => $arrays)
			{
				#$the_array = array();
				if($arrays['parent_menu'] == $parent)
				{
					#$the_array[$arrays['menu_id']] = $arrays;
					$arrays['children'] = $this->mapTree($dataset,$arrays['menu_id']);
					$tree[$arrays['menu_id']] = $arrays;
				}
			}
		}
		return $tree;
	}
	
	function display_structure($nodes, $indent=0, $max_depth = "") {	
		if(!empty($max_depth) and $indent >= $max_depth)
			return "";	
		
		$output = "";
		if(count($nodes) > 0 and is_array($nodes) > 0)
		{
			$output = "\n".'<ul>'."\n";
			foreach ($nodes as $node) {
				#$output .= str_repeat('&nbsp;',$indent*4);
				$href = base_url().$node['controller'].'/'.$node['function'];
				if(strpos($href,'#') !== false)
					$href = '#';
					
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output_tmp = $this->display_structure($node['children'],$indent+1, $max_depth);
					$output_tmp2 = str_replace("\n","",$output_tmp);
					if($output_tmp2 == "<ul></ul>" and $href == '#')
					{}else{
						$output .= '<li><a href="'.$href.'">'.$node['menu_title'].'<span class="ui-icon ui-icon-arrowthick-1-e"></span></a>';
						$output .= $output_tmp;
						$output .= '</li>'."\n";
					}
				}else{
					if($href != '#')
					{
						$output .= '<li><a href="'.$href.'">'.$node['menu_title'].'</a>';
						$output .= '</li>'."\n";
					}
				}
				
			}
			$output .= '</ul>'."\n";
		}
		
		return $output;
	}
	
	function display_structure2($nodes, $indent=0, $max_depth = "") {	
		if(!empty($max_depth) and $indent >= $max_depth)
			return "";	
		
		$output = "";
		if(count($nodes) > 0 and is_array($nodes) > 0)
		{
			$output = '<ul>'."\n";
			foreach ($nodes as $node) {
				#$output .= str_repeat('&nbsp;',$indent*4);
				$href = base_url().$node['controller'].'/'.$node['function'];
				if(strpos($href,'#') !== false)
					$href = '#';
					
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output .= '<li><a href="'.$href.'"><span class="ui-icon  '. $node['icon']. '"></span>'.$node['menu_title'].'<span class="ui-icon ui-icon-arrowthick-1-e"></span></a>';
					$output .= $this->display_structure2($node['children'],$indent+1, $max_depth);
				}else{
					if($href != '#')
						$output .= '<li><a href="'.$href.'"><span class="ui-icon  '. $node['icon']. '"></span>'.$node['menu_title'].'</a>';
				}
				
				$output .= '</li>'."\n";
			}
			$output .= '</ul>'."\n";
		}
		
		return $output;
	}

	function get_menus_allowed_structured($user_id = "",$depth = "") {		
		$a = $this->get_menus_allowed($user_id);
		$mapped = array();
		$mapped = $this->mapTree($a);
		$output = $this->display_structure($mapped,0,$depth);
		return $output;
	}
	
	function get_menu($where = array())
	{
		if(!empty($where))
			$this->CI->db->where($where);
		$q = $this->CI->db->get("menu");
		$menu = $q->row_array();
		return $menu;
	}
	
	function get_menu_by_parent($parent_menu = "0",$user_id = "")
	{
		$user_level = $this->get_level($user_id);
		$query = $this->CI->db->query("SELECT * FROM user_level_roles,menu WHERE menu.parent_menu = '".$parent_menu."' AND ((user_level_roles.controller = menu.controller AND user_level_roles.function = 'index' AND user_level_roles.user_level_id = '$user_level') or menu.controller = '#') GROUP BY menu.controller,menu.function,menu.menu_name ORDER BY sort_order ASC");
		$menus = $query->result_array();		
		return $menus;
	}
	
	/* Untuk mendapatkan data user
	 * Isi parameter untuk spesifik user
	 * kosongkan untuk current user
	 * Param : user id
	 * Return : true/false
	*/
	function get_user($user_id = "")
	{
		$user_id = (empty($user_id))?$this->current_user_id:$user_id;
		
		$this->CI->db->where(array("user_id" => $user_id));
		$query = $this->CI->db->get("users");
		
		if($query->num_rows() == 0)
			return false;
			
		$user_data = $query->row_array();
		if(isset($user_data['password']))
			unset($user_data['password']);
			
		return $user_data;
	}
	
	/* Untuk mendapatkan data user
	 * Isi parameter untuk spesifik user
	 * kosongkan untuk current user
	 * Param : user id
	 * Return : true/false
	*/
	function get_user_by_username($user_name = "")
	{		
		$this->CI->db->where(array("user_name" => $user_name));
		$query = $this->CI->db->get("users");
		
		if($query->num_rows() == 0)
			return false;
			
		$user_data = $query->row_array();
		
		if(isset($user_data['password']))
			unset($user_data['password']);
			
		return $user_data;
	}
	
	/* Untuk menampilkan user detail apakah lembaga, pengunjung, dll
	 * Isi parameter untuk spesifik user
	 * kosongkan untuk current user
	 * Param : user id
	 * Return : array
	*/
	function get_user_detail($user_id = "")
	{
		$user = $this->get_user($user_id);
		if($user === false)
			return array();
		
		$user_detail = array();
		$user_type = $user['user_type'];
		if ($this->CI->db->table_exists($user_type))
		{
			$this->CI->db->where(array('user_id' => $user_id));
			$query = $this->CI->db->get($user_type);
			$user_detail = $query->row_array();
		}
		return $user_detail;
	}
	
	/* Untuk menampilkan user level
	 * Isi parameter untuk spesifik user
	 * kosongkan untuk current user
	 * Param : user id
	 * Return : true/false
	*/
	function get_level($user_id = "")
	{
		$user_data = $this->get_user($user_id);
		$user_level = (isset($user_data['user_level_id']))?$user_data['user_level_id']:0;
		
		return $user_level;
	}
	
	/* Untuk menampilkan username
	 * Isi parameter untuk spesifik user
	 * kosongkan untuk current user
	 * Param : user id
	 * Return : true/false
	*/
	function get_user_name($user_id = "")
	{
		$user_data = $this->get_user($user_id);
		$user_name = (isset($user_data['user_name']))?$user_data['user_name']:'';
		
		return $user_name;
	}
	
	/* Untuk menampilkan username
	 * Isi parameter untuk spesifik user
	 * kosongkan untuk current user
	 * Param : user id
	 * Return : true/false
	*/
	function get_user_id($user_id = "")
	{
		$user_data = $this->get_user($user_id);
		$user_id = (isset($user_data['user_id']))?$user_data['user_id']:0;
		
		return $user_id;
	}
	
	/* Untuk menampilkan user email
	 * Isi parameter untuk spesifik user
	 * kosongkan untuk current user
	 * Param : user id
	 * Return : true/false
	*/
	function get_user_email($user_id = "")
	{
		$user_data = $this->get_user($user_id);
		$email = (isset($user_data['email']))?$user_data['email']:'';
		
		return $email;
	}
	
	
	/* Untuk mengecek user apakah sudah login atau belum
	 * Return : true/false
	*/
	function is_login()
	{
		$user_id = $this->CI->session->userdata("user_id");
		$secure_nonce = $this->CI->session->userdata("secure_nonce");
		
		if(!empty($user_id) and !empty($secure_nonce))
			return true;
			
		return false;
	}
	
	/* Proses login
	 * Param : username,password
	 * Return : true/false
	 */
	function do_login($user_name = "",$password = "")
	{
		$query = $this->CI->db->query("SELECT * FROM users WHERE user_name = '".$user_name."' AND password = '".$password."' AND status = 'active'");
		$user = $query->row_array();
		if($query->num_rows() > 0 and isset($user['user_id']) and !empty($user['user_id']))
		{
			$this->CI->session->set_userdata("user_id",$user['user_id']);
			$this->CI->session->set_userdata("secure_nonce",time().uniqid(true).$user['user_id']);
			return true;
		}
		
		return false;
	}
	
	/* Proses logout
	 * Param : username
	 * Return : true/false
	 */
	function do_logout()
	{
		$this->CI->session->unset_userdata(array("user_id" => "","secure_nonce" => ""));
	}
	
	/* Untuk mengambil user role
	 * Param : user id,controller,function
	 * 
	 */
	function get_roles($user_id = "",$controller = "",$function = "")
	{
		$user_level_id = $this->get_level($user_id);
		$where = array();
		$where['user_level_id'] = $user_level_id;
		
		if(!empty($controller))
			$where['controller'] = $controller;
		
		if(!empty($function))
			$where['function'] = $function;
		
		$this->CI->db->where($where);
		$query = $this->CI->db->get("user_level_roles");

		if($query->num_rows() == 0)
			return 0;
		
		$user_roles = $query->result_array();
		
		if(!empty($function))
			$user_roles = $query->row_array();
			
		return $user_roles;
	}
	
	function generate_nonce($controller = "",$function = "")
	{
		$prefix_page = "";
		if(!empty($controller) or !empty($function))
			$prefix_page = $controller . $function . '_';
			
		$this->CI->session->set_userdata($prefix_page."secure_nonce",time().uniqid(true).$this->current_user_id);
	}
	
	function get_nonce($controller = "",$function = "")
	{
		$prefix_page = "";
		if(!empty($controller) or !empty($function))
			$prefix_page = $controller.$function.'_';
			
		$secure_nonce = $this->CI->session->userdata($prefix_page."secure_nonce");
		if(empty($secure_nonce))
		{
			$this->generate_nonce($controller,$function);
			$secure_nonce = $this->CI->session->userdata($prefix_page."secure_nonce");
		}
		return $secure_nonce;
	}
	
	function get_url($controller = "",$function = "index")
	{
		$function = (empty($function))?"index":$function;
		return base_url().$controller.'/'.$function;
	}
}
