-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 28, 2013 at 07:49 PM
-- Server version: 5.1.72
-- PHP Version: 5.3.2-1ubuntu4.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simbintek`
--

-- --------------------------------------------------------

--
-- Table structure for table `atk`
--

CREATE TABLE IF NOT EXISTS `atk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_bintek_id` int(11) NOT NULL,
  `kode_atk` char(25) DEFAULT NULL,
  `nama_atk` varchar(255) DEFAULT NULL,
  `jumlah` char(25) DEFAULT NULL,
  `harga_satuan` char(25) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `atk`
--


-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `judul` char(100) DEFAULT NULL,
  `penulis` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `isiberita` text,
  `thumbnail` char(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `judul`, `penulis`, `tanggal`, `isiberita`, `thumbnail`) VALUES
(70, 'BKD Selenggarakan Pelatihan dan Ujian Sertifikasi Keahlian Pengadaan Barang/Jasa Pemerintah Tahun 20', NULL, '2013-10-10', 'Badan Kepegawaian Daerah Kota Surakarta menyelenggarakan Pelatihan dan Ujian Serifikasi Keahlian Pengadaan Barang/Jasa Pemerintah Kota Surakarta dari hari Selasa - Jumat tanggal 8 s.d 11 Oktober 2013 bertempat di Hotel Riyadi Palace, Jl. Slamet Riyadi Solo. Kepala BKD Kota Surakarta Drs. Hari Prihatno secara langsung membuka kegiatan tersebut.', 'e36f19c6aaa9cb802a34adaceb87dcb2.jpg'),
(69, 'Dishubkominfo Selenggarakan Bintek Pegawai Dishubkominfo Tahun 2013', NULL, '2013-10-10', 'Dinas Perhubungan Komunikasi dan Informatika Kota Surakarta menyelenggarakan Pembinaan Teknis bagi Pegawai Dishubkominfo selama 4 hari yang terbagi menjadi 2 gelombang, yaitu gelombang I Senin s.d Selasa, 7-8 Oktober, dan Gelombang II Rabu s.d Kamis, 9-10 Oktober 2013, Kepala Dishubkominfo Kota Surakarta, Drs. Yosca Herman Soedradjad, MM secara langsung membuka acara bintek di Hotel Grand Setyakawan Solo.', '8bf403528110dacbdf2f3c484bd6b1ed.jpg'),
(71, 'Pemkot Solo Raih Piala WTN 7 Kali Berturut-turut', NULL, '2013-10-10', 'Pemerintah Kota Surakarta kembali meraih penghargaan Piala Wahana Tata Nugraha tahun 2013 kategori kota besar. Selain itu, pemkot juga mendapat bantuan transportasi massal bus dari Kementrian Perhubungan.', '85a1f547d0806176a382ceb49ceb01f3.jpg'),
(72, 'Sondakan Gelar NBS', NULL, '2013-10-10', 'Pokdarwis Kelurahan Sondakan kembali menggelar agenda tahunan, Nampak Budaya Samanhoedi (NBS) 2013. NBS tahun ini nampak spesial, karena sekaligus mengukuhkan komitmen Sondakan menjadi desa wisata. Dimana, Kelurahan Sondakan masuk dalam nominasi 17 desa terpilih dalam penhargaan desa wisata se-Indonesia.', 'ce9ed00c8d1da95945aa6cb16a5a8444.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `bintek`
--

CREATE TABLE IF NOT EXISTS `bintek` (
  `bintek_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_bintek` char(25) DEFAULT NULL,
  `nama_bintek` char(50) DEFAULT NULL,
  PRIMARY KEY (`bintek_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bintek`
--

INSERT INTO `bintek` (`bintek_id`, `kode_bintek`, `nama_bintek`) VALUES
(1, 'BG105PTD1', 'Pengujian Tanah'),
(2, 'BG105SPG01', 'Sistem Plumbing & Gypsum'),
(3, 'BG15FPK1', 'Finishing Pekerjaan Kayu'),
(6, 'test4', 'test5');

-- --------------------------------------------------------

--
-- Table structure for table `bintek_certificates`
--

CREATE TABLE IF NOT EXISTS `bintek_certificates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_eval_id` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `certificate_type` char(50) NOT NULL,
  `no_certificate` char(255) NOT NULL,
  `avg_nilai` double(11,2) NOT NULL,
  `total` int(11) NOT NULL,
  `predikat` char(20) NOT NULL,
  `ttd_tempat` char(100) NOT NULL,
  `ttd_tanggal` date NOT NULL,
  `ttd_jabatan` text NOT NULL,
  `ttd_nama` char(100) NOT NULL,
  `ttd_nip` char(100) NOT NULL,
  `ttd_data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bintek_certificates`
--

INSERT INTO `bintek_certificates` (`id`, `member_eval_id`, `id_member`, `id_jadwal`, `certificate_type`, `no_certificate`, `avg_nilai`, `total`, `predikat`, `ttd_tempat`, `ttd_tanggal`, `ttd_jabatan`, `ttd_nama`, `ttd_nip`, `ttd_data`) VALUES
(1, 2, 6, 1, 'sertifikat', '123', 77.86, 545, 'Baik', '', '2013-11-18', '', '', '', ''),
(2, 3, 0, 0, 'sertifikat', 'asdfasdf', 40.00, 280, 'Tidak Lulus', '', '2013-11-18', '', '', '', ''),
(3, 3, 3, 1, 'surat keterangan', 'SK 002', 40.00, 280, 'Tidak Lulus', '', '2013-11-19', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `bintek_eval_instructure`
--

CREATE TABLE IF NOT EXISTS `bintek_eval_instructure` (
  `eval_instructure_id` int(11) NOT NULL AUTO_INCREMENT,
  `instructure_id` int(11) NOT NULL,
  `jadwal_id` int(11) NOT NULL,
  `nilai` text NOT NULL,
  PRIMARY KEY (`eval_instructure_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `bintek_eval_instructure`
--

INSERT INTO `bintek_eval_instructure` (`eval_instructure_id`, `instructure_id`, `jadwal_id`, `nilai`) VALUES
(1, 1, 1, '[{"label":"1","value":"10"},{"label":"2","value":"100"},{"label":"3","value":"10"},{"label":"4","value":"100"},{"label":"5","value":"10"},{"label":"6","value":"100"},{"label":"7","value":"10"},{"label":"8","value":"100"},{"label":"9","value":"10"},{"label":"10","value":"100"}]');

-- --------------------------------------------------------

--
-- Table structure for table `bintek_eval_material`
--

CREATE TABLE IF NOT EXISTS `bintek_eval_material` (
  `eval_material_id` int(11) NOT NULL AUTO_INCREMENT,
  `instructure_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `jadwal_id` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  PRIMARY KEY (`eval_material_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `bintek_eval_material`
--

INSERT INTO `bintek_eval_material` (`eval_material_id`, `instructure_id`, `material_id`, `jadwal_id`, `nilai`) VALUES
(5, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bintek_eval_member`
--

CREATE TABLE IF NOT EXISTS `bintek_eval_member` (
  `eval_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `jadwal_id` int(11) NOT NULL,
  `nilai` text NOT NULL,
  PRIMARY KEY (`eval_member_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bintek_eval_member`
--

INSERT INTO `bintek_eval_member` (`eval_member_id`, `member_id`, `jadwal_id`, `nilai`) VALUES
(2, 6, 1, '[{"label":"1","value":"50"},{"label":"2","value":"60"},{"label":"3","value":"70"},{"label":"4","value":"80"},{"label":"5","value":"90"},{"label":"6","value":"95"},{"label":"7","value":"100"}]'),
(3, 3, 1, '[{"label":"1","value":"10"},{"label":"2","value":"20"},{"label":"3","value":"30"},{"label":"4","value":"40"},{"label":"5","value":"50"},{"label":"6","value":"60"},{"label":"7","value":"70"}]');

-- --------------------------------------------------------

--
-- Table structure for table `bintek_instruktur`
--

CREATE TABLE IF NOT EXISTS `bintek_instruktur` (
  `instruktur_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` char(20) DEFAULT NULL,
  `nip` char(20) DEFAULT NULL,
  `gelar` char(20) DEFAULT NULL,
  `gol` char(10) DEFAULT NULL,
  `tempat_lahir_propinsi` int(11) DEFAULT NULL,
  `tempat_lahir_kota` int(11) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jabatan` char(20) DEFAULT NULL,
  `status` char(20) DEFAULT NULL,
  `instansi` char(100) DEFAULT NULL,
  `alamat_instansi` char(255) DEFAULT NULL,
  `propinsi_instansi` int(11) DEFAULT NULL,
  `kota_instansi` int(11) DEFAULT NULL,
  `telepon_instansi` char(20) DEFAULT NULL,
  `fax_instansi` char(20) DEFAULT NULL,
  `hp_instansi` char(20) DEFAULT NULL,
  `alamat_rumah` char(255) DEFAULT NULL,
  `propinsi_rumah` int(11) DEFAULT NULL,
  `kota_rumah` int(11) DEFAULT NULL,
  `telp_rumah` char(20) DEFAULT NULL,
  `fax_rumah` char(20) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `pengalaman_kerja` text,
  `pendidikan_tertinggi` text,
  `pelatihan_teknik` text,
  `spesialisasi_pelatihan` text,
  `foto` char(255) DEFAULT NULL,
  PRIMARY KEY (`instruktur_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bintek_instruktur`
--

INSERT INTO `bintek_instruktur` (`instruktur_id`, `nama`, `nip`, `gelar`, `gol`, `tempat_lahir_propinsi`, `tempat_lahir_kota`, `tanggal_lahir`, `jabatan`, `status`, `instansi`, `alamat_instansi`, `propinsi_instansi`, `kota_instansi`, `telepon_instansi`, `fax_instansi`, `hp_instansi`, `alamat_rumah`, `propinsi_rumah`, `kota_rumah`, `telp_rumah`, `fax_rumah`, `email`, `pengalaman_kerja`, `pendidikan_tertinggi`, `pelatihan_teknik`, `spesialisasi_pelatihan`, `foto`) VALUES
(1, 'Taofik', '1234234', 'ST', 'IIIA', 2147483647, 234234, '2013-10-05', 'Programmer', 'Widyaiswara Kem. PU', 'Pemerintahan', 'Jl. Jalan', 234234234, 234234, '234324234', '23423423', '423423', '4234234', 23423, 23423, 'fsdfsdf', 'sdfsdf', 'nietaldarkopik@gmail.com', '4 tahun', 'SMK', 'sdaf sdfsdfsdf', 'sa dfasdf sdf', '597f3379c16064b43e1ae6030dd5a464.jpg'),
(2, 'Drs. Yos Pariaman, S', '838392757892', 'Drs', 'D4', 0, 0, '1993-05-08', 'ketua', 'Widyaiswara Kem. PU', 'instansi', 'jl.h.sanusi rt.04 rw.02 kec.rancasari kel.mekarjaya', 0, 0, '02238484837', '8483', '8908937923898', 'jl.sukamulya', 0, 0, '9384776778', '74836', 'ibnu.it@gmail.com', '2 tahun', 'Sarjana S1', 'banyak', 'banyak', 'b792d5630d1d2166862c3ded8d815f82.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `bintek_material`
--

CREATE TABLE IF NOT EXISTS `bintek_material` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_id` int(11) NOT NULL,
  `instructure_id` int(11) NOT NULL,
  `material_name` char(255) NOT NULL,
  `jam_pelajaran` int(11) NOT NULL,
  PRIMARY KEY (`material_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `bintek_material`
--

INSERT INTO `bintek_material` (`material_id`, `jadwal_id`, `instructure_id`, `material_name`, `jam_pelajaran`) VALUES
(1, 1, 1, 'Dinamika Kelompok', 2),
(2, 1, 1, 'Proyeksi Kebutuhan Air dan identifikasi Pola Fluktuasi Pemakaian Air.', 2),
(3, 1, 1, '1. Jaringan Pipa distribusi. 2. Fungsi Reservoir', 2),
(4, 1, 1, 'Gradien hidrolika dan Persamaan Nomogram Hazen William', 2),
(5, 1, 1, 'Prinsip Kontinuitas', 2),
(6, 1, 1, 'Energi, Persamaan Bernoulli dan Persamaan Darcy', 2),
(7, 1, 1, 'Pengenalan Hidrolika dan Tekanan Air', 2);

-- --------------------------------------------------------

--
-- Table structure for table `bintek_monev`
--

CREATE TABLE IF NOT EXISTS `bintek_monev` (
  `monev_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_id` int(11) NOT NULL,
  `peserta_id` int(11) NOT NULL,
  `pertanyaan` text NOT NULL,
  `jawaban` char(255) NOT NULL,
  `uraian` text NOT NULL,
  `pertanyaan_untuk` char(100) NOT NULL,
  PRIMARY KEY (`monev_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `bintek_monev`
--


-- --------------------------------------------------------

--
-- Table structure for table `bintek_peserta`
--

CREATE TABLE IF NOT EXISTS `bintek_peserta` (
  `peserta_bintek_id` int(11) NOT NULL AUTO_INCREMENT,
  `lembaga` int(11) NOT NULL,
  `nama` char(30) DEFAULT NULL,
  `tempat_lahir` char(30) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `unit_kerja` char(30) DEFAULT NULL,
  `pendidikan_terakhir` char(10) DEFAULT NULL,
  `jurusan` char(10) DEFAULT NULL,
  `status_kepegawaian` char(10) DEFAULT NULL,
  `nip` char(15) DEFAULT NULL,
  `no_ktp` char(50) DEFAULT NULL,
  `jabatan` char(10) DEFAULT NULL,
  `pangkat` char(10) DEFAULT NULL,
  `golongan` char(10) DEFAULT NULL,
  `bidang_pekerjaan` char(100) DEFAULT NULL,
  `masa_kerja` char(15) DEFAULT NULL,
  `mulai_tahun` int(4) DEFAULT NULL,
  `alamat_kantor` char(255) DEFAULT NULL,
  `alamat_rumah` char(255) DEFAULT NULL,
  `telepon_kantor` char(20) DEFAULT NULL,
  `telepon_rumah` char(20) DEFAULT NULL,
  `hp` char(20) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `bank` char(10) DEFAULT NULL,
  `no_rekening` char(20) DEFAULT NULL,
  `foto` char(255) DEFAULT NULL,
  PRIMARY KEY (`peserta_bintek_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bintek_peserta`
--

INSERT INTO `bintek_peserta` (`peserta_bintek_id`, `lembaga`, `nama`, `tempat_lahir`, `tanggal_lahir`, `unit_kerja`, `pendidikan_terakhir`, `jurusan`, `status_kepegawaian`, `nip`, `no_ktp`, `jabatan`, `pangkat`, `golongan`, `bidang_pekerjaan`, `masa_kerja`, `mulai_tahun`, `alamat_kantor`, `alamat_rumah`, `telepon_kantor`, `telepon_rumah`, `hp`, `email`, `bank`, `no_rekening`, `foto`) VALUES
(6, 16, 'Taofik', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '0300030230', 'programmer', 'III', 'D', 'IT', '4', 2008, 'Jl. Sari Indah 5E', 'Jl. H. Sanusi', '03020302', '30230230', '023020302', 'nietaldarkopik@gmial.com', 'bca', '20038302', 'b71a033537e72ee365262f8841fdf4e4.jpg'),
(3, 20, 'Ibnu Thobari, ST', 'Bandung', '1993-05-08', 'unit kerja', 'sarjana s1', 'teknik inf', 'tetap', '123456789', '987654321', 'programmer', 'jendral', 'B12', 'IT', '4', 2008, 'jl.h.sanusi', 'sama jl.h. sanusi', '08997885612', '72827746', '08997885712', 'ibnu.it@gmail.com', 'bca', '837363357', NULL),
(4, 19, 'johan', 'medan', '2013-10-30', 'unit', 's1', 'pendidikan', 'tetap', '123456789', '987654321', 'programmer', 'jendral', 'B12', 'IT', '4', 2008, 'jl.h.sanusi', 'sama jl.h. sanusi', '08997885612', '72827746', '08997885712', 'ibnu.it@gmail.com', 'bca', '881298192i', 'd2480abff261fe1281dd183dd03f4e9c.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `fasilitas`
--

CREATE TABLE IF NOT EXISTS `fasilitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL,
  `nama_fasilitas` char(255) NOT NULL,
  `harga_beli` double(11,2) NOT NULL,
  `harga_jual` double(11,2) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `gambar` char(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `fasilitas`
--

INSERT INTO `fasilitas` (`id`, `parent`, `nama_fasilitas`, `harga_beli`, `harga_jual`, `jumlah`, `gambar`) VALUES
(1, 0, 'Gedung A', 100000000.00, 150000000.00, 1, '15ac8fdf0fdfe7a93fb340f74c358b10.jpg'),
(2, 1, 'Ruang Kamar', 0.00, 50000.00, 15, 'e06503637d324e929e43039b7e93de94.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `forum`
--

CREATE TABLE IF NOT EXISTS `forum` (
  `kdtopik` int(10) NOT NULL AUTO_INCREMENT,
  `isitopik` text,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author` int(11) DEFAULT NULL,
  PRIMARY KEY (`kdtopik`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `forum`
--

INSERT INTO `forum` (`kdtopik`, `isitopik`, `tanggal`, `author`) VALUES
(1, 'Test', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `forum_det`
--

CREATE TABLE IF NOT EXISTS `forum_det` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kdtopik` int(10) DEFAULT NULL,
  `penanggap` char(100) DEFAULT NULL,
  `isitanggapan` text,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `forum_det`
--


-- --------------------------------------------------------

--
-- Table structure for table `gedung`
--

CREATE TABLE IF NOT EXISTS `gedung` (
  `gedung_id` int(11) NOT NULL AUTO_INCREMENT,
  `nmgedung` char(50) DEFAULT NULL,
  `milik` char(50) DEFAULT NULL,
  PRIMARY KEY (`gedung_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gedung`
--


-- --------------------------------------------------------

--
-- Table structure for table `jadwal_bintek`
--

CREATE TABLE IF NOT EXISTS `jadwal_bintek` (
  `jadwal_bintek_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_bintek` char(25) DEFAULT NULL,
  `tempat` char(255) NOT NULL,
  `tglawal` date DEFAULT NULL,
  `tglakhir` date DEFAULT NULL,
  `tglregawal` date DEFAULT NULL,
  `tglregakhir` date DEFAULT NULL,
  `thn` char(4) DEFAULT NULL,
  `judul_bintek` char(200) DEFAULT NULL,
  `penyelenggara` char(255) NOT NULL,
  `deskripsi` char(255) DEFAULT NULL,
  PRIMARY KEY (`jadwal_bintek_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jadwal_bintek`
--

INSERT INTO `jadwal_bintek` (`jadwal_bintek_id`, `kode_bintek`, `tempat`, `tglawal`, `tglakhir`, `tglregawal`, `tglregakhir`, `thn`, `judul_bintek`, `penyelenggara`, `deskripsi`) VALUES
(1, 'BG105PTD1', 'Bandung', '2013-09-23', '2013-09-25', '2013-09-15', '2013-09-21', '2013', 'Pengujian Tanah 2013', '', '<p>Deskripsi</p>'),
(2, 'BG15FPK1', 'Balai Bintek', '2013-11-28', '2013-11-30', '2013-11-28', '2013-11-30', '2013', 'Finishing Kayu Pake Cet', '', 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_bintek_acara`
--

CREATE TABLE IF NOT EXISTS `jadwal_bintek_acara` (
  `jadwal_acara_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_bintek_id` int(11) DEFAULT NULL,
  `hari_ke` int(2) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jam_awal` time DEFAULT NULL,
  `jam_akhir` time NOT NULL,
  `acara` char(255) DEFAULT NULL,
  `material_id` int(11) NOT NULL,
  `instruktur` int(11) DEFAULT NULL,
  `instructure_name` char(50) NOT NULL,
  `keterangan` char(255) DEFAULT NULL,
  PRIMARY KEY (`jadwal_acara_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `jadwal_bintek_acara`
--

INSERT INTO `jadwal_bintek_acara` (`jadwal_acara_id`, `jadwal_bintek_id`, `hari_ke`, `tanggal`, `jam_awal`, `jam_akhir`, `acara`, `material_id`, `instruktur`, `instructure_name`, `keterangan`) VALUES
(48, 1, 3, '2013-09-25', '10:00:00', '11:00:00', '', 4, 1, 'Taofik', ''),
(47, 1, 3, '2013-09-25', '09:00:00', '10:00:00', '', 4, 1, 'Taofik', ''),
(46, 1, 3, '2013-09-25', '08:00:00', '09:00:00', '', 7, 1, 'Taofik', ''),
(45, 1, 2, '2013-09-24', '10:00:00', '11:00:00', '', 4, 1, 'Taofik', ''),
(44, 1, 2, '2013-09-24', '09:00:00', '10:00:00', '', 4, 1, 'Taofik', ''),
(43, 1, 2, '2013-09-24', '08:00:00', '09:00:00', '', 0, 0, 'Taofik Basuki 09-10', ''),
(42, 1, 1, '2013-09-23', '10:00:00', '11:00:00', '', 5, 1, 'Taofik', ''),
(41, 1, 1, '2013-09-23', '09:00:00', '10:00:00', '', 0, 0, 'Taofik Basuki 08-09', ''),
(40, 1, 1, '2013-09-23', '08:00:00', '09:00:00', '', 4, 1, 'Taofik', '');

-- --------------------------------------------------------

--
-- Table structure for table `jurlembaga`
--

CREATE TABLE IF NOT EXISTS `jurlembaga` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kdlembaga` char(25) DEFAULT NULL,
  `kdkel` char(2) DEFAULT NULL,
  `kdjur` char(2) DEFAULT NULL,
  `mulaithn` char(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jurlembaga`
--


-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE IF NOT EXISTS `jurusan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kdjur` char(5) DEFAULT NULL,
  `kdkel` char(5) DEFAULT NULL,
  `nama` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jurusan`
--


-- --------------------------------------------------------

--
-- Table structure for table `kabupaten`
--

CREATE TABLE IF NOT EXISTS `kabupaten` (
  `kabupaten_id` int(11) NOT NULL AUTO_INCREMENT,
  `kdkab` char(10) DEFAULT NULL,
  `propinsi_id` int(11) DEFAULT NULL,
  `nama` char(30) DEFAULT NULL,
  PRIMARY KEY (`kabupaten_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=499 ;

--
-- Dumping data for table `kabupaten`
--

INSERT INTO `kabupaten` (`kabupaten_id`, `kdkab`, `propinsi_id`, `nama`) VALUES
(1, '11-001', 4, 'Kabupaten Aceh Barat Daya'),
(2, '11-002', 4, 'Kabupaten Aceh Barat'),
(3, '11-003', 4, 'Kabupaten Aceh Besar'),
(4, '11-004', 4, 'Kabupaten Aceh Jaya'),
(5, '11-005', 4, 'Kabupaten Aceh Selatan'),
(6, '11-006', 4, 'Kabupaten Aceh Singkil'),
(7, '11-007', 4, 'Kabupaten Aceh Tamiang'),
(8, '11-008', 4, 'Kabupaten Aceh Tengah'),
(9, '11-009', 4, 'Kabupaten Aceh Tenggara'),
(10, '11-010', 4, 'Kabupaten Aceh Timur'),
(11, '11-011', 4, 'Kabupaten Aceh Utara'),
(12, '11-012', 4, 'Kabupaten Bener Meriah'),
(13, '11-013', 4, 'Kabupaten Bireuen'),
(14, '11-014', 4, 'Kabupaten Gayo Lues'),
(15, '11-015', 4, 'Kabupaten Nagan Raya'),
(16, '11-016', 4, 'Kabupaten Pidie Jaya'),
(17, '11-017', 4, 'Kabupaten Pidie'),
(18, '11-018', 4, 'Kabupaten Simeulue'),
(19, '11-019', 4, 'Kota Banda Aceh'),
(20, '11-020', 4, 'Kota Langsa'),
(21, '11-021', 4, 'Kota Lhokseumawe'),
(22, '11-022', 4, 'Kota Sabang'),
(23, '11-023', 4, 'Kota Subulussalam'),
(24, '51-001', 20, 'Kabupaten Badung'),
(25, '51-002', 20, 'Kabupaten Bangli'),
(26, '51-003', 20, 'Kabupaten Buleleng'),
(27, '51-004', 20, 'Kabupaten Gianyar'),
(28, '51-005', 20, 'Kabupaten Jembrana'),
(29, '51-006', 20, 'Kabupaten Karangasem'),
(30, '51-007', 20, 'Kabupaten Klungkung'),
(31, '51-008', 20, 'Kabupaten Tabanan'),
(32, '51-009', 20, 'Kota Denpasar'),
(33, '36-001', 19, 'Kabupaten Lebak'),
(34, '36-002', 19, 'Kabupaten Pandeglang'),
(35, '36-003', 19, 'Kabupaten Serang'),
(36, '36-004', 19, 'Kabupaten Tangerang'),
(37, '36-005', 19, 'Kota Cilegon'),
(38, '36-006', 19, 'Kota Serang'),
(39, '36-007', 19, 'Kota Tangerang Selatan'),
(40, '36-008', 19, 'Kota Tangerang'),
(41, '17-001', 10, 'Kabupaten Bengkulu Selatan'),
(42, '17-002', 10, 'Kabupaten Bengkulu Tengah'),
(43, '17-003', 10, 'Kabupaten Bengkulu Utara'),
(44, '17-004', 10, 'Kabupaten Kaur'),
(45, '17-005', 10, 'Kabupaten Kepahiang'),
(46, '17-006', 10, 'Kabupaten Lebong'),
(47, '17-007', 10, 'Kabupaten Mukomuko'),
(48, '17-008', 10, 'Kabupaten Rejang Lebong'),
(49, '17-009', 10, 'Kabupaten Seluma'),
(50, '17-010', 10, 'Kota Bengkulu'),
(51, '75-001', 31, 'Kabupaten Boalemo'),
(52, '75-002', 31, 'Kabupaten Bone Bolango'),
(53, '75-003', 31, 'Kabupaten Gorontalo Utara'),
(54, '75-004', 31, 'Kabupaten Gorontalo'),
(55, '75-005', 31, 'Kabupaten Pohuwato'),
(56, '75-006', 31, 'Kota Gorontalo'),
(57, '31-001', 14, 'Kabupaten Administrasi Kepulau'),
(58, '31-002', 14, 'Kota Administrasi Jakarta Bara'),
(59, '31-003', 14, 'Kota Administrasi Jakarta Pusa'),
(60, '31-004', 14, 'Kota Administrasi Jakarta Sela'),
(61, '31-005', 14, 'Kota Administrasi Jakarta Timu'),
(62, '31-006', 14, 'Kota Administrasi Jakarta Utar'),
(63, '15-001', 8, 'Kabupaten Batanghari'),
(64, '15-002', 8, 'Kabupaten Bungo'),
(65, '15-003', 8, 'Kabupaten Kerinci'),
(66, '15-004', 8, 'Kabupaten Merangin'),
(67, '15-005', 8, 'Kabupaten Muaro Jambi'),
(68, '15-006', 8, 'Kabupaten Sarolangun'),
(69, '15-007', 8, 'Kabupaten Tanjung Jabung Barat'),
(70, '15-008', 8, 'Kabupaten Tanjung Jabung Timur'),
(71, '15-009', 8, 'Kabupaten Tebo'),
(72, '15-010', 8, 'Kota Jambi'),
(73, '15-011', 8, 'Kota Sungai Penuh'),
(74, '32-001', 15, 'Kabupaten Bandung Barat'),
(75, '32-002', 15, 'Kabupaten Bandung'),
(76, '32-003', 15, 'Kabupaten Bekasi'),
(77, '32-004', 15, 'Kabupaten Bogor'),
(78, '32-005', 15, 'Kabupaten Ciamis'),
(79, '32-006', 15, 'Kabupaten Cianjur'),
(80, '32-007', 15, 'Kabupaten Cirebon'),
(81, '32-008', 15, 'Kabupaten Garut'),
(82, '32-009', 15, 'Kabupaten Indramayu'),
(83, '32-010', 15, 'Kabupaten Karawang'),
(84, '32-011', 15, 'Kabupaten Kuningan'),
(85, '32-012', 15, 'Kabupaten Majalengka'),
(86, '32-013', 15, 'Kabupaten Majalengka'),
(87, '32-014', 15, 'Kabupaten Purwakarta'),
(88, '32-015', 15, 'Kabupaten Subang'),
(89, '32-016', 15, 'Kabupaten Sukabumi'),
(90, '32-017', 15, 'Kabupaten Sumedang'),
(91, '32-018', 15, 'Kabupaten Tasikmalaya'),
(92, '32-019', 15, 'Kota Bandung'),
(93, '32-020', 15, 'Kota Banjar'),
(94, '32-021', 15, 'Kota Bekasi'),
(95, '32-022', 15, 'Kota Bogor'),
(96, '32-023', 15, 'Kota Cimahi'),
(97, '32-024', 15, 'Kota Cirebon'),
(98, '32-025', 15, 'Kota Depok'),
(99, '32-026', 15, 'Kota Sukabumi'),
(100, '32-027', 15, 'Kota Tasikmalaya'),
(101, '33-001', 16, 'Kabupaten Banjarnegara'),
(102, '33-002', 16, 'Kabupaten Banyumas'),
(103, '33-003', 16, 'Kabupaten Batang'),
(104, '33-004', 16, 'Kabupaten Blora'),
(105, '33-005', 16, 'Kabupaten Boyolali'),
(106, '33-006', 16, 'Kabupaten Brebes'),
(107, '33-007', 16, 'Kabupaten Cilacap'),
(108, '33-008', 16, 'Kabupaten Demak'),
(109, '33-009', 16, 'Kabupaten Grobogan'),
(110, '33-010', 16, 'Kabupaten Jepara'),
(111, '33-011', 16, 'Kabupaten Karanganyar'),
(112, '33-012', 16, 'Kabupaten Kebumen'),
(113, '33-013', 16, 'Kabupaten Kendal'),
(114, '33-014', 16, 'Kabupaten Klaten'),
(115, '33-015', 16, 'Kabupaten Kudus'),
(116, '33-016', 16, 'Kabupaten Magelang'),
(117, '33-017', 16, 'Kabupaten Pati'),
(118, '33-018', 16, 'Kabupaten Pekalongan'),
(119, '33-019', 16, 'Kabupaten Pemalang'),
(120, '33-020', 16, 'Kabupaten Purbalingga'),
(121, '33-021', 16, 'Kabupaten Purworejo'),
(122, '33-022', 16, 'Kabupaten Rembang'),
(123, '33-023', 16, 'Kabupaten Semarang'),
(124, '33-024', 16, 'Kabupaten Sragen'),
(125, '33-025', 16, 'Kabupaten Sukoharjo'),
(126, '33-026', 16, 'Kabupaten Tegal'),
(127, '33-027', 16, 'Kabupaten Temanggung'),
(128, '33-028', 16, 'Kabupaten Wonogiri'),
(129, '33-029', 16, 'Kabupaten Wonosobo'),
(130, '33-030', 16, 'Kota Magelang'),
(131, '33-031', 16, 'Kota Pekalongan'),
(132, '33-032', 16, 'Kota Salatiga'),
(133, '33-033', 16, 'Kota Semarang'),
(134, '33-034', 16, 'Kota Surakarta'),
(135, '33-035', 16, 'Kota Tegal'),
(136, '35-001', 18, 'Kabupaten Bangkalan'),
(137, '35-002', 18, 'Kabupaten Banyuwangi'),
(138, '35-003', 18, 'Kabupaten Blitar'),
(139, '35-004', 18, 'Kabupaten Bojonegoro'),
(140, '35-005', 18, 'Kabupaten Bondowoso'),
(141, '35-006', 18, 'Kabupaten Gresik'),
(142, '35-007', 18, 'Kabupaten Jember'),
(143, '35-008', 18, 'Kabupaten Jombang'),
(144, '35-009', 18, 'Kabupaten Kediri'),
(145, '35-010', 18, 'Kabupaten Lamongan'),
(146, '35-011', 18, 'Kabupaten Lumajang'),
(147, '35-012', 18, 'Kabupaten Madiun'),
(148, '35-013', 18, 'Kabupaten Malang'),
(149, '35-014', 18, 'Kabupaten Mojokerto'),
(150, '35-015', 18, 'Kabupaten Nganjuk'),
(151, '35-016', 18, 'Kabupaten Ngawi'),
(152, '35-017', 18, 'Kabupaten Pacitan'),
(153, '35-018', 18, 'Kabupaten Pamekasan'),
(154, '35-019', 18, 'Kabupaten Pasuruan'),
(155, '35-020', 18, 'Kabupaten Ponorogo'),
(156, '35-021', 18, 'Kabupaten Probolinggo'),
(157, '35-022', 18, 'Kabupaten Sampang'),
(158, '35-023', 18, 'Kabupaten Sidoarjo'),
(159, '35-024', 18, 'Kabupaten Situbondo'),
(160, '35-025', 18, 'Kabupaten Sumenep'),
(161, '35-026', 18, 'Kabupaten Trenggalek'),
(162, '35-027', 18, 'Kabupaten Tuban'),
(163, '35-028', 18, 'Kabupaten Tulungagung'),
(164, '35-029', 18, 'Kota Batu'),
(165, '35-030', 18, 'Kota Blitar'),
(166, '35-031', 18, 'Kota Kediri'),
(167, '35-032', 18, 'Kota Madiun'),
(168, '35-033', 18, 'Kota Malang'),
(169, '35-034', 18, 'Kota Mojokerto'),
(170, '35-035', 18, 'Kota Pasuruan'),
(171, '35-036', 18, 'Kota Probolinggo'),
(172, '35-037', 18, 'Kota Surabaya'),
(173, '61-001', 23, 'Kabupaten Bengkayang'),
(174, '61-002', 23, 'Kabupaten Kapuas Hulu'),
(175, '61-003', 23, 'Kabupaten Kayong Utara'),
(176, '61-004', 23, 'Kabupaten Ketapang'),
(177, '61-005', 23, 'Kabupaten Kubu Raya'),
(178, '61-006', 23, 'Kabupaten Landak'),
(179, '61-007', 23, 'Kabupaten Melawi'),
(180, '61-008', 23, 'Kabupaten Pontianak'),
(181, '61-009', 23, 'Kabupaten Sambas'),
(182, '61-010', 23, 'Kabupaten Sanggau'),
(183, '61-011', 23, 'Kabupaten Sekadau'),
(184, '61-012', 23, 'Kabupaten Sintang'),
(185, '61-013', 23, 'Kota Pontianak'),
(186, '61-014', 23, 'Kota Singkawang'),
(187, '63-001', 25, 'Kabupaten Balangan'),
(188, '63-002', 25, 'Kabupaten Banjar'),
(189, '63-003', 25, 'Kabupaten Barito Kuala'),
(190, '63-004', 25, 'Kabupaten Hulu Sungai Selatan'),
(191, '63-005', 25, 'Kabupaten Hulu Sungai Tengah'),
(192, '63-006', 25, 'Kabupaten Hulu Sungai Utara'),
(193, '63-007', 25, 'Kabupaten Kotabaru'),
(194, '63-008', 25, 'Kabupaten Tabalong'),
(195, '63-009', 25, 'Kabupaten Tanah Bumbu'),
(196, '63-010', 25, 'Kabupaten Tanah Laut'),
(197, '63-011', 25, 'Kabupaten Tapin'),
(198, '63-012', 25, 'Kota Banjarbaru'),
(199, '63-013', 25, 'Kota Banjarmasin'),
(200, '62-001', 24, 'Kabupaten Barito Selatan'),
(201, '62-002', 24, 'Kabupaten Barito Timur'),
(202, '62-003', 24, 'Kabupaten Barito Utara'),
(203, '62-004', 24, 'Kabupaten Gunung Mas'),
(204, '62-005', 24, 'Kabupaten Kapuas'),
(205, '62-006', 24, 'Kabupaten Katingan'),
(206, '62-007', 24, 'Kabupaten Kotawaringin Barat'),
(207, '62-008', 24, 'Kabupaten Kotawaringin Timur'),
(208, '62-009', 24, 'Kabupaten Lamandau'),
(209, '62-010', 24, 'Kabupaten Murung Raya'),
(210, '62-011', 24, 'Kabupaten Pulang Pisau'),
(211, '62-012', 24, 'Kabupaten Seruyan'),
(212, '62-013', 24, 'Kabupaten Sukamara'),
(213, '62-014', 24, 'Kota Palangka Raya'),
(214, '64-001', 26, 'Kabupaten Berau'),
(215, '64-002', 26, 'Kabupaten Kutai Barat'),
(216, '64-003', 26, 'Kabupaten Kutai Kartanegara'),
(217, '64-004', 26, 'Kabupaten Kutai Timur'),
(218, '64-005', 26, 'Kabupaten Mahakam Ulu'),
(219, '64-006', 26, 'Kabupaten Paser'),
(220, '64-007', 26, 'Kabupaten Penajam Paser Utara'),
(221, '64-008', 26, 'Kota Balikpapan'),
(222, '64-009', 26, 'Kota Bontang'),
(223, '64-010', 26, 'Kota Samarinda'),
(224, '--001', 3, 'Kabupaten Bulungan'),
(225, '--002', 3, 'Kabupaten Malinau'),
(226, '--003', 3, 'Kabupaten Nunukan'),
(227, '--004', 3, 'Kabupaten Tana Tidung'),
(228, '--005', 3, 'Kota Tarakan'),
(229, '19-001', 12, 'Kabupaten Bangka Barat'),
(230, '19-002', 12, 'Kabupaten Bangka Selatan'),
(231, '19-003', 12, 'Kabupaten Bangka Tengah'),
(232, '19-004', 12, 'Kabupaten Bangka'),
(233, '19-005', 12, 'Kabupaten Belitung Timur'),
(234, '19-006', 12, 'Kabupaten Belitung'),
(235, '19-007', 12, 'Kota Pangkal Pinang'),
(236, '21-001', 13, 'Kabupaten Bintan'),
(237, '21-002', 13, 'Kabupaten Karimun'),
(238, '21-003', 13, 'Kabupaten Kepulauan Anambas'),
(239, '21-004', 13, 'Kabupaten Lingga'),
(240, '21-005', 13, 'Kabupaten Natuna'),
(241, '21-006', 13, 'Kota Batam'),
(242, '21-007', 13, 'Kota Tanjung Pinang'),
(243, '18-001', 11, 'Kabupaten Lampung Barat'),
(244, '18-002', 11, 'Kabupaten Lampung Selatan'),
(245, '18-003', 11, 'Kabupaten Lampung Tengah'),
(246, '18-004', 11, 'Kabupaten Lampung Timur'),
(247, '18-005', 11, 'Kabupaten Lampung Utara'),
(248, '18-006', 11, 'Kabupaten Mesuji'),
(249, '18-007', 11, 'Kabupaten Pesawaran'),
(250, '18-008', 11, 'Kabupaten Pringsewu'),
(251, '18-009', 11, 'Kabupaten Tanggamus'),
(252, '18-010', 11, 'Kabupaten Tulang Bawang Barat'),
(253, '18-011', 11, 'Kabupaten Tulang Bawang'),
(254, '18-012', 11, 'Kabupaten Way Kanan'),
(255, '18-013', 11, 'Kota Bandar Lampung'),
(256, '18-014', 11, 'Kota Metro'),
(257, '81-001', 33, 'Kabupaten Buru Selatan'),
(258, '81-002', 33, 'Kabupaten Buru'),
(259, '81-003', 33, 'Kabupaten Kepulauan Aru'),
(260, '81-004', 33, 'Kabupaten Maluku Barat Daya'),
(261, '81-005', 33, 'Kabupaten Maluku Tengah'),
(262, '81-006', 33, 'Kabupaten Maluku Tenggara Bara'),
(263, '81-007', 33, 'Kabupaten Maluku Tenggara'),
(264, '81-008', 33, 'Kabupaten Seram Bagian Barat'),
(265, '81-009', 33, 'Kabupaten Seram Bagian Timur'),
(266, '81-010', 33, 'Kota Ambon'),
(267, '81-011', 33, 'Kota Tual'),
(268, '82-001', 34, 'Kabupaten Halmahera Barat'),
(269, '82-002', 34, 'Kabupaten Halmahera Selatan'),
(270, '82-003', 34, 'Kabupaten Halmahera Tengah'),
(271, '82-004', 34, 'Kabupaten Halmahera Timur'),
(272, '82-005', 34, 'Kabupaten Halmahera Utara'),
(273, '82-006', 34, 'Kabupaten Kepulauan Sula'),
(274, '82-007', 34, 'Kabupaten Pulau Morotai'),
(275, '82-008', 34, 'Kota Ternate'),
(276, '82-009', 34, 'Kota Tidore Kepulauan'),
(277, '52-001', 21, 'Kabupaten Bima'),
(278, '52-002', 21, 'Kabupaten Dompu'),
(279, '52-003', 21, 'Kabupaten Lombok Barat'),
(280, '52-004', 21, 'Kabupaten Lombok Tengah'),
(281, '52-005', 21, 'Kabupaten Lombok Timur'),
(282, '52-006', 21, 'Kabupaten Lombok Utara'),
(283, '52-007', 21, 'Kabupaten Sumbawa Barat'),
(284, '52-008', 21, 'Kabupaten Sumbawa'),
(285, '52-009', 21, 'Kota Bima'),
(286, '52-010', 21, 'Kota Mataram'),
(287, '53-001', 22, 'Kabupaten Alor'),
(288, '53-002', 22, 'Kabupaten Belu'),
(289, '53-003', 22, 'Kabupaten Ende'),
(290, '53-004', 22, 'Kabupaten Flores Timur'),
(291, '53-005', 22, 'Kabupaten Kupang'),
(292, '53-006', 22, 'Kabupaten Lembata'),
(293, '53-007', 22, 'Kabupaten Manggarai Barat'),
(294, '53-008', 22, 'Kabupaten Manggarai Timur'),
(295, '53-009', 22, 'Kabupaten Manggarai'),
(296, '53-010', 22, 'Kabupaten Nagekeo'),
(297, '53-011', 22, 'Kabupaten Ngada'),
(298, '53-012', 22, 'Kabupaten Rote Ndao'),
(299, '53-013', 22, 'Kabupaten Sabu Raijua'),
(300, '53-014', 22, 'Kabupaten Sikka'),
(301, '53-015', 22, 'Kabupaten Sumba Barat Daya'),
(302, '53-016', 22, 'Kabupaten Sumba Barat'),
(303, '53-017', 22, 'Kabupaten Sumba Tengah'),
(304, '53-018', 22, 'Kabupaten Sumba Timur'),
(305, '53-019', 22, 'Kabupaten Timor Tengah Selatan'),
(306, '53-020', 22, 'Kabupaten Timor Tengah Utara'),
(307, '53-021', 22, 'Kota Kupang'),
(308, '94-001', 36, 'Kabupaten Asmat'),
(309, '94-002', 36, 'Kabupaten Biak Numfor'),
(310, '94-003', 36, 'Kabupaten Boven Digoel'),
(311, '94-004', 36, 'Kabupaten Deiyai'),
(312, '94-005', 36, 'Kabupaten Dogiyai'),
(313, '94-006', 36, 'Kabupaten Intan Jaya'),
(314, '94-007', 36, 'Kabupaten Jayapura'),
(315, '94-008', 36, 'Kabupaten Jayawijaya'),
(316, '94-009', 36, 'Kabupaten Keerom'),
(317, '94-010', 36, 'Kabupaten Kepulauan Yapen'),
(318, '94-011', 36, 'Kabupaten Lanny Jaya'),
(319, '94-012', 36, 'Kabupaten Mamberamo Raya'),
(320, '94-013', 36, 'Kabupaten Mamberamo Tengah'),
(321, '94-014', 36, 'Kabupaten Mappi'),
(322, '94-015', 36, 'Kabupaten Merauke'),
(323, '94-016', 36, 'Kabupaten Mimika'),
(324, '94-017', 36, 'Kabupaten Nabire'),
(325, '94-018', 36, 'Kabupaten Nduga'),
(326, '94-019', 36, 'Kabupaten Paniai'),
(327, '94-020', 36, 'Kabupaten Pegunungan Bintang'),
(328, '94-021', 36, 'Kabupaten Puncak Jaya'),
(329, '94-022', 36, 'Kabupaten Puncak'),
(330, '94-023', 36, 'Kabupaten Sarmi'),
(331, '94-024', 36, 'Kabupaten Supiori'),
(332, '94-025', 36, 'Kabupaten Tolikara'),
(333, '94-026', 36, 'Kabupaten Waropen'),
(334, '94-027', 36, 'Kabupaten Yahukimo'),
(335, '94-028', 36, 'Kabupaten Yalimo'),
(336, '94-029', 36, 'Kota Jayapura'),
(337, '91-001', 35, 'Kabupaten Fakfak'),
(338, '91-002', 35, 'Kabupaten Kaimana'),
(339, '91-003', 35, 'Kabupaten Manokwari'),
(340, '91-004', 35, 'Kabupaten Maybrat'),
(341, '91-005', 35, 'Kabupaten Raja Ampat'),
(342, '91-006', 35, 'Kabupaten Sorong Selatan'),
(343, '91-007', 35, 'Kabupaten Sorong'),
(344, '91-008', 35, 'Kabupaten Tambrauw'),
(345, '91-009', 35, 'Kabupaten Teluk Bintuni'),
(346, '91-010', 35, 'Kabupaten Teluk Wondama'),
(347, '91-011', 35, 'Kota Sorong'),
(348, '14-001', 7, 'Kabupaten Bengkalis'),
(349, '14-002', 7, 'Kabupaten Indragiri Hilir'),
(350, '14-003', 7, 'Kabupaten Indragiri Hulu'),
(351, '14-004', 7, 'Kabupaten Kampar'),
(352, '14-005', 7, 'Kabupaten Kepulauan Meranti'),
(353, '14-006', 7, 'Kabupaten Kuantan Singingi'),
(354, '14-007', 7, 'Kabupaten Pelalawan'),
(355, '14-008', 7, 'Kabupaten Rokan Hilir'),
(356, '14-009', 7, 'Kabupaten Rokan Hulu'),
(357, '14-010', 7, 'Kabupaten Siak'),
(358, '14-011', 7, 'Kota Dumai'),
(359, '14-012', 7, 'Kota Pekanbaru'),
(360, '76-001', 32, 'Kabupaten Majene'),
(361, '76-002', 32, 'Kabupaten Mamasa'),
(362, '76-003', 32, 'Kabupaten Mamuju Utara'),
(363, '76-004', 32, 'Kabupaten Mamuju'),
(364, '76-005', 32, 'Kabupaten Polewali Mandar'),
(365, '73-001', 29, 'Kabupaten Bantaeng'),
(366, '73-002', 29, 'Kabupaten Barru'),
(367, '73-003', 29, 'Kabupaten Bone'),
(368, '73-004', 29, 'Kabupaten Bulukumba'),
(369, '73-005', 29, 'Kabupaten Enrekang'),
(370, '73-006', 29, 'Kabupaten Gowa'),
(371, '73-007', 29, 'Kabupaten Jeneponto'),
(372, '73-008', 29, 'Kabupaten Kepulauan Selayar'),
(373, '73-009', 29, 'Kabupaten Luwu Timur'),
(374, '73-010', 29, 'Kabupaten Luwu Utara'),
(375, '73-011', 29, 'Kabupaten Luwu'),
(376, '73-012', 29, 'Kabupaten Maros'),
(377, '73-013', 29, 'Kabupaten Pangkajene dan Kepul'),
(378, '73-014', 29, 'Kabupaten Pinrang'),
(379, '73-015', 29, 'Kabupaten Sidenreng Rappang'),
(380, '73-016', 29, 'Kabupaten Sinjai'),
(381, '73-017', 29, 'Kabupaten Soppeng'),
(382, '73-018', 29, 'Kabupaten Takalar'),
(383, '73-019', 29, 'Kabupaten Tana Toraja'),
(384, '73-020', 29, 'Kabupaten Toraja Utara'),
(385, '73-021', 29, 'Kabupaten Wajo'),
(386, '73-022', 29, 'Kota Makassar'),
(387, '73-023', 29, 'Kota Palopo'),
(388, '73-024', 29, 'Kota Parepare'),
(389, '72-001', 28, 'Kabupaten Banggai Kepulauan'),
(390, '72-002', 28, 'Kabupaten Banggai'),
(391, '72-003', 28, 'Kabupaten Buol'),
(392, '72-004', 28, 'Kabupaten Donggala'),
(393, '72-005', 28, 'Kabupaten Morowali'),
(394, '72-006', 28, 'Kabupaten Parigi Moutong'),
(395, '72-007', 28, 'Kabupaten Poso'),
(396, '72-008', 28, 'Kabupaten Sigi'),
(397, '72-009', 28, 'Kabupaten Tojo Una-Una'),
(398, '72-010', 28, 'Kabupaten Toli-Toli'),
(399, '72-011', 28, 'Kota Palu'),
(400, '74-001', 30, 'Kabupaten Bombana'),
(401, '74-002', 30, 'Kabupaten Buton Utara'),
(402, '74-003', 30, 'Kabupaten Buton'),
(403, '74-004', 30, 'Kabupaten Kolaka Utara'),
(404, '74-005', 30, 'Kabupaten Kolaka'),
(405, '74-006', 30, 'Kabupaten Konawe Selatan'),
(406, '74-007', 30, 'Kabupaten Konawe Utara'),
(407, '74-008', 30, 'Kabupaten Konawe'),
(408, '74-009', 30, 'Kabupaten Muna'),
(409, '74-010', 30, 'Kabupaten Wakatobi'),
(410, '74-011', 30, 'Kota Bau-Bau'),
(411, '74-012', 30, 'Kota Kendari'),
(412, '71-001', 27, 'Kabupaten Bolaang Mongondow Se'),
(413, '71-002', 27, 'Kabupaten Bolaang Mongondow Ti'),
(414, '71-003', 27, 'Kabupaten Bolaang Mongondow Ut'),
(415, '71-004', 27, 'Kabupaten Bolaang Mongondow'),
(416, '71-005', 27, 'Kabupaten Kepulauan Sangihe'),
(417, '71-006', 27, 'Kabupaten Kepulauan Siau Tagul'),
(418, '71-007', 27, 'Kabupaten Kepulauan Talaud'),
(419, '71-008', 27, 'Kabupaten Minahasa Selatan'),
(420, '71-009', 27, 'Kabupaten Minahasa Tenggara'),
(421, '71-010', 27, 'Kabupaten Minahasa Utara'),
(422, '71-011', 27, 'Kabupaten Minahasa'),
(423, '71-012', 27, 'Kota Bitung'),
(424, '71-013', 27, 'Kota Kotamobagu'),
(425, '71-014', 27, 'Kota Manado'),
(426, '71-015', 27, 'Kota Tomohon'),
(427, '13-001', 6, 'Kabupaten Agam'),
(428, '13-002', 6, 'Kabupaten Dharmasraya'),
(429, '13-003', 6, 'Kabupaten Kepulauan Mentawai'),
(430, '13-004', 6, 'Kabupaten Lima Puluh Kota'),
(431, '13-005', 6, 'Kabupaten Padang Pariaman'),
(432, '13-006', 6, 'Kabupaten Pasaman Barat'),
(433, '13-007', 6, 'Kabupaten Pasaman'),
(434, '13-008', 6, 'Kabupaten Pesisir Selatan'),
(435, '13-009', 6, 'Kabupaten Sijunjung'),
(436, '13-010', 6, 'Kabupaten Solok Selatan'),
(437, '13-011', 6, 'Kabupaten Solok'),
(438, '13-012', 6, 'Kabupaten Tanah Datar'),
(439, '13-013', 6, 'Kota Bukittinggi'),
(440, '13-014', 6, 'Kota Padang'),
(441, '13-015', 6, 'Kota Padangpanjang'),
(442, '13-016', 6, 'Kota Pariaman'),
(443, '13-017', 6, 'Kota Payakumbuh'),
(444, '13-018', 6, 'Kota Sawahlunto'),
(445, '13-019', 6, 'Kota Solok'),
(446, '16-001', 9, 'Kabupaten Banyuasin'),
(447, '16-002', 9, 'Kabupaten Empat Lawang'),
(448, '16-003', 9, 'Kabupaten Lahat'),
(449, '16-004', 9, 'Kabupaten Muara Enim'),
(450, '16-005', 9, 'Kabupaten Musi Banyuasin'),
(451, '16-006', 9, 'Kabupaten Musi Rawas'),
(452, '16-007', 9, 'Kabupaten Ogan Ilir'),
(453, '16-008', 9, 'Kabupaten Ogan Komering Ilir'),
(454, '16-009', 9, 'Kabupaten Ogan Komering Ulu Se'),
(455, '16-010', 9, 'Kabupaten Ogan Komering Ulu Ti'),
(456, '16-011', 9, 'Kabupaten Ogan Komering Ulu'),
(457, '16-012', 9, 'Kota Lubuklinggau'),
(458, '16-013', 9, 'Kota Pagar Alam'),
(459, '16-014', 9, 'Kota Palembang'),
(460, '16-015', 9, 'Kota Prabumulih'),
(461, '12-001', 5, 'Kabupaten Asahan'),
(462, '12-002', 5, 'Kabupaten Batubara'),
(463, '12-003', 5, 'Kabupaten Dairi'),
(464, '12-004', 5, 'Kabupaten Deli Serdang'),
(465, '12-005', 5, 'Kabupaten Humbang Hasundutan'),
(466, '12-006', 5, 'Kabupaten Karo'),
(467, '12-007', 5, 'Kabupaten Labuhanbatu Selatan'),
(468, '12-008', 5, 'Kabupaten Labuhanbatu Utara'),
(469, '12-009', 5, 'Kabupaten Labuhanbatu'),
(470, '12-010', 5, 'Kabupaten Langkat'),
(471, '12-011', 5, 'Kabupaten Mandailing Natal'),
(472, '12-012', 5, 'Kabupaten Nias Barat'),
(473, '12-013', 5, 'Kabupaten Nias Selatan'),
(474, '12-014', 5, 'Kabupaten Nias Utara'),
(475, '12-015', 5, 'Kabupaten Nias'),
(476, '12-016', 5, 'Kabupaten Padang Lawas Utara'),
(477, '12-017', 5, 'Kabupaten Padang Lawas'),
(478, '12-018', 5, 'Kabupaten Pakpak Bharat'),
(479, '12-019', 5, 'Kabupaten Samosir'),
(480, '12-020', 5, 'Kabupaten Serdang Bedagai'),
(481, '12-021', 5, 'Kabupaten Simalungun'),
(482, '12-022', 5, 'Kabupaten Tapanuli Selatan'),
(483, '12-023', 5, 'Kabupaten Tapanuli Tengah'),
(484, '12-024', 5, 'Kabupaten Tapanuli Utara'),
(485, '12-025', 5, 'Kabupaten Toba Samosir'),
(486, '12-026', 5, 'Kota Binjai'),
(487, '12-027', 5, 'Kota Gunungsitoli'),
(488, '12-028', 5, 'Kota Medan'),
(489, '12-029', 5, 'Kota Padangsidempuan'),
(490, '12-030', 5, 'Kota Pematangsiantar'),
(491, '12-031', 5, 'Kota Sibolga'),
(492, '12-032', 5, 'Kota Tanjungbalai'),
(493, '12-033', 5, 'Kota Tebing Tinggi'),
(494, '34-001', 17, 'Kabupaten Bantul'),
(495, '34-002', 17, 'Kabupaten Gunung Kidul'),
(496, '34-003', 17, 'Kabupaten Kulon Progo'),
(497, '34-004', 17, 'Kabupaten Sleman'),
(498, '34-005', 17, 'Kota Yogyakarta');

-- --------------------------------------------------------

--
-- Table structure for table `kamar`
--

CREATE TABLE IF NOT EXISTS `kamar` (
  `idkamar` int(11) NOT NULL AUTO_INCREMENT,
  `gedung_id` int(11) DEFAULT NULL,
  `nmkamar` char(50) DEFAULT NULL,
  `kelas` int(11) DEFAULT NULL,
  `kapasitas` int(5) DEFAULT NULL,
  PRIMARY KEY (`idkamar`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `kamar`
--


-- --------------------------------------------------------

--
-- Table structure for table `kamar_kelas`
--

CREATE TABLE IF NOT EXISTS `kamar_kelas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas` char(25) DEFAULT NULL,
  `nama` char(50) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `kamar_kelas`
--


-- --------------------------------------------------------

--
-- Table structure for table `kehadiran`
--

CREATE TABLE IF NOT EXISTS `kehadiran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kdbintek` char(25) DEFAULT NULL,
  `kdpel` char(25) DEFAULT NULL,
  `kdlembaga` char(25) DEFAULT NULL,
  `kdguru` char(25) DEFAULT NULL,
  `kdpengajar` char(25) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `jam` time DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `ket` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `kehadiran`
--


-- --------------------------------------------------------

--
-- Table structure for table `kelompok`
--

CREATE TABLE IF NOT EXISTS `kelompok` (
  `kdkel` char(5) DEFAULT NULL,
  `nama` char(50) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `kelompok`
--


-- --------------------------------------------------------

--
-- Table structure for table `lembaga`
--

CREATE TABLE IF NOT EXISTS `lembaga` (
  `lembaga_id` int(11) NOT NULL AUTO_INCREMENT,
  `kdlembaga` char(10) DEFAULT NULL,
  `nss` char(20) DEFAULT NULL,
  `nmlembaga` char(50) DEFAULT NULL,
  `alamat` char(100) DEFAULT NULL,
  `kdkab` char(10) DEFAULT NULL,
  `kdprop` char(4) DEFAULT NULL,
  `kdkel` char(1) DEFAULT NULL,
  `telp` char(13) DEFAULT NULL,
  `telp2` char(13) DEFAULT NULL,
  `web` char(20) DEFAULT NULL,
  `email` char(30) DEFAULT NULL,
  `negeri` char(1) DEFAULT NULL,
  `jenis` char(1) DEFAULT NULL,
  `pimpinan` char(50) DEFAULT NULL,
  `hppimpinan` char(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lembaga_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `lembaga`
--

INSERT INTO `lembaga` (`lembaga_id`, `kdlembaga`, `nss`, `nmlembaga`, `alamat`, `kdkab`, `kdprop`, `kdkel`, `telp`, `telp2`, `web`, `email`, `negeri`, `jenis`, `pimpinan`, `hppimpinan`, `user_id`) VALUES
(19, '01', NULL, 'Dinas Kebakaran', 'Jl. H. Sanusi', '11-001', '4', NULL, NULL, NULL, NULL, 'meilan.amaliah@gmail.com', NULL, NULL, NULL, NULL, 38),
(16, '001', '111', 'LSM', 'Jl. H. Sanusi', '11-002', '4', NULL, NULL, NULL, NULL, 'nietaldarkopik@gmail.com', NULL, NULL, NULL, NULL, 0),
(20, '001', 'sdfsdf', 'Test', 'Jl. H. Sanusi', '51-001', '20', NULL, NULL, NULL, NULL, 'nietaldarkopik@gmail.com', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `url` text,
  `access` char(20) DEFAULT NULL,
  `request` text,
  `user_agent` text,
  `ip_address` char(30) NOT NULL,
  `browser` int(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `log`
--


-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_menu` int(11) DEFAULT NULL,
  `menu_title` char(255) DEFAULT NULL,
  `menu_name` char(255) DEFAULT NULL,
  `controller` char(50) DEFAULT NULL,
  `function` char(50) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `icon` char(30) DEFAULT NULL,
  `type` char(15) DEFAULT NULL,
  `status` char(20) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=92 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `parent_menu`, `menu_title`, `menu_name`, `controller`, `function`, `sort_order`, `icon`, `type`, `status`) VALUES
(1, 0, 'Home', 'dashboard_index758', 'dashboard', 'index', 0, 'ui-icon-home', 'home', 'fixed'),
(2, 12, 'Pengumuman/Berita', 'news_add', 'news', 'index', 0, 'ui-icon-clipboard', NULL, 'fixed'),
(3, 12, 'Data Propinsi', 'province_index', 'province', 'index', 1, 'ui-icon-document-b', NULL, 'fixed'),
(4, 12, 'Data Kabupaten/Kota', 'city_index', 'city', 'index', 2, 'ui-icon-document-b', NULL, 'fixed'),
(5, 12, 'Data Lembaga', 'institution_index', 'institution', 'index', 3, 'ui-icon-document-b', NULL, 'fixed'),
(6, 0, 'Bintek', 'bintek_member_register81', 'bintek_member', 'register', 2, 'ui-icon-gear', 'bintek', 'fixed'),
(7, 0, 'Uji Kompetensi', 'ukom_member_register851', 'ukom_member', 'register', 3, 'ui-icon-gear', 'uji_kompetensi', 'fixed'),
(8, 0, 'PNBP', 'facility_index334', 'facility', 'index', 5, 'ui-icon-gear', 'pnbp', 'fixed'),
(9, 0, 'Administrasi', 'users_index185', 'users', 'index', 6, 'ui-icon-person', 'administrasi', 'fixed'),
(80, 0, 'Logout', 'dashboard_do_logout392', 'dashboard', 'do_logout', 8, 'ui-icon-gear', 'home', NULL),
(12, 0, 'Utility', 'news_index302', 'news', 'index', 7, 'ui-icon-wrench', 'utility', 'fixed'),
(14, 12, 'Log', 'log_index', 'log', 'index', 7, 'ui-icon-note', NULL, 'fixed'),
(75, 20, 'Data Monitoring & Evaluasi', NULL, 'bintek_monev', 'index', 8, 'ui-icon-gear', 'admin', NULL),
(16, 0, 'User Manual', 'user_manual_index737', 'user_manual', 'index', 1, 'ui-icon-gear', 'general', 'fixed'),
(18, 6, 'Registrasi Peserta Bintek', 'member_bintek_add', 'bintek_member', 'register', 0, 'ui-icon-gear', 'admin', 'fixed'),
(19, 6, 'User Manual', 'user_manual_bintek', 'user_manual', 'bintek', 1, 'ui-icon-gear', NULL, 'fixed'),
(20, 6, 'Setup', 'setup20', '#', 'index', 2, 'ui-icon-gear', 'admin', 'fixed'),
(21, 20, 'Data Peserta', 'member_bintek_index', 'bintek_member', 'index', 3, 'ui-icon-gear', 'admin', 'fixed'),
(22, 20, 'Data Instruktur', 'instructure_index', 'bintek_instructure', 'index', 2, 'ui-icon-gear', 'admin', 'fixed'),
(23, 20, 'Data Evaluasi Instruktur', 'evaluation_instruktur_index', 'bintek_evaluation_instructure', 'index', 6, 'ui-icon-gear', 'admin', 'fixed'),
(24, 20, 'Data Bintek', 'type_bintek_index', 'bintek_type', 'index', 0, 'ui-icon-gear', 'admin', 'fixed'),
(25, 20, 'Data Jadwal', 'schedule_add', 'bintek_schedule', 'index', 4, 'ui-icon-gear', 'admin', 'fixed'),
(26, 20, 'Data Materi', 'material_index', 'bintek_material', 'index', 1, 'ui-icon-gear', 'admin', 'fixed'),
(27, 20, 'Data Evaluasi Materi', 'evaluation_material_index', 'bintek_evaluation_material', 'index', 7, 'ui-icon-gear', 'admin', 'fixed'),
(28, 20, 'Data Pendanaan', 'finance_index', 'bintek_finance', 'index', 10, 'ui-icon-gear', 'admin', 'fixed'),
(29, 20, 'Data ATK', 'atk_index', 'bintek_atk', 'index', 9, 'ui-icon-gear', 'admin', 'fixed'),
(31, 6, 'Laporan', 'laporan31', '#', 'index', 3, 'ui-icon-gear', NULL, 'fixed'),
(32, 31, 'Daftar Peserta', 'member_bintek_report', 'bintek_member', 'report', 3, 'ui-icon-gear', 'admin', 'fixed'),
(33, 31, 'Daftar Instruktur', 'instructure_report', 'bintek_instructure', 'report', 2, 'ui-icon-gear', 'admin', 'fixed'),
(34, 31, 'Daftar Jadwal Bintek', 'schedule_report', 'bintek_schedule', 'report', 0, 'ui-icon-gear', 'admin', 'fixed'),
(35, 31, 'Daftar Materi Bintek', 'material_report', 'bintek_material', 'report', 1, 'ui-icon-gear', 'admin', 'fixed'),
(36, 31, 'Daftar Pendanaan', 'finance_report', 'bintek_finance', 'report', 8, 'ui-icon-gear', 'admin', 'fixed'),
(37, 31, 'Daftar ATK', 'atk_report', 'bintek_atk', 'report', 7, 'ui-icon-gear', 'admin', 'fixed'),
(38, 31, 'Daftar Monev', 'monev_report', 'bintek_monev', 'report', 6, 'ui-icon-gear', 'admin', 'fixed'),
(39, 31, 'Evaluasi Materi Bintek', 'evaluation_material_report', 'bintek_evaluation_material', 'report', 5, 'ui-icon-gear', 'admin', 'fixed'),
(40, 31, 'Evaluasi Instruktur', 'evaluation_instructure_report', 'bintek_evaluation_instructure', 'report', 4, 'ui-icon-gear', 'admin', 'fixed'),
(41, 7, 'Registrasi Peserta Uji Kompetensi', 'member_ukom_add', 'ukom_member', 'register', 0, 'ui-icon-gear', 'admin', 'fixed'),
(42, 7, 'Setup', 'setup42', '#', 'index', 1, 'ui-icon-gear', NULL, 'fixed'),
(43, 42, 'Data Uji Kompetensi', 'ukom_index', 'ukom', 'index', 4, 'ui-icon-gear', 'admin', 'fixed'),
(44, 42, 'Data Penilai/Asesor', 'assesor_index', 'ukom_assesor', 'index', 2, 'ui-icon-gear', 'admin', 'fixed'),
(45, 42, 'Data Aspek Yang Dinilai', 'aspects_evaluation_index', 'ukom_aspects_evaluation', 'index', 1, 'ui-icon-gear', 'admin', 'fixed'),
(46, 42, 'Data Jenis Uji Kompetensi', 'type_ukom_index', 'ukom_type', 'index', 0, 'ui-icon-gear', 'admin', 'fixed'),
(47, 42, 'Data Peserta Uji Kompetensi', 'member_ukom_index', 'ukom_member', 'index', 3, 'ui-icon-gear', 'admin', 'fixed'),
(48, 42, 'Data Hasil Uji Kompetensi', 'ukom_results_add', 'ukom_results', 'index', 5, 'ui-icon-gear', 'admin', 'fixed'),
(49, 74, 'Laporan', 'laporan49', '#', 'index', 2, 'ui-icon-gear', NULL, 'fixed'),
(50, 73, 'Daftar Hasil Uji Kompetensi', 'ukom_certification_results_index', 'ukom_results', 'report', 0, 'ui-icon-gear', 'admin', 'fixed'),
(51, 8, 'Data Fasilitas', 'facility_index', 'facility', 'index', 0, 'ui-icon-gear', NULL, 'fixed'),
(52, 8, 'Data Biaya', 'data_biaya', '#', 'index', 1, 'ui-icon-gear', NULL, 'fixed'),
(53, 52, 'Pengeluaran (Pemeliharaan)', 'finance_outcome', 'finance_outcome', 'index', 0, 'ui-icon-gear', 'admin', 'fixed'),
(54, 52, 'Pemasukan (Penyewaan)', 'finance_income', 'finance_income', 'index', 1, 'ui-icon-gear', 'admin', 'fixed'),
(55, 8, 'Laporan Biaya', 'finance_income_outcome', 'finance_income_outcome', 'report', 2, 'ui-icon-gear', 'admin', 'fixed'),
(56, 9, 'User & Operator', 'user_index', 'users', 'index', 0, 'ui-icon-gear', NULL, 'fixed'),
(57, 9, 'User Level', 'user_level_index', 'user_level', 'index', 1, 'ui-icon-gear', NULL, 'fixed'),
(59, 9, 'User Manual', 'user_manual_add', 'user_manual', 'add', 3, 'ui-icon-gear', NULL, 'fixed'),
(60, 10, 'Forum', 'forum_index', 'forum', 'index', 0, 'ui-icon-gear', NULL, 'fixed'),
(61, 10, 'Tanggapan', 'forum_feedback_index', 'forum_feedback', 'index', 1, 'ui-icon-gear', NULL, 'fixed'),
(62, 11, 'Polling', 'polling_index', 'polling', 'index', 0, 'ui-icon-gear', NULL, 'fixed'),
(63, 11, 'Tanggapan', 'polling_feedback', 'polling', 'feedback', 1, 'ui-icon-gear', NULL, 'fixed'),
(64, 12, 'Ganti Password Admin', 'user_change_admin_password', 'users', 'change_password_admin', 5, 'ui-icon-gear', 'admin', 'fixed'),
(65, 12, 'Reset & Kirim Username & Password', 'user_reset_password', 'users', 'reset_password', 6, 'ui-icon-gear', 'admin', 'fixed'),
(66, 9, 'Right User Access', 'user_level_roles_index', 'user_level_role', 'index', 2, 'ui-icon-gear', NULL, 'fixed'),
(68, 74, 'Registrasi Peserta Sertifikasi', 'member_certification_add', 'certification_member', 'register', 0, 'ui-icon-gear', 'admin', 'fixed'),
(69, 74, 'Setup', 'setup69', '#', 'index', 1, 'ui-icon-gear', NULL, 'fixed'),
(70, 69, 'Data Sertifikasi', 'certification_index', 'certification', 'index', 4, 'ui-icon-gear', 'admin', 'fixed'),
(71, 69, 'Data Penilai/Asesor', 'certification_assesor_index', 'certification_assesor', 'index', 2, 'ui-icon-gear', 'admin', 'fixed'),
(72, 69, 'Data Aspek Yang Dinilai', 'certification_aspects_evaluation_index', 'certification_aspects_evaluation', 'index', 1, 'ui-icon-gear', 'admin', 'fixed'),
(73, 7, 'Laporan', 'laporan73', '#', 'index', 2, 'ui-icon-gear', 'admin', 'fixed'),
(74, 0, 'Sertifikasi', 'certification_member_register430', 'certification_member', 'register', 4, 'ui-icon-gear', 'sertifikasi', 'fixed'),
(76, 49, 'Daftar Hasil Sertifikasi', 'certification_results_report', 'certification_results', 'report', 0, 'ui-icon-gear', 'admin', NULL),
(77, 69, 'Data Jenis Sertifikasi', 'certification_type_index', 'certification_type', 'index', 0, 'ui-icon-gear', 'admin', NULL),
(78, 69, 'Data Peserta Sertifikasi', 'certification_member_index', 'certification_member', 'index', 3, 'ui-icon-gear', 'admin', NULL),
(79, 69, 'Data Hasil Sertifikasi', 'certification_results_index', 'certification_results', 'index', 5, 'ui-icon-gear', 'admin', NULL),
(82, 31, 'Sertifikat', 'bintek_certificate_index807', 'bintek_certificate', 'index', 9, '', '', NULL),
(83, 31, 'Surat Keterangan', 'bintek_citation_index999', 'bintek_citation', 'index', 10, '', '', NULL),
(84, 31, 'Grafik', 'bintek_member_grafic', 'bintek_member', 'grafic', 11, '', NULL, NULL),
(85, 73, 'Sertifikat', 'ukom_certificate_index204', 'ukom_certificate', 'index', 1, '', '', NULL),
(86, 73, 'Grafik', 'ukom_results_grafic', 'ukom_results', 'grafic', 2, '', NULL, NULL),
(87, 49, 'Grafik', 'certification_results_grafic', 'certification_results', 'grafic', 2, '', NULL, NULL),
(88, 49, 'Sertifikat', 'certification_certificate_index252', 'certification_certificate', 'index', 1, '', '', NULL),
(89, 12, 'Setting Sertifikat', 'setting_certificate_index', 'setting_certificate', 'index', 4, '', NULL, NULL),
(90, 8, 'Grafik', 'finance_income_outcome_grafic', 'finance_income_outcome', 'grafic', 3, '', NULL, NULL),
(91, 20, 'Data Evaluasi Peserta', 'bintek_evaluation_member_index818', 'bintek_evaluation_member', 'index', 5, 'ui-icon-gear', 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_assesment`
--

CREATE TABLE IF NOT EXISTS `m_assesment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL,
  `label` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_assesment`
--

INSERT INTO `m_assesment` (`id`, `name`, `label`) VALUES
(1, 'lanjut', 'Lanjut'),
(2, 'tidak lanjut', 'Tidak Lanjut');

-- --------------------------------------------------------

--
-- Table structure for table `m_nilai`
--

CREATE TABLE IF NOT EXISTS `m_nilai` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nilai` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `m_nilai`
--

INSERT INTO `m_nilai` (`id`, `nilai`) VALUES
(1, 10),
(2, 20),
(3, 40),
(4, 80),
(5, 100);

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `kdbintek` char(25) DEFAULT NULL,
  `kdpel` char(25) DEFAULT NULL,
  `kdguru` char(25) DEFAULT NULL,
  `kdpengajar` char(25) DEFAULT NULL,
  `tahun` char(4) DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `huruf` char(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--


-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `judul` char(100) DEFAULT NULL,
  `penulis` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `content` text,
  `thumbnail` char(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pages`
--


-- --------------------------------------------------------

--
-- Table structure for table `pelajaran`
--

CREATE TABLE IF NOT EXISTS `pelajaran` (
  `kdpel` char(25) DEFAULT NULL,
  `namapel` char(50) DEFAULT NULL,
  `jmljam` int(11) DEFAULT NULL,
  `ket` char(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelajaran`
--


-- --------------------------------------------------------

--
-- Table structure for table `pelajaran_bintek`
--

CREATE TABLE IF NOT EXISTS `pelajaran_bintek` (
  `id` int(5) DEFAULT NULL,
  `kdbintek` char(25) DEFAULT NULL,
  `kdpel` char(25) DEFAULT NULL,
  `kdpengajar` char(25) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `ket` char(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelajaran_bintek`
--


-- --------------------------------------------------------

--
-- Table structure for table `pemasukan`
--

CREATE TABLE IF NOT EXISTS `pemasukan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sarana_id` int(11) DEFAULT NULL,
  `nama_sarana` char(100) DEFAULT NULL,
  `keterangan` char(100) DEFAULT NULL,
  `jumlah` int(16) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` char(100) DEFAULT 'CREDIT',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pemasukan`
--

INSERT INTO `pemasukan` (`id`, `sarana_id`, `nama_sarana`, `keterangan`, `jumlah`, `tanggal`, `status`) VALUES
(1, 1, 'Sewa Gedung', 'Penyewaan acara seminar tentang IT', 1500000, '2013-11-08', 'CREDIT'),
(2, 2, 'Sewa Kamar', 'Sewa kamar Pak Joko', 50000, '2013-11-07', 'CREDIT'),
(3, 2, 'Penyewaan Kamar Pak Agus', 'Penyewaan Kamar Pak Agus selama 3 minggu', 1500000, '2013-11-08', 'CREDIT');

-- --------------------------------------------------------

--
-- Table structure for table `penandatangan`
--

CREATE TABLE IF NOT EXISTS `penandatangan` (
  `kdttd` int(3) DEFAULT NULL,
  `nip` char(20) DEFAULT NULL,
  `nama` char(50) DEFAULT NULL,
  `jabatan` char(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penandatangan`
--


-- --------------------------------------------------------

--
-- Table structure for table `pendanaan`
--

CREATE TABLE IF NOT EXISTS `pendanaan` (
  `pendanaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_bintek_id` int(11) DEFAULT NULL,
  `peserta_bintek_id` int(11) DEFAULT NULL,
  `uang_makan` int(11) DEFAULT NULL,
  `uang_transport` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`pendanaan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pendanaan`
--


-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran`
--

CREATE TABLE IF NOT EXISTS `pengeluaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sarana_id` int(11) DEFAULT NULL,
  `nama_sarana` char(100) DEFAULT NULL,
  `keterangan` char(100) DEFAULT NULL,
  `jumlah` int(16) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` char(100) DEFAULT 'DEBIT',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `pengeluaran`
--

INSERT INTO `pengeluaran` (`id`, `sarana_id`, `nama_sarana`, `keterangan`, `jumlah`, `tanggal`, `status`) VALUES
(1, 1, 'Pemasangan Internet', 'Installasi speedy, hotspot dll', 1500000, '2013-11-09', 'DEBIT'),
(2, 1, 'Renovasi Gedung', 'Perbaikan atap', 2000000, '2013-11-09', 'DEBIT'),
(3, 1, 'Pembuatan Taman', 'Pembuatan Taman', 1000000, '2013-11-23', 'DEBIT'),
(4, 1, 'Pembuatan Taman', 'Pembuatan Taman', 2000000, '2013-07-01', 'DEBIT');

-- --------------------------------------------------------

--
-- Table structure for table `penghuni`
--

CREATE TABLE IF NOT EXISTS `penghuni` (
  `idtrans` int(3) DEFAULT NULL,
  `kdbintek` char(25) DEFAULT NULL,
  `idpeserta` char(25) DEFAULT NULL,
  `nmpeserta` char(30) DEFAULT NULL,
  `idgedung` char(25) DEFAULT NULL,
  `idkamar` char(25) DEFAULT NULL,
  `masuk` date DEFAULT NULL,
  `keluar` datetime DEFAULT NULL,
  `catatan` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penghuni`
--


-- --------------------------------------------------------

--
-- Table structure for table `polling`
--

CREATE TABLE IF NOT EXISTS `polling` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan` char(200) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `polling`
--


-- --------------------------------------------------------

--
-- Table structure for table `polling_data`
--

CREATE TABLE IF NOT EXISTS `polling_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `polling_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `jawaban_id` int(11) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_agent` char(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `polling_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `polling_jawaban`
--

CREATE TABLE IF NOT EXISTS `polling_jawaban` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `polling_id` int(11) DEFAULT NULL,
  `jawaban` char(255) DEFAULT NULL,
  `urutan` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `polling_jawaban`
--


-- --------------------------------------------------------

--
-- Table structure for table `propinsi`
--

CREATE TABLE IF NOT EXISTS `propinsi` (
  `propinsi_id` int(11) NOT NULL AUTO_INCREMENT,
  `kdprop` char(4) DEFAULT NULL,
  `kode_iso` char(8) DEFAULT NULL,
  `nama` char(30) DEFAULT NULL,
  PRIMARY KEY (`propinsi_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `propinsi`
--

INSERT INTO `propinsi` (`propinsi_id`, `kdprop`, `kode_iso`, `nama`) VALUES
(3, '-', 'ID-KU', 'Kalimantan Utara'),
(4, '11', 'ID-AC', 'Aceh'),
(5, '12', 'ID-SU', 'Sumatera Utara'),
(6, '13', 'ID-SB', 'Sumatera Barat'),
(7, '14', 'ID-RI', 'Riau'),
(8, '15', 'ID-JA', 'Jambi'),
(9, '16', 'ID-SS', 'Sumatera Selatan'),
(10, '17', 'ID-BE', 'Bengkulu'),
(11, '18', 'ID-LA', 'Lampung'),
(12, '19', 'ID-BB', 'Kepulauan Bangka Belitung'),
(13, '21', 'ID-KR', 'Kepulauan Riau'),
(14, '31', 'ID-JK', 'Jakarta'),
(15, '32', 'ID-JB', 'Jawa Barat'),
(16, '33', 'ID-JT', 'Jawa Tengah'),
(17, '34', 'ID-YO', 'Yogyakarta'),
(18, '35', 'ID-JI', 'Jawa Timur'),
(19, '36', 'ID-BT', 'Banten'),
(20, '51', 'ID-BA', 'Bali'),
(21, '52', 'ID-NB', 'Nusa Tenggara Barat'),
(22, '53', 'ID-NT', 'Nusa Tenggara Timur'),
(23, '61', 'ID-KB', 'Kalimantan Barat'),
(24, '62', 'ID-KT', 'Kalimantan Tengah'),
(25, '63', 'ID-KS', 'Kalimantan Selatan'),
(26, '64', 'ID-KI', 'Kalimantan Timur'),
(27, '71', 'ID-SA', 'Sulawesi Utara'),
(28, '72', 'ID-ST', 'Sulawesi Tengah'),
(29, '73', 'ID-SN', 'Sulawesi Selatan'),
(30, '74', 'ID-SG', 'Sulawesi Tenggara'),
(31, '75', 'ID-GO', 'Gorontalo'),
(32, '76', 'ID-SR', 'Sulawesi Barat'),
(33, '81', 'ID-MA', 'Maluku'),
(34, '82', 'ID-MU', 'Maluku Utara'),
(35, '91', 'ID-PB', 'Papua Barat'),
(36, '94', 'ID-PA', 'Papua');

-- --------------------------------------------------------

--
-- Table structure for table `recycle_bin_data`
--

CREATE TABLE IF NOT EXISTS `recycle_bin_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table` char(30) DEFAULT NULL,
  `data` text,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `recycle_bin_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `recycle_bin_file`
--

CREATE TABLE IF NOT EXISTS `recycle_bin_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recycle_bin_data_id` int(11) DEFAULT NULL,
  `old_path` char(100) DEFAULT NULL,
  `trash_path` char(100) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `recycle_bin_file`
--


-- --------------------------------------------------------

--
-- Table structure for table `relation_bintek_instructure`
--

CREATE TABLE IF NOT EXISTS `relation_bintek_instructure` (
  `relation_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_bintek_id` int(11) NOT NULL,
  `instruktur_id` int(11) NOT NULL,
  PRIMARY KEY (`relation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `relation_bintek_instructure`
--

INSERT INTO `relation_bintek_instructure` (`relation_id`, `jadwal_bintek_id`, `instruktur_id`) VALUES
(3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `relation_bintek_member`
--

CREATE TABLE IF NOT EXISTS `relation_bintek_member` (
  `relation_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_bintek_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `tanggal_registrasi` date NOT NULL,
  `status_registrasi` char(15) NOT NULL,
  PRIMARY KEY (`relation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `relation_bintek_member`
--

INSERT INTO `relation_bintek_member` (`relation_id`, `jadwal_bintek_id`, `member_id`, `tanggal_registrasi`, `status_registrasi`) VALUES
(19, 1, 3, '2013-11-03', 'sudah'),
(21, 1, 6, '2013-11-03', 'sudah'),
(23, 1, 4, '2013-11-17', 'belum');

-- --------------------------------------------------------

--
-- Table structure for table `rformal`
--

CREATE TABLE IF NOT EXISTS `rformal` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kdpengajar` char(25) DEFAULT NULL,
  `tkt` char(4) DEFAULT NULL,
  `sekolah` char(50) DEFAULT NULL,
  `lokasi` char(50) DEFAULT NULL,
  `jurusan` char(50) DEFAULT NULL,
  `thnlulus` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `rformal`
--


-- --------------------------------------------------------

--
-- Table structure for table `rjabatan`
--

CREATE TABLE IF NOT EXISTS `rjabatan` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kdpengajar` char(25) DEFAULT NULL,
  `jenis` char(1) DEFAULT NULL,
  `jabatan` char(50) DEFAULT NULL,
  `lembaga` char(50) DEFAULT NULL,
  `nosk` char(50) DEFAULT NULL,
  `thn` char(4) DEFAULT NULL,
  `ttugas` char(20) DEFAULT NULL,
  `tmt` date DEFAULT NULL,
  `akhirtmt` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `rjabatan`
--


-- --------------------------------------------------------

--
-- Table structure for table `rmengajar`
--

CREATE TABLE IF NOT EXISTS `rmengajar` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kdpengajar` char(25) DEFAULT NULL,
  `kdjur` char(2) DEFAULT NULL,
  `topik` char(50) DEFAULT NULL,
  `deskripsi` char(50) DEFAULT NULL,
  `lembaga` char(50) DEFAULT NULL,
  `nosk` char(50) DEFAULT NULL,
  `thn` char(4) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `tempat` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `rmengajar`
--


-- --------------------------------------------------------

--
-- Table structure for table `rnilai`
--

CREATE TABLE IF NOT EXISTS `rnilai` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kode` char(2) DEFAULT NULL,
  `des` char(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `rnilai`
--


-- --------------------------------------------------------

--
-- Table structure for table `rnonformal`
--

CREATE TABLE IF NOT EXISTS `rnonformal` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kdpengajar` char(25) DEFAULT NULL,
  `nmkursus` char(50) DEFAULT NULL,
  `pelaksana` char(50) DEFAULT NULL,
  `lokasi` char(50) DEFAULT NULL,
  `thn` char(4) DEFAULT NULL,
  `jmljam` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `rnonformal`
--


-- --------------------------------------------------------

--
-- Table structure for table `rsertifikat`
--

CREATE TABLE IF NOT EXISTS `rsertifikat` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kdpengajar` char(25) DEFAULT NULL,
  `nmsertifikat` char(50) DEFAULT NULL,
  `status` char(50) DEFAULT NULL,
  `lembaga` char(50) DEFAULT NULL,
  `nosertifikat` char(50) DEFAULT NULL,
  `thn` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `rsertifikat`
--


-- --------------------------------------------------------

--
-- Table structure for table `sarana`
--

CREATE TABLE IF NOT EXISTS `sarana` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sarana` char(100) DEFAULT NULL,
  `harga` int(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sarana`
--


-- --------------------------------------------------------

--
-- Table structure for table `sertifikasi`
--

CREATE TABLE IF NOT EXISTS `sertifikasi` (
  `sertifikasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `peserta_id` int(11) NOT NULL,
  `accessor_id` int(11) NOT NULL,
  `jenis_sertifikasi` char(25) NOT NULL,
  `tanggal` date NOT NULL,
  `nilai` text NOT NULL,
  `status` char(30) NOT NULL,
  PRIMARY KEY (`sertifikasi_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sertifikasi`
--

INSERT INTO `sertifikasi` (`sertifikasi_id`, `peserta_id`, `accessor_id`, `jenis_sertifikasi`, `tanggal`, `nilai`, `status`) VALUES
(1, 1, 1, '001', '2013-10-09', '', 'lanjut'),
(2, 2, 3, '004', '2013-11-18', '[{"label":"30","value":"10"},{"label":"31","value":"20"},{"label":"32","value":"0"},{"label":"33","value":"0"},{"label":"34","value":"0"},{"label":"35","value":"0"},{"label":"36","value":"0"},{"label":"37","value":"0"},{"label":"38","value":"0"},{"label":"39","value":"0"},{"label":"40","value":"0"},{"label":"41","value":"0"},{"label":"42","value":"0"},{"label":"43","value":"0"},{"label":"44","value":"0"},{"label":"45","value":"0"},{"label":"46","value":"0"}]', 'lanjut');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikasi_asesor`
--

CREATE TABLE IF NOT EXISTS `sertifikasi_asesor` (
  `asesor_id` int(11) NOT NULL AUTO_INCREMENT,
  `sertifikasi_id` int(11) DEFAULT NULL,
  `nama` char(20) DEFAULT NULL,
  `nip` char(20) DEFAULT NULL,
  `gelar` char(20) DEFAULT NULL,
  `gol` char(10) DEFAULT NULL,
  `tempat_lahir_propinsi` char(10) DEFAULT NULL,
  `tempat_lahir_kota` char(10) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jabatan` char(20) DEFAULT NULL,
  `status` char(20) DEFAULT NULL,
  `instansi` char(100) DEFAULT NULL,
  `alamat_instansi` char(255) DEFAULT NULL,
  `propinsi_instansi` char(10) DEFAULT NULL,
  `kota_instansi` char(10) DEFAULT NULL,
  `telepon_instansi` char(20) DEFAULT NULL,
  `fax_instansi` char(20) DEFAULT NULL,
  `hp_instansi` char(20) DEFAULT NULL,
  `alamat_rumah` char(255) DEFAULT NULL,
  `propinsi_rumah` char(10) DEFAULT NULL,
  `kota_rumah` char(10) DEFAULT NULL,
  `telp_rumah` char(20) DEFAULT NULL,
  `fax_rumah` char(20) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `pengalaman_kerja` text,
  `pendidikan_tertinggi` text,
  `pelatihan_teknik` text,
  `spesialisasi_pelatihan` text,
  `foto` char(255) DEFAULT NULL,
  PRIMARY KEY (`asesor_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sertifikasi_asesor`
--

INSERT INTO `sertifikasi_asesor` (`asesor_id`, `sertifikasi_id`, `nama`, `nip`, `gelar`, `gol`, `tempat_lahir_propinsi`, `tempat_lahir_kota`, `tanggal_lahir`, `jabatan`, `status`, `instansi`, `alamat_instansi`, `propinsi_instansi`, `kota_instansi`, `telepon_instansi`, `fax_instansi`, `hp_instansi`, `alamat_rumah`, `propinsi_rumah`, `kota_rumah`, `telp_rumah`, `fax_rumah`, `email`, `pengalaman_kerja`, `pendidikan_tertinggi`, `pelatihan_teknik`, `spesialisasi_pelatihan`, `foto`) VALUES
(1, NULL, 'Taofik', '123434', 'IR', 'IIIA', '16', '16-001', '2013-10-08', 'Programmer', 'Widyaiswara Kem. PU', 'DINAS KEBERSIHAN', 'JL. H. SANUSI', '51', '51-003', '23432', '3434', '234234', 'JL. SARI INDAH 5E', '32', '32-019', '230430', '34034', 'nietaldarkopik@gmail.com', '4 tahun', 'SMK', 'Pelatihan Drainase', 'Pelatihan Air Minum', '9e8175cedd512d57fc0d8da6332330d0.jpg'),
(2, NULL, 'hg', 'jhgh', 'ghj', 'gjhg', '31', '31-001', '2013-10-09', 'lk', 'Widyaiswara Kem. PU', 'lkjk', 'j', '-', '--001', 'lk', 'dsfsdf', 'jhj', 'hjh', '15', '15-001', 'jjh', 'jhk', 'nietaldarkopik@gmail.com', '4 tahun', '{"lem_dik":["","LEMBAGA","PENDIDIKAN"],"kot_neg":["","KOTA","NEGARA"],"gelar":["","GELAR1","GELAR2"],"special":["","SPESIALISASI1","SPESIALISASI2"]}', '{"ur_nam_kur":["","URAIAN","NAMA","KURSUS"],"kot_neg":["","KOTA\\/NEGARA1","KOTA\\/NEGARA2","KOTA\\/NEGARA3"],"lama":["","LAMA1","LAMA2","LAMA3"]}', '{"mat_pel":["","MATA","PELAJARAN"]}', '37563e3be5ab676556c760beafbb63bf.jpg'),
(3, NULL, 'Taofik Basuki', '10583023', 'IR.', 'IIIA', '-', '--002', '1990-03-31', 'Programmer', 'Widyaiswara Kem. PU', 'Dinas Pemadam Kebakaran', 'Jl. Sari Indah 5E', '12', '12-001', '0220302023', '0223020320', '0857303032', 'Jl. H. Sanusi', '32', '32-019', '022030234', '022030234', 'nietaldarkopik@gmail.com', '4 tahun', '{"lem_dik":["","UNPAD","ITB"],"kot_neg":["","Bandung","Bandung"],"gelar":["","ST","SE"],"special":["","REKAYASA PERANGKAT LUNAK",""]}', '{"ur_nam_kur":["","Bahasa Inggris"],"kot_neg":["","Bandung"],"lama":["","1 tahun"]}', '{"mat_pel":["","Database","Matematika"]}', 'aeca72cb3b705f5061d00e72bd28bcfe.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikasi_bukti_pendukung`
--

CREATE TABLE IF NOT EXISTS `sertifikasi_bukti_pendukung` (
  `bukti_pendukung_id` int(11) NOT NULL AUTO_INCREMENT,
  `kompetensi_id` char(30) NOT NULL,
  `nilai` int(11) NOT NULL,
  `assesment` char(20) NOT NULL,
  `sertifikasi_id` char(30) NOT NULL,
  `jenis_sertifikasi` char(30) NOT NULL,
  `material_id` int(11) NOT NULL,
  `kode_material` char(30) NOT NULL,
  `judul_material` text NOT NULL,
  `peserta_id` char(30) NOT NULL,
  `accessor_id` char(30) NOT NULL,
  PRIMARY KEY (`bukti_pendukung_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sertifikasi_bukti_pendukung`
--

INSERT INTO `sertifikasi_bukti_pendukung` (`bukti_pendukung_id`, `kompetensi_id`, `nilai`, `assesment`, `sertifikasi_id`, `jenis_sertifikasi`, `material_id`, `kode_material`, `judul_material`, `peserta_id`, `accessor_id`) VALUES
(4, 'PAM.MM02.303.01', 10, 'lanjut', '1', '001', 10, '13231', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', '1', '1'),
(5, 'PAM.MM02.303.01', 20, 'tidak lanjut', '1', '001', 11, '13123', 'Surat referensi dari supervisor / perusahaan mengenai pekerjaan anda', '1', '1'),
(6, 'PAM.MM02.303.01', 40, 'lanjut', '1', '001', 12, 'sfdfsf', 'Contoh pekerjaan yang pernah anda buat (produk jadi)', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikasi_certificates`
--

CREATE TABLE IF NOT EXISTS `sertifikasi_certificates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_eval_id` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `certificate_type` char(50) NOT NULL,
  `no_certificate` char(255) NOT NULL,
  `avg_nilai` double(11,2) NOT NULL,
  `total` int(11) NOT NULL,
  `predikat` char(20) NOT NULL,
  `ttd_tempat` char(100) NOT NULL,
  `ttd_tanggal` date NOT NULL,
  `ttd_jabatan` text NOT NULL,
  `ttd_nama` char(100) NOT NULL,
  `ttd_nip` char(100) NOT NULL,
  `ttd_data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sertifikasi_certificates`
--


-- --------------------------------------------------------

--
-- Table structure for table `sertifikasi_jenis`
--

CREATE TABLE IF NOT EXISTS `sertifikasi_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_sertifikasi` char(25) DEFAULT NULL,
  `nama_sertifikasi` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `sertifikasi_jenis`
--

INSERT INTO `sertifikasi_jenis` (`id`, `kode_sertifikasi`, `nama_sertifikasi`) VALUES
(14, '007', 'OPERATOR RESERVOIR DAN SISTEM DISTRIBUSI'),
(13, '006', 'OPERATOR SUMUR DALAM'),
(8, '001', 'AHLI MANAJEMEN AIR MINUM TINGKAT UTAMA'),
(9, '002', 'AHLI MANAJEMEN AIR MINUM TINGKAT MADYA'),
(10, '003', 'AHLI MANAJEMEN AIR MINUM TINGKAT MUDA'),
(11, '004', 'OPERATOR UNIT AIR BAKU DAN UNIT PRODUKSI'),
(12, '005', 'OPERATOR BANGUNAN PENANGKAP MATA AIR'),
(15, '008', 'OPERATOR INSTRUMENTASI SISTEM PENYEDIAAN AIR MINUM'),
(16, '009', 'AHLI COMMISSIONING IPA'),
(17, '010', 'PELAKSANA PEMERIKSAAN KUALITAS AIR'),
(18, '011', 'PELAKSANA LAPANGAN DETEKSI KEBOCORAN'),
(19, '012', 'AHLI COMMISSIONING JARINGAN PIPA'),
(20, '013', 'AHLI PENGENDALIAN KEHILANGAN AIR'),
(21, '014', 'PELAKSANA REHABILITASI JARINGAN PIPA');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikasi_kompetensi`
--

CREATE TABLE IF NOT EXISTS `sertifikasi_kompetensi` (
  `kompetensi_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kompetensi` char(50) NOT NULL,
  `judul_kompetensi` char(255) NOT NULL,
  `parent_kompetensi` char(25) NOT NULL,
  `jenis_sertifikasi` char(25) NOT NULL,
  PRIMARY KEY (`kompetensi_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `sertifikasi_kompetensi`
--

INSERT INTO `sertifikasi_kompetensi` (`kompetensi_id`, `kode_kompetensi`, `judul_kompetensi`, `parent_kompetensi`, `jenis_sertifikasi`) VALUES
(7, 'PAM.MM02.303.01', 'Melakukan manajemen Bisnis Air Minum Pengembangan', '', '001'),
(8, 'PAM.MM02.305.01', 'Melaksanakan Manajemen Strategik Corporate', '', '001'),
(9, 'PAM.MM02.307.01', 'Melaksanakan Kepemimpinan Visioner', '', '001'),
(10, 'PAM.MM02.309.01', 'Melaksanakan Sistem Manajemen Mutu', '', '001'),
(11, 'PAM.MM02.213.01', 'Melaksanakan Manajemen Produktivitas SDM', '', '001'),
(12, 'PAM.MM02.202.01', 'Melakukan Manajemen Bisnis Air Minum Penyehatan', '', '002'),
(13, 'PAM.MM02.204.01', 'Melaksanakan Manajemen Strategik Divisi', '', '002'),
(14, 'PAM.MM02.206.01', 'Melaksanakan Kepemimpinan Situasional', '', '002'),
(15, 'PAM.MM02.208.01', 'Melaksanakan Manajemen Mutu', '', '002'),
(16, 'PAM.MM02.211.01', 'Melaksanakan Operasi dan Pemeliharaan Sistem Penyediaan Air Minum Sistem Penyediaan Air Minum', '', '002'),
(17, 'PAM.MM02.112.01', 'Melaksanakan Manajemen Sumber Daya Manusia', '', '002'),
(18, 'PAM.MM02.215.01', 'Melaksanakan Manajemen Aset', '', '002'),
(19, 'PAM.MM02.217.01', 'Melaksanakan Manajemen Keuangan Investasi', '', '002'),
(20, 'PAM.MM02.219.01', 'Melaksanakan Komunikasi Bisnis', '', '002'),
(21, 'PAM.MM01.101.01', 'Menerapkan Sistem Manajemen Kesehatan dan Keselamatan kerja', '', '003'),
(22, 'PAM.MM01.102.01', 'Melaksanakan Manajemen Umum', '', '003'),
(23, 'PAM.MM01.103.01', 'Melaksanakan Kepemimpinan Dasar', '', '003'),
(24, 'PAM.MM02.101.01', 'Melakukan Manajemen Bisnis Air Minum', '', '003'),
(25, 'PAM.MM02.110.01', 'Melaksanakan Manajemen Operasi dan Pemeliharaan Sistem Penyediaan Air Minum', '', '003'),
(26, 'PAM.MM02.114.01', 'Melaksanakan Manajemen Barang', '', '003'),
(27, 'PAM.MM02.116.01', 'Melaksanakan Manajemen Keuangan dan Akuntansi', '', '003'),
(28, 'PAM.MM02.118.01', 'Melakukan komunikasi', '', '003'),
(29, 'PAM.MM02.120.01', 'Melaksanakan Manajemen Informasi', '', '003'),
(30, 'PAM MO01.001.01', 'Menerapkan proses pengolahan air minum', '', '004'),
(31, 'PAM MO01.002.01', 'Menerapkan prinsip Kuantitas-kualitas dan kontinuitas', '', '004'),
(32, 'PAM MO01.003.01', 'Melaksanakan kesehatan dan keselamatan kerja', '', '004'),
(33, 'PAM MO02.001.01', 'Mengoperasikan dan memelihara unit air baku dan pengendapan pendahuluan', '', '004'),
(34, 'PAM MO02.002.01', 'Mengoperasikan Dan Memelihara Unit Pengaduk Cepat', '', '004'),
(35, 'PAM MO02.003.01', 'Mengoperasikan Dan Memelihara Sarana Pencampur Kimia', '', '004'),
(36, 'PAM MO02.004.01', 'Mengoperasikan Dan Memelihara Pompa Pembubuh', '', '004'),
(37, 'PAM MO02.005.01', 'Mengoperasikan Dan Memelihara Unit Pengaduk Lambat', '', '004'),
(38, 'PAM MO02.006.01', 'Mengoperasikan Dan Memelihara Unit Pengendapan', '', '004'),
(39, 'PAM MO02.007.01', 'Mengoperasikan Dan Memelihara Unit Penyaring', '', '004'),
(40, 'PAM MO02.010.01', 'Mengoperasikan dan Memelihara Meter Induk', '', '004'),
(41, 'PAM MO02.011.01', 'Mengoperasikan dan Memelihara Bangunan Pengambilan Air Baku, Air Permukaan (Water Intake)', '', '004'),
(42, 'PAM MO02.013.01', 'Mengoperasikan Dan Memelihara Waduk Tempat Penyimpanan Sementara Air Baku (Impounding Reservoir)', '', '004'),
(43, 'PAM MO02.014.01', 'Melakukan pemeliharaan perpipaan pada instalasi pengolahan air minum', '', '004'),
(44, 'PAM MO02.015.01', 'Mencari Dan Memperbaiki Kebocoran Pipa', '', '004'),
(45, 'PAM MO02.016.01', 'Memeriksa, Memelihara Dan Memperbaiki Katup-Katup Udara, Meter Dan Alat-Alat Perlengkapan Lainnya', '', '004'),
(46, 'PAM MO02.019.01', 'Mengoperasikan dan memelihara peralatan mesin / Pompa distribusi', '', '004'),
(47, 'PAM.MM02.303.01.01', 'Melakukan perencanaan pengembangan usaha', 'PAM.MM02.303.01', '001'),
(48, 'PAM.MM02.303.02', 'Melakukan penggalian sumber pembiayaan investasi', 'PAM.MM02.303.01', '001'),
(49, 'PAM.MM02.303.03', 'Melaksanakan strategi kemitraan', 'PAM.MM02.303.01', '001'),
(50, 'PAM.MM02.303.04', 'Melaksanakan kemitraan dengan badan usaha', 'PAM.MM02.303.01', '001'),
(51, 'PAM.MM02.303.01.05', 'Melaksanakan pengelolaan resiko', 'PAM.MM02.303.01', '001'),
(52, 'PAM.MM02.305.01.01', 'Merumuskan Visi dan Misi perusahaan', 'PAM.MM02.305.01', '001'),
(53, 'PAM.MM02.305.01.02', 'Melakukan analisis SWOT', 'PAM.MM02.305.01', '001'),
(54, 'PAM.MM02.305.01.03', 'Menetapkan Tujuan dan Sasaran', 'PAM.MM02.305.01', '001'),
(55, 'PAM.MM02.305.01.04', 'Menerapkan Strategi', 'PAM.MM02.305.01', '001'),
(56, 'PAM.MM02.305.01.05', 'Menyusun Program, Kegiatan Anggaran dan Proyeksi Keuangan', 'PAM.MM02.305.01', '001'),
(57, 'PAM.MM02.307.01.01', 'Menerapkan kepemimpinan visioner', 'PAM.MM02.307.01', '001'),
(58, 'PAM.MM02.307.01.02', 'Melaksanakan perubahan internal', 'PAM.MM02.307.01', '001'),
(59, 'PAM.MM02.307.01.03', 'Melaksanakan pembinaan berbasis kinerja', 'PAM.MM02.307.01', '001'),
(60, 'PAM.MM02.307.01.04', 'Melaksanakan evaluasi efektivitas', 'PAM.MM02.307.01', '001'),
(61, 'PAM.MM02.309.01.01', 'Melakukan perencanaan manajemen mutu', 'PAM.MM02.309.01', '001'),
(62, 'PAM.MM02.309.01.02', 'Menerapkan sistem manajemen mutu', 'PAM.MM02.309.01', '001'),
(63, 'PAM.MM02.309.01.03', 'Melakukan pengendalian mutu', 'PAM.MM02.309.01', '001'),
(64, 'PAM.MM02.309.01.04', 'Melakukan perbaikan mutu', 'PAM.MM02.309.01', '001'),
(65, 'PAM.MM02.213.01.01', 'Menganalisis kebutuhan', 'PAM.MM02.213.01', '001'),
(66, 'PAM.MM02.213.01.02', 'Melaksanakan Rekruitment', 'PAM.MM02.213.01', '001'),
(67, 'PAM.MM02.213.01.03', 'Melaksanakan penempatan SDM', 'PAM.MM02.213.01', '001'),
(68, 'PAM.MM02.213.01.04', 'Menyusun penggajian', 'PAM.MM02.213.01', '001'),
(69, 'PAM.MM02.213.01.05', 'Melaksanakan pembinaan', 'PAM.MM02.213.01', '001'),
(70, 'PAM.MM02.213.01.06', 'Merumuskan jaminan sosial', 'PAM.MM02.213.01', '001');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikasi_material`
--

CREATE TABLE IF NOT EXISTS `sertifikasi_material` (
  `sertifikasi_material` int(11) NOT NULL AUTO_INCREMENT,
  `sertifikasi_jenis_id` char(20) NOT NULL,
  `kompetensi_id` char(25) NOT NULL,
  `element_kompetensi` char(25) NOT NULL,
  `kode_material` char(20) NOT NULL,
  `judul_material` char(255) NOT NULL,
  `type` char(15) NOT NULL DEFAULT 'asesor',
  PRIMARY KEY (`sertifikasi_material`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `sertifikasi_material`
--

INSERT INTO `sertifikasi_material` (`sertifikasi_material`, `sertifikasi_jenis_id`, `kompetensi_id`, `element_kompetensi`, `kode_material`, `judul_material`, `type`) VALUES
(3, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.01.01', 'PAM.MM02.303.01.01', 'Apakah anda mampu menganalisis peluang pengembangan melalui teori pendekatan yang tepat dan melalui survei permintaan ?', 'asesor'),
(4, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.01.01', '1.2', 'Apakah anda mampu melakukan studi kela-yakan dengan menganalisis kelayakan ekono-mi dan finansial serta kemampuan manajerial?', 'asesor'),
(5, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.01.01', '1.3', 'Apakah anda mampu menyampaikan proposal pengembangan ke pemilik dilampiri dengan hasil studi kelayakan untuk memperoleh persetujuan ?', 'asesor'),
(6, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.04', '2.1', 'Apakah anda mampu melakukan komunikasi bisnis untuk penggalian sumber pembiayaan sesuai kewenangan dan prosedur yang ber-laku ?', 'asesor'),
(7, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.02', '2.2', 'Apakah anda mampu menjelaskan pemben-tukan tim negosiator dengan mengikutkan unsur lain dalam perusahaan sesuai dengan lingkup atau cakupan yang akan dinegosia-sikan ?', 'asesor'),
(8, '001', 'PAM.MM02.305.01', 'PAM.MM02.305.01.01', '2.3', 'Apakah anda mampu memperhitungkan peni-laian peluang dan tingkat biaya melalui anali-sis bisnis yang akurat ?', 'asesor'),
(9, '001', 'PAM.MM02.305.01', 'PAM.MM02.305.01.01', '2.4', 'Apakah anda mampu menyampaikan laporan kepada pemilik untuk pengambilan keputusan sesuai dengan prosedur dan dilengkapi dengan masukan untuk pertimbangan ?', 'asesor'),
(10, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.03', '3.1', 'Apakah anda mampu menjelaskan hal yang perlu dilakukan dalam kerjasama penyediaan air baku untuk menjamin rencana pengem-bangan melalui prosedur yang berlaku ?', 'asesor'),
(11, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.03', '3.2', 'Apakah anda mampu melakukan pening-katan kompetensi karyawan dan manajemen melalui strategi yang tepat sesuai dengan kemampuan perusahaan dan tingkat kemen-desakan ?', 'asesor'),
(12, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.03', '3.3', 'Apakah anda mampu merencanakan kerja-sama dengan PDAM maju, khususnya dalam transfer pengalaman ?', 'asesor'),
(13, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.04', '4.1', 'Apakah anda mampu mengkaji kesesuaian bentuk kemitraan dengan kebutuhan pengem-bangan usaha, dengan memperhatikan keku-atan serta kelemahan yang dimiliki perusaha-an saat ini ?', 'asesor'),
(14, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.04', '4.2', 'Apakah anda mampu melakukan prosedur penyelenggaraan kemitraan dengan badan usaha sesuai dengan peraturan perundangan yang berlaku, untuk menjamin manfaat ke-pada semua pihak: masyarakat, pemerintah dan badan usaha ?', 'asesor'),
(15, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.04', '4.3', 'Apakah anda mampu menjaga proses solicited, untuk diterapkan sesuai dengan ketetapan yang berlaku ?', 'asesor');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikasi_material_support`
--

CREATE TABLE IF NOT EXISTS `sertifikasi_material_support` (
  `sertifikasi_material` int(11) NOT NULL AUTO_INCREMENT,
  `sertifikasi_jenis_id` char(20) NOT NULL,
  `kompetensi_id` char(25) NOT NULL,
  `kode_material` char(20) NOT NULL,
  `judul_material` char(255) NOT NULL,
  `type` char(15) NOT NULL DEFAULT 'asesor',
  PRIMARY KEY (`sertifikasi_material`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `sertifikasi_material_support`
--

INSERT INTO `sertifikasi_material_support` (`sertifikasi_material`, `sertifikasi_jenis_id`, `kompetensi_id`, `kode_material`, `judul_material`, `type`) VALUES
(10, '001', 'PAM.MM02.303.01', '13231', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', 'asesor'),
(11, '001', 'PAM.MM02.303.01', '13123', 'Surat referensi dari supervisor / perusahaan mengenai pekerjaan anda', 'asesor'),
(12, '001', 'PAM.MM02.303.01', 'sfdfsf', 'Contoh pekerjaan yang pernah anda buat (produk jadi)', 'asesor'),
(13, '001', 'PAM.MM02.303.01', 'sfsdfsdf', '‘Job description’ dari perusahaan mengenai pekerjaan anda', 'asesor'),
(14, '001', 'PAM.MM02.303.01', '13231321', 'Wawancara dengan supervisor, teman sebaya atau klien', 'asesor'),
(15, '001', 'PAM.MM02.303.01', 'fdfdsf', 'Demonstrasi pekerjaan / keterampilan yang dipersyaratkan', 'asesor'),
(16, '001', 'PAM.MM02.303.01', 'dsadsadsad', 'Pengalaman Industri (on the job training, magang, kerja praktek, dll)', 'asesor'),
(17, '001', 'PAM.MM02.303.01', 'asdasdasd', 'Bukti-bukti lainnya yang relevan', 'asesor'),
(18, '001', 'PAM.MM02.305.01', 'erererw', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', 'asesor'),
(19, '001', 'PAM.MM02.305.01', 'dasdsd', 'Surat referensi dari supervisor / perusahaan mengenai pekerjaan anda', 'asesor'),
(20, '001', 'PAM.MM02.305.01', 'dasdsadas', 'Contoh pekerjaan yang pernah anda buat (produk jadi)', 'asesor'),
(21, '001', 'PAM.MM02.305.01', 'sdfdsfdsf', '‘Job description’ dari perusahaan mengenai pekerjaan anda', 'asesor'),
(22, '001', 'PAM.MM02.305.01', 'adasdsad', 'Wawancara dengan supervisor, teman sebaya atau klien', 'asesor'),
(23, '001', 'PAM.MM02.305.01', 'asds dsda', 'Demonstrasi pekerjaan / keterampilan yang dipersyaratkan', 'asesor'),
(24, '001', 'PAM.MM02.305.01', 'sadsdsa', 'Pengalaman Industri (on the job training, magang, kerja praktek, dll)', 'asesor'),
(25, '001', 'PAM.MM02.305.01', '2132321', 'Bukti-bukti lainnya yang relevan', 'asesor'),
(26, '001', 'PAM.MM02.307.01', '213ewqewq', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', 'asesor'),
(27, '001', 'PAM.MM02.307.01', 'dassad', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', 'asesor'),
(28, '001', 'PAM.MM02.307.01', 'sadsadas', 'Surat referensi dari supervisor / perusahaan mengenai pekerjaan anda', 'asesor'),
(29, '001', 'PAM.MM02.307.01', 'adasdsad', 'Contoh pekerjaan yang pernah anda buat (produk jadi)', 'asesor'),
(30, '001', 'PAM.MM02.307.01', 'asdsad', '‘Job description’ dari perusahaan mengenai pekerjaan anda', 'asesor'),
(31, '001', 'PAM.MM02.307.01', 'dsadasd', 'Wawancara dengan supervisor, teman sebaya atau klien', 'asesor'),
(32, '001', 'PAM.MM02.307.01', 'adasdas', 'Demonstrasi pekerjaan / keterampilan yang dipersyaratkan', 'asesor'),
(33, '001', 'PAM.MM02.307.01', 'sdsadsad', 'Pengalaman Industri (on the job training, magang, kerja praktek, dll)', 'asesor'),
(34, '001', 'PAM.MM02.307.01', 'wadsdasd', 'Bukti-bukti lainnya yang relevan', 'asesor'),
(35, '001', 'PAM.MM02.309.01', 'sadsadasd', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', 'asesor'),
(36, '001', 'PAM.MM02.309.01', 'dfdsfsdf', 'Surat referensi dari supervisor / perusahaan mengenai pekerjaan anda', 'asesor'),
(37, '001', 'PAM.MM02.309.01', 'dsadsadas', 'Contoh pekerjaan yang pernah anda buat (produk jadi)', 'asesor'),
(38, '001', 'PAM.MM02.309.01', 'asdsadasd', '‘Job description’ dari perusahaan mengenai pekerjaan anda', 'asesor'),
(39, '001', 'PAM.MM02.309.01', 'sdsadsadasd', 'Wawancara dengan supervisor, teman sebaya atau klien', 'asesor'),
(40, '001', 'PAM.MM02.309.01', 'sdasds', 'Demonstrasi pekerjaan / keterampilan yang dipersyaratkan', 'asesor'),
(41, '001', 'PAM.MM02.309.01', 'dsadasd', 'Pengalaman Industri (on the job training, magang, kerja praktek, dll)', 'asesor'),
(42, '001', 'PAM.MM02.309.01', 'asdssd', 'Bukti-bukti lainnya yang relevan', 'asesor'),
(43, '001', 'PAM.MM02.213.01', 'asdsdsa', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', 'asesor'),
(44, '001', 'PAM.MM02.213.01', 'sdasdasd', 'Surat referensi dari supervisor / perusahaan mengenai pekerjaan anda', 'asesor'),
(45, '001', 'PAM.MM02.213.01', 'asdsadasd', 'Contoh pekerjaan yang pernah anda buat (produk jadi)', 'asesor'),
(46, '001', 'PAM.MM02.213.01', 'adsdasdsd', '‘Job description’ dari perusahaan mengenai pekerjaan anda', 'asesor'),
(47, '001', 'PAM.MM02.213.01', 'asdasds', 'Wawancara dengan supervisor, teman sebaya atau klien', 'asesor'),
(48, '001', 'PAM.MM02.213.01', 'sadsadsa', 'Demonstrasi pekerjaan / keterampilan yang dipersyaratkan', 'asesor'),
(49, '001', 'PAM.MM02.213.01', 'asdsad', 'Pengalaman Industri (on the job training, magang, kerja praktek, dll)', 'asesor'),
(50, '001', 'PAM.MM02.213.01', 'asdsadasd', 'Bukti-bukti lainnya yang relevan', 'asesor');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikasi_nilai_mandiri`
--

CREATE TABLE IF NOT EXISTS `sertifikasi_nilai_mandiri` (
  `nilai_mandiri_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_sertifikasi` char(30) NOT NULL,
  `kompetensi_id` char(30) NOT NULL,
  `material_id` char(30) NOT NULL,
  `kode_material` char(50) NOT NULL,
  `judul_material` char(50) NOT NULL,
  `peserta_id` char(30) NOT NULL,
  `accessor_id` char(30) NOT NULL,
  `nilai` int(11) NOT NULL,
  `sertifikasi_id` char(30) NOT NULL,
  `bukti` text NOT NULL,
  PRIMARY KEY (`nilai_mandiri_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `sertifikasi_nilai_mandiri`
--

INSERT INTO `sertifikasi_nilai_mandiri` (`nilai_mandiri_id`, `jenis_sertifikasi`, `kompetensi_id`, `material_id`, `kode_material`, `judul_material`, `peserta_id`, `accessor_id`, `nilai`, `sertifikasi_id`, `bukti`) VALUES
(7, '001', 'PAM.MM02.303.01', '3', 'PAM.MM02.303.01.01', 'Apakah anda mampu menganalisis peluang pengembanga', '1', '1', 40, '1', 'd'),
(8, '001', 'PAM.MM02.303.01', '4', '1.2', 'Apakah anda mampu melakukan studi kela-yakan denga', '1', '1', 20, '1', 'e'),
(9, '001', 'PAM.MM02.303.01', '5', '1.3', 'Apakah anda mampu menyampaikan proposal pengembang', '1', '1', 10, '1', 'f');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikasi_peserta`
--

CREATE TABLE IF NOT EXISTS `sertifikasi_peserta` (
  `peserta_sertifikasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `sertifikasi_id` int(11) DEFAULT NULL,
  `lembaga` int(11) NOT NULL,
  `nama` char(30) DEFAULT NULL,
  `tempat_lahir` char(30) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `unit_kerja` char(30) DEFAULT NULL,
  `pendidikan_terakhir` char(10) DEFAULT NULL,
  `jurusan` char(10) DEFAULT NULL,
  `status_kepegawaian` char(10) DEFAULT NULL,
  `nip` char(15) DEFAULT NULL,
  `no_ktp` char(50) DEFAULT NULL,
  `jabatan` char(10) DEFAULT NULL,
  `pangkat` char(10) DEFAULT NULL,
  `golongan` char(10) DEFAULT NULL,
  `bidang_pekerjaan` char(100) DEFAULT NULL,
  `masa_kerja` int(15) DEFAULT NULL,
  `mulai_tahun` int(15) DEFAULT NULL,
  `alamat_kantor` char(255) DEFAULT NULL,
  `alamat_rumah` char(255) DEFAULT NULL,
  `telepon_kantor` char(20) DEFAULT NULL,
  `telepon_rumah` char(20) DEFAULT NULL,
  `hp` char(20) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `bank` char(10) DEFAULT NULL,
  `no_rekening` char(20) DEFAULT NULL,
  `foto` char(255) DEFAULT NULL,
  `tanggal_registrasi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_registrasi` char(15) DEFAULT NULL,
  PRIMARY KEY (`peserta_sertifikasi_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `sertifikasi_peserta`
--

INSERT INTO `sertifikasi_peserta` (`peserta_sertifikasi_id`, `sertifikasi_id`, `lembaga`, `nama`, `tempat_lahir`, `tanggal_lahir`, `unit_kerja`, `pendidikan_terakhir`, `jurusan`, `status_kepegawaian`, `nip`, `no_ktp`, `jabatan`, `pangkat`, `golongan`, `bidang_pekerjaan`, `masa_kerja`, `mulai_tahun`, `alamat_kantor`, `alamat_rumah`, `telepon_kantor`, `telepon_rumah`, `hp`, `email`, `bank`, `no_rekening`, `foto`, `tanggal_registrasi`, `status_registrasi`) VALUES
(1, NULL, 20, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-11-08 00:00:00', 'belum'),
(2, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-09-15 00:00:00', 'sudah'),
(3, NULL, 20, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2011-09-08 00:00:00', 'belum'),
(4, NULL, 20, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(5, NULL, 20, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-09-08 00:00:00', 'belum'),
(6, NULL, 20, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2011-09-08 00:00:00', 'belum'),
(7, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(8, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(9, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(10, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(11, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(12, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(13, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(14, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(15, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(16, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(17, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(18, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(19, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(20, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(21, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2011-10-08 00:00:00', 'belum'),
(22, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(23, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(24, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(25, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(26, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2010-10-08 00:00:00', 'belum'),
(27, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(28, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(29, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(30, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(31, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2009-10-08 00:00:00', 'belum'),
(32, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(33, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(34, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(35, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(36, NULL, 19, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(37, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(38, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(39, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(40, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(41, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(42, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(43, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(44, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(45, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(46, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(47, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(48, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(49, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(50, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(51, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(52, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(53, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(54, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(55, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(56, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(57, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(58, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(59, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah'),
(60, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikat`
--

CREATE TABLE IF NOT EXISTS `sertifikat` (
  `sertifikat_id` int(11) NOT NULL AUTO_INCREMENT,
  `no_sertifikat` char(30) DEFAULT NULL,
  `kdbintek` char(15) DEFAULT NULL,
  `kdguru` char(25) DEFAULT NULL,
  `tgl_sertifikat` date DEFAULT NULL,
  `ttd1` int(5) DEFAULT NULL,
  `ttd2` int(10) DEFAULT NULL,
  `rata` int(10) DEFAULT NULL,
  `huruf` char(20) DEFAULT NULL,
  PRIMARY KEY (`sertifikat_id`),
  KEY `kdsertifikat` (`sertifikat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sertifikat`
--


-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` char(50) DEFAULT NULL,
  `ip_address` char(45) DEFAULT NULL,
  `user_agent` char(120) DEFAULT NULL,
  `last_activity` int(10) DEFAULT NULL,
  `user_data` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
(14, '5c2524fed88cfbd6fbbc40994e9580dc', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.22 (KHTML, like Gecko) Ubuntu Chromium/25.0.1364.160 Chrome/25.0.1364.160', 1385636813, 'a:8:{s:9:"user_data";s:0:"";s:10:"menu_group";s:6:"bintek";s:7:"user_id";s:1:"1";s:12:"secure_nonce";s:25:"138556094915295fb751d3981";s:25:"bintek_member_data_filter";s:0:"";s:27:"bintek_schedule_data_filter";s:0:"";s:23:"bintek_type_data_filter";s:0:"";s:36:"bintek_evaluation_member_data_filter";s:0:"";}');

-- --------------------------------------------------------

--
-- Table structure for table `setting_certificates`
--

CREATE TABLE IF NOT EXISTS `setting_certificates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(250) NOT NULL,
  `name` char(100) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `setting_certificates`
--

INSERT INTO `setting_certificates` (`id`, `title`, `name`, `data`) VALUES
(1, 'Surat Keterangan Bintek Air Minum Depan', 'bintek_sertifikat_depan', '{"ttd_jabatan":"SEKRETARIS<br \\/> DIREKTORAT JENDERAL CIPTA KARYA<br \\/> KEMENTERIAN PEKERJAAN UMUM<br \\/>","jenis_surat":"SURAT KETERANGAN","lembaga":"Kementrian Pekerjaan Umum","ttd_tempat_sertifikat":"Surabaya","ttd_nip":"<strong>19550101 198012 1 001<\\/strong>","ttd_nama":"Ir. Dadan Krisnandar, MT","0":false,"background":"a4198a555a9a33130339fd84a6c262c9.png","deskripsi_sertifikat":"Pada Pembinaan Teknis <strong>{$nama_bintek$}<\\/strong> yang diselenggarakan oleh <strong>{$penyelenggara_bintek$}<\\/strong>, &amp; pada tanggal {$tanggal_bintek$} di {$tempat_bintek$}."}'),
(2, 'Surat Keterangan Bintek Air Minum Belakang', 'bintek_sertifikat_belakang', ''),
(3, 'Surat Keterangan Bintek Sanitasi Depan', 'surat_keterangan_depan', '{"ttd_jabatan":"SEKRETARIS<br \\/> DIREKTORAT JENDERAL CIPTA KARYA<br \\/> KEMENTERIAN PEKERJAAN UMUM<br \\/>","jenis_surat":"SURAT KETERANGAN","lembaga":"Kementrian Pekerjaan Umum","ttd_tempat_sertifikat":"Surabaya","ttd_nip":"<strong>19550101 198012 1 001<\\/strong>","ttd_nama":"Ir. Dadan Krisnandar, MT","0":false,"background":"67f3ce9001331668430a9d6c59769618.png","deskripsi_sertifikat":"Pada Pembinaan Teknis <strong>{$nama_bintek$}<\\/strong> yang diselenggarakan oleh <strong>{$penyelenggara_bintek$}<\\/strong>, &amp; pada tanggal {$tanggal_bintek$} di {$tempat_bintek$}."}'),
(4, 'Surat Keterangan Bintek Sanitasi Belakang', 'surat_keterangan_belakang', ''),
(5, 'Sertifikat Air Minum & Sanitasi Depan', 'bintek_sertifikat_depan', '{"ttd_jabatan":"SEKRETARIS<br \\/> DIREKTORAT JENDERAL CIPTA KARYA<br \\/> KEMENTERIAN PEKERJAAN UMUM<br \\/>","jenis_surat":"SERTIFIKAT","lembaga":"Kementrian Pekerjaan Umum","ttd_tempat_sertifikat":"Surabaya","ttd_nip":"<strong>19550101 198012 1 001<\\/strong>","ttd_nama":"Ir. Dadan Krisnandar, MT","0":false,"background":"67f3ce9001331668430a9d6c59769618.png","deskripsi_sertifikat":"Pada Pembinaan Teknis <strong>{$nama_bintek$}<\\/strong> yang diselenggarakan oleh <strong>{$penyelenggara_bintek$}<\\/strong>, &amp; pada tanggal {$tanggal_bintek$} di {$tempat_bintek$}."}'),
(6, 'Sertifikat Air Minum & Sanitasi Belakang', 'bintek_sertifikat_belakang', ''),
(7, 'Sertifikat Sertifikasi Depan', 'bintek_sertifikat_depan', '{"ttd_jabatan":"SEKRETARIS<br \\/> DIREKTORAT JENDERAL CIPTA KARYA<br \\/> KEMENTERIAN PEKERJAAN UMUM<br \\/>","jenis_surat":"SERTIFIKAT","lembaga":"Kementrian Pekerjaan Umum","ttd_tempat_sertifikat":"Surabaya","ttd_nip":"<strong>19550101 198012 1 001<\\/strong>","ttd_nama":"Ir. Dadan Krisnandar, MT","0":false,"background":"67f3ce9001331668430a9d6c59769618.png","deskripsi_sertifikat":"Pada Pembinaan Teknis <strong>{$nama_bintek$}<\\/strong> yang diselenggarakan oleh <strong>{$penyelenggara_bintek$}<\\/strong>, &amp; pada tanggal {$tanggal_bintek$} di {$tempat_bintek$}."}'),
(8, 'Sertifikat Sertifikasi Belakang', 'bintek_sertifikat_belakang', ''),
(9, 'Sertifikat Uji Kompetensi Depan', 'bintek_sertifikat_depan', '{"ttd_jabatan":"SEKRETARIS<br \\/> DIREKTORAT JENDERAL CIPTA KARYA<br \\/> KEMENTERIAN PEKERJAAN UMUM<br \\/>","jenis_surat":"SERTIFIKAT","lembaga":"Kementrian Pekerjaan Umum","ttd_tempat_sertifikat":"Surabaya","ttd_nip":"<strong>19550101 198012 1 001<\\/strong>","ttd_nama":"Ir. Dadan Krisnandar, MT","0":false,"background":"67f3ce9001331668430a9d6c59769618.png","deskripsi_sertifikat":"Pada Pembinaan Teknis <strong>{$nama_bintek$}<\\/strong> yang diselenggarakan oleh <strong>{$penyelenggara_bintek$}<\\/strong>, &amp; pada tanggal {$tanggal_bintek$} di {$tempat_bintek$}."}'),
(10, 'Sertifikat Uji Kompetensi Belakang', 'bintek_sertifikat_belakang', '');

-- --------------------------------------------------------

--
-- Table structure for table `ujikom`
--

CREATE TABLE IF NOT EXISTS `ujikom` (
  `ujikom_id` int(11) NOT NULL AUTO_INCREMENT,
  `peserta_id` int(11) NOT NULL,
  `accessor_id` int(11) NOT NULL,
  `jenis_ujikom` char(25) NOT NULL,
  `tanggal` date NOT NULL,
  `nilai` text NOT NULL,
  `status` char(30) NOT NULL,
  PRIMARY KEY (`ujikom_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ujikom`
--

INSERT INTO `ujikom` (`ujikom_id`, `peserta_id`, `accessor_id`, `jenis_ujikom`, `tanggal`, `nilai`, `status`) VALUES
(1, 1, 1, '001', '2013-10-09', '[{"label":"7","value":"100"},{"label":"8","value":"100"},{"label":"9","value":"30"},{"label":"10","value":"40"},{"label":"11","value":"50"},{"label":"47","value":"60"},{"label":"48","value":"70"},{"label":"49","value":"80"},{"label":"50","value":"90"},{"label":"51","value":"100"},{"label":"52","value":"90"},{"label":"53","value":"80"},{"label":"54","value":"70"},{"label":"55","value":"60"},{"label":"56","value":"50"},{"label":"57","value":"40"},{"label":"58","value":"30"},{"label":"59","value":"20"},{"label":"60","value":"100"},{"label":"61","value":"55"},{"label":"62","value":"65"},{"label":"63","value":"75"},{"label":"64","value":"85"},{"label":"65","value":"95"},{"label":"66","value":"50"},{"label":"67","value":"60"},{"label":"68","value":"70"},{"label":"69","value":"80"},{"label":"70","value":"90"}]', 'lanjut'),
(2, 1, 1, '001', '2013-11-18', '', 'tidak lanjut');

-- --------------------------------------------------------

--
-- Table structure for table `ujikom_asesor`
--

CREATE TABLE IF NOT EXISTS `ujikom_asesor` (
  `asesor_id` int(11) NOT NULL AUTO_INCREMENT,
  `ujikom_id` int(11) DEFAULT NULL,
  `nama` char(20) DEFAULT NULL,
  `nip` char(20) DEFAULT NULL,
  `gelar` char(20) DEFAULT NULL,
  `gol` char(10) DEFAULT NULL,
  `tempat_lahir_propinsi` char(10) DEFAULT NULL,
  `tempat_lahir_kota` char(10) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jabatan` char(20) DEFAULT NULL,
  `status` char(20) DEFAULT NULL,
  `instansi` char(100) DEFAULT NULL,
  `alamat_instansi` char(255) DEFAULT NULL,
  `propinsi_instansi` char(10) DEFAULT NULL,
  `kota_instansi` char(10) DEFAULT NULL,
  `telepon_instansi` char(20) DEFAULT NULL,
  `fax_instansi` char(20) DEFAULT NULL,
  `hp_instansi` char(20) DEFAULT NULL,
  `alamat_rumah` char(255) DEFAULT NULL,
  `propinsi_rumah` char(10) DEFAULT NULL,
  `kota_rumah` char(10) DEFAULT NULL,
  `telp_rumah` char(20) DEFAULT NULL,
  `fax_rumah` char(20) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `pengalaman_kerja` text,
  `pendidikan_tertinggi` text,
  `pelatihan_teknik` text,
  `spesialisasi_pelatihan` text,
  `foto` char(255) DEFAULT NULL,
  PRIMARY KEY (`asesor_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ujikom_asesor`
--

INSERT INTO `ujikom_asesor` (`asesor_id`, `ujikom_id`, `nama`, `nip`, `gelar`, `gol`, `tempat_lahir_propinsi`, `tempat_lahir_kota`, `tanggal_lahir`, `jabatan`, `status`, `instansi`, `alamat_instansi`, `propinsi_instansi`, `kota_instansi`, `telepon_instansi`, `fax_instansi`, `hp_instansi`, `alamat_rumah`, `propinsi_rumah`, `kota_rumah`, `telp_rumah`, `fax_rumah`, `email`, `pengalaman_kerja`, `pendidikan_tertinggi`, `pelatihan_teknik`, `spesialisasi_pelatihan`, `foto`) VALUES
(1, NULL, 'Taofik', '123434', 'IR', 'IIIA', '16', '16-001', '2013-10-08', 'Programmer', 'Widyaiswara Kem. PU', 'DINAS KEBERSIHAN', 'JL. H. SANUSI', '51', '51-003', '23432', '3434', '234234', 'JL. SARI INDAH 5E', '32', '32-019', '230430', '34034', 'nietaldarkopik@gmail.com', '4 tahun', 'SMK', 'Pelatihan Drainase', 'Pelatihan Air Minum', '9e8175cedd512d57fc0d8da6332330d0.jpg'),
(2, NULL, 'hg', 'jhgh', 'ghj', 'gjhg', '31', '31-001', '2013-10-09', 'lk', 'Widyaiswara Kem. PU', 'lkjk', 'j', '-', '--001', 'lk', 'dsfsdf', 'jhj', 'hjh', '15', '15-001', 'jjh', 'jhk', 'nietaldarkopik@gmail.com', '4 tahun', '{"lem_dik":["","LEMBAGA","PENDIDIKAN"],"kot_neg":["","KOTA","NEGARA"],"gelar":["","GELAR1","GELAR2"],"special":["","SPESIALISASI1","SPESIALISASI2"]}', '{"ur_nam_kur":["","URAIAN","NAMA","KURSUS"],"kot_neg":["","KOTA\\/NEGARA1","KOTA\\/NEGARA2","KOTA\\/NEGARA3"],"lama":["","LAMA1","LAMA2","LAMA3"]}', '{"mat_pel":["","MATA","PELAJARAN"]}', '37563e3be5ab676556c760beafbb63bf.jpg'),
(3, NULL, 'Taofik Basuki', '10583023', 'IR.', 'IIIA', '-', '--002', '1990-03-31', 'Programmer', 'Widyaiswara Kem. PU', 'Dinas Pemadam Kebakaran', 'Jl. Sari Indah 5E', '12', '12-001', '0220302023', '0223020320', '0857303032', 'Jl. H. Sanusi', '32', '32-019', '022030234', '022030234', 'nietaldarkopik@gmail.com', '4 tahun', '{"lem_dik":["","UNPAD","ITB"],"kot_neg":["","Bandung","Bandung"],"gelar":["","ST","SE"],"special":["","REKAYASA PERANGKAT LUNAK",""]}', '{"ur_nam_kur":["","Bahasa Inggris"],"kot_neg":["","Bandung"],"lama":["","1 tahun"]}', '{"mat_pel":["","Database","Matematika"]}', 'aeca72cb3b705f5061d00e72bd28bcfe.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ujikom_bukti_pendukung`
--

CREATE TABLE IF NOT EXISTS `ujikom_bukti_pendukung` (
  `bukti_pendukung_id` int(11) NOT NULL AUTO_INCREMENT,
  `kompetensi_id` char(30) NOT NULL,
  `nilai` int(11) NOT NULL,
  `assesment` char(20) NOT NULL,
  `ujikom_id` char(30) NOT NULL,
  `jenis_ujikom` char(30) NOT NULL,
  `material_id` int(11) NOT NULL,
  `kode_material` char(30) NOT NULL,
  `judul_material` text NOT NULL,
  `peserta_id` char(30) NOT NULL,
  `accessor_id` char(30) NOT NULL,
  PRIMARY KEY (`bukti_pendukung_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `ujikom_bukti_pendukung`
--

INSERT INTO `ujikom_bukti_pendukung` (`bukti_pendukung_id`, `kompetensi_id`, `nilai`, `assesment`, `ujikom_id`, `jenis_ujikom`, `material_id`, `kode_material`, `judul_material`, `peserta_id`, `accessor_id`) VALUES
(4, 'PAM.MM02.303.01', 10, 'lanjut', '1', '001', 10, '13231', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', '1', '1'),
(5, 'PAM.MM02.303.01', 20, 'tidak lanjut', '1', '001', 11, '13123', 'Surat referensi dari supervisor / perusahaan mengenai pekerjaan anda', '1', '1'),
(6, 'PAM.MM02.303.01', 40, 'lanjut', '1', '001', 12, 'sfdfsf', 'Contoh pekerjaan yang pernah anda buat (produk jadi)', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ujikom_certificates`
--

CREATE TABLE IF NOT EXISTS `ujikom_certificates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_eval_id` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `certificate_type` char(50) NOT NULL,
  `no_certificate` char(255) NOT NULL,
  `avg_nilai` double(11,2) NOT NULL,
  `total` int(11) NOT NULL,
  `predikat` char(20) NOT NULL,
  `ttd_tempat` char(100) NOT NULL,
  `ttd_tanggal` date NOT NULL,
  `ttd_jabatan` text NOT NULL,
  `ttd_nama` char(100) NOT NULL,
  `ttd_nip` char(100) NOT NULL,
  `ttd_data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ujikom_certificates`
--


-- --------------------------------------------------------

--
-- Table structure for table `ujikom_jenis`
--

CREATE TABLE IF NOT EXISTS `ujikom_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_ujikom` char(25) DEFAULT NULL,
  `nama_ujikom` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `ujikom_jenis`
--

INSERT INTO `ujikom_jenis` (`id`, `kode_ujikom`, `nama_ujikom`) VALUES
(14, '007', 'OPERATOR RESERVOIR DAN SISTEM DISTRIBUSI'),
(13, '006', 'OPERATOR SUMUR DALAM'),
(8, '001', 'AHLI MANAJEMEN AIR MINUM TINGKAT UTAMA'),
(9, '002', 'AHLI MANAJEMEN AIR MINUM TINGKAT MADYA'),
(10, '003', 'AHLI MANAJEMEN AIR MINUM TINGKAT MUDA'),
(11, '004', 'OPERATOR UNIT AIR BAKU DAN UNIT PRODUKSI'),
(12, '005', 'OPERATOR BANGUNAN PENANGKAP MATA AIR'),
(15, '008', 'OPERATOR INSTRUMENTASI SISTEM PENYEDIAAN AIR MINUM'),
(16, '009', 'AHLI COMMISSIONING IPA'),
(17, '010', 'PELAKSANA PEMERIKSAAN KUALITAS AIR'),
(18, '011', 'PELAKSANA LAPANGAN DETEKSI KEBOCORAN'),
(19, '012', 'AHLI COMMISSIONING JARINGAN PIPA'),
(20, '013', 'AHLI PENGENDALIAN KEHILANGAN AIR'),
(21, '014', 'PELAKSANA REHABILITASI JARINGAN PIPA');

-- --------------------------------------------------------

--
-- Table structure for table `ujikom_kompetensi`
--

CREATE TABLE IF NOT EXISTS `ujikom_kompetensi` (
  `kompetensi_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kompetensi` char(50) NOT NULL,
  `judul_kompetensi` char(255) NOT NULL,
  `parent_kompetensi` char(25) NOT NULL,
  `jenis_ujikom` char(25) NOT NULL,
  PRIMARY KEY (`kompetensi_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `ujikom_kompetensi`
--

INSERT INTO `ujikom_kompetensi` (`kompetensi_id`, `kode_kompetensi`, `judul_kompetensi`, `parent_kompetensi`, `jenis_ujikom`) VALUES
(7, 'PAM.MM02.303.01', 'Melakukan manajemen Bisnis Air Minum Pengembangan', '', '001'),
(8, 'PAM.MM02.305.01', 'Melaksanakan Manajemen Strategik Corporate', '', '001'),
(9, 'PAM.MM02.307.01', 'Melaksanakan Kepemimpinan Visioner', '', '001'),
(10, 'PAM.MM02.309.01', 'Melaksanakan Sistem Manajemen Mutu', '', '001'),
(11, 'PAM.MM02.213.01', 'Melaksanakan Manajemen Produktivitas SDM', '', '001'),
(12, 'PAM.MM02.202.01', 'Melakukan Manajemen Bisnis Air Minum Penyehatan', '', '002'),
(13, 'PAM.MM02.204.01', 'Melaksanakan Manajemen Strategik Divisi', '', '002'),
(14, 'PAM.MM02.206.01', 'Melaksanakan Kepemimpinan Situasional', '', '002'),
(15, 'PAM.MM02.208.01', 'Melaksanakan Manajemen Mutu', '', '002'),
(16, 'PAM.MM02.211.01', 'Melaksanakan Operasi dan Pemeliharaan Sistem Penyediaan Air Minum Sistem Penyediaan Air Minum', '', '002'),
(17, 'PAM.MM02.112.01', 'Melaksanakan Manajemen Sumber Daya Manusia', '', '002'),
(18, 'PAM.MM02.215.01', 'Melaksanakan Manajemen Aset', '', '002'),
(19, 'PAM.MM02.217.01', 'Melaksanakan Manajemen Keuangan Investasi', '', '002'),
(20, 'PAM.MM02.219.01', 'Melaksanakan Komunikasi Bisnis', '', '002'),
(21, 'PAM.MM01.101.01', 'Menerapkan Sistem Manajemen Kesehatan dan Keselamatan kerja', '', '003'),
(22, 'PAM.MM01.102.01', 'Melaksanakan Manajemen Umum', '', '003'),
(23, 'PAM.MM01.103.01', 'Melaksanakan Kepemimpinan Dasar', '', '003'),
(24, 'PAM.MM02.101.01', 'Melakukan Manajemen Bisnis Air Minum', '', '003'),
(25, 'PAM.MM02.110.01', 'Melaksanakan Manajemen Operasi dan Pemeliharaan Sistem Penyediaan Air Minum', '', '003'),
(26, 'PAM.MM02.114.01', 'Melaksanakan Manajemen Barang', '', '003'),
(27, 'PAM.MM02.116.01', 'Melaksanakan Manajemen Keuangan dan Akuntansi', '', '003'),
(28, 'PAM.MM02.118.01', 'Melakukan komunikasi', '', '003'),
(29, 'PAM.MM02.120.01', 'Melaksanakan Manajemen Informasi', '', '003'),
(30, 'PAM MO01.001.01', 'Menerapkan proses pengolahan air minum', '', '004'),
(31, 'PAM MO01.002.01', 'Menerapkan prinsip Kuantitas-kualitas dan kontinuitas', '', '004'),
(32, 'PAM MO01.003.01', 'Melaksanakan kesehatan dan keselamatan kerja', '', '004'),
(33, 'PAM MO02.001.01', 'Mengoperasikan dan memelihara unit air baku dan pengendapan pendahuluan', '', '004'),
(34, 'PAM MO02.002.01', 'Mengoperasikan Dan Memelihara Unit Pengaduk Cepat', '', '004'),
(35, 'PAM MO02.003.01', 'Mengoperasikan Dan Memelihara Sarana Pencampur Kimia', '', '004'),
(36, 'PAM MO02.004.01', 'Mengoperasikan Dan Memelihara Pompa Pembubuh', '', '004'),
(37, 'PAM MO02.005.01', 'Mengoperasikan Dan Memelihara Unit Pengaduk Lambat', '', '004'),
(38, 'PAM MO02.006.01', 'Mengoperasikan Dan Memelihara Unit Pengendapan', '', '004'),
(39, 'PAM MO02.007.01', 'Mengoperasikan Dan Memelihara Unit Penyaring', '', '004'),
(40, 'PAM MO02.010.01', 'Mengoperasikan dan Memelihara Meter Induk', '', '004'),
(41, 'PAM MO02.011.01', 'Mengoperasikan dan Memelihara Bangunan Pengambilan Air Baku, Air Permukaan (Water Intake)', '', '004'),
(42, 'PAM MO02.013.01', 'Mengoperasikan Dan Memelihara Waduk Tempat Penyimpanan Sementara Air Baku (Impounding Reservoir)', '', '004'),
(43, 'PAM MO02.014.01', 'Melakukan pemeliharaan perpipaan pada instalasi pengolahan air minum', '', '004'),
(44, 'PAM MO02.015.01', 'Mencari Dan Memperbaiki Kebocoran Pipa', '', '004'),
(45, 'PAM MO02.016.01', 'Memeriksa, Memelihara Dan Memperbaiki Katup-Katup Udara, Meter Dan Alat-Alat Perlengkapan Lainnya', '', '004'),
(46, 'PAM MO02.019.01', 'Mengoperasikan dan memelihara peralatan mesin / Pompa distribusi', '', '004'),
(47, 'PAM.MM02.303.01.01', 'Melakukan perencanaan pengembangan usaha', 'PAM.MM02.303.01', '001'),
(48, 'PAM.MM02.303.02', 'Melakukan penggalian sumber pembiayaan investasi', 'PAM.MM02.303.01', '001'),
(49, 'PAM.MM02.303.03', 'Melaksanakan strategi kemitraan', 'PAM.MM02.303.01', '001'),
(50, 'PAM.MM02.303.04', 'Melaksanakan kemitraan dengan badan usaha', 'PAM.MM02.303.01', '001'),
(51, 'PAM.MM02.303.01.05', 'Melaksanakan pengelolaan resiko', 'PAM.MM02.303.01', '001'),
(52, 'PAM.MM02.305.01.01', 'Merumuskan Visi dan Misi perusahaan', 'PAM.MM02.305.01', '001'),
(53, 'PAM.MM02.305.01.02', 'Melakukan analisis SWOT', 'PAM.MM02.305.01', '001'),
(54, 'PAM.MM02.305.01.03', 'Menetapkan Tujuan dan Sasaran', 'PAM.MM02.305.01', '001'),
(55, 'PAM.MM02.305.01.04', 'Menerapkan Strategi', 'PAM.MM02.305.01', '001'),
(56, 'PAM.MM02.305.01.05', 'Menyusun Program, Kegiatan Anggaran dan Proyeksi Keuangan', 'PAM.MM02.305.01', '001'),
(57, 'PAM.MM02.307.01.01', 'Menerapkan kepemimpinan visioner', 'PAM.MM02.307.01', '001'),
(58, 'PAM.MM02.307.01.02', 'Melaksanakan perubahan internal', 'PAM.MM02.307.01', '001'),
(59, 'PAM.MM02.307.01.03', 'Melaksanakan pembinaan berbasis kinerja', 'PAM.MM02.307.01', '001'),
(60, 'PAM.MM02.307.01.04', 'Melaksanakan evaluasi efektivitas', 'PAM.MM02.307.01', '001'),
(61, 'PAM.MM02.309.01.01', 'Melakukan perencanaan manajemen mutu', 'PAM.MM02.309.01', '001'),
(62, 'PAM.MM02.309.01.02', 'Menerapkan sistem manajemen mutu', 'PAM.MM02.309.01', '001'),
(63, 'PAM.MM02.309.01.03', 'Melakukan pengendalian mutu', 'PAM.MM02.309.01', '001'),
(64, 'PAM.MM02.309.01.04', 'Melakukan perbaikan mutu', 'PAM.MM02.309.01', '001'),
(65, 'PAM.MM02.213.01.01', 'Menganalisis kebutuhan', 'PAM.MM02.213.01', '001'),
(66, 'PAM.MM02.213.01.02', 'Melaksanakan Rekruitment', 'PAM.MM02.213.01', '001'),
(67, 'PAM.MM02.213.01.03', 'Melaksanakan penempatan SDM', 'PAM.MM02.213.01', '001'),
(68, 'PAM.MM02.213.01.04', 'Menyusun penggajian', 'PAM.MM02.213.01', '001'),
(69, 'PAM.MM02.213.01.05', 'Melaksanakan pembinaan', 'PAM.MM02.213.01', '001'),
(70, 'PAM.MM02.213.01.06', 'Merumuskan jaminan sosial', 'PAM.MM02.213.01', '001');

-- --------------------------------------------------------

--
-- Table structure for table `ujikom_material`
--

CREATE TABLE IF NOT EXISTS `ujikom_material` (
  `ujikom_material` int(11) NOT NULL AUTO_INCREMENT,
  `ujikom_jenis_id` char(20) NOT NULL,
  `kompetensi_id` char(25) NOT NULL,
  `element_kompetensi` char(25) NOT NULL,
  `kode_material` char(20) NOT NULL,
  `judul_material` char(255) NOT NULL,
  `type` char(15) NOT NULL DEFAULT 'asesor',
  PRIMARY KEY (`ujikom_material`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `ujikom_material`
--

INSERT INTO `ujikom_material` (`ujikom_material`, `ujikom_jenis_id`, `kompetensi_id`, `element_kompetensi`, `kode_material`, `judul_material`, `type`) VALUES
(3, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.01.01', 'PAM.MM02.303.01.01', 'Apakah anda mampu menganalisis peluang pengembangan melalui teori pendekatan yang tepat dan melalui survei permintaan ?', 'asesor'),
(4, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.01.01', '1.2', 'Apakah anda mampu melakukan studi kela-yakan dengan menganalisis kelayakan ekono-mi dan finansial serta kemampuan manajerial?', 'asesor'),
(5, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.01.01', '1.3', 'Apakah anda mampu menyampaikan proposal pengembangan ke pemilik dilampiri dengan hasil studi kelayakan untuk memperoleh persetujuan ?', 'asesor'),
(6, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.04', '2.1', 'Apakah anda mampu melakukan komunikasi bisnis untuk penggalian sumber pembiayaan sesuai kewenangan dan prosedur yang ber-laku ?', 'asesor'),
(7, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.02', '2.2', 'Apakah anda mampu menjelaskan pemben-tukan tim negosiator dengan mengikutkan unsur lain dalam perusahaan sesuai dengan lingkup atau cakupan yang akan dinegosia-sikan ?', 'asesor'),
(8, '001', 'PAM.MM02.305.01', 'PAM.MM02.305.01.01', '2.3', 'Apakah anda mampu memperhitungkan peni-laian peluang dan tingkat biaya melalui anali-sis bisnis yang akurat ?', 'asesor'),
(9, '001', 'PAM.MM02.305.01', 'PAM.MM02.305.01.01', '2.4', 'Apakah anda mampu menyampaikan laporan kepada pemilik untuk pengambilan keputusan sesuai dengan prosedur dan dilengkapi dengan masukan untuk pertimbangan ?', 'asesor'),
(10, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.03', '3.1', 'Apakah anda mampu menjelaskan hal yang perlu dilakukan dalam kerjasama penyediaan air baku untuk menjamin rencana pengem-bangan melalui prosedur yang berlaku ?', 'asesor'),
(11, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.03', '3.2', 'Apakah anda mampu melakukan pening-katan kompetensi karyawan dan manajemen melalui strategi yang tepat sesuai dengan kemampuan perusahaan dan tingkat kemen-desakan ?', 'asesor'),
(12, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.03', '3.3', 'Apakah anda mampu merencanakan kerja-sama dengan PDAM maju, khususnya dalam transfer pengalaman ?', 'asesor'),
(13, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.04', '4.1', 'Apakah anda mampu mengkaji kesesuaian bentuk kemitraan dengan kebutuhan pengem-bangan usaha, dengan memperhatikan keku-atan serta kelemahan yang dimiliki perusaha-an saat ini ?', 'asesor'),
(14, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.04', '4.2', 'Apakah anda mampu melakukan prosedur penyelenggaraan kemitraan dengan badan usaha sesuai dengan peraturan perundangan yang berlaku, untuk menjamin manfaat ke-pada semua pihak: masyarakat, pemerintah dan badan usaha ?', 'asesor'),
(15, '001', 'PAM.MM02.303.01', 'PAM.MM02.303.04', '4.3', 'Apakah anda mampu menjaga proses solicited, untuk diterapkan sesuai dengan ketetapan yang berlaku ?', 'asesor');

-- --------------------------------------------------------

--
-- Table structure for table `ujikom_material_support`
--

CREATE TABLE IF NOT EXISTS `ujikom_material_support` (
  `ujikom_material` int(11) NOT NULL AUTO_INCREMENT,
  `ujikom_jenis_id` char(20) NOT NULL,
  `kompetensi_id` char(25) NOT NULL,
  `kode_material` char(20) NOT NULL,
  `judul_material` char(255) NOT NULL,
  `type` char(15) NOT NULL DEFAULT 'asesor',
  PRIMARY KEY (`ujikom_material`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `ujikom_material_support`
--

INSERT INTO `ujikom_material_support` (`ujikom_material`, `ujikom_jenis_id`, `kompetensi_id`, `kode_material`, `judul_material`, `type`) VALUES
(10, '001', 'PAM.MM02.303.01', '13231', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', 'asesor'),
(11, '001', 'PAM.MM02.303.01', '13123', 'Surat referensi dari supervisor / perusahaan mengenai pekerjaan anda', 'asesor'),
(12, '001', 'PAM.MM02.303.01', 'sfdfsf', 'Contoh pekerjaan yang pernah anda buat (produk jadi)', 'asesor'),
(13, '001', 'PAM.MM02.303.01', 'sfsdfsdf', '‘Job description’ dari perusahaan mengenai pekerjaan anda', 'asesor'),
(14, '001', 'PAM.MM02.303.01', '13231321', 'Wawancara dengan supervisor, teman sebaya atau klien', 'asesor'),
(15, '001', 'PAM.MM02.303.01', 'fdfdsf', 'Demonstrasi pekerjaan / keterampilan yang dipersyaratkan', 'asesor'),
(16, '001', 'PAM.MM02.303.01', 'dsadsadsad', 'Pengalaman Industri (on the job training, magang, kerja praktek, dll)', 'asesor'),
(17, '001', 'PAM.MM02.303.01', 'asdasdasd', 'Bukti-bukti lainnya yang relevan', 'asesor'),
(18, '001', 'PAM.MM02.305.01', 'erererw', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', 'asesor'),
(19, '001', 'PAM.MM02.305.01', 'dasdsd', 'Surat referensi dari supervisor / perusahaan mengenai pekerjaan anda', 'asesor'),
(20, '001', 'PAM.MM02.305.01', 'dasdsadas', 'Contoh pekerjaan yang pernah anda buat (produk jadi)', 'asesor'),
(21, '001', 'PAM.MM02.305.01', 'sdfdsfdsf', '‘Job description’ dari perusahaan mengenai pekerjaan anda', 'asesor'),
(22, '001', 'PAM.MM02.305.01', 'adasdsad', 'Wawancara dengan supervisor, teman sebaya atau klien', 'asesor'),
(23, '001', 'PAM.MM02.305.01', 'asds dsda', 'Demonstrasi pekerjaan / keterampilan yang dipersyaratkan', 'asesor'),
(24, '001', 'PAM.MM02.305.01', 'sadsdsa', 'Pengalaman Industri (on the job training, magang, kerja praktek, dll)', 'asesor'),
(25, '001', 'PAM.MM02.305.01', '2132321', 'Bukti-bukti lainnya yang relevan', 'asesor'),
(26, '001', 'PAM.MM02.307.01', '213ewqewq', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', 'asesor'),
(27, '001', 'PAM.MM02.307.01', 'dassad', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', 'asesor'),
(28, '001', 'PAM.MM02.307.01', 'sadsadas', 'Surat referensi dari supervisor / perusahaan mengenai pekerjaan anda', 'asesor'),
(29, '001', 'PAM.MM02.307.01', 'adasdsad', 'Contoh pekerjaan yang pernah anda buat (produk jadi)', 'asesor'),
(30, '001', 'PAM.MM02.307.01', 'asdsad', '‘Job description’ dari perusahaan mengenai pekerjaan anda', 'asesor'),
(31, '001', 'PAM.MM02.307.01', 'dsadasd', 'Wawancara dengan supervisor, teman sebaya atau klien', 'asesor'),
(32, '001', 'PAM.MM02.307.01', 'adasdas', 'Demonstrasi pekerjaan / keterampilan yang dipersyaratkan', 'asesor'),
(33, '001', 'PAM.MM02.307.01', 'sdsadsad', 'Pengalaman Industri (on the job training, magang, kerja praktek, dll)', 'asesor'),
(34, '001', 'PAM.MM02.307.01', 'wadsdasd', 'Bukti-bukti lainnya yang relevan', 'asesor'),
(35, '001', 'PAM.MM02.309.01', 'sadsadasd', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', 'asesor'),
(36, '001', 'PAM.MM02.309.01', 'dfdsfsdf', 'Surat referensi dari supervisor / perusahaan mengenai pekerjaan anda', 'asesor'),
(37, '001', 'PAM.MM02.309.01', 'dsadsadas', 'Contoh pekerjaan yang pernah anda buat (produk jadi)', 'asesor'),
(38, '001', 'PAM.MM02.309.01', 'asdsadasd', '‘Job description’ dari perusahaan mengenai pekerjaan anda', 'asesor'),
(39, '001', 'PAM.MM02.309.01', 'sdsadsadasd', 'Wawancara dengan supervisor, teman sebaya atau klien', 'asesor'),
(40, '001', 'PAM.MM02.309.01', 'sdasds', 'Demonstrasi pekerjaan / keterampilan yang dipersyaratkan', 'asesor'),
(41, '001', 'PAM.MM02.309.01', 'dsadasd', 'Pengalaman Industri (on the job training, magang, kerja praktek, dll)', 'asesor'),
(42, '001', 'PAM.MM02.309.01', 'asdssd', 'Bukti-bukti lainnya yang relevan', 'asesor'),
(43, '001', 'PAM.MM02.213.01', 'asdsdsa', 'Sertifikat atau kualifikasi (contoh : pelatihan, keakhlian)', 'asesor'),
(44, '001', 'PAM.MM02.213.01', 'sdasdasd', 'Surat referensi dari supervisor / perusahaan mengenai pekerjaan anda', 'asesor'),
(45, '001', 'PAM.MM02.213.01', 'asdsadasd', 'Contoh pekerjaan yang pernah anda buat (produk jadi)', 'asesor'),
(46, '001', 'PAM.MM02.213.01', 'adsdasdsd', '‘Job description’ dari perusahaan mengenai pekerjaan anda', 'asesor'),
(47, '001', 'PAM.MM02.213.01', 'asdasds', 'Wawancara dengan supervisor, teman sebaya atau klien', 'asesor'),
(48, '001', 'PAM.MM02.213.01', 'sadsadsa', 'Demonstrasi pekerjaan / keterampilan yang dipersyaratkan', 'asesor'),
(49, '001', 'PAM.MM02.213.01', 'asdsad', 'Pengalaman Industri (on the job training, magang, kerja praktek, dll)', 'asesor'),
(50, '001', 'PAM.MM02.213.01', 'asdsadasd', 'Bukti-bukti lainnya yang relevan', 'asesor');

-- --------------------------------------------------------

--
-- Table structure for table `ujikom_nilai_mandiri`
--

CREATE TABLE IF NOT EXISTS `ujikom_nilai_mandiri` (
  `nilai_mandiri_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_ujikom` char(30) NOT NULL,
  `kompetensi_id` char(30) NOT NULL,
  `material_id` char(30) NOT NULL,
  `kode_material` char(50) NOT NULL,
  `judul_material` text NOT NULL,
  `peserta_id` char(30) NOT NULL,
  `accessor_id` char(30) NOT NULL,
  `nilai` int(11) NOT NULL,
  `ujikom_id` char(30) NOT NULL,
  `bukti` text NOT NULL,
  PRIMARY KEY (`nilai_mandiri_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `ujikom_nilai_mandiri`
--

INSERT INTO `ujikom_nilai_mandiri` (`nilai_mandiri_id`, `jenis_ujikom`, `kompetensi_id`, `material_id`, `kode_material`, `judul_material`, `peserta_id`, `accessor_id`, `nilai`, `ujikom_id`, `bukti`) VALUES
(7, '001', 'PAM.MM02.303.01', '3', 'PAM.MM02.303.01.01', 'Apakah anda mampu menganalisis peluang pengembanga', '1', '1', 40, '1', 'd'),
(8, '001', 'PAM.MM02.303.01', '4', '1.2', 'Apakah anda mampu melakukan studi kela-yakan denga', '1', '1', 20, '1', 'e'),
(9, '001', 'PAM.MM02.303.01', '5', '1.3', 'Apakah anda mampu menyampaikan proposal pengembang', '1', '1', 10, '1', 'f');

-- --------------------------------------------------------

--
-- Table structure for table `ujikom_peserta`
--

CREATE TABLE IF NOT EXISTS `ujikom_peserta` (
  `peserta_ujikom_id` int(11) NOT NULL AUTO_INCREMENT,
  `ujikom_id` int(11) DEFAULT NULL,
  `lembaga` int(11) NOT NULL,
  `nama` char(30) DEFAULT NULL,
  `tempat_lahir` char(30) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `unit_kerja` char(30) DEFAULT NULL,
  `pendidikan_terakhir` char(10) DEFAULT NULL,
  `jurusan` char(10) DEFAULT NULL,
  `status_kepegawaian` char(10) DEFAULT NULL,
  `nip` char(15) DEFAULT NULL,
  `no_ktp` char(50) DEFAULT NULL,
  `jabatan` char(10) DEFAULT NULL,
  `pangkat` char(10) DEFAULT NULL,
  `golongan` char(10) DEFAULT NULL,
  `bidang_pekerjaan` char(100) DEFAULT NULL,
  `masa_kerja` int(15) DEFAULT NULL,
  `mulai_tahun` int(15) DEFAULT NULL,
  `alamat_kantor` char(255) DEFAULT NULL,
  `alamat_rumah` char(255) DEFAULT NULL,
  `telepon_kantor` char(20) DEFAULT NULL,
  `telepon_rumah` char(20) DEFAULT NULL,
  `hp` char(20) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `bank` char(10) DEFAULT NULL,
  `no_rekening` char(20) DEFAULT NULL,
  `foto` char(255) DEFAULT NULL,
  `tanggal_registrasi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_registrasi` char(15) DEFAULT NULL,
  PRIMARY KEY (`peserta_ujikom_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ujikom_peserta`
--

INSERT INTO `ujikom_peserta` (`peserta_ujikom_id`, `ujikom_id`, `lembaga`, `nama`, `tempat_lahir`, `tanggal_lahir`, `unit_kerja`, `pendidikan_terakhir`, `jurusan`, `status_kepegawaian`, `nip`, `no_ktp`, `jabatan`, `pangkat`, `golongan`, `bidang_pekerjaan`, `masa_kerja`, `mulai_tahun`, `alamat_kantor`, `alamat_rumah`, `telepon_kantor`, `telepon_rumah`, `hp`, `email`, `bank`, `no_rekening`, `foto`, `tanggal_registrasi`, `status_registrasi`) VALUES
(1, NULL, 17, 'Taofik Basuki', 'Bandung', '1990-03-31', 'SMK INFORMATIKA', 'SMK', 'RPL', 'tetap', '30230230', '32030230', 'PROGRAMMER', 'KOMANDAN', 'IIIA', 'IT', 4, 2008, 'JL. H. SANUSI', 'JL. SARI INDAH 5E', '02203020302', '03003040303', '0302830403', 'NIETALDARKOPIK@GMAIL.COM', 'BCA', '28002030203', 'd52522db21438333b6402d5bafb24ecf.jpg', '2013-10-08 00:00:00', 'belum'),
(2, NULL, 16, 'Opik Uki', 'Bandung', '1990-03-31', 'Unit Kerja', 'SMK', 'RPL', 'tetap', '1050178', '131312034503043', 'Programmer', 'IIIA', 'Prajamuda', 'Informasi technology', 4, 2008, 'Jl. H. Sanusi', 'Jl. Sari Indah 5E', '022030403', '02302034', '08567392032', 'nietaldarkopik@gmail.com', 'BCA', '2008302032', '44201ab2e0834a095cb70c2875d7f354.jpg', '2013-10-15 00:00:00', 'sudah');

-- --------------------------------------------------------

--
-- Table structure for table `unsur_penilaian_instruktur`
--

CREATE TABLE IF NOT EXISTS `unsur_penilaian_instruktur` (
  `id_unsur_penilaian` int(11) NOT NULL AUTO_INCREMENT,
  `unsur_penilaian` char(255) DEFAULT NULL,
  PRIMARY KEY (`id_unsur_penilaian`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `unsur_penilaian_instruktur`
--

INSERT INTO `unsur_penilaian_instruktur` (`id_unsur_penilaian`, `unsur_penilaian`) VALUES
(1, 'Penguasaan materi'),
(2, 'Ketepatan Waktu'),
(3, 'Sistematika Penyajian'),
(4, 'penggunaan metode & alat bantu'),
(5, 'Daya Simpati, Gaya & Sikap'),
(6, 'Penggunaan Bahasa & Volume Suara'),
(7, 'Pemberian Motivasi'),
(8, 'Pencapaian Tujuan Instruksional'),
(9, 'Kesempatan Tanya Jawab'),
(10, 'Kepuasan Terhadap Jawaban Pengajar');

-- --------------------------------------------------------

--
-- Table structure for table `unsur_penilaian_materi`
--

CREATE TABLE IF NOT EXISTS `unsur_penilaian_materi` (
  `id_unsur_penilaian` int(11) NOT NULL AUTO_INCREMENT,
  `unsur_penilaian` char(255) DEFAULT NULL,
  PRIMARY KEY (`id_unsur_penilaian`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `unsur_penilaian_materi`
--

INSERT INTO `unsur_penilaian_materi` (`id_unsur_penilaian`, `unsur_penilaian`) VALUES
(1, 'Keterkaitan Materi dengan tugas'),
(2, 'Tingkat Manfaat Materi'),
(5, 'Tingkat Kesulitan Materi');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` char(100) DEFAULT NULL,
  `password` char(255) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `user_level_id` int(11) DEFAULT NULL,
  `user_type` char(20) DEFAULT NULL,
  `status` char(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `password`, `email`, `user_level_id`, `user_type`, `status`) VALUES
(1, 'administrator', '200ceb26807d6bf99fd6f4f0d1ca54d4', 'nietaldarkopik@gmail.com', 1, '', 'active'),
(36, 'operator', '4b583376b2767b923c3e1da60d10de59', 'nietal_dark_opik@yahoo.co.id', 2, 'lembaga', 'active'),
(38, 'meilan.amaliah@gmail.com', '6cc0c76a13e46ef729a7c647138c702f', 'meilan.amaliah@gmail.com', 3, 'lembaga', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `user_levels`
--

CREATE TABLE IF NOT EXISTS `user_levels` (
  `user_level_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_level_name` char(50) DEFAULT NULL,
  PRIMARY KEY (`user_level_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_levels`
--

INSERT INTO `user_levels` (`user_level_id`, `user_level_name`) VALUES
(1, 'administrator'),
(2, 'operator'),
(3, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `user_level_roles`
--

CREATE TABLE IF NOT EXISTS `user_level_roles` (
  `user_level_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_level_id` int(11) DEFAULT NULL,
  `user_role_name` char(50) DEFAULT NULL,
  `controller` char(255) DEFAULT NULL,
  `function` char(255) DEFAULT NULL,
  PRIMARY KEY (`user_level_role_id`),
  KEY `user_role_name` (`user_role_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12258 ;

--
-- Dumping data for table `user_level_roles`
--

INSERT INTO `user_level_roles` (`user_level_role_id`, `user_level_id`, `user_role_name`, `controller`, `function`) VALUES
(12239, 1, 'user_level_view', 'user_level', 'view'),
(12238, 1, 'user_level_add', 'user_level', 'add'),
(12237, 1, 'user_level_edit', 'user_level', 'edit'),
(12236, 1, 'user_level_delete', 'user_level', 'delete'),
(12235, 1, 'user_level_index', 'user_level', 'index'),
(12234, 1, 'ukom_type_listing', 'ukom_type', 'listing'),
(12233, 1, 'ukom_type_unit_kompetensi', 'ukom_type', 'unit_kompetensi'),
(12232, 1, 'ukom_type_bukti_pendukung', 'ukom_type', 'bukti_pendukung'),
(12231, 1, 'ukom_type_daftar_pertanyaan', 'ukom_type', 'daftar_pertanyaan'),
(12230, 1, 'ukom_type_element_kompetensi', 'ukom_type', 'element_kompetensi'),
(12229, 1, 'ukom_type_view', 'ukom_type', 'view'),
(12228, 1, 'ukom_type_add', 'ukom_type', 'add'),
(12227, 1, 'ukom_type_edit', 'ukom_type', 'edit'),
(12226, 1, 'ukom_type_delete', 'ukom_type', 'delete'),
(12225, 1, 'ukom_type_index', 'ukom_type', 'index'),
(12224, 1, 'ukom_results_grafic', 'ukom_results', 'grafic'),
(12223, 1, 'ukom_results_citation', 'ukom_results', 'citation'),
(12222, 1, 'ukom_results_certificate', 'ukom_results', 'certificate'),
(12221, 1, 'ukom_results_report', 'ukom_results', 'report'),
(12220, 1, 'ukom_results_penilaian_mandiri', 'ukom_results', 'penilaian_mandiri'),
(12219, 1, 'ukom_results_penilaian', 'ukom_results', 'penilaian'),
(12218, 1, 'ukom_results_hasil_penilaian_mandiri', 'ukom_results', 'hasil_penilaian_mandiri'),
(12217, 1, 'ukom_results_hasil', 'ukom_results', 'hasil'),
(12216, 1, 'ukom_results_listing', 'ukom_results', 'listing'),
(12215, 1, 'ukom_results_view', 'ukom_results', 'view'),
(12214, 1, 'ukom_results_add', 'ukom_results', 'add'),
(12213, 1, 'ukom_results_edit', 'ukom_results', 'edit'),
(12212, 1, 'ukom_results_delete', 'ukom_results', 'delete'),
(12211, 1, 'ukom_results_index', 'ukom_results', 'index'),
(12210, 1, 'ukom_member_report', 'ukom_member', 'report'),
(12209, 1, 'ukom_member_listing', 'ukom_member', 'listing'),
(12208, 1, 'ukom_member_view', 'ukom_member', 'view'),
(12207, 1, 'ukom_member_register', 'ukom_member', 'register'),
(12206, 1, 'ukom_member_add', 'ukom_member', 'add'),
(12205, 1, 'ukom_member_edit', 'ukom_member', 'edit'),
(12204, 1, 'ukom_member_delete', 'ukom_member', 'delete'),
(12203, 1, 'ukom_member_index', 'ukom_member', 'index'),
(12202, 1, 'ukom_element_aspects_evaluation_unit_kompetensi', 'ukom_element_aspects_evaluation', 'unit_kompetensi'),
(12201, 1, 'ukom_element_aspects_evaluation_bukti_pendukung', 'ukom_element_aspects_evaluation', 'bukti_pendukung'),
(12200, 1, 'ukom_element_aspects_evaluation_daftar_pertanyaan', 'ukom_element_aspects_evaluation', 'daftar_pertanyaan'),
(12199, 1, 'ukom_element_aspects_evaluation_element_kompetensi', 'ukom_element_aspects_evaluation', 'element_kompetensi'),
(12198, 1, 'ukom_element_aspects_evaluation_listing', 'ukom_element_aspects_evaluation', 'listing'),
(12197, 1, 'ukom_element_aspects_evaluation_view', 'ukom_element_aspects_evaluation', 'view'),
(12196, 1, 'ukom_element_aspects_evaluation_add', 'ukom_element_aspects_evaluation', 'add'),
(12195, 1, 'ukom_element_aspects_evaluation_edit', 'ukom_element_aspects_evaluation', 'edit'),
(12194, 1, 'ukom_element_aspects_evaluation_delete', 'ukom_element_aspects_evaluation', 'delete'),
(12193, 1, 'ukom_element_aspects_evaluation_index', 'ukom_element_aspects_evaluation', 'index'),
(12192, 1, 'ukom_certificate_unsur_penilaian', 'ukom_certificate', 'unsur_penilaian'),
(12191, 1, 'ukom_certificate_report', 'ukom_certificate', 'report'),
(12190, 1, 'ukom_certificate_grafic', 'ukom_certificate', 'grafic'),
(12189, 1, 'ukom_certificate_citation', 'ukom_certificate', 'citation'),
(12188, 1, 'ukom_certificate_listing', 'ukom_certificate', 'listing'),
(12187, 1, 'ukom_certificate_view', 'ukom_certificate', 'view'),
(12186, 1, 'ukom_certificate_add', 'ukom_certificate', 'add'),
(12185, 1, 'ukom_certificate_edit', 'ukom_certificate', 'edit'),
(12184, 1, 'ukom_certificate_index', 'ukom_certificate', 'index'),
(12183, 1, 'ukom_assesor_report', 'ukom_assesor', 'report'),
(12182, 1, 'ukom_assesor_listing', 'ukom_assesor', 'listing'),
(12181, 1, 'ukom_assesor_view', 'ukom_assesor', 'view'),
(12180, 1, 'ukom_assesor_add', 'ukom_assesor', 'add'),
(12179, 1, 'ukom_assesor_edit', 'ukom_assesor', 'edit'),
(12178, 1, 'ukom_assesor_delete', 'ukom_assesor', 'delete'),
(12177, 1, 'ukom_assesor_index', 'ukom_assesor', 'index'),
(12176, 1, 'ukom_aspects_evaluation_support_penilaian', 'ukom_aspects_evaluation_support', 'penilaian'),
(12175, 1, 'ukom_aspects_evaluation_support_unit_kompetensi', 'ukom_aspects_evaluation_support', 'unit_kompetensi'),
(12174, 1, 'ukom_aspects_evaluation_support_bukti_pendukung', 'ukom_aspects_evaluation_support', 'bukti_pendukung'),
(12173, 1, 'ukom_aspects_evaluation_support_daftar_pertanyaan', 'ukom_aspects_evaluation_support', 'daftar_pertanyaan'),
(12172, 1, 'ukom_aspects_evaluation_support_element_kompetensi', 'ukom_aspects_evaluation_support', 'element_kompetensi'),
(12171, 1, 'ukom_aspects_evaluation_support_listing', 'ukom_aspects_evaluation_support', 'listing'),
(12170, 1, 'ukom_aspects_evaluation_support_view', 'ukom_aspects_evaluation_support', 'view'),
(12169, 1, 'ukom_aspects_evaluation_support_add', 'ukom_aspects_evaluation_support', 'add'),
(12168, 1, 'ukom_aspects_evaluation_support_edit', 'ukom_aspects_evaluation_support', 'edit'),
(12167, 1, 'ukom_aspects_evaluation_support_delete', 'ukom_aspects_evaluation_support', 'delete'),
(12166, 1, 'ukom_aspects_evaluation_support_index', 'ukom_aspects_evaluation_support', 'index'),
(12165, 1, 'ukom_aspects_evaluation_question_penilaian_mandiri', 'ukom_aspects_evaluation_question', 'penilaian_mandiri'),
(12164, 1, 'ukom_aspects_evaluation_question_unit_kompetensi', 'ukom_aspects_evaluation_question', 'unit_kompetensi'),
(12163, 1, 'ukom_aspects_evaluation_question_bukti_pendukung', 'ukom_aspects_evaluation_question', 'bukti_pendukung'),
(12162, 1, 'ukom_aspects_evaluation_question_daftar_pertanyaan', 'ukom_aspects_evaluation_question', 'daftar_pertanyaan'),
(12161, 1, 'ukom_aspects_evaluation_question_element_kompetens', 'ukom_aspects_evaluation_question', 'element_kompetensi'),
(12160, 1, 'ukom_aspects_evaluation_question_listing', 'ukom_aspects_evaluation_question', 'listing'),
(12159, 1, 'ukom_aspects_evaluation_question_view', 'ukom_aspects_evaluation_question', 'view'),
(12158, 1, 'ukom_aspects_evaluation_question_add', 'ukom_aspects_evaluation_question', 'add'),
(12157, 1, 'ukom_aspects_evaluation_question_edit', 'ukom_aspects_evaluation_question', 'edit'),
(12156, 1, 'ukom_aspects_evaluation_question_delete', 'ukom_aspects_evaluation_question', 'delete'),
(12155, 1, 'ukom_aspects_evaluation_question_index', 'ukom_aspects_evaluation_question', 'index'),
(12154, 1, 'ukom_aspects_evaluation_unit_kompetensi', 'ukom_aspects_evaluation', 'unit_kompetensi'),
(12153, 1, 'ukom_aspects_evaluation_bukti_pendukung', 'ukom_aspects_evaluation', 'bukti_pendukung'),
(12152, 1, 'ukom_aspects_evaluation_daftar_pertanyaan', 'ukom_aspects_evaluation', 'daftar_pertanyaan'),
(12151, 1, 'ukom_aspects_evaluation_element_kompetensi', 'ukom_aspects_evaluation', 'element_kompetensi'),
(12150, 1, 'ukom_aspects_evaluation_listing', 'ukom_aspects_evaluation', 'listing'),
(12149, 1, 'ukom_aspects_evaluation_view', 'ukom_aspects_evaluation', 'view'),
(12148, 1, 'ukom_aspects_evaluation_add', 'ukom_aspects_evaluation', 'add'),
(12147, 1, 'ukom_aspects_evaluation_edit', 'ukom_aspects_evaluation', 'edit'),
(12146, 1, 'ukom_aspects_evaluation_delete', 'ukom_aspects_evaluation', 'delete'),
(12145, 1, 'ukom_aspects_evaluation_index', 'ukom_aspects_evaluation', 'index'),
(12144, 1, 'ukom_listing', 'ukom', 'listing'),
(12143, 1, 'ukom_view', 'ukom', 'view'),
(12142, 1, 'ukom_add', 'ukom', 'add'),
(12141, 1, 'ukom_edit', 'ukom', 'edit'),
(12140, 1, 'ukom_delete', 'ukom', 'delete'),
(12139, 1, 'ukom_index', 'ukom', 'index'),
(12138, 1, 'test_chart', 'test', 'chart'),
(12137, 1, 'test_index', 'test', 'index'),
(12136, 1, 'setting_certificate_listing', 'setting_certificate', 'listing'),
(12135, 1, 'setting_certificate_view', 'setting_certificate', 'view'),
(12134, 1, 'setting_certificate_add', 'setting_certificate', 'add'),
(12133, 1, 'setting_certificate_edit', 'setting_certificate', 'edit'),
(12132, 1, 'setting_certificate_delete', 'setting_certificate', 'delete'),
(12131, 1, 'setting_certificate_index', 'setting_certificate', 'index'),
(12130, 1, 'recycle_bin_listing', 'recycle_bin', 'listing'),
(12129, 1, 'recycle_bin_view', 'recycle_bin', 'view'),
(12128, 1, 'recycle_bin_add', 'recycle_bin', 'add'),
(12127, 1, 'recycle_bin_edit', 'recycle_bin', 'edit'),
(12126, 1, 'recycle_bin_delete', 'recycle_bin', 'delete'),
(12125, 1, 'recycle_bin_index', 'recycle_bin', 'index'),
(12124, 1, 'province_listing', 'province', 'listing'),
(12123, 1, 'province_view', 'province', 'view'),
(12122, 1, 'province_add', 'province', 'add'),
(12121, 1, 'province_edit', 'province', 'edit'),
(12120, 1, 'province_delete', 'province', 'delete'),
(12119, 1, 'province_index', 'province', 'index'),
(12118, 1, 'polling_feedback_listing', 'polling_feedback', 'listing'),
(12117, 1, 'polling_feedback_view', 'polling_feedback', 'view'),
(12116, 1, 'polling_feedback_add', 'polling_feedback', 'add'),
(12115, 1, 'polling_feedback_edit', 'polling_feedback', 'edit'),
(12114, 1, 'polling_feedback_index', 'polling_feedback', 'index'),
(12113, 1, 'polling_listing', 'polling', 'listing'),
(12112, 1, 'polling_view', 'polling', 'view'),
(12111, 1, 'polling_add', 'polling', 'add'),
(12110, 1, 'polling_edit', 'polling', 'edit'),
(12109, 1, 'polling_delete', 'polling', 'delete'),
(12108, 1, 'polling_index', 'polling', 'index'),
(12107, 1, 'pages_listing', 'pages', 'listing'),
(12106, 1, 'pages_view', 'pages', 'view'),
(12105, 1, 'pages_add', 'pages', 'add'),
(12104, 1, 'pages_edit', 'pages', 'edit'),
(12103, 1, 'pages_delete', 'pages', 'delete'),
(12102, 1, 'pages_index', 'pages', 'index'),
(12101, 1, 'news_listing', 'news', 'listing'),
(12100, 1, 'news_view', 'news', 'view'),
(12099, 1, 'news_add', 'news', 'add'),
(12098, 1, 'news_edit', 'news', 'edit'),
(12097, 1, 'news_delete', 'news', 'delete'),
(12096, 1, 'news_index', 'news', 'index'),
(12095, 1, 'menu_listing', 'menu', 'listing'),
(12094, 1, 'menu_view', 'menu', 'view'),
(12093, 1, 'menu_add', 'menu', 'add'),
(12092, 1, 'menu_edit', 'menu', 'edit'),
(12091, 1, 'menu_delete', 'menu', 'delete'),
(12090, 1, 'menu_index', 'menu', 'index'),
(12089, 1, 'log_listing', 'log', 'listing'),
(12088, 1, 'log_view', 'log', 'view'),
(12087, 1, 'log_add', 'log', 'add'),
(12086, 1, 'log_edit', 'log', 'edit'),
(12085, 1, 'log_clear', 'log', 'clear'),
(12084, 1, 'log_delete', 'log', 'delete'),
(12083, 1, 'log_index', 'log', 'index'),
(12082, 1, 'institution_listing', 'institution', 'listing'),
(12081, 1, 'institution_view', 'institution', 'view'),
(12080, 1, 'institution_add', 'institution', 'add'),
(12079, 1, 'institution_edit', 'institution', 'edit'),
(12078, 1, 'institution_delete', 'institution', 'delete'),
(12077, 1, 'institution_index', 'institution', 'index'),
(12076, 1, 'forum_feedback_listing', 'forum_feedback', 'listing'),
(12075, 1, 'forum_feedback_view', 'forum_feedback', 'view'),
(12074, 1, 'forum_feedback_add', 'forum_feedback', 'add'),
(12073, 1, 'forum_feedback_edit', 'forum_feedback', 'edit'),
(12072, 1, 'forum_feedback_delete', 'forum_feedback', 'delete'),
(12071, 1, 'forum_feedback_index', 'forum_feedback', 'index'),
(12070, 1, 'forum_listing', 'forum', 'listing'),
(12069, 1, 'forum_view', 'forum', 'view'),
(12068, 1, 'forum_add', 'forum', 'add'),
(12067, 1, 'forum_edit', 'forum', 'edit'),
(12066, 1, 'forum_delete', 'forum', 'delete'),
(12065, 1, 'forum_index', 'forum', 'index'),
(12064, 1, 'finance_outcome_listing', 'finance_outcome', 'listing'),
(12063, 1, 'finance_outcome_view', 'finance_outcome', 'view'),
(12062, 1, 'finance_outcome_add', 'finance_outcome', 'add'),
(12061, 1, 'finance_outcome_edit', 'finance_outcome', 'edit'),
(12060, 1, 'finance_outcome_delete', 'finance_outcome', 'delete'),
(12059, 1, 'finance_outcome_index', 'finance_outcome', 'index'),
(12058, 1, 'finance_income_outcome_grafic', 'finance_income_outcome', 'grafic'),
(12057, 1, 'finance_income_outcome_printing', 'finance_income_outcome', 'printing'),
(12056, 1, 'finance_income_outcome_download_pdf', 'finance_income_outcome', 'download_pdf'),
(12055, 1, 'finance_income_outcome_download_excel', 'finance_income_outcome', 'download_excel'),
(12054, 1, 'finance_income_outcome_report', 'finance_income_outcome', 'report'),
(12053, 1, 'finance_income_outcome_listing', 'finance_income_outcome', 'listing'),
(12052, 1, 'finance_income_outcome_view', 'finance_income_outcome', 'view'),
(12051, 1, 'finance_income_outcome_add', 'finance_income_outcome', 'add'),
(12050, 1, 'finance_income_outcome_edit', 'finance_income_outcome', 'edit'),
(12049, 1, 'finance_income_outcome_delete', 'finance_income_outcome', 'delete'),
(12048, 1, 'finance_income_outcome_index', 'finance_income_outcome', 'index'),
(12047, 1, 'finance_income_listing', 'finance_income', 'listing'),
(12046, 1, 'finance_income_view', 'finance_income', 'view'),
(12045, 1, 'finance_income_add', 'finance_income', 'add'),
(12044, 1, 'finance_income_edit', 'finance_income', 'edit'),
(12043, 1, 'finance_income_delete', 'finance_income', 'delete'),
(12042, 1, 'finance_income_index', 'finance_income', 'index'),
(12041, 1, 'facility_listing', 'facility', 'listing'),
(12040, 1, 'facility_view', 'facility', 'view'),
(12039, 1, 'facility_add', 'facility', 'add'),
(12038, 1, 'facility_edit', 'facility', 'edit'),
(12037, 1, 'facility_delete', 'facility', 'delete'),
(12036, 1, 'facility_index', 'facility', 'index'),
(12035, 1, 'dashboard_top_menu', 'dashboard', 'top_menu'),
(12034, 1, 'dashboard_rightbar', 'dashboard', 'rightbar'),
(12033, 1, 'dashboard_right_menu', 'dashboard', 'right_menu'),
(12032, 1, 'dashboard_do_logout', 'dashboard', 'do_logout'),
(12031, 1, 'dashboard_is_login', 'dashboard', 'is_login'),
(12030, 1, 'dashboard_do_login', 'dashboard', 'do_login'),
(12029, 1, 'dashboard_login', 'dashboard', 'login'),
(12028, 1, 'dashboard_index', 'dashboard', 'index'),
(12027, 1, 'city_listing', 'city', 'listing'),
(12026, 1, 'city_view', 'city', 'view'),
(12025, 1, 'city_add', 'city', 'add'),
(12024, 1, 'city_edit', 'city', 'edit'),
(12023, 1, 'city_delete', 'city', 'delete'),
(12022, 1, 'city_index', 'city', 'index'),
(12021, 1, 'certification_type_listing', 'certification_type', 'listing'),
(12020, 1, 'certification_type_unit_kompetensi', 'certification_type', 'unit_kompetensi'),
(12019, 1, 'certification_type_bukti_pendukung', 'certification_type', 'bukti_pendukung'),
(12018, 1, 'certification_type_daftar_pertanyaan', 'certification_type', 'daftar_pertanyaan'),
(12017, 1, 'certification_type_element_kompetensi', 'certification_type', 'element_kompetensi'),
(12016, 1, 'certification_type_view', 'certification_type', 'view'),
(12015, 1, 'certification_type_add', 'certification_type', 'add'),
(12014, 1, 'certification_type_edit', 'certification_type', 'edit'),
(12013, 1, 'certification_type_delete', 'certification_type', 'delete'),
(12012, 1, 'certification_type_index', 'certification_type', 'index'),
(12011, 1, 'certification_results_grafic', 'certification_results', 'grafic'),
(12010, 1, 'certification_results_citation', 'certification_results', 'citation'),
(12009, 1, 'certification_results_certificate', 'certification_results', 'certificate'),
(12008, 1, 'certification_results_report', 'certification_results', 'report'),
(12007, 1, 'certification_results_penilaian_mandiri', 'certification_results', 'penilaian_mandiri'),
(12006, 1, 'certification_results_penilaian', 'certification_results', 'penilaian'),
(12005, 1, 'certification_results_hasil_penilaian_mandiri', 'certification_results', 'hasil_penilaian_mandiri'),
(12004, 1, 'certification_results_hasil', 'certification_results', 'hasil'),
(12003, 1, 'certification_results_listing', 'certification_results', 'listing'),
(12002, 1, 'certification_results_view', 'certification_results', 'view'),
(12001, 1, 'certification_results_add', 'certification_results', 'add'),
(12000, 1, 'certification_results_edit', 'certification_results', 'edit'),
(11999, 1, 'certification_results_delete', 'certification_results', 'delete'),
(11998, 1, 'certification_results_index', 'certification_results', 'index'),
(11997, 1, 'certification_member_report', 'certification_member', 'report'),
(11996, 1, 'certification_member_listing', 'certification_member', 'listing'),
(11995, 1, 'certification_member_view', 'certification_member', 'view'),
(11994, 1, 'certification_member_register', 'certification_member', 'register'),
(11993, 1, 'certification_member_add', 'certification_member', 'add'),
(11992, 1, 'certification_member_edit', 'certification_member', 'edit'),
(11991, 1, 'certification_member_delete', 'certification_member', 'delete'),
(11990, 1, 'certification_member_index', 'certification_member', 'index'),
(11989, 1, 'certification_element_aspects_evaluation_unit_komp', 'certification_element_aspects_evaluation', 'unit_kompetensi'),
(11988, 1, 'certification_element_aspects_evaluation_bukti_pen', 'certification_element_aspects_evaluation', 'bukti_pendukung'),
(11987, 1, 'certification_element_aspects_evaluation_daftar_pe', 'certification_element_aspects_evaluation', 'daftar_pertanyaan'),
(11986, 1, 'certification_element_aspects_evaluation_element_k', 'certification_element_aspects_evaluation', 'element_kompetensi'),
(11985, 1, 'certification_element_aspects_evaluation_listing', 'certification_element_aspects_evaluation', 'listing'),
(11984, 1, 'certification_element_aspects_evaluation_view', 'certification_element_aspects_evaluation', 'view'),
(11983, 1, 'certification_element_aspects_evaluation_add', 'certification_element_aspects_evaluation', 'add'),
(11982, 1, 'certification_element_aspects_evaluation_edit', 'certification_element_aspects_evaluation', 'edit'),
(11981, 1, 'certification_element_aspects_evaluation_delete', 'certification_element_aspects_evaluation', 'delete'),
(11980, 1, 'certification_element_aspects_evaluation_index', 'certification_element_aspects_evaluation', 'index'),
(11979, 1, 'certification_certificate_unsur_penilaian', 'certification_certificate', 'unsur_penilaian'),
(11978, 1, 'certification_certificate_report', 'certification_certificate', 'report'),
(11977, 1, 'certification_certificate_grafic', 'certification_certificate', 'grafic'),
(11976, 1, 'certification_certificate_citation', 'certification_certificate', 'citation'),
(11975, 1, 'certification_certificate_listing', 'certification_certificate', 'listing'),
(11974, 1, 'certification_certificate_view', 'certification_certificate', 'view'),
(11973, 1, 'certification_certificate_add', 'certification_certificate', 'add'),
(11972, 1, 'certification_certificate_edit', 'certification_certificate', 'edit'),
(11971, 1, 'certification_certificate_index', 'certification_certificate', 'index'),
(11970, 1, 'certification_assesor_report', 'certification_assesor', 'report'),
(11969, 1, 'certification_assesor_listing', 'certification_assesor', 'listing'),
(11968, 1, 'certification_assesor_view', 'certification_assesor', 'view'),
(11967, 1, 'certification_assesor_add', 'certification_assesor', 'add'),
(11966, 1, 'certification_assesor_edit', 'certification_assesor', 'edit'),
(11965, 1, 'certification_assesor_delete', 'certification_assesor', 'delete'),
(11964, 1, 'certification_assesor_index', 'certification_assesor', 'index'),
(11963, 1, 'certification_aspects_evaluation_support_penilaian', 'certification_aspects_evaluation_support', 'penilaian'),
(11962, 1, 'certification_aspects_evaluation_support_unit_komp', 'certification_aspects_evaluation_support', 'unit_kompetensi'),
(11961, 1, 'certification_aspects_evaluation_support_bukti_pen', 'certification_aspects_evaluation_support', 'bukti_pendukung'),
(11960, 1, 'certification_aspects_evaluation_support_daftar_pe', 'certification_aspects_evaluation_support', 'daftar_pertanyaan'),
(11959, 1, 'certification_aspects_evaluation_support_element_k', 'certification_aspects_evaluation_support', 'element_kompetensi'),
(11958, 1, 'certification_aspects_evaluation_support_listing', 'certification_aspects_evaluation_support', 'listing'),
(11957, 1, 'certification_aspects_evaluation_support_view', 'certification_aspects_evaluation_support', 'view'),
(11956, 1, 'certification_aspects_evaluation_support_add', 'certification_aspects_evaluation_support', 'add'),
(11955, 1, 'certification_aspects_evaluation_support_edit', 'certification_aspects_evaluation_support', 'edit'),
(11954, 1, 'certification_aspects_evaluation_support_delete', 'certification_aspects_evaluation_support', 'delete'),
(11953, 1, 'certification_aspects_evaluation_support_index', 'certification_aspects_evaluation_support', 'index'),
(11952, 1, 'certification_aspects_evaluation_question_penilaia', 'certification_aspects_evaluation_question', 'penilaian_mandiri'),
(11951, 1, 'certification_aspects_evaluation_question_unit_kom', 'certification_aspects_evaluation_question', 'unit_kompetensi'),
(11950, 1, 'certification_aspects_evaluation_question_bukti_pe', 'certification_aspects_evaluation_question', 'bukti_pendukung'),
(11949, 1, 'certification_aspects_evaluation_question_daftar_p', 'certification_aspects_evaluation_question', 'daftar_pertanyaan'),
(11948, 1, 'certification_aspects_evaluation_question_element_', 'certification_aspects_evaluation_question', 'element_kompetensi'),
(11947, 1, 'certification_aspects_evaluation_question_listing', 'certification_aspects_evaluation_question', 'listing'),
(11946, 1, 'certification_aspects_evaluation_question_view', 'certification_aspects_evaluation_question', 'view'),
(11945, 1, 'certification_aspects_evaluation_question_add', 'certification_aspects_evaluation_question', 'add'),
(11944, 1, 'certification_aspects_evaluation_question_edit', 'certification_aspects_evaluation_question', 'edit'),
(11943, 1, 'certification_aspects_evaluation_question_delete', 'certification_aspects_evaluation_question', 'delete'),
(11942, 1, 'certification_aspects_evaluation_question_index', 'certification_aspects_evaluation_question', 'index'),
(11941, 1, 'certification_aspects_evaluation_unit_kompetensi', 'certification_aspects_evaluation', 'unit_kompetensi'),
(11940, 1, 'certification_aspects_evaluation_bukti_pendukung', 'certification_aspects_evaluation', 'bukti_pendukung'),
(11939, 1, 'certification_aspects_evaluation_daftar_pertanyaan', 'certification_aspects_evaluation', 'daftar_pertanyaan'),
(11938, 1, 'certification_aspects_evaluation_element_kompetens', 'certification_aspects_evaluation', 'element_kompetensi'),
(11937, 1, 'certification_aspects_evaluation_listing', 'certification_aspects_evaluation', 'listing'),
(11936, 1, 'certification_aspects_evaluation_view', 'certification_aspects_evaluation', 'view'),
(11935, 1, 'certification_aspects_evaluation_add', 'certification_aspects_evaluation', 'add'),
(11934, 1, 'certification_aspects_evaluation_edit', 'certification_aspects_evaluation', 'edit'),
(11933, 1, 'certification_aspects_evaluation_delete', 'certification_aspects_evaluation', 'delete'),
(11932, 1, 'certification_aspects_evaluation_index', 'certification_aspects_evaluation', 'index'),
(11931, 1, 'certification_listing', 'certification', 'listing'),
(11930, 1, 'certification_view', 'certification', 'view'),
(11929, 1, 'certification_add', 'certification', 'add'),
(11928, 1, 'certification_edit', 'certification', 'edit'),
(11927, 1, 'certification_delete', 'certification', 'delete'),
(11926, 1, 'certification_index', 'certification', 'index'),
(11925, 1, 'blocks_rightbar', 'blocks', 'rightbar'),
(11924, 1, 'blocks_login_form', 'blocks', 'login_form'),
(11923, 1, 'blocks_index', 'blocks', 'index'),
(11922, 1, 'bintek_unsur_penilaian_materi_evaluasi_materi', 'bintek_unsur_penilaian_materi', 'evaluasi_materi'),
(11921, 1, 'bintek_unsur_penilaian_materi_listing', 'bintek_unsur_penilaian_materi', 'listing'),
(11920, 1, 'bintek_unsur_penilaian_materi_view', 'bintek_unsur_penilaian_materi', 'view'),
(11919, 1, 'bintek_unsur_penilaian_materi_add', 'bintek_unsur_penilaian_materi', 'add'),
(11918, 1, 'bintek_unsur_penilaian_materi_edit', 'bintek_unsur_penilaian_materi', 'edit'),
(11917, 1, 'bintek_unsur_penilaian_materi_delete', 'bintek_unsur_penilaian_materi', 'delete'),
(11916, 1, 'bintek_unsur_penilaian_materi_index', 'bintek_unsur_penilaian_materi', 'index'),
(11915, 1, 'bintek_unsur_penilaian_instruktur_evaluasi_instruk', 'bintek_unsur_penilaian_instruktur', 'evaluasi_instruktur'),
(11914, 1, 'bintek_unsur_penilaian_instruktur_listing', 'bintek_unsur_penilaian_instruktur', 'listing'),
(11913, 1, 'bintek_unsur_penilaian_instruktur_view', 'bintek_unsur_penilaian_instruktur', 'view'),
(11912, 1, 'bintek_unsur_penilaian_instruktur_add', 'bintek_unsur_penilaian_instruktur', 'add'),
(11911, 1, 'bintek_unsur_penilaian_instruktur_edit', 'bintek_unsur_penilaian_instruktur', 'edit'),
(11910, 1, 'bintek_unsur_penilaian_instruktur_delete', 'bintek_unsur_penilaian_instruktur', 'delete'),
(11909, 1, 'bintek_unsur_penilaian_instruktur_index', 'bintek_unsur_penilaian_instruktur', 'index'),
(11908, 1, 'bintek_type_printing', 'bintek_type', 'printing'),
(11907, 1, 'bintek_type_download_pdf', 'bintek_type', 'download_pdf'),
(11906, 1, 'bintek_type_download_excel', 'bintek_type', 'download_excel'),
(11905, 1, 'bintek_type_report', 'bintek_type', 'report'),
(11904, 1, 'bintek_type_listing', 'bintek_type', 'listing'),
(11903, 1, 'bintek_type_view', 'bintek_type', 'view'),
(11902, 1, 'bintek_type_add', 'bintek_type', 'add'),
(11901, 1, 'bintek_type_edit', 'bintek_type', 'edit'),
(11900, 1, 'bintek_type_delete', 'bintek_type', 'delete'),
(11899, 1, 'bintek_type_index', 'bintek_type', 'index'),
(11898, 1, 'bintek_schedule_report', 'bintek_schedule', 'report'),
(11897, 1, 'bintek_schedule_data_instruktur', 'bintek_schedule', 'data_instruktur'),
(11896, 1, 'bintek_schedule_data_member', 'bintek_schedule', 'data_member'),
(11895, 1, 'bintek_schedule_listing', 'bintek_schedule', 'listing'),
(11894, 1, 'bintek_schedule_view', 'bintek_schedule', 'view'),
(11893, 1, 'bintek_schedule_add', 'bintek_schedule', 'add'),
(11892, 1, 'bintek_schedule_edit', 'bintek_schedule', 'edit'),
(11891, 1, 'bintek_schedule_delete', 'bintek_schedule', 'delete'),
(11890, 1, 'bintek_schedule_index', 'bintek_schedule', 'index'),
(11889, 1, 'bintek_relation_schedule_member_report', 'bintek_relation_schedule_member', 'report'),
(11888, 1, 'bintek_relation_schedule_member_listing', 'bintek_relation_schedule_member', 'listing'),
(11887, 1, 'bintek_relation_schedule_member_add', 'bintek_relation_schedule_member', 'add'),
(11886, 1, 'bintek_relation_schedule_member_status_sudah', 'bintek_relation_schedule_member', 'status_sudah'),
(11885, 1, 'bintek_relation_schedule_member_status_belum', 'bintek_relation_schedule_member', 'status_belum'),
(11884, 1, 'bintek_relation_schedule_member_delete', 'bintek_relation_schedule_member', 'delete'),
(11883, 1, 'bintek_relation_schedule_member_index', 'bintek_relation_schedule_member', 'index'),
(11882, 1, 'bintek_relation_schedule_instructure_report', 'bintek_relation_schedule_instructure', 'report'),
(11881, 1, 'bintek_relation_schedule_instructure_listing', 'bintek_relation_schedule_instructure', 'listing'),
(11880, 1, 'bintek_relation_schedule_instructure_add', 'bintek_relation_schedule_instructure', 'add'),
(11879, 1, 'bintek_relation_schedule_instructure_delete', 'bintek_relation_schedule_instructure', 'delete'),
(11878, 1, 'bintek_relation_schedule_instructure_index', 'bintek_relation_schedule_instructure', 'index'),
(11877, 1, 'bintek_monev_report', 'bintek_monev', 'report'),
(11876, 1, 'bintek_monev_listing', 'bintek_monev', 'listing'),
(11875, 1, 'bintek_monev_view', 'bintek_monev', 'view'),
(11874, 1, 'bintek_monev_add', 'bintek_monev', 'add'),
(11873, 1, 'bintek_monev_edit', 'bintek_monev', 'edit'),
(11872, 1, 'bintek_monev_delete', 'bintek_monev', 'delete'),
(11871, 1, 'bintek_monev_index', 'bintek_monev', 'index'),
(11870, 1, 'bintek_member_printing', 'bintek_member', 'printing'),
(11869, 1, 'bintek_member_download_pdf', 'bintek_member', 'download_pdf'),
(11868, 1, 'bintek_member_download_excel', 'bintek_member', 'download_excel'),
(11867, 1, 'bintek_member_grafic', 'bintek_member', 'grafic'),
(11866, 1, 'bintek_member_citation', 'bintek_member', 'citation'),
(11865, 1, 'bintek_member_certificate', 'bintek_member', 'certificate'),
(11864, 1, 'bintek_member_report', 'bintek_member', 'report'),
(11863, 1, 'bintek_member_listing', 'bintek_member', 'listing'),
(11862, 1, 'bintek_member_view', 'bintek_member', 'view'),
(11861, 1, 'bintek_member_register', 'bintek_member', 'register'),
(11860, 1, 'bintek_member_add', 'bintek_member', 'add'),
(11859, 1, 'bintek_member_edit', 'bintek_member', 'edit'),
(11858, 1, 'bintek_member_delete', 'bintek_member', 'delete'),
(11857, 1, 'bintek_member_index', 'bintek_member', 'index'),
(11856, 1, 'bintek_material_printing', 'bintek_material', 'printing'),
(11855, 1, 'bintek_material_download_pdf', 'bintek_material', 'download_pdf'),
(11854, 1, 'bintek_material_download_excel', 'bintek_material', 'download_excel'),
(11853, 1, 'bintek_material_report', 'bintek_material', 'report'),
(11852, 1, 'bintek_material_listing', 'bintek_material', 'listing'),
(11851, 1, 'bintek_material_view', 'bintek_material', 'view'),
(11850, 1, 'bintek_material_add', 'bintek_material', 'add'),
(11849, 1, 'bintek_material_edit', 'bintek_material', 'edit'),
(11848, 1, 'bintek_material_delete', 'bintek_material', 'delete'),
(11847, 1, 'bintek_material_index', 'bintek_material', 'index'),
(11846, 1, 'bintek_instructure_printing', 'bintek_instructure', 'printing'),
(11845, 1, 'bintek_instructure_download_pdf', 'bintek_instructure', 'download_pdf'),
(9011, 2, 'user_manual_bintek', 'user_manual', 'bintek'),
(9010, 2, 'user_manual_index', 'user_manual', 'index'),
(9009, 2, 'ukom_type_listing', 'ukom_type', 'listing'),
(9008, 2, 'ukom_type_unit_kompetensi', 'ukom_type', 'unit_kompetensi'),
(9007, 2, 'ukom_type_bukti_pendukung', 'ukom_type', 'bukti_pendukung'),
(9006, 2, 'ukom_type_daftar_pertanyaan', 'ukom_type', 'daftar_pertanyaan'),
(9005, 2, 'ukom_type_element_kompetensi', 'ukom_type', 'element_kompetensi'),
(9004, 2, 'ukom_type_view', 'ukom_type', 'view'),
(9003, 2, 'ukom_type_add', 'ukom_type', 'add'),
(9002, 2, 'ukom_type_edit', 'ukom_type', 'edit'),
(9001, 2, 'ukom_type_delete', 'ukom_type', 'delete'),
(9000, 2, 'ukom_type_index', 'ukom_type', 'index'),
(8999, 2, 'ukom_results_report', 'ukom_results', 'report'),
(8998, 2, 'ukom_results_penilaian_mandiri', 'ukom_results', 'penilaian_mandiri'),
(8997, 2, 'ukom_results_penilaian', 'ukom_results', 'penilaian'),
(8996, 2, 'ukom_results_hasil_penilaian_mandiri', 'ukom_results', 'hasil_penilaian_mandiri'),
(8995, 2, 'ukom_results_hasil', 'ukom_results', 'hasil'),
(8994, 2, 'ukom_results_listing', 'ukom_results', 'listing'),
(8993, 2, 'ukom_results_view', 'ukom_results', 'view'),
(8992, 2, 'ukom_results_add', 'ukom_results', 'add'),
(8991, 2, 'ukom_results_edit', 'ukom_results', 'edit'),
(8990, 2, 'ukom_results_delete', 'ukom_results', 'delete'),
(8989, 2, 'ukom_results_index', 'ukom_results', 'index'),
(8988, 2, 'ukom_member_report', 'ukom_member', 'report'),
(8987, 2, 'ukom_member_listing', 'ukom_member', 'listing'),
(8986, 2, 'ukom_member_view', 'ukom_member', 'view'),
(8985, 2, 'ukom_member_add', 'ukom_member', 'add'),
(8984, 2, 'ukom_member_edit', 'ukom_member', 'edit'),
(8983, 2, 'ukom_member_delete', 'ukom_member', 'delete'),
(8982, 2, 'ukom_member_index', 'ukom_member', 'index'),
(8981, 2, 'ukom_element_aspects_evaluation_unit_kompetensi', 'ukom_element_aspects_evaluation', 'unit_kompetensi'),
(8980, 2, 'ukom_element_aspects_evaluation_bukti_pendukung', 'ukom_element_aspects_evaluation', 'bukti_pendukung'),
(8979, 2, 'ukom_element_aspects_evaluation_daftar_pertanyaan', 'ukom_element_aspects_evaluation', 'daftar_pertanyaan'),
(8978, 2, 'ukom_element_aspects_evaluation_element_kompetensi', 'ukom_element_aspects_evaluation', 'element_kompetensi'),
(8977, 2, 'ukom_element_aspects_evaluation_listing', 'ukom_element_aspects_evaluation', 'listing'),
(8976, 2, 'ukom_element_aspects_evaluation_view', 'ukom_element_aspects_evaluation', 'view'),
(8975, 2, 'ukom_element_aspects_evaluation_add', 'ukom_element_aspects_evaluation', 'add'),
(8974, 2, 'ukom_element_aspects_evaluation_edit', 'ukom_element_aspects_evaluation', 'edit'),
(8973, 2, 'ukom_element_aspects_evaluation_delete', 'ukom_element_aspects_evaluation', 'delete'),
(8972, 2, 'ukom_element_aspects_evaluation_index', 'ukom_element_aspects_evaluation', 'index'),
(8971, 2, 'ukom_assesor_report', 'ukom_assesor', 'report'),
(8970, 2, 'ukom_assesor_listing', 'ukom_assesor', 'listing'),
(8969, 2, 'ukom_assesor_view', 'ukom_assesor', 'view'),
(8968, 2, 'ukom_assesor_add', 'ukom_assesor', 'add'),
(8967, 2, 'ukom_assesor_edit', 'ukom_assesor', 'edit'),
(8966, 2, 'ukom_assesor_delete', 'ukom_assesor', 'delete'),
(8965, 2, 'ukom_assesor_index', 'ukom_assesor', 'index'),
(8964, 2, 'ukom_aspects_evaluation_support_penilaian', 'ukom_aspects_evaluation_support', 'penilaian'),
(8963, 2, 'ukom_aspects_evaluation_support_unit_kompetensi', 'ukom_aspects_evaluation_support', 'unit_kompetensi'),
(8962, 2, 'ukom_aspects_evaluation_support_bukti_pendukung', 'ukom_aspects_evaluation_support', 'bukti_pendukung'),
(8961, 2, 'ukom_aspects_evaluation_support_daftar_pertanyaan', 'ukom_aspects_evaluation_support', 'daftar_pertanyaan'),
(8960, 2, 'ukom_aspects_evaluation_support_element_kompetensi', 'ukom_aspects_evaluation_support', 'element_kompetensi'),
(8959, 2, 'ukom_aspects_evaluation_support_listing', 'ukom_aspects_evaluation_support', 'listing'),
(8958, 2, 'ukom_aspects_evaluation_support_view', 'ukom_aspects_evaluation_support', 'view'),
(8957, 2, 'ukom_aspects_evaluation_support_add', 'ukom_aspects_evaluation_support', 'add'),
(8956, 2, 'ukom_aspects_evaluation_support_edit', 'ukom_aspects_evaluation_support', 'edit'),
(8955, 2, 'ukom_aspects_evaluation_support_delete', 'ukom_aspects_evaluation_support', 'delete'),
(8954, 2, 'ukom_aspects_evaluation_support_index', 'ukom_aspects_evaluation_support', 'index'),
(8953, 2, 'ukom_aspects_evaluation_question_penilaian_mandiri', 'ukom_aspects_evaluation_question', 'penilaian_mandiri'),
(8952, 2, 'ukom_aspects_evaluation_question_unit_kompetensi', 'ukom_aspects_evaluation_question', 'unit_kompetensi'),
(8951, 2, 'ukom_aspects_evaluation_question_bukti_pendukung', 'ukom_aspects_evaluation_question', 'bukti_pendukung'),
(8950, 2, 'ukom_aspects_evaluation_question_daftar_pertanyaan', 'ukom_aspects_evaluation_question', 'daftar_pertanyaan'),
(8949, 2, 'ukom_aspects_evaluation_question_element_kompetens', 'ukom_aspects_evaluation_question', 'element_kompetensi'),
(8948, 2, 'ukom_aspects_evaluation_question_listing', 'ukom_aspects_evaluation_question', 'listing'),
(8947, 2, 'ukom_aspects_evaluation_question_view', 'ukom_aspects_evaluation_question', 'view'),
(8946, 2, 'ukom_aspects_evaluation_question_add', 'ukom_aspects_evaluation_question', 'add'),
(8945, 2, 'ukom_aspects_evaluation_question_edit', 'ukom_aspects_evaluation_question', 'edit'),
(8944, 2, 'ukom_aspects_evaluation_question_delete', 'ukom_aspects_evaluation_question', 'delete'),
(8943, 2, 'ukom_aspects_evaluation_question_index', 'ukom_aspects_evaluation_question', 'index'),
(8942, 2, 'ukom_aspects_evaluation_unit_kompetensi', 'ukom_aspects_evaluation', 'unit_kompetensi'),
(8941, 2, 'ukom_aspects_evaluation_bukti_pendukung', 'ukom_aspects_evaluation', 'bukti_pendukung'),
(8940, 2, 'ukom_aspects_evaluation_daftar_pertanyaan', 'ukom_aspects_evaluation', 'daftar_pertanyaan'),
(8939, 2, 'ukom_aspects_evaluation_element_kompetensi', 'ukom_aspects_evaluation', 'element_kompetensi'),
(8938, 2, 'ukom_aspects_evaluation_listing', 'ukom_aspects_evaluation', 'listing'),
(8937, 2, 'ukom_aspects_evaluation_view', 'ukom_aspects_evaluation', 'view'),
(8936, 2, 'ukom_aspects_evaluation_add', 'ukom_aspects_evaluation', 'add'),
(8935, 2, 'ukom_aspects_evaluation_edit', 'ukom_aspects_evaluation', 'edit'),
(8934, 2, 'ukom_aspects_evaluation_delete', 'ukom_aspects_evaluation', 'delete'),
(8933, 2, 'ukom_aspects_evaluation_index', 'ukom_aspects_evaluation', 'index'),
(8932, 2, 'ukom_listing', 'ukom', 'listing'),
(8931, 2, 'ukom_view', 'ukom', 'view'),
(8930, 2, 'ukom_add', 'ukom', 'add'),
(8929, 2, 'ukom_edit', 'ukom', 'edit'),
(8928, 2, 'ukom_delete', 'ukom', 'delete'),
(8927, 2, 'ukom_index', 'ukom', 'index'),
(8926, 2, 'recycle_bin_listing', 'recycle_bin', 'listing'),
(8925, 2, 'recycle_bin_view', 'recycle_bin', 'view'),
(8924, 2, 'recycle_bin_add', 'recycle_bin', 'add'),
(8923, 2, 'recycle_bin_edit', 'recycle_bin', 'edit'),
(8922, 2, 'recycle_bin_delete', 'recycle_bin', 'delete'),
(8921, 2, 'recycle_bin_index', 'recycle_bin', 'index'),
(8920, 2, 'polling_feedback_listing', 'polling_feedback', 'listing'),
(8919, 2, 'polling_feedback_view', 'polling_feedback', 'view'),
(8918, 2, 'polling_feedback_add', 'polling_feedback', 'add'),
(8917, 2, 'polling_feedback_edit', 'polling_feedback', 'edit'),
(8916, 2, 'polling_feedback_index', 'polling_feedback', 'index'),
(8915, 2, 'polling_listing', 'polling', 'listing'),
(8914, 2, 'polling_view', 'polling', 'view'),
(8913, 2, 'polling_add', 'polling', 'add'),
(8912, 2, 'polling_edit', 'polling', 'edit'),
(8911, 2, 'polling_delete', 'polling', 'delete'),
(8910, 2, 'polling_index', 'polling', 'index'),
(8909, 2, 'pages_listing', 'pages', 'listing'),
(8908, 2, 'pages_view', 'pages', 'view'),
(8907, 2, 'pages_add', 'pages', 'add'),
(8906, 2, 'pages_edit', 'pages', 'edit'),
(8905, 2, 'pages_delete', 'pages', 'delete'),
(8904, 2, 'pages_index', 'pages', 'index'),
(8903, 2, 'news_listing', 'news', 'listing'),
(8902, 2, 'news_view', 'news', 'view'),
(8901, 2, 'news_add', 'news', 'add'),
(8900, 2, 'news_edit', 'news', 'edit'),
(8899, 2, 'news_delete', 'news', 'delete'),
(8898, 2, 'news_index', 'news', 'index'),
(8897, 2, 'institution_listing', 'institution', 'listing'),
(8896, 2, 'institution_view', 'institution', 'view'),
(8895, 2, 'institution_add', 'institution', 'add'),
(8894, 2, 'institution_edit', 'institution', 'edit'),
(8893, 2, 'institution_delete', 'institution', 'delete'),
(8892, 2, 'institution_index', 'institution', 'index'),
(8891, 2, 'forum_feedback_listing', 'forum_feedback', 'listing'),
(8890, 2, 'forum_feedback_view', 'forum_feedback', 'view'),
(8889, 2, 'forum_feedback_add', 'forum_feedback', 'add'),
(8888, 2, 'forum_feedback_edit', 'forum_feedback', 'edit'),
(8887, 2, 'forum_feedback_delete', 'forum_feedback', 'delete'),
(8886, 2, 'forum_feedback_index', 'forum_feedback', 'index'),
(8885, 2, 'forum_listing', 'forum', 'listing'),
(8884, 2, 'forum_view', 'forum', 'view'),
(8883, 2, 'forum_add', 'forum', 'add'),
(8882, 2, 'forum_edit', 'forum', 'edit'),
(8881, 2, 'forum_delete', 'forum', 'delete'),
(8880, 2, 'forum_index', 'forum', 'index'),
(8879, 2, 'finance_outcome_listing', 'finance_outcome', 'listing'),
(8878, 2, 'finance_outcome_view', 'finance_outcome', 'view'),
(8877, 2, 'finance_outcome_add', 'finance_outcome', 'add'),
(8876, 2, 'finance_outcome_edit', 'finance_outcome', 'edit'),
(8875, 2, 'finance_outcome_delete', 'finance_outcome', 'delete'),
(8874, 2, 'finance_outcome_index', 'finance_outcome', 'index'),
(8873, 2, 'finance_income_outcome_printing', 'finance_income_outcome', 'printing'),
(8872, 2, 'finance_income_outcome_download_pdf', 'finance_income_outcome', 'download_pdf'),
(8871, 2, 'finance_income_outcome_download_excel', 'finance_income_outcome', 'download_excel'),
(8870, 2, 'finance_income_outcome_report', 'finance_income_outcome', 'report'),
(8869, 2, 'finance_income_outcome_listing', 'finance_income_outcome', 'listing'),
(8868, 2, 'finance_income_outcome_view', 'finance_income_outcome', 'view'),
(8867, 2, 'finance_income_outcome_add', 'finance_income_outcome', 'add'),
(8866, 2, 'finance_income_outcome_edit', 'finance_income_outcome', 'edit'),
(8865, 2, 'finance_income_outcome_delete', 'finance_income_outcome', 'delete'),
(8864, 2, 'finance_income_outcome_index', 'finance_income_outcome', 'index'),
(8863, 2, 'finance_income_listing', 'finance_income', 'listing'),
(8862, 2, 'finance_income_view', 'finance_income', 'view'),
(8861, 2, 'finance_income_add', 'finance_income', 'add'),
(8860, 2, 'finance_income_edit', 'finance_income', 'edit'),
(8859, 2, 'finance_income_delete', 'finance_income', 'delete'),
(8858, 2, 'finance_income_index', 'finance_income', 'index'),
(8857, 2, 'facility_listing', 'facility', 'listing'),
(8856, 2, 'facility_view', 'facility', 'view'),
(8855, 2, 'facility_add', 'facility', 'add'),
(8854, 2, 'facility_edit', 'facility', 'edit'),
(8853, 2, 'facility_delete', 'facility', 'delete'),
(8852, 2, 'facility_index', 'facility', 'index'),
(8851, 2, 'dashboard_top_menu', 'dashboard', 'top_menu'),
(8850, 2, 'dashboard_rightbar', 'dashboard', 'rightbar'),
(8849, 2, 'dashboard_right_menu', 'dashboard', 'right_menu'),
(8848, 2, 'dashboard_do_logout', 'dashboard', 'do_logout'),
(8847, 2, 'dashboard_is_login', 'dashboard', 'is_login'),
(8846, 2, 'dashboard_do_login', 'dashboard', 'do_login'),
(8845, 2, 'dashboard_login', 'dashboard', 'login'),
(8844, 2, 'dashboard_index', 'dashboard', 'index'),
(8843, 2, 'certification_type_listing', 'certification_type', 'listing'),
(8842, 2, 'certification_type_unit_kompetensi', 'certification_type', 'unit_kompetensi'),
(8841, 2, 'certification_type_bukti_pendukung', 'certification_type', 'bukti_pendukung'),
(8840, 2, 'certification_type_daftar_pertanyaan', 'certification_type', 'daftar_pertanyaan'),
(8839, 2, 'certification_type_element_kompetensi', 'certification_type', 'element_kompetensi'),
(8838, 2, 'certification_type_view', 'certification_type', 'view'),
(8837, 2, 'certification_type_add', 'certification_type', 'add'),
(8836, 2, 'certification_type_edit', 'certification_type', 'edit'),
(8835, 2, 'certification_type_delete', 'certification_type', 'delete'),
(8834, 2, 'certification_type_index', 'certification_type', 'index'),
(8833, 2, 'certification_results_report', 'certification_results', 'report'),
(8832, 2, 'certification_results_penilaian_mandiri', 'certification_results', 'penilaian_mandiri'),
(8831, 2, 'certification_results_penilaian', 'certification_results', 'penilaian'),
(8830, 2, 'certification_results_hasil_penilaian_mandiri', 'certification_results', 'hasil_penilaian_mandiri'),
(8829, 2, 'certification_results_hasil', 'certification_results', 'hasil'),
(8828, 2, 'certification_results_listing', 'certification_results', 'listing'),
(8827, 2, 'certification_results_view', 'certification_results', 'view'),
(8826, 2, 'certification_results_add', 'certification_results', 'add'),
(8825, 2, 'certification_results_edit', 'certification_results', 'edit'),
(8824, 2, 'certification_results_delete', 'certification_results', 'delete'),
(8823, 2, 'certification_results_index', 'certification_results', 'index'),
(8822, 2, 'certification_member_report', 'certification_member', 'report'),
(8821, 2, 'certification_member_listing', 'certification_member', 'listing'),
(8820, 2, 'certification_member_view', 'certification_member', 'view'),
(8819, 2, 'certification_member_register', 'certification_member', 'register'),
(8818, 2, 'certification_member_add', 'certification_member', 'add'),
(8817, 2, 'certification_member_edit', 'certification_member', 'edit'),
(8816, 2, 'certification_member_delete', 'certification_member', 'delete'),
(8815, 2, 'certification_member_index', 'certification_member', 'index'),
(8814, 2, 'certification_element_aspects_evaluation_unit_komp', 'certification_element_aspects_evaluation', 'unit_kompetensi'),
(8813, 2, 'certification_element_aspects_evaluation_bukti_pen', 'certification_element_aspects_evaluation', 'bukti_pendukung'),
(8812, 2, 'certification_element_aspects_evaluation_daftar_pe', 'certification_element_aspects_evaluation', 'daftar_pertanyaan'),
(8811, 2, 'certification_element_aspects_evaluation_element_k', 'certification_element_aspects_evaluation', 'element_kompetensi'),
(8810, 2, 'certification_element_aspects_evaluation_listing', 'certification_element_aspects_evaluation', 'listing'),
(8809, 2, 'certification_element_aspects_evaluation_view', 'certification_element_aspects_evaluation', 'view'),
(8808, 2, 'certification_element_aspects_evaluation_add', 'certification_element_aspects_evaluation', 'add'),
(8807, 2, 'certification_element_aspects_evaluation_edit', 'certification_element_aspects_evaluation', 'edit'),
(8806, 2, 'certification_element_aspects_evaluation_delete', 'certification_element_aspects_evaluation', 'delete'),
(8805, 2, 'certification_element_aspects_evaluation_index', 'certification_element_aspects_evaluation', 'index'),
(8804, 2, 'certification_assesor_report', 'certification_assesor', 'report'),
(8803, 2, 'certification_assesor_listing', 'certification_assesor', 'listing'),
(8802, 2, 'certification_assesor_view', 'certification_assesor', 'view'),
(8801, 2, 'certification_assesor_add', 'certification_assesor', 'add'),
(8800, 2, 'certification_assesor_edit', 'certification_assesor', 'edit'),
(8799, 2, 'certification_assesor_delete', 'certification_assesor', 'delete'),
(8798, 2, 'certification_assesor_index', 'certification_assesor', 'index'),
(8797, 2, 'certification_aspects_evaluation_support_penilaian', 'certification_aspects_evaluation_support', 'penilaian'),
(8796, 2, 'certification_aspects_evaluation_support_unit_komp', 'certification_aspects_evaluation_support', 'unit_kompetensi'),
(8795, 2, 'certification_aspects_evaluation_support_bukti_pen', 'certification_aspects_evaluation_support', 'bukti_pendukung'),
(8794, 2, 'certification_aspects_evaluation_support_daftar_pe', 'certification_aspects_evaluation_support', 'daftar_pertanyaan'),
(8793, 2, 'certification_aspects_evaluation_support_element_k', 'certification_aspects_evaluation_support', 'element_kompetensi'),
(8792, 2, 'certification_aspects_evaluation_support_listing', 'certification_aspects_evaluation_support', 'listing'),
(8791, 2, 'certification_aspects_evaluation_support_view', 'certification_aspects_evaluation_support', 'view'),
(8790, 2, 'certification_aspects_evaluation_support_add', 'certification_aspects_evaluation_support', 'add'),
(8789, 2, 'certification_aspects_evaluation_support_edit', 'certification_aspects_evaluation_support', 'edit'),
(8788, 2, 'certification_aspects_evaluation_support_delete', 'certification_aspects_evaluation_support', 'delete'),
(8787, 2, 'certification_aspects_evaluation_support_index', 'certification_aspects_evaluation_support', 'index'),
(8786, 2, 'certification_aspects_evaluation_question_penilaia', 'certification_aspects_evaluation_question', 'penilaian_mandiri'),
(8785, 2, 'certification_aspects_evaluation_question_unit_kom', 'certification_aspects_evaluation_question', 'unit_kompetensi'),
(8784, 2, 'certification_aspects_evaluation_question_bukti_pe', 'certification_aspects_evaluation_question', 'bukti_pendukung'),
(8783, 2, 'certification_aspects_evaluation_question_daftar_p', 'certification_aspects_evaluation_question', 'daftar_pertanyaan'),
(8782, 2, 'certification_aspects_evaluation_question_element_', 'certification_aspects_evaluation_question', 'element_kompetensi'),
(8781, 2, 'certification_aspects_evaluation_question_listing', 'certification_aspects_evaluation_question', 'listing'),
(8780, 2, 'certification_aspects_evaluation_question_view', 'certification_aspects_evaluation_question', 'view'),
(8779, 2, 'certification_aspects_evaluation_question_add', 'certification_aspects_evaluation_question', 'add'),
(8778, 2, 'certification_aspects_evaluation_question_edit', 'certification_aspects_evaluation_question', 'edit'),
(8777, 2, 'certification_aspects_evaluation_question_delete', 'certification_aspects_evaluation_question', 'delete'),
(8776, 2, 'certification_aspects_evaluation_question_index', 'certification_aspects_evaluation_question', 'index'),
(8775, 2, 'certification_aspects_evaluation_unit_kompetensi', 'certification_aspects_evaluation', 'unit_kompetensi'),
(8774, 2, 'certification_aspects_evaluation_bukti_pendukung', 'certification_aspects_evaluation', 'bukti_pendukung'),
(8773, 2, 'certification_aspects_evaluation_daftar_pertanyaan', 'certification_aspects_evaluation', 'daftar_pertanyaan'),
(8772, 2, 'certification_aspects_evaluation_element_kompetens', 'certification_aspects_evaluation', 'element_kompetensi'),
(8771, 2, 'certification_aspects_evaluation_listing', 'certification_aspects_evaluation', 'listing'),
(8770, 2, 'certification_aspects_evaluation_view', 'certification_aspects_evaluation', 'view'),
(8769, 2, 'certification_aspects_evaluation_add', 'certification_aspects_evaluation', 'add'),
(8768, 2, 'certification_aspects_evaluation_edit', 'certification_aspects_evaluation', 'edit'),
(8767, 2, 'certification_aspects_evaluation_delete', 'certification_aspects_evaluation', 'delete'),
(8766, 2, 'certification_aspects_evaluation_index', 'certification_aspects_evaluation', 'index'),
(8765, 2, 'certification_listing', 'certification', 'listing'),
(8764, 2, 'certification_view', 'certification', 'view'),
(8763, 2, 'certification_add', 'certification', 'add'),
(8762, 2, 'certification_edit', 'certification', 'edit'),
(8761, 2, 'certification_delete', 'certification', 'delete'),
(8760, 2, 'certification_index', 'certification', 'index'),
(8759, 2, 'blocks_rightbar', 'blocks', 'rightbar'),
(8758, 2, 'blocks_login_form', 'blocks', 'login_form'),
(8757, 2, 'blocks_index', 'blocks', 'index'),
(8756, 2, 'bintek_unsur_penilaian_materi_evaluasi_materi', 'bintek_unsur_penilaian_materi', 'evaluasi_materi'),
(8755, 2, 'bintek_unsur_penilaian_materi_listing', 'bintek_unsur_penilaian_materi', 'listing'),
(8754, 2, 'bintek_unsur_penilaian_materi_view', 'bintek_unsur_penilaian_materi', 'view'),
(8753, 2, 'bintek_unsur_penilaian_materi_add', 'bintek_unsur_penilaian_materi', 'add'),
(8752, 2, 'bintek_unsur_penilaian_materi_edit', 'bintek_unsur_penilaian_materi', 'edit'),
(8751, 2, 'bintek_unsur_penilaian_materi_delete', 'bintek_unsur_penilaian_materi', 'delete'),
(8750, 2, 'bintek_unsur_penilaian_materi_index', 'bintek_unsur_penilaian_materi', 'index'),
(8749, 2, 'bintek_unsur_penilaian_instruktur_evaluasi_instruk', 'bintek_unsur_penilaian_instruktur', 'evaluasi_instruktur'),
(8748, 2, 'bintek_unsur_penilaian_instruktur_listing', 'bintek_unsur_penilaian_instruktur', 'listing'),
(8747, 2, 'bintek_unsur_penilaian_instruktur_view', 'bintek_unsur_penilaian_instruktur', 'view'),
(8746, 2, 'bintek_unsur_penilaian_instruktur_add', 'bintek_unsur_penilaian_instruktur', 'add'),
(8745, 2, 'bintek_unsur_penilaian_instruktur_edit', 'bintek_unsur_penilaian_instruktur', 'edit'),
(8744, 2, 'bintek_unsur_penilaian_instruktur_delete', 'bintek_unsur_penilaian_instruktur', 'delete'),
(8743, 2, 'bintek_unsur_penilaian_instruktur_index', 'bintek_unsur_penilaian_instruktur', 'index'),
(8742, 2, 'bintek_type_report', 'bintek_type', 'report'),
(8741, 2, 'bintek_type_listing', 'bintek_type', 'listing'),
(8740, 2, 'bintek_type_view', 'bintek_type', 'view');
INSERT INTO `user_level_roles` (`user_level_role_id`, `user_level_id`, `user_role_name`, `controller`, `function`) VALUES
(8739, 2, 'bintek_type_add', 'bintek_type', 'add'),
(8738, 2, 'bintek_type_edit', 'bintek_type', 'edit'),
(8737, 2, 'bintek_type_delete', 'bintek_type', 'delete'),
(8736, 2, 'bintek_type_index', 'bintek_type', 'index'),
(8735, 2, 'bintek_schedule_report', 'bintek_schedule', 'report'),
(8734, 2, 'bintek_schedule_data_instruktur', 'bintek_schedule', 'data_instruktur'),
(8733, 2, 'bintek_schedule_data_member', 'bintek_schedule', 'data_member'),
(8732, 2, 'bintek_schedule_listing', 'bintek_schedule', 'listing'),
(8731, 2, 'bintek_schedule_view', 'bintek_schedule', 'view'),
(8730, 2, 'bintek_schedule_add', 'bintek_schedule', 'add'),
(8729, 2, 'bintek_schedule_edit', 'bintek_schedule', 'edit'),
(8728, 2, 'bintek_schedule_delete', 'bintek_schedule', 'delete'),
(8727, 2, 'bintek_schedule_index', 'bintek_schedule', 'index'),
(8726, 2, 'bintek_relation_schedule_member_report', 'bintek_relation_schedule_member', 'report'),
(8725, 2, 'bintek_relation_schedule_member_listing', 'bintek_relation_schedule_member', 'listing'),
(8724, 2, 'bintek_relation_schedule_member_add', 'bintek_relation_schedule_member', 'add'),
(8723, 2, 'bintek_relation_schedule_member_status_sudah', 'bintek_relation_schedule_member', 'status_sudah'),
(8722, 2, 'bintek_relation_schedule_member_status_belum', 'bintek_relation_schedule_member', 'status_belum'),
(8721, 2, 'bintek_relation_schedule_member_delete', 'bintek_relation_schedule_member', 'delete'),
(8720, 2, 'bintek_relation_schedule_member_index', 'bintek_relation_schedule_member', 'index'),
(8719, 2, 'bintek_relation_schedule_instructure_report', 'bintek_relation_schedule_instructure', 'report'),
(8718, 2, 'bintek_relation_schedule_instructure_listing', 'bintek_relation_schedule_instructure', 'listing'),
(8717, 2, 'bintek_relation_schedule_instructure_add', 'bintek_relation_schedule_instructure', 'add'),
(8716, 2, 'bintek_relation_schedule_instructure_delete', 'bintek_relation_schedule_instructure', 'delete'),
(8715, 2, 'bintek_relation_schedule_instructure_index', 'bintek_relation_schedule_instructure', 'index'),
(8714, 2, 'bintek_monev_report', 'bintek_monev', 'report'),
(8713, 2, 'bintek_monev_listing', 'bintek_monev', 'listing'),
(8712, 2, 'bintek_monev_view', 'bintek_monev', 'view'),
(8711, 2, 'bintek_monev_add', 'bintek_monev', 'add'),
(8710, 2, 'bintek_monev_edit', 'bintek_monev', 'edit'),
(8709, 2, 'bintek_monev_delete', 'bintek_monev', 'delete'),
(8708, 2, 'bintek_monev_index', 'bintek_monev', 'index'),
(8707, 2, 'bintek_member_report', 'bintek_member', 'report'),
(8706, 2, 'bintek_member_listing', 'bintek_member', 'listing'),
(8705, 2, 'bintek_member_view', 'bintek_member', 'view'),
(8704, 2, 'bintek_member_register', 'bintek_member', 'register'),
(8703, 2, 'bintek_member_add', 'bintek_member', 'add'),
(8702, 2, 'bintek_member_edit', 'bintek_member', 'edit'),
(8701, 2, 'bintek_member_delete', 'bintek_member', 'delete'),
(8700, 2, 'bintek_member_index', 'bintek_member', 'index'),
(8699, 2, 'bintek_material_report', 'bintek_material', 'report'),
(8698, 2, 'bintek_material_listing', 'bintek_material', 'listing'),
(8697, 2, 'bintek_material_view', 'bintek_material', 'view'),
(8696, 2, 'bintek_material_add', 'bintek_material', 'add'),
(8695, 2, 'bintek_material_edit', 'bintek_material', 'edit'),
(8694, 2, 'bintek_material_delete', 'bintek_material', 'delete'),
(8693, 2, 'bintek_material_index', 'bintek_material', 'index'),
(8692, 2, 'bintek_instructure_report', 'bintek_instructure', 'report'),
(8691, 2, 'bintek_instructure_listing', 'bintek_instructure', 'listing'),
(8690, 2, 'bintek_instructure_view', 'bintek_instructure', 'view'),
(8689, 2, 'bintek_instructure_add', 'bintek_instructure', 'add'),
(8688, 2, 'bintek_instructure_edit', 'bintek_instructure', 'edit'),
(8687, 2, 'bintek_instructure_delete', 'bintek_instructure', 'delete'),
(8686, 2, 'bintek_instructure_index', 'bintek_instructure', 'index'),
(8685, 2, 'bintek_finance_report', 'bintek_finance', 'report'),
(8684, 2, 'bintek_finance_listing', 'bintek_finance', 'listing'),
(8683, 2, 'bintek_finance_view', 'bintek_finance', 'view'),
(8682, 2, 'bintek_finance_add', 'bintek_finance', 'add'),
(8681, 2, 'bintek_finance_edit', 'bintek_finance', 'edit'),
(8680, 2, 'bintek_finance_delete', 'bintek_finance', 'delete'),
(8679, 2, 'bintek_finance_index', 'bintek_finance', 'index'),
(8678, 2, 'bintek_evaluation_material_unsur_penilaian', 'bintek_evaluation_material', 'unsur_penilaian'),
(8677, 2, 'bintek_evaluation_material_report', 'bintek_evaluation_material', 'report'),
(8676, 2, 'bintek_evaluation_material_listing', 'bintek_evaluation_material', 'listing'),
(8675, 2, 'bintek_evaluation_material_view', 'bintek_evaluation_material', 'view'),
(8674, 2, 'bintek_evaluation_material_add', 'bintek_evaluation_material', 'add'),
(8673, 2, 'bintek_evaluation_material_edit', 'bintek_evaluation_material', 'edit'),
(8672, 2, 'bintek_evaluation_material_delete', 'bintek_evaluation_material', 'delete'),
(8671, 2, 'bintek_evaluation_material_index', 'bintek_evaluation_material', 'index'),
(8670, 2, 'bintek_evaluation_instructure_unsur_penilaian', 'bintek_evaluation_instructure', 'unsur_penilaian'),
(8669, 2, 'bintek_evaluation_instructure_report', 'bintek_evaluation_instructure', 'report'),
(8668, 2, 'bintek_evaluation_instructure_listing', 'bintek_evaluation_instructure', 'listing'),
(8667, 2, 'bintek_evaluation_instructure_view', 'bintek_evaluation_instructure', 'view'),
(8666, 2, 'bintek_evaluation_instructure_add', 'bintek_evaluation_instructure', 'add'),
(8665, 2, 'bintek_evaluation_instructure_edit', 'bintek_evaluation_instructure', 'edit'),
(8664, 2, 'bintek_evaluation_instructure_delete', 'bintek_evaluation_instructure', 'delete'),
(8663, 2, 'bintek_evaluation_instructure_index', 'bintek_evaluation_instructure', 'index'),
(11844, 1, 'bintek_instructure_download_excel', 'bintek_instructure', 'download_excel'),
(11843, 1, 'bintek_instructure_report', 'bintek_instructure', 'report'),
(11842, 1, 'bintek_instructure_listing', 'bintek_instructure', 'listing'),
(11841, 1, 'bintek_instructure_view', 'bintek_instructure', 'view'),
(11840, 1, 'bintek_instructure_add', 'bintek_instructure', 'add'),
(11839, 1, 'bintek_instructure_edit', 'bintek_instructure', 'edit'),
(11838, 1, 'bintek_instructure_delete', 'bintek_instructure', 'delete'),
(11837, 1, 'bintek_instructure_index', 'bintek_instructure', 'index'),
(11836, 1, 'bintek_finance_report', 'bintek_finance', 'report'),
(11835, 1, 'bintek_finance_listing', 'bintek_finance', 'listing'),
(11834, 1, 'bintek_finance_view', 'bintek_finance', 'view'),
(10874, 3, 'ukom_results_hasil_penilaian_mandiri', 'ukom_results', 'hasil_penilaian_mandiri'),
(10873, 3, 'ukom_results_hasil', 'ukom_results', 'hasil'),
(10872, 3, 'ukom_results_listing', 'ukom_results', 'listing'),
(10871, 3, 'ukom_results_view', 'ukom_results', 'view'),
(10870, 3, 'ukom_results_index', 'ukom_results', 'index'),
(10869, 3, 'ukom_member_listing', 'ukom_member', 'listing'),
(10868, 3, 'ukom_member_view', 'ukom_member', 'view'),
(10867, 3, 'ukom_member_register', 'ukom_member', 'register'),
(10866, 3, 'ukom_member_add', 'ukom_member', 'add'),
(10865, 3, 'ukom_member_edit', 'ukom_member', 'edit'),
(10864, 3, 'ukom_member_delete', 'ukom_member', 'delete'),
(10863, 3, 'ukom_member_index', 'ukom_member', 'index'),
(10862, 3, 'dashboard_top_menu', 'dashboard', 'top_menu'),
(10861, 3, 'dashboard_rightbar', 'dashboard', 'rightbar'),
(10860, 3, 'dashboard_right_menu', 'dashboard', 'right_menu'),
(10859, 3, 'dashboard_do_logout', 'dashboard', 'do_logout'),
(10858, 3, 'dashboard_is_login', 'dashboard', 'is_login'),
(10857, 3, 'dashboard_do_login', 'dashboard', 'do_login'),
(10856, 3, 'dashboard_login', 'dashboard', 'login'),
(10855, 3, 'dashboard_index', 'dashboard', 'index'),
(10854, 3, 'certification_results_hasil_penilaian_mandiri', 'certification_results', 'hasil_penilaian_mandiri'),
(10853, 3, 'certification_results_hasil', 'certification_results', 'hasil'),
(10852, 3, 'certification_results_listing', 'certification_results', 'listing'),
(10851, 3, 'certification_results_view', 'certification_results', 'view'),
(10850, 3, 'certification_results_index', 'certification_results', 'index'),
(10849, 3, 'certification_member_listing', 'certification_member', 'listing'),
(10848, 3, 'certification_member_view', 'certification_member', 'view'),
(10847, 3, 'certification_member_register', 'certification_member', 'register'),
(10846, 3, 'certification_member_add', 'certification_member', 'add'),
(10845, 3, 'certification_member_edit', 'certification_member', 'edit'),
(10844, 3, 'certification_member_delete', 'certification_member', 'delete'),
(10843, 3, 'certification_member_index', 'certification_member', 'index'),
(10842, 3, 'bintek_schedule_data_instruktur', 'bintek_schedule', 'data_instruktur'),
(10841, 3, 'bintek_schedule_data_member', 'bintek_schedule', 'data_member'),
(10840, 3, 'bintek_schedule_listing', 'bintek_schedule', 'listing'),
(10839, 3, 'bintek_schedule_view', 'bintek_schedule', 'view'),
(10838, 3, 'bintek_schedule_index', 'bintek_schedule', 'index'),
(10837, 3, 'bintek_member_listing', 'bintek_member', 'listing'),
(10836, 3, 'bintek_member_view', 'bintek_member', 'view'),
(10835, 3, 'bintek_member_register', 'bintek_member', 'register'),
(10834, 3, 'bintek_member_add', 'bintek_member', 'add'),
(11833, 1, 'bintek_finance_add', 'bintek_finance', 'add'),
(11832, 1, 'bintek_finance_edit', 'bintek_finance', 'edit'),
(11831, 1, 'bintek_finance_delete', 'bintek_finance', 'delete'),
(11830, 1, 'bintek_finance_index', 'bintek_finance', 'index'),
(11829, 1, 'bintek_evaluation_member_unsur_penilaian', 'bintek_evaluation_member', 'unsur_penilaian'),
(11828, 1, 'bintek_evaluation_member_report', 'bintek_evaluation_member', 'report'),
(8662, 2, 'bintek_atk_report', 'bintek_atk', 'report'),
(8661, 2, 'bintek_atk_listing', 'bintek_atk', 'listing'),
(8660, 2, 'bintek_atk_view', 'bintek_atk', 'view'),
(8659, 2, 'bintek_atk_add', 'bintek_atk', 'add'),
(8658, 2, 'bintek_atk_edit', 'bintek_atk', 'edit'),
(8657, 2, 'bintek_atk_delete', 'bintek_atk', 'delete'),
(8656, 2, 'bintek_atk_index', 'bintek_atk', 'index'),
(10833, 3, 'bintek_member_edit', 'bintek_member', 'edit'),
(10832, 3, 'bintek_member_delete', 'bintek_member', 'delete'),
(10831, 3, 'bintek_member_index', 'bintek_member', 'index'),
(11827, 1, 'bintek_evaluation_member_grafic', 'bintek_evaluation_member', 'grafic'),
(11826, 1, 'bintek_evaluation_member_citation', 'bintek_evaluation_member', 'citation'),
(11825, 1, 'bintek_evaluation_member_certificate', 'bintek_evaluation_member', 'certificate'),
(11824, 1, 'bintek_evaluation_member_listing', 'bintek_evaluation_member', 'listing'),
(11823, 1, 'bintek_evaluation_member_view', 'bintek_evaluation_member', 'view'),
(11822, 1, 'bintek_evaluation_member_add_sertifikat', 'bintek_evaluation_member', 'add_sertifikat'),
(11821, 1, 'bintek_evaluation_member_add', 'bintek_evaluation_member', 'add'),
(11820, 1, 'bintek_evaluation_member_edit', 'bintek_evaluation_member', 'edit'),
(11819, 1, 'bintek_evaluation_member_delete', 'bintek_evaluation_member', 'delete'),
(11818, 1, 'bintek_evaluation_member_index', 'bintek_evaluation_member', 'index'),
(11817, 1, 'bintek_evaluation_material_unsur_penilaian', 'bintek_evaluation_material', 'unsur_penilaian'),
(11816, 1, 'bintek_evaluation_material_report', 'bintek_evaluation_material', 'report'),
(11815, 1, 'bintek_evaluation_material_listing', 'bintek_evaluation_material', 'listing'),
(11814, 1, 'bintek_evaluation_material_view', 'bintek_evaluation_material', 'view'),
(11813, 1, 'bintek_evaluation_material_add', 'bintek_evaluation_material', 'add'),
(11812, 1, 'bintek_evaluation_material_edit', 'bintek_evaluation_material', 'edit'),
(11811, 1, 'bintek_evaluation_material_delete', 'bintek_evaluation_material', 'delete'),
(11810, 1, 'bintek_evaluation_material_index', 'bintek_evaluation_material', 'index'),
(11809, 1, 'bintek_evaluation_instructure_unsur_penilaian', 'bintek_evaluation_instructure', 'unsur_penilaian'),
(11808, 1, 'bintek_evaluation_instructure_report', 'bintek_evaluation_instructure', 'report'),
(11807, 1, 'bintek_evaluation_instructure_listing', 'bintek_evaluation_instructure', 'listing'),
(11806, 1, 'bintek_evaluation_instructure_view', 'bintek_evaluation_instructure', 'view'),
(11805, 1, 'bintek_evaluation_instructure_add', 'bintek_evaluation_instructure', 'add'),
(11804, 1, 'bintek_evaluation_instructure_edit', 'bintek_evaluation_instructure', 'edit'),
(11803, 1, 'bintek_evaluation_instructure_delete', 'bintek_evaluation_instructure', 'delete'),
(11802, 1, 'bintek_evaluation_instructure_index', 'bintek_evaluation_instructure', 'index'),
(11801, 1, 'bintek_citation_unsur_penilaian', 'bintek_citation', 'unsur_penilaian'),
(11800, 1, 'bintek_citation_report', 'bintek_citation', 'report'),
(11799, 1, 'bintek_citation_grafic', 'bintek_citation', 'grafic'),
(11798, 1, 'bintek_citation_citation', 'bintek_citation', 'citation'),
(11797, 1, 'bintek_citation_listing', 'bintek_citation', 'listing'),
(11796, 1, 'bintek_citation_view', 'bintek_citation', 'view'),
(11795, 1, 'bintek_citation_add', 'bintek_citation', 'add'),
(11794, 1, 'bintek_citation_edit', 'bintek_citation', 'edit'),
(11793, 1, 'bintek_citation_index', 'bintek_citation', 'index'),
(11792, 1, 'bintek_certificate_unsur_penilaian', 'bintek_certificate', 'unsur_penilaian'),
(11791, 1, 'bintek_certificate_report', 'bintek_certificate', 'report'),
(11790, 1, 'bintek_certificate_grafic', 'bintek_certificate', 'grafic'),
(11789, 1, 'bintek_certificate_citation', 'bintek_certificate', 'citation'),
(11788, 1, 'bintek_certificate_listing', 'bintek_certificate', 'listing'),
(11787, 1, 'bintek_certificate_view', 'bintek_certificate', 'view'),
(11786, 1, 'bintek_certificate_add', 'bintek_certificate', 'add'),
(11785, 1, 'bintek_certificate_edit', 'bintek_certificate', 'edit'),
(11784, 1, 'bintek_certificate_index', 'bintek_certificate', 'index'),
(11783, 1, 'bintek_atk_report', 'bintek_atk', 'report'),
(11782, 1, 'bintek_atk_listing', 'bintek_atk', 'listing'),
(11781, 1, 'bintek_atk_view', 'bintek_atk', 'view'),
(11780, 1, 'bintek_atk_add', 'bintek_atk', 'add'),
(11779, 1, 'bintek_atk_edit', 'bintek_atk', 'edit'),
(11778, 1, 'bintek_atk_delete', 'bintek_atk', 'delete'),
(11777, 1, 'bintek_atk_index', 'bintek_atk', 'index'),
(12240, 1, 'user_level_listing', 'user_level', 'listing'),
(12241, 1, 'user_level_role_index', 'user_level_role', 'index'),
(12242, 1, 'user_manual_index', 'user_manual', 'index'),
(12243, 1, 'user_manual_bintek', 'user_manual', 'bintek'),
(12244, 1, 'users_index', 'users', 'index'),
(12245, 1, 'users_delete', 'users', 'delete'),
(12246, 1, 'users_edit', 'users', 'edit'),
(12247, 1, 'users_add', 'users', 'add'),
(12248, 1, 'users_view', 'users', 'view'),
(12249, 1, 'users_listing', 'users', 'listing'),
(12250, 1, 'users_change_password_admin', 'users', 'change_password_admin'),
(12251, 1, 'users_reset_password', 'users', 'reset_password'),
(12252, 1, 'utility_index', 'utility', 'index'),
(12253, 1, 'utility_delete', 'utility', 'delete'),
(12254, 1, 'utility_edit', 'utility', 'edit'),
(12255, 1, 'utility_add', 'utility', 'add'),
(12256, 1, 'utility_view', 'utility', 'view'),
(12257, 1, 'utility_listing', 'utility', 'listing');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_name` char(50) DEFAULT NULL,
  `user_role_title` char(50) DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_role_id`, `user_role_name`, `user_role_title`) VALUES
(1, 'index', 'List Data'),
(2, 'add', 'Add Data'),
(3, 'edit', 'Edit Data'),
(4, 'delete', 'Delete Data'),
(5, 'view', 'View Detail'),
(6, 'print', 'Print'),
(7, 'download', 'Download'),
(8, 'search', 'Search Data');
